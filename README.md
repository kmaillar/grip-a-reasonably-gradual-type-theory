# GRIP A Reasonably Gradual Type Theory

Formal developments for GRIP (Gradual Reasonable Internal Precision)

The subdirectory CoqFormalization contains a formalization of the components
providing a model for GRIP using partial preorder. Refer to
`CoqFormalization/Readme.md` for installation instruction and walkthrough.

The subdirectory AgdaGripPoC provides a proof-of-concept implementation of GRIP
postulating the gradual combinators (cast, err, ?, precision ⊑ and its
properties) and endowing those with computational principles using
rewrite rules (c.f. REWRITE pragmas).

It contains the following files:
* ett-rr.agda : axiomatises Exceptional Type Theory, provides errors
* setoid-rr.agda : axiomatises Observational Type Theory, provides a setoidal equality
* grip-poc.agda : axiomatises cast and precision
* grip-poc-examples.agda : illustrates GRIP on examples
