From Coq Require Import ssreflect ssrfun.
From Equations Require Import Equations.
From PartialPreorder Require Import strictPropLogic.
From PartialPreorder Require Import prelude ppoDef.

Set Implicit Arguments.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Primitive Projections.
Import SPropNotations.

(** Negative "Empty" partial preorder collaspsing the error and unknown *)
(* Employed as the decoding of err in the universe *)
(* not to be confused with the positive zero type with distinct error and
   unknown used in the paper *)

Section Empty.
  Universe u.
  Definition pempty : Type@{u} := Datatypes.unit.
  Definition prec_empty : srel pempty := fun _ _ => sUnit.
  #[program]
  Definition prec_empty_is_ppo : is_ppo@{u} prec_empty :=
    mkIsPpo prec_empty _ _ _.
  Definition empty_ppo : ppo@{u} pempty := mkPpo prec_empty_is_ppo.
  Definition Empty : Ppo := pack_ppo empty_ppo.

  #[global]
  Instance empty_err : Err pempty := tt.
  #[global]
  Instance empty_smallest : SmallestElem empty_ppo :=
    mkSmallest empty_ppo tt stt (fun _ _=> stt).

  #[global]
  Instance empty_dyn : Dyn pempty := tt.
  #[global]
  Instance empty_greatest : GreatestElem empty_ppo :=
    mkGreatest empty_ppo tt stt (fun _ _=> stt).

  #[global]
  Instance empty_meet : meetOp pempty := fun _ _=> tt.
  #[global]
  Instance empty_has_meet : isMeet empty_ppo.
  Proof. unshelve econstructor=> * //=. Qed.

  #[global]
  Instance empty_join : joinOp pempty := fun _ _=> tt.
  #[global]
  Instance empty_has_join : isJoin empty_ppo.
  Proof. unshelve econstructor=> * //=. Qed.

  Definition empty_upcast (A : Type@{u}) `{Err@{u} A} : pempty -> A :=
    fun _ => err.
  Definition empty_downcast (A : Type@{u}) : A -> pempty := fun _ => tt.

  (* Empty is initial in the category of partial preorders and eppairs *)
  Section Initial.
    Context (A : Type@{u}) (RA : ppo@{u} A) `{SmallestElem@{u} _ RA}.

    Lemma from_empty_up : monotone empty_ppo RA (empty_upcast (A:=A)).
    Proof. move=> ???; apply: smallest_refl. Qed.

    Lemma from_empty_down : monotone RA empty_ppo (empty_downcast (A:=A)).
    Proof. constructor. Qed.

    #[global,program]
    Definition from_empty_eppair : eppair empty_ppo RA :=
      mkEppair _ _ from_empty_up from_empty_down _ _ _.
    Next Obligation. move=> *; apply: smaller=> //. Qed.

    Lemma unicity_empty_eppair (ep ep' : eppair empty_ppo RA) :
      equiveppair ep ep'.
    Proof.
      apply: equiveppair_helper; last do 2 constructor.
      move=> a aa; split.
      1-2:apply: downtoup; last constructor; by apply: upmon.
    Qed.

  End Initial.
End Empty.
