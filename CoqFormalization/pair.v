From Coq Require Import ssreflect ssrfun.
From Equations Require Import Equations.

Section UpToFlip.
  Context (A : Type) (R : A -> A -> Prop).

  Definition up_to_flip : A * A -> A * A -> Prop :=
    fun '(a, b) '(c, d) => (R a c /\ R b d) \/ (R a d /\ R b c).

  Context `{WellFounded A R}.

  Instance up_to_flip_wf : WellFounded up_to_flip.
  Proof.
    move=> [a].
    have [//] : (forall b, Acc up_to_flip (a,b)) /\(forall b, Acc up_to_flip (b,a)).
    pattern a; set P := (fun _ => _).
    refine (FixWf (R:=R) P _ a); unfold P; clear P a.
    move=> a ih; split=> b; constructor=> -[y1 y2] [] [??].
    1: match goal with | [ h : R _ a |- _ ] => by case: (ih _ h) end.
    1: match goal with | [ h : R _ a |- _ ] => by case: (ih _ h) end.
    1: match goal with | [ h : R _ a |- _ ] => by case: (ih _ h) end.
    1: match goal with | [ h : R _ a |- _ ] => by case: (ih _ h) end.
  Qed.

End UpToFlip.
