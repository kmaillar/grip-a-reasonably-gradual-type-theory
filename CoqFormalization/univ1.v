From Coq Require Import ssreflect ssrfun.
From Equations Require Import Equations.
From PartialPreorder Require Import strictPropLogic.
From PartialPreorder Require Import prelude ppoDef bool prop box empty pi beckChevalley listwf sigma.
From PartialPreorder Require Import univ0 unk1.

Set Implicit Arguments.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Primitive Projections.
Import SPropNotations.


Set Printing Universes.

(** Definition of the second universe u1 containing u0
    both as a type and by cumulativity
    and its term precision *)

Section U1.
  Universes u v.
  Constraint u < v.

  (** Universe of codes for the second universe *)

  Inductive code1 : Type@{v} -> Type@{v+1} :=
  | cerr1 : code1 Empty@{u}
  | cunk1 : code1 unk1@{u v}
  | cinj01 (A : u0@{u}) : code1 (el0 A)
  | cu01 : code1 u0@{u}
  | carr1 (A : Type@{v})
          (cA : code1 A)
          (B : A -> Type@{v})
          (cB : forall a : A, code1 (B a))
    : code1 (forall (a : A), B a).

  Record u1 := mkU1 { el1 : Type@{v} ; elcode1 : code1 el1 }.

  Definition erru1 := mkU1 cerr1.
  Definition unku1 := mkU1 cunk1.
  Definition iota1 (A : u0) : u1 := mkU1 (cinj01 A).
  Definition u0u1 : u1 := mkU1 cu01.
  Definition piu1 (A : u1) (B : el1 A -> u1) : u1 :=
    Eval cbn in
    mkU1 (carr1 (elcode1 A) (fun a => el1 (B a)) (fun a=> elcode1 (B a))).
  Definition arrU1 (A B : u1) := piu1 A (fun _ => B).

  #[global]
  Instance err_u1 : Err u1 := erru1.
  #[global]
  Instance dyn_u1 : Dyn u1 := unku1.

  (** Elimination principle on codes (targetting Type/SProp)*)

  Section U1elim.
    Universe w.
    Context (P : u1 -> Type@{w})
            (herr : P erru1)
            (hunk : P unku1)
            (hiota1 : forall (A : u0@{u}), P (iota1 A))
            (hu0 : P u0u1)
            (hpi : forall (A : u1) (B : el1 A -> u1),
                P A -> (forall a : el1 A, P (B a)) -> P (piu1 A B)).

    Definition u1elim (X : u1) : P X.
    Proof.
      move: X=> [X cX]; elim: cX=> [||A||A cA ihA B cB ihB].
      - exact herr.
      - exact hunk.
      - exact (hiota1 A).
      - exact hu0.
      - exact (hpi (fun a : el1 (mkU1 cA) => mkU1 (cB a)) ihA ihB).
    Defined.
  End U1elim.

  Section U1selim.
    Context (P : u1 -> SProp)
            (herr : P erru1)
            (hunk : P unku1)
            (hiota1 : forall (A : u0@{u}), P (iota1 A))
            (hu0 : P u0u1)
            (hpi : forall (A : u1) (B : el1 A -> u1),
                P A -> (forall a : el1 A, P (B a)) -> P (piu1 A B)).

    Definition u1selim (X : u1) : P X.
    Proof.
      move: X=> [X cX]; elim: cX=> [||A||A cA ihA B cB ihB].
      - exact herr.
      - exact hunk.
      - exact (hiota1 A).
      - exact hu0.
      - exact (hpi (fun a : el1 (mkU1 cA) => mkU1 (cB a)) ihA ihB).
    Defined.
  End U1selim.


  (** Exceptional terms on the decoding of codes *)

  Section ErrEl1.
    Universe u'.
    Constraint u <= u'.

    (* #[global] *)
    Instance err1_at (A : u1) : Err@{u'} (el1 A).
    Proof.
      elim/u1elim: A=> /=; try typeclasses eauto.
      1: apply: err0_at.
    Defined.

    Instance dyn1_at (A : u1) : Dyn@{u'} (el1 A).
    Proof.
      elim/u1elim: A=> /=; try typeclasses eauto.
      1: apply: dyn0_at.
    Defined.
  End ErrEl1.
  Hint Extern 1 (Err (el1 _)) => apply: err1_at : typeclass_instances.

  Hint Extern 1 (Dyn (el1 _)) => apply: dyn1_at : typeclass_instances.


  (** Implementation of cast between codes *)

  Definition cast_err@{u} (X Y : Type@{u}) `{Err@{u} X} `{Err@{u} Y} : (X -> Y) * (Y -> X) := (fun=> err, fun=> err).

  Definition cast_id@{w} (X : Type@{w}) : (X -> X) * (X -> X) := (id, id).

  Section El1Cast.

    Universe w.
    Constraint v < w.

    (* universe w needed for the induction hypothesis containing u1 : Type@{v} *)
    Definition el1_cast@{} (A B : u1) : (el1 A -> el1 B) * (el1 B -> el1 A).
    Proof.
      move: A B; elim/u1elim@{w}=> [||A||A B ihA ihB].
      - move=> ?; split; [apply: empty_upcast| apply: empty_downcast].
      - elim/u1elim@{w}=> [||?||????].
        + split; [apply: empty_downcast| apply: empty_upcast].
        + apply: cast_id.
        + split; [apply: downcast_unk1_to_el0|apply: upcast_el0_to_unk1].
        + split; [apply: downcast_unk1_to_u0| apply: upcast_u0_to_unk1].
        + exact cast_err.
      - move=> [_ [||?||????]].
        + split; [apply: empty_downcast| apply: empty_upcast].
        + split; [apply: upcast_el0_to_unk1| apply: downcast_unk1_to_el0].
        + apply: el0_cast@{u v}.
        + exact cast_err.
        + exact cast_err.
      - move=> [_ [||?||????]].
        + split; [apply: empty_downcast| apply: empty_upcast].
        + split; [apply: upcast_u0_to_unk1| apply:downcast_unk1_to_u0].
        + apply: cast_err.
        + apply: cast_id.
        + exact cast_err.
      - elim/u1elim=> [||?||A' B' _ _].
        + split; [apply: empty_downcast| apply: empty_upcast].
        + exact cast_err.
        + exact cast_err.
        + exact cast_err.
        + split; apply: pi_cast.
          1,3: by case: (ihA A').
          * move=> a a'; by case (ihB a (B' a')).
          * move=> a' a; by case (ihB a (B' a')).
    Defined.

    Definition el1_upcast {A B : u1} := fst (el1_cast A B).
    Definition el1_downcast {A B : u1} := snd (el1_cast A B).

    Lemma el1_upcast_pi A A' B B' :
      @el1_upcast (piu1 A B) (piu1 A' B') =
      pi_cast (el1 \o B) (el1 \o B') (@el1_downcast A A') (fun a a' => @el1_upcast (B a) (B' a')).
    Proof. reflexivity. Qed.

    Lemma el1_downcast_pi A A' B B' :
      @el1_downcast (piu1 A' B') (piu1 A B) =
      pi_cast (el1 \o B) (el1 \o B') (@el1_upcast  A' A) (fun a a' => @el1_downcast (B' a') (B a)).
    Proof. reflexivity. Qed.


  End El1Cast.

  (* Introduction of a third universe level *)

  Universe w.
  Constraint v < w.


  Definition fwc_u1@{} (A : Type@{v}) (B : A -> u1) : fwc@{v v} A :=
    mkFwC (fun a => el1 (B a))
          (fun ax ay => @el1_upcast@{w} (B ax) (B ay))
          (fun ax ay => @el1_downcast@{w} (B ax) (B ay)).


  Definition prec_el1@{} (A : u1) : srel (el1 A).
  Proof.
    elim/u1elim@{v}: A=> [||A||A B ihA ihB].
    - exact prec_empty.
    - exact prec_unk1.
    - exact (prec_el0@{u v} A).
    - exact u0_ppo@{u v}.
    - refine (prec_pi0@{v} ihA (fwc_u1 B) ihB).
  Defined.

  Definition el1_fwc : fwc@{w w} u1 :=
    mkFwC el1 (@el1_upcast) (@el1_downcast).

  Hint Extern 1 (Err (@family _ el1_fwc _)) => apply: err1_at : typeclass_instances.

  Hint Extern 1 (Dyn (@family _ el1_fwc _)) => apply: dyn1_at : typeclass_instances.

  (** Term precision on the universe *)

  Definition sp_u1 (A : u1) : SProp :=
      match elcode1 A with
      | carr1 Ac B Bc => sEmpty
      | cinj01 A => prec_u0_pic@{u v} A A
      | _ => sUnit
      end.

  Inductive prec_u1@{} : srel u1 :=
  | err_any1 A : sp_u1 A -> prec_u1 erru1 A
  | unk_any1 A : sp_u1 A -> prec_u1 A unku1
  | u0refl1 : prec_u1 u0u1 u0u1
  | inj0cong1 A0 A1 : prec_u0_pic@{u v} A0 A1 -> prec_u1 (iota1 A0) (iota1 A1).

  (** Inversion lemmas on precision *)

  Lemma prec_u1_unk_inv A : prec_u1 unku1 A -> A = unku1.
  Proof.
    elim/u1elim: A => [||?||A B ihA ihB].
    2: reflexivity.
    all: move=> h; sexfalso; inversion h; contradiction.
  Qed.

  Lemma prec_u1_err_inv A : prec_u1 A err -> A = err.
  Proof.
    elim/u1elim: A => [||?||A B ihA ihB]; first reflexivity.
    all: move=> h; sexfalso; inversion h; contradiction.
  Qed.

  Lemma prec_u1_iota_inv A0 A1 : prec_u1 (iota1 A0) (iota1 A1) -> prec_u0_pic A0 A1.
  Proof. move=> h; inversion h; by subst. Qed.


  (** lemmas on self precision *)

  Lemma sp_prec_u1 A : sp_u1 A -> prec_u1 A A.
  Proof.
    elim/u1selim: A=> [||? ||A B ihA ihB []] h; by constructor.
  Qed.

  Lemma prec_u1_sp_l AX AY : prec_u1 AX AY -> sp_u1 AX.
  Proof.
    elim/u1selim: AX => [||A||A B ihA ihB] //=.
    - move=> h; inversion h; subst; cbn in *; first assumption.
      apply: prec_u0_pic_lrefl; eassumption.
    - move=> h ; inversion h; contradiction.
  Qed.

  Lemma prec_u1_sp_r AX AY : prec_u1 AX AY -> sp_u1 AY.
  Proof.
    elim/u1selim: AY => [||?||A B ihA ihB] //=.
    - move=> h; inversion h; subst; cbn in *; first assumption.
      apply: prec_u0_pic_urefl; eassumption.
    - move=> h ; inversion h; contradiction.
  Qed.

  (** Partial preorder structure on u1  (for term precision) *)

  Lemma prec_u1_urefl : upper_reflexive prec_u1.
  Proof.
    move=> ??; elim; try constructor=> //.
    1: apply: sp_prec_u1.
    apply: prec_u0_pic_urefl; eassumption.
  Qed.

  Lemma prec_u1_lrefl : lower_reflexive prec_u1.
  Proof.
    move=> ??; elim; try constructor=> //.
    1: apply: sp_prec_u1.
    apply: prec_u0_pic_lrefl; eassumption.
  Qed.


  Lemma prec_u1_trans : transitive prec_u1.
  Proof.
    move=> ???; elim=> // [? _ |A spA /prec_u1_unk_inv ->|A0 A1 A01 h].
    - move=> /prec_u1_sp_r; by constructor.
    - by constructor.
    - inversion h; subst; constructor.
      + apply: prec_u0_pic_lrefl; eassumption.
      + apply: prec_u0_pic_trans; eassumption.
  Qed.

  Definition prec_u1_is_ppo@{} : is_ppo@{w} prec_u1 :=
    mkIsPpo prec_u1 prec_u1_trans prec_u1_urefl prec_u1_lrefl.

  Definition u1_ppo : ppo@{w} u1 := mkPpo prec_u1_is_ppo.


  (** Definition of the partial preoder structure on self precise types at u1 *)

  Definition is_ppo_at1@{} (A : u1) (AA : prec_u1 A A) : is_ppo (prec_el1 A).
  Proof.
    elim/u1selim: A AA=> [||?||A B ihA ihB] H; last sabsurd H.
    - exact empty_ppo.
    - exact unk1_ppo.
    - exact (el0_pic_ppo (prec_u1_iota_inv H)).
    - exact u0_ppo.
  Defined.

  Let el1_at {A} AA := mkPpo (@is_ppo_at1 A AA).

  Definition smallest1_at@{} (A : u1) (AA : prec_u1 A A) :
    SmallestElem@{v} (el1_at AA).
  Proof.
    elim/u1selim: A AA => [||?||????] /= H; last sabsurd H.
    3: pose proof (prec_u1_iota_inv H); by apply: el0_pic_smallest.
    all: typeclasses eauto.
  Qed.


  Definition smallest1_at'@{u' | v < u' +} (A : u1) (AA : prec_u1 A A) :
    SmallestElem@{u'} (el1_at AA).
  Proof.
    elim/u1selim: A AA => [||?||????] /= H; last sabsurd H.
    3: pose proof (prec_u1_iota_inv H); by apply: el0_pic_smallest.
    all: typeclasses eauto.
  Qed.

  Definition greatest1_at@{} (A : u1) (AA : prec_u1 A A) :
    GreatestElem@{v} (el1_at AA).
  Proof.
    elim/u1selim: A AA => [||?||????] /= H; last sabsurd H.
    3: pose proof (prec_u1_iota_inv H); by apply: el0_pic_greatest.
    all: cbn; typeclasses eauto.
  Qed.

  Definition el1_on_is_eppair@{} (A B : u1) (AB : u1_ppo A B) :
    eppair_prop@{v} (el1_at (prec_u1_lrefl AB)) (el1_at (prec_u1_urefl AB))
               el1_upcast@{w} el1_downcast@{w}.
  Proof.
    elim/u1selim: A B AB=> [||?||?? _ _]; elim/u1selim=> [||?||?? _ _] /= H.
    all: try sabsurd H.
    all: try refine (ideppair@{v} _).
    - exact (@from_empty_eppair@{v} _ _ _ empty_smallest).
    - exact (@from_empty_eppair@{v} _ _ _ unk1_smallest).
    - exact (@from_empty_eppair@{v} _ _ _ (el0_pic_smallest (prec_u1_sp_r H))).
    - exact (@from_empty_eppair@{v} _ _ _ u0_smallest).
    - exact (eppair_el0_to_unk1 (prec_u1_sp_l H)).
    - exact (el0_pic_eppair (prec_u1_iota_inv H)).
    - exact (eppair_u0_to_unk1).
  Defined.

  Definition el1_on@{} (A B : u1) (AB : u1_ppo A B) : eppair@{v} (el1_at ⌊AB⌋) (el1_at ⌈AB⌉) :=
    pack_eppair (el1_on_is_eppair AB).


  Lemma el1_trans1 A B C (AB : u1_ppo A B) (BC : u1_ppo B C) (AC : u1_ppo A C) :
    compeppair (el1_on AB) (el1_on BC) ≃ el1_on AC.
  Proof.
    move: C BC AC; induction AB as [B1 spB1| A1 spA1 | |?? pq]=> C BC AC.
    - move=> *; apply: unicity_empty_eppair.
    - pose proof (e := prec_u1_unk_inv BC). subst.
      apply: equiveppair_helper_up=> ??; apply: sym_refl; by apply: upmon.
    - inversion BC; subst; apply: equiveppair_helper_up=> ??;
       apply: sym_refl; by apply: (upmon (el1_on BC)).
    - inversion BC;subst.
      + apply: compepair_el0_to_unk1.
      + apply: el0_pic_comp0.
  Qed.

  Lemma el1_trans A B C (AB : u1_ppo A B) (BC : u1_ppo B C)  :
    equiveppair (compeppair (el1_on AB) (el1_on BC)) (el1_on (prec_u1_trans AB BC)).
  Proof. apply el1_trans1. Qed.

  Lemma el1_refl1 (A : u1) (AA : u1_ppo A A) :
    equiveppair (el1_on AA) (ideppair (el1_at AA)).
  Proof.
    elim/u1selim: A AA=> [||?||????] AA; inversion AA; subst.
    3:apply: el0_pic_id.
    all: apply: equiveppair_refl.
  Qed.

  Definition U1 : Ppo@{w} := pack_ppo@{w} u1_ppo.

  #[global]
  Instance u1_err : Err@{w} u1 := erru1.

  #[global,program]
  Instance u1_smallest : SmallestElem@{w} u1_ppo :=
    mkSmallest u1_ppo erru1 _ _.
  Next Obligation. constructor=> //. Qed.
  Next Obligation. opacify=> A /prec_u1_sp_l ? ; by constructor. Qed.

  #[global]
  Instance u1_dyn : Dyn@{w} u1 := unku1.
  #[global,program]
  Instance u1_greatest : GreatestElem@{w} u1_ppo :=
    mkGreatest u1_ppo unku1 _ _.
  Next Obligation. by constructor. Qed.
  Next Obligation. opacify=> A /prec_u1_sp_l; by constructor. Qed.

  (** Term precision form an ippo structure on the second universe *)

  Definition El1_ippo : ippo U1 el1_fwc :=
    mkIppo u1_ppo el1_fwc prec_el1 is_ppo_at1
           el1_on_is_eppair el1_trans el1_refl1.


End U1.
