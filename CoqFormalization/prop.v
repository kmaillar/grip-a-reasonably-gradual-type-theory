From Coq Require Import ssreflect ssrfun.
From Equations Require Import Equations.
From PartialPreorder Require Import strictPropLogic.
From PartialPreorder Require Import prelude ppoDef.

Set Implicit Arguments.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Primitive Projections.
Import SPropNotations.

Section PropPpo.

  Definition prec_prop (p q : SProp) := p -> q.
  Arguments prec_prop _ _ /.
  #[global,program]
  Definition prec_prop_is_ppo : is_ppo prec_prop :=
    mkIsPpo prec_prop _ _ _.
  Next Obligation. move=> ? * /= ; auto. Qed.
  Next Obligation. move=> *; by []. Qed.
  Next Obligation. move=> *; by cbn. Qed.

  Definition prop_ppo : ppo SProp := mkPpo prec_prop_is_ppo.

  Definition Prop_ : Ppo := pack_ppo prop_ppo.
  Definition err_prop : Prop_ := sEmpty.
  Definition unk_prop : Prop_ := sUnit.
  Definition pi_prop (P : Prop_) (Q : P -> Prop_) := forall p, Q p.
  Definition piTy_prop (A : Type) (Q : A -> Prop_) := forall a, Q a.

  #[global]
  Instance prop_err : Err SProp := sEmpty.
  #[global,program]
   Instance prop_smallest : SmallestElem prop_ppo.
  Next Obligation. opacify; cbv; done. Qed.

  #[global]
  Instance prop_dyn : Dyn SProp := sUnit.
  #[global,program]
  Instance prop_greatest : GreatestElem prop_ppo.
  Next Obligation. opacify; done. Qed.

  #[global]
  Instance prop_meet : meetOp SProp := fun p q => p s/\ q.

  #[global]
  Instance prop_has_meet@{u} : isMeet@{u} Prop_@{u}.
  Proof.
    unshelve econstructor.
    - move=> *; apply: sprl.
    - move=> *; apply: sprr.
    - move=> *; split; intuition.
  Qed.

  #[global]
  Instance prop_join : joinOp SProp := fun p q => p s\/ q.

  #[global]
  Instance prop_has_join@{u} : isJoin@{u} Prop_@{u}.
  Proof.
    unshelve econstructor.
    1,2: move=> *; by constructor.
    move=> * [] //.
  Qed.

End PropPpo.
