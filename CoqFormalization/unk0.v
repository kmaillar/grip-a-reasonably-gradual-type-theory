From Coq Require Import ssreflect ssrfun.
From Equations Require Import Equations.
From PartialPreorder Require Import strictPropLogic.
From PartialPreorder Require Import prelude ppoDef bool prop unknown.

Set Implicit Arguments.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Primitive Projections.
Import SPropNotations.

(** Instantiation of unknown for the first universe univ0 *)

Section Unk0.
  Universe u.

  (* Note that we do not add a head constructor for Box
     since it is equivalent to Empty for precision *)
  Inductive Head0 : Type@{u} :=
  | hbool0 : Head0
  | hprop0 : Head0.

  (* Deriving EqDec started to fail when I added the explicit quantification on universe levels on Head0 *)
  Derive NoConfusion (* EqDec *) for Head0.

  (* We rely on decidable equality of the base Head0
     to build the family needed for unknown *)
  #[global]
  Instance: EqDec Head0.
  Proof. red. decide equality. Defined.

  Set Equations Transparent.
  Equations ElHead0 (h : Head0) : Type@{u} :=
    ElHead0 hbool0 := pbool@{u} ;
    ElHead0 hprop0 := SProp.

  Instance head0_err (h : Head0) : Err (ElHead0 h) :=
    match h with | hbool0 => pbool_err | hprop0 => prop_err end.

  Definition FwCHead0 := fwc_dom_eq_dec ElHead0.

  Equations ppo_Head0 (h : Head0) : ppo@{u} (ElHead0 h) :=
    ppo_Head0 hbool0 := bool_ppo ;
    ppo_Head0 hprop0 := prop_ppo.

  Definition pHead0 := ppo_from_eq Head0.

  Definition RHead0 := ippo_dom_eq_dec ElHead0 ppo_Head0.

  Instance head0_smallest (h : Head0) : SmallestElem (ppo_Head0 h).
  Proof.
    case: h.
    - exact bool_smallest.
    - exact prop_smallest.
  Defined.

  Instance head0_dyn (h : Head0) : Dyn (ElHead0 h) :=
    match h with | hbool0 => pbool_dyn | hprop0 => prop_dyn end.

  Instance head0_greatest (h : Head0) : GreatestElem (ppo_Head0 h).
  Proof.
    case: h.
    - exact bool_greatest.
    - exact prop_greatest.
  Defined.

  Definition unk0 : Type@{u} := unk@{u u} FwCHead0.

  Definition prec_unk0 : srel unk0 := prec_unk@{u u} RHead0.

  Lemma RHead0_isrel_intro :
    forall (h0 h1 : Head0) (v0 : fwc_dom_eq_dec ElHead0 h0)
      (v1 : fwc_dom_eq_dec ElHead0 h1) (h00 : ppo_from_eq Head0 h0 h0)
      (h11 : ppo_from_eq Head0 h1 h1),
    RHead0 h0 h00 v0 (fdowncast (fwc_dom_eq_dec ElHead0) h0 h1 v1) ->
    RHead0 h1 h11 v1 v1 -> i[ srel_at RHead0] h0 h1 v0 v1.
  Proof.
    move=> ? h1 ???? v01 v11; repeat split=> //;first exact ⌊v01⌋.
    move: v01=> /=; move E: (eq_dec _ _)=> [e|ne].
    + rewrite eq_dec_sym_left; by depelim e.
    + destruct (eq_dec_sym_right ne) as [? ->]; move=> _ ; by apply: smaller.
  Qed.

  Instance: unknown_hypotheses RHead0.
  Proof.
    unshelve econstructor.
    - apply: ippo_dom_eq_downerr.
    - apply: ippo_dom_eq_downdyn.
    - apply: ippo_dom_eq_fdowncastcomp.
    - move=> ???? hb01. induction hb01=> ? vv.
      apply: prec_iprec.
      apply: ippo_dom_eq_fdowncastmon.
      refine (prec_iprec RHead0 _ _ _ _ vv).
    - move=> *; apply: ippo_dom_eq_fdowncastmon=> //.
    - apply: RHead0_isrel_intro.
    - move=> ??; apply: smaller; apply: greatest_refl.
  Qed.

  (** Instantiation of the constructions from unknown *)

  Definition unk0_ppo : ppo@{u} unk0 := unk_ppo@{u u} (RElH := RHead0).

  Definition injunk0 (h : Head0) (x : ElHead0 h) : unk0 :=
    injUnkC FwCHead0 h x.

  #[global]
  Instance unk0_err : Err unk0 := err_unk FwCHead0.
  #[global]
  Instance unk0_smallest : SmallestElem unk0_ppo :=
    unk_smallest (RElH := RHead0).

  #[global]
  Instance unk0_dyn : Dyn unk0 := dyn_unk FwCHead0.
  #[global]
  Instance unk0_greatest : GreatestElem unk0_ppo :=
    unk_greatest (RElH := RHead0).

  Definition upcast_to_unk0 : forall h, ElHead0 h -> unk0 :=
    @inj_upcast Head0 FwCHead0.
  Definition downcast_from_unk0 : forall h, unk0 -> ElHead0 h :=
    @inj_downcast Head0 FwCHead0 _ _.

  Definition eppair_to_unk0 (h : Head0) : eppair (ppo_Head0 h) unk0_ppo :=
    inj_eppair h (sEq_refl h).

  Instance ElHead0_meet (h : Head0) : meetOp (ElHead0 h).
  Proof. case: h=> /=; typeclasses eauto. Defined.

  Definition ElHead0_has_meet (h : Head0) : isMeet (RHead0 h (sEq_refl h)).
  Proof. unfold ippo_at; case: h=> /= ; typeclasses eauto. Qed.

End Unk0.
