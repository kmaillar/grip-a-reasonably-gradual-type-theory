From Coq Require Import ssreflect ssrfun.
From Equations Require Import Equations.
From PartialPreorder Require Import strictPropLogic.
From PartialPreorder Require Import prelude ppoDef.

Set Implicit Arguments.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Primitive Projections.
Import SPropNotations.

(** Partial preorder structure on list over a partial preorder A *)

Section List.
  Universe u.
  Context (A : Type@{u}) (RA : ppo@{u} A).

  Inductive list : Type@{u} :=
  | errlist
  | unklist
  | nil
  | cons (a : A) (l : list).

  Fixpoint list_sp (l : list) : SProp :=
    match l with
    | errlist => sUnit
    | unklist => sUnit
    | nil => sUnit
    | cons a l => RA a a s/\ list_sp l
    end.

  Inductive prec_list : srel list :=
  | err_prec_list l : list_sp l -> prec_list errlist l
  | unk_prec_list l : list_sp l -> prec_list l unklist
  | nil_prec_list : prec_list nil nil
  | cons_prec_list a a' l l' :
    RA a a' -> prec_list l l' -> prec_list (cons a l) (cons a' l').

  Lemma prec_list_sp_l l l' : prec_list l l' -> list_sp l.
  Proof.
    elim=> [? spl|? spl||?????? ih] //=; split=> //.
    apply: lower_refl; eassumption.
  Qed.

  Lemma prec_list_sp_r l l' : prec_list l l' -> list_sp l'.
  Proof.
    elim=> [? spl|? spl||?????? ih] //=; split=> //.
    apply: upper_refl; eassumption.
  Qed.

  Lemma list_sp_prec_list l : list_sp l -> prec_list l l.
  Proof.
    elim: l=> [|||?? ih]; try solve [move=> ?; by constructor].
    move=> [? ?]; constructor=> //; by apply: ih.
  Qed.

  Lemma prec_list_trans : transitive prec_list.
  Proof.
    move=> ?? z xy; elim:xy z (xy) => [l spl|l spl||?????? ih] ? ? yz; inversion yz; subst=> //.
    - do 2 constructor.
    - constructor; apply: prec_list_sp_r; eassumption.
    - constructor; apply: prec_list_sp_l; eassumption.
    - constructor; first (apply: trans; eassumption).
      by apply: ih.
  Qed.

  Lemma prec_list_urefl : upper_reflexive prec_list.
  Proof.
    move=> ??/prec_list_sp_r; apply: list_sp_prec_list.
  Qed.

  Lemma prec_list_lrefl : lower_reflexive prec_list.
  Proof.
    move=> ??/prec_list_sp_l; apply: list_sp_prec_list.
  Qed.

  Definition prec_list_is_ppo : is_ppo@{u} prec_list :=
    mkIsPpo prec_list prec_list_trans prec_list_urefl prec_list_lrefl.

  Definition list_ppo : ppo@{u} list := mkPpo prec_list_is_ppo.

  Instance list_err : Err list := errlist.
  Instance list_smallest : SmallestElem list_ppo.
  Proof.
    constructor; first do 2 constructor.
    move=> ? /prec_list_sp_l; apply: err_prec_list.
  Qed.

  Instance list_dyn : Dyn list := unklist.
  Instance list_greatest : GreatestElem list_ppo.
  Proof.
    constructor; first do 2 constructor.
    move=> ? /prec_list_sp_l; apply: unk_prec_list.
  Qed.

End List.

Arguments errlist {_} : assert.
Arguments unklist {_} : assert.
Arguments nil {_} : assert.
Section Map.
  Universe u.
  Context (AX AY : Type@{u}) (f : AX -> AY).
  Equations list_map (l : list@{u} AX) : list@{u} AY :=
  | errlist => errlist
  | unklist => unklist
  | nil => nil
  | cons ax l => cons (f ax) (list_map l).

  Context (RAX : ppo@{u} AX) (RAY : ppo@{u} AY).
  Lemma list_map_sp : monotone RAX RAY f -> forall l, list_sp RAX l -> list_sp RAY (list_map l).
  Proof.
    move=> mf; elim=> [|||?? ih]//= [/mf ??]; split=> //; by apply: ih.
  Qed.

  Lemma list_map_monotone : monotone RAX RAY f -> monotone (list_ppo RAX) (list_ppo RAY) list_map.
  Proof.
    move=> mf l l'; elim=> [? spl|? spl||?????? ih]; simp list_map.
    - constructor; by apply: list_map_sp.
    - constructor; by apply: list_map_sp.
    - constructor.
    - constructor=> //; by apply: mf.
  Qed.
End Map.


Section ListEppair.
  Universe u.
  Context (AX AY : Type@{u}) (RAX : ppo@{u} AX) (RAY : ppo@{u} AY)
          (AXY : eppair RAX RAY).

  Lemma list_eppair_prop : eppair_prop@{u} (list_ppo RAX) (list_ppo RAY)
                                      (list_map (upcast AXY))
                                      (list_map (downcast AXY)).
  Proof.
    constructor.
    1,2: apply: list_map_monotone; (apply: upmon + apply: downmon).
    - move=> l l' /prec_list_sp_l; elim: l l'=> [|||?? ih] l' ; simp list_map.
      1: move=> _ /prec_list_sp_r ? ; constructor; apply: list_map_sp; first apply: downmon; eassumption.
      all: move=> sp h; inversion h; subst; try solve [repeat constructor=> //].
      move: sp=> [??]; simp list_map; constructor; by (apply: uptodown + apply: ih).
    - move=> l l' /prec_list_sp_l; elim: l l'=> [|||?? ih] l' ; simp list_map.
      1: move=> ? _; by constructor.
      all: case: l'=> [|||??]; simp list_map=> sp h; inversion h ; constructor=> // ; subst; simp list_map.

      + refine (list_map_sp _ _ (upmon AXY) _ H).
      + case: sp=> [??] ; apply: downtoup=> //.
      + case: sp=> [??]; apply: ih=> //.
    - move=> + /prec_list_sp_l; elim=> [|||?? ih] sp ; simp list_map.
      1-3: do 2 constructor.
      case: sp=> [? ?]; constructor; by (apply: retract + apply: ih).
  Qed.

End ListEppair.
