From Coq Require Import List.
From Equations Require Import Equations.
From Coq Require Import ssreflect.
From PartialPreorder Require Import strictPropLogic.


(** Poor man replacement for multiset extension of a well-founded relation *)
(* used for well-founded induction in univ0.v to prove properties of
   type level precision *)

Import ListNotations.

Section Sublist.
  Context (A : Type).

  Inductive sublist : list A -> list A -> Prop :=
  | nil_sub : sublist nil nil
  | cons_sub a l1 l2 : sublist l1 l2 -> sublist (a :: l1) (a :: l2)
  | drop_sub a l1 l2 : sublist l1 l2 -> sublist l1 (a :: l2).

  Definition sublist_id l : sublist l l.
  Proof. elim: l=> [|x xs ih]; by constructor. Qed.

  Lemma sublist_len l1 l2 : sublist l1 l2 -> length l1 <= length l2.
  Proof.
    elim=> [|????|????]=> //=; first apply: le_n_S; apply: le_S.
  Qed.

  Lemma sublist_eq_len l1 l2 : sublist l1 l2 -> length l1 = length l2 -> l1 = l2.
  Proof.
    elim=>[//|???? ih|??? sub ih] /=; first move=> [=] /ih -> //.
    move=> eq; move: eq (sublist_len _ _ sub)=> -> /PeanoNat.Nat.nle_succ_diag_l [].
  Qed.

  Lemma sublist_trans l1 l2 l3 : sublist l1 l2 -> sublist l2 l3 -> sublist l1 l3.
  Proof.
    move=> sub12 sub23 ; elim: sub23 l1 sub12 => [//|???? ih|???? ih] l1.
    - move=> h; inversion h; subst.
      + constructor; by apply: ih.
      + apply: drop_sub; by apply: ih.
    - move=> ?; apply: drop_sub; by apply: ih.
  Qed.

  Definition ssublist (l1 l2 : list A) := sublist l1 l2 /\ length l1 < length l2.

  Lemma ssublist_trans l1 l2 l3 : ssublist l1 l2 -> ssublist l2 l3 -> ssublist l1 l3.
  Proof.
    move=> [??] [??]; split; [apply: sublist_trans|apply: PeanoNat.Nat.lt_trans]; eassumption.
  Qed.

  Instance ssublist_wf : WellFounded ssublist.
  Proof.
    move: (measure_wf (Wf_nat.lt_wf) (length (A:=A))).
    apply: Inclusion.wf_incl; by move=> ??[].
  Qed.

  Lemma not_ssub_nil l : ssublist l [] -> False.
  Proof. move=> [] //= _ h; inversion h. Qed.

  Lemma ssub_cons_inv a l1 l2 :
    ssublist l1 (a :: l2) -> l1 = l2 \/ ssublist l1 l2 \/ exists l', l1 = a :: l' /\ ssublist l' l2.
  Proof.
    move=> [] h; inversion h; subst.
    - move=> /Lt.lt_S_n ? ; right; right; exists l0; split=> //.
    - move: (PeanoNat.Nat.eq_dec (length l1) (length l2))=> [].
      * move=> ? _; left; by apply: sublist_eq_len.
      * move=> /[dup] neq /Lt.nat_total_order[].
        + move=> ? _; right ; left; split=> //.
        + move=> ??; exfalso.
          apply: PeanoNat.Nat.lt_irrefl;apply: PeanoNat.Nat.lt_le_trans.
          2: apply: (sublist_len _ _ H1).
          assumption.
  Qed.

  Context (ltA : A -> A -> Prop).
  Definition cover (a : A) (l : list A) := Forall (fun y => ltA y a) l.

  Inductive cart_direct : list A -> list A -> Prop :=
  | cart_direct_ssub l1 l2 : ssublist l1 l2 -> cart_direct l1 l2
  | cart_direct_cover_nil a l : cover a l -> cart_direct l [a]
  | cart_direct_cover_cons a l l1 l2 : cover a l -> cart_direct l1 l2 -> cart_direct (l ++ l1) (a :: l2).

  Lemma cart_direct_cover l1 ll2 : l1 <> nil -> Forall2 cover l1 ll2 -> cart_direct (concat ll2) l1.
  Proof.
    elim: l1 ll2=> [|a1 l1 ih].
    - move=> ? h; exfalso; by apply h.
    - move=> ? _ h; inversion h; subst=> /=.
      case: l1 ih h H3=> [|??] ih h h'.
      + inversion h' ; apply: cart_direct_cover_nil=> /=.
        by rewrite app_nil_r.
      + apply: cart_direct_cover_cons=> //.
        apply: ih=>//.
  Qed.

  Lemma not_cart_direct_nil l : cart_direct l [] -> False.
  Proof.
    move=> h; inversion h; subst.
    apply: not_ssub_nil; eassumption.
  Qed.


  Lemma acc_cart_direct_nil : Acc cart_direct [].
  Proof. constructor. move=> ? /not_cart_direct_nil []. Qed.


  Lemma cart_direct_cons_acc `{WellFounded A ltA} : forall a l, Acc cart_direct l -> Acc cart_direct (a :: l).
  Proof.
    apply: FixWf=> a0 ih l; elim=> {l}[l ih' ih''].
    constructor=> y hy; inversion hy; subst.
    - move: H0 => /ssub_cons_inv [-> //|[?|[l1 [-> subl1]]]].
      apply: ih'; by constructor.
      apply: ih''; by constructor.
    - elim: y {hy} H2=> [//|y ys ihy]=> h.
      inversion h; subst. apply: ih. assumption. by apply: ihy.
    - elim: l0 {hy} H3 H4=> [|y ys ihy].
      + move=> ? /=; apply: ih'.
      + move=> h /=; inversion h; subst=> ?.
        apply: ih. assumption. apply: ihy; assumption.
  Qed.

  Instance cart_direct_wf `{WellFounded A ltA} : WellFounded cart_direct.
  Proof.
    elim=> [|x xs ih]; [apply: acc_cart_direct_nil| by apply: cart_direct_cons_acc].
  Qed.

  Definition cart := Relation_Operators.clos_trans (list A) cart_direct.

  Lemma not_cart_nil l : cart l [] -> False.
  Proof.
    remember [] as l' eqn:E; move=> h; elim: h E.
    - move=> ?? /[swap] -> /not_cart_direct_nil [].
    - move=> ?????? /[apply] //.
  Qed.

  Definition cart_ssub [l1 l2] : ssublist l1 l2 -> cart l1 l2.
  Proof. by do 2 constructor. Qed.

  Definition cart_cover [l a] : cover a l -> cart l [a].
  Proof. move=> ?; by do 2 constructor. Qed.

  Lemma cart_cover_gen  [l1 ll2] : l1 <> nil -> Forall2 cover l1 ll2 -> cart (concat ll2) l1.
  Proof. move=> *; constructor; apply: cart_direct_cover=> //. Qed.

  (* Messy but should cover my needs *)
  Definition cart_cover2 l1 [a1] l2 [a2] :
    cover a1 l1 -> cover a2 l2 -> cart (l1 ++ l2) [a1 ; a2].
  Proof.
    move=> ??; constructor.
    apply: cart_direct_cover_cons=> //.
    by constructor.
  Qed.

  Definition cart_cover3 l1 [a1] l2 [a2] l3 [a3] :
    cover a1 l1 ->
    cover a2 l2 ->
    cover a3 l3 ->
    cart (l1 ++ l2 ++ l3) [a1 ; a2; a3].
  Proof.
    move=> ??; constructor.
    apply: cart_direct_cover_cons=> //.
    apply: cart_direct_cover_cons=> //.
    by constructor.
  Qed.


  Context (P : list A -> SProp).
  Import SPropNotations.
  Definition blist := { l : list A ≫  P l }.
  Definition list_of_blist (l : blist) := l∙1.
  Definition bcart : blist -> blist -> Prop := MR cart list_of_blist.

  Instance bcart_wf `{WellFounded _ ltA} : WellFounded bcart.
  Proof.
    apply: measure_wf.
    apply: Transitive_Closure.wf_clos_trans.
    apply: cart_direct_wf.
  Defined.

End Sublist.

