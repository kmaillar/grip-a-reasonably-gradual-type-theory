From Coq Require Import ssreflect ssrfun.
From Equations Require Import Equations.
From PartialPreorder Require Import strictPropLogic.
From PartialPreorder Require Import prelude ppoDef.

Set Implicit Arguments.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Primitive Projections.
Import SPropNotations.


(** Partial preorder structure on Bool (+ err + unk) *)

Section Bool.
  Universe u.
  (* Without the Type annotation the inductive fall in Set
     and the typeclass search below for SmallestElem fails (why ?) *)
  Inductive pbool : Type@{u} :=
  | ptrue
  | pfalse
  | punkbool
  | perrbool.

  Inductive prec_bool : pbool -> pbool -> SProp :=
  | errboolbot b : prec_bool perrbool b
  | truerefl : prec_bool ptrue ptrue
  | falserefl : prec_bool pfalse pfalse
  | unkboolrefl : prec_bool punkbool punkbool
  | trueunk : prec_bool ptrue punkbool
  | falseunk : prec_bool pfalse punkbool
  .

  Lemma prec_bool_refl : forall b, prec_bool b b.
  Proof. elim; constructor. Qed.

  Lemma prec_bool_trans : transitive prec_bool.
  Proof. move=> ???; elim=> [b|||||] H; inversion H ; constructor. Qed.

  Definition prec_bool_is_ppo : is_ppo@{u} prec_bool :=
    mkIsPpo prec_bool prec_bool_trans
          (fun _ _ _ => prec_bool_refl _)
          (fun _ _ _ => prec_bool_refl _).

  Definition bool_ppo : ppo@{u} pbool :=
    mkPpo prec_bool_is_ppo.

  Definition Bool : Ppo := pack_ppo bool_ppo.

  #[global]
  Instance pbool_err : Err pbool := perrbool.
  #[global,program]
  Instance bool_smallest : SmallestElem bool_ppo.
  Next Obligation. opacify; constructor. Qed.
  Next Obligation. opacify; constructor. Qed.

  #[global]
  Instance pbool_dyn : Dyn pbool := punkbool.
  #[global,program]
   Instance bool_greatest : GreatestElem bool_ppo.
  Next Obligation. opacify; constructor. Qed.
  Next Obligation. opacify; move=> [] ?; constructor. Qed.


  Equations pbool_meet : meetOp pbool :=
  | perrbool, _ => perrbool
  | _, perrbool => perrbool
  | ptrue, pfalse => perrbool
  | pfalse, ptrue => perrbool
  | punkbool, x => x
  | x, punkbool => x
  | x, _ => x.

  Global Transparent pbool_meet.
  Global Existing Instance pbool_meet.

  #[global]
  Instance pbool_has_meet : isMeet bool_ppo.
  Proof.
    unshelve econstructor.
    1,2: move=> [] [] aa bb;
        inversion aa; inversion bb; cbn; constructor.
    move=> [] [] [] ca cb;
      inversion ca; inversion cb; cbn; constructor.
  Qed.

  Equations pbool_join : joinOp pbool :=
  | punkbool, _ => punkbool
  | _, punkbool => punkbool
  | ptrue, pfalse => punkbool
  | pfalse, ptrue => punkbool
  | perrbool, x => x
  | x, perrbool => x
  | x, _ => x.

  Global Transparent pbool_join.
  Global Existing Instance pbool_join.

  #[global]
  Instance pbool_has_join : isJoin bool_ppo.
  Proof.
    unshelve econstructor.
    1,2: move=> [] [] aa bb;
        inversion aa; inversion bb; cbn; constructor.
    move=> [] [] [] ca cb;
      inversion ca; inversion cb; cbn; constructor.
  Qed.

End Bool.
