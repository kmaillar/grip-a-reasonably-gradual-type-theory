# Prerequisite

- Coq >= 8.14
- Equations >= 1.3

# Installation

Official instructions for [installing Coq](https://coq.inria.fr/download)
This development should be compatible with any version of the Coq platform.

Using opam, from a fresh switch:

```
  $ opam pin add coq 8.14.1
  $ opam repo add coq-released https://coq.inria.fr/opam/released
  $ opam install coq-equations.1.3+8.14
```

# Compilation

From this directory, issue:
```
  $ make
```

# Files organisation

- strictPropLogic.v: Definitions, notations and lemmas to work with strict propositions
- prelude.v: General purpose lemmas
- ppoDef.v: Definition of partial preorders, ep-pairs, indexed partial preorders
- beckChevalley.v: lemmas to state the Beck-Chevalley condition in an indexed partial preorder
- arr.v, pi.v, bool.v, empty.v, box.v, sigma.v, list.v, prop.v: partial preorders and action on ep-pairs for type constructors
- unknown.v: construction of unknown parametrized by a family of base types
- unk0.v: instantiation of unknown for the first universe
- listwf.v: well-founded order on list of elements (helper for universe)
- univ0.v: construction of the first universe and its indexed partial preorder
- unk1.v, univ1.v: constructions for the second universe level


# Compilation options, Flags and Axioms

This project does not use any specific compilation flags on top of vanilla Coq.
It does not use any supplementary axioms (no instance of `Axiom`) or admitted proofs 
(no occurences of the tactic `admit` or proof concluded by `Admitted`).

The following flags are set in most files:
```coq
  Set Implicit Arguments.
  Set Universe Polymorphism.
  Set Polymorphic Inductive Cumulativity.
  Set Primitive Projections.
  Set Printing Universes.
```
- The first flag sets the elaboration mode for arguments, 
instructing Coq to infer which arguments can be made implicits
(no impact on kernel representation).

- The second flag set definitions to be universe polymorphic by default.
In practice, universe levels are managed explicitly whenever it is needed
(explicit `universe` and `constraint` declarations).

- The third flag makes universe polymorphic inductives cumulative.

- The fourth flag makes records extensional (adds eta equality).

- The last flag only affects output, printing the universe levels.

# Correspondence with the paper

The different layers for the model are built for each type constructor (prop,
pi, sigma, list, bool, unknown, universe).

The universe of code corresponding to figure 7 is defined in `univ0.v`,
definitions `code0` and `u0`. Casts on this universe are provided in the same file,
definitions `el0_cast`, `el0_upcast` and `el0_downcast`.
This universe is closed under prop, bool, box and pi 
(but not by nat or list, a departure from the paper).

A second universe of codes containing univ0 is defined in `univ1.v` in a similar fashion.

The construction of unknown, left of figure 8, is given in a parametrized
fashion in `unknown.v`, definition `unk`, and instantiated in `unk0.v` for the first
universe and `unk1.v` for the second. The precision on unknown, right of figure 8,
correspond to `prec_unk` in `unknown.v`.

The definitions of partial preorders, embedding-projection pairs and indexed
partial preorders can be found in `ppoDef.v`, definitions `is_ppo`, `eppair_prop`,
`ippo`. Their instances for each type constructors can be found in the
corresponding files.

The axioms of precision in figure 5 are realized through the partial preorder structure
and indexed partial preorder structure in `univ0.v` and `univ1.v`.
Upper-decomposition of heterogeneous precision can be found
in univ0.v, lemma `isrel_univ0_decomposition` while that of cast is lemma `upper_decomposition`.

Theorem 9 is proved in univ0.v for the first universe,
definitions `u0_ppo` and `El0_ippo` for term precision, 
definitions `u0_pic_ppo` and `El0_pic_ippo` for type precision.

The fact that unknown is a greatest element for term precision is
proven in `u0_greatest`.

The counterexample witnessing the failure of threesomes is provided
at the very end of `univ0.v`, lemma `BC_u0_counterexample`.

