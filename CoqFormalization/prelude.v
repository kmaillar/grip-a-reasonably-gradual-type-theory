From Coq Require Import ssreflect.
From Equations Require Import Equations.
From PartialPreorder Require Import strictPropLogic.

Set Implicit Arguments.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Primitive Projections.
Import SPropNotations.

Set Printing Universes.

(** Reset the obligation tactic for Equations *)
(* Show Obligation Tactic. *)
Ltac otac :=
  intros;
  simpl in *;
  Tactics.program_simplify ;
  CoreTactics.equations_simpl ;
  try Tactics.program_solve_wf.

Obligation Tactic := try solve [ otac ].


(** Tactics *)

(* If x : I t1 .. tn is an element of an inductive predicate in SProp,
   try to replace all occurences of x with a (necessarily convertible)
   constructor of I *)
Ltac change_sproof x :=
  let T := type of x in
  let t := fresh "t" in
  simple refine (let t : T := _ in _);
  [constructor|];
  let tu := eval unfold t in t in change x with tu;
                              clear t.

(** Inversion ssr-style intro pattern, used as
  move=> /invert.
 *)
Notation "'invert'" := ltac:(let h := fresh "__invert_" in move=> h; inversion h)  (only parsing) : ssripat_scope.



(** Lemmas on equality *)

Definition transport {A : Type} (P : A -> Type) {x y : A} (e : x = y) (v : P x) : P y :=
  match e in _ = z return P z with | eq_refl => v end.

Import EqNotations.
Lemma sigma_inv A P (a a' : A) (b : P a) (b' : P a') (G : SProp):
  (forall (e : a = a'), rew [P] e in b = b' -> G) ->
    sigmaI P a b = sigmaI P a' b' -> G.
Proof.
  move=> H e. apply: unbox. depelim e. apply: box.
  apply: H. reflexivity.
Qed.

Lemma projT2_eq_uip {A : Type} `{UIP A} {P : A -> Type} {a : A} {u v : P a} :
  existT _ a u = existT _ a v -> u = v.
Proof.
  move=> e; move: (projT2_eq e).
  by rewrite (uip (projT1_eq e) eq_refl).
Qed.

Lemma eq_dec_sym_left A `{EqDec A} a0 a1 (e : a0 = a1) :
  eq_dec a1 a0 = left (eq_sym e).
Proof. depelim e; rewrite eq_dec_refl //. Qed.

Lemma eq_dec_sym_right A `{EqDec A} a0 a1 (ne : a0 <> a1) :
  exists ne', eq_dec a1 a0 = right ne'.
Proof.
  case: (eq_dec a1 a0)=> [e|ne'].
  - exfalso; intuition.
  - by exists ne'.
Qed.


