From Coq Require Import ssreflect ssrfun.
From Equations Require Import Equations.
From PartialPreorder Require Import strictPropLogic.
From PartialPreorder Require Import prelude ppoDef sigma unknown univ0.

Set Implicit Arguments.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Primitive Projections.
Import SPropNotations.

Set Printing Universes.

(** Instantiation of unknown for universe 1 *)

Section Unk1.
  Universe u v.

  Inductive Head1 : Type@{v} :=
  (* head constructor for u0 *)
  | hu01 : Head1
  (* head constructor for inhabitants A of u0 (cumulativity) *)
  | hinj01 (A : u0@{u}) : Head1.

  Set Equations Transparent.
  Equations ElHead1 (h : Head1) : Type@{v} :=
  | hu01 := u0@{u}
  | hinj01 A := el0 A.

  Instance head1_err (h : Head1) : Err@{v} (ElHead1 h) :=
    match h with
    | hu01 => u0_err@{u v}
    | hinj01 A => err0_at@{u u} A end.

  Equations ElHead1_upcast (h0 h1 : Head1) : ElHead1 h0 -> ElHead1 h1 :=
  | hu01, hu01 => id
  | hu01, hinj01 _ => fun _ => err
  | hinj01 _, hu01 => fun _ => err
  | hinj01 A, hinj01 B => @el0_upcast@{u v} A B.

  Equations ElHead1_downcast (h0 h1 : Head1) : ElHead1 h1 -> ElHead1 h0 :=
  | hu01, hu01 => id
  | hu01, hinj01 _ => fun _ => err
  | hinj01 _, hu01 => fun _ => err
  | hinj01 A, hinj01 B => @el0_downcast@{u v} A B.

  Definition FwCHead1 : fwc@{v v} Head1.
  Proof.
    exists ElHead1; [exact ElHead1_upcast| exact ElHead1_downcast].
  Defined.

  Equations prec_Head1 (h0 h1 : Head1) : SProp :=
  | hu01, hu01 => sUnit
  | hinj01 A, hinj01 B => u0_pic@{u v} A B
  | _ , _ => sEmpty.

  Lemma prec_Head1_lrefl : lower_reflexive prec_Head1.
  Proof. move=> [|A] [|B] // AB; refine (lower_refl AB). Qed.

  Lemma prec_Head1_urefl : upper_reflexive prec_Head1.
  Proof. move=> [|A] [|B] // AB; refine (upper_refl AB). Qed.

  Lemma prec_Head1_trans : transitive prec_Head1.
  Proof. move=> [|A] [|B] [|C] // AB BC; refine (trans AB BC). Qed.

  Definition prec_Head1_is_ppo :=
    mkIsPpo prec_Head1 prec_Head1_trans prec_Head1_urefl prec_Head1_lrefl.

  Definition ppo_Head1 : ppo@{v} Head1 := mkPpo prec_Head1_is_ppo.
  Let pHead1 := ppo_Head1.

  Equations srel_ElHead1 (h : Head1) : srel (ElHead1 h) :=
    srel_ElHead1 hu01 := u0_ppo@{u v} ;
    srel_ElHead1 (hinj01 A) := prec_el0'@{u v} A.

  Equations ppo_ElHead1 (h : Head1) : pHead1 h h -> is_ppo@{v} (srel_ElHead1 h) :=
    ppo_ElHead1 hu01 _ := u0_ppo ;
    ppo_ElHead1 (hinj01 A) AA := el0_pic_ippo A AA.

  Definition RHead1@{} : ippo pHead1 FwCHead1.
  Proof.
    unshelve econstructor. (* 2-5: opacify. *)
    - exact srel_ElHead1.
    - exact ppo_ElHead1.
    - move=> [|A] [|B] //; first (move=> ?; apply: ideppair).
      move=> AB; refine (ippo_on el0_pic_ippo AB).
    - move=> [|A] [|B] [|C] //.
      + move=> ??; apply: equiveppair_helper_up; apply: sym_refl.
      + apply: (ippo_trans el0_pic_ippo).
    - move=> [|A].
      + move=> ?; apply: equiveppair_helper_up; apply: sym_refl.
      + apply: (ippo_refl el0_pic_ippo).
  Defined.

  Instance head1_smallest (h : Head1) (hh : pHead1 h h) : SmallestElem (RHead1 h hh).
  Proof.
    case: h hh=> [|A] AA.
    - exact u0_smallest.
    - exact (el0_pic_smallest@{u v} AA).
  Qed.

  Instance head1_dyn (h : Head1) : Dyn@{v} (ElHead1 h) :=
    match h with | hu01 => dyn_u0 | hinj01 A => dyn0_at@{u u} A end.

  Instance head1_greatest (h : Head1) (hh : pHead1 h h) : GreatestElem (RHead1 h hh).
  Proof.
    case: h hh=> hh.
    - exact u0_greatest.
    - apply: el0_pic_greatest.
  Defined.

  Definition unk1 : Type@{v} := unk@{v v} FwCHead1.

  Definition prec_unk1 : srel unk1 := prec_unk@{v v} RHead1.


  Instance: unknown_hypotheses RHead1.
  Proof.
    unshelve econstructor.
    - move=> [|A0] [|A1] h00 h11 v; cbn -[ippo_at]; simp ElHead1_downcast.
      1,2: move=> _; apply: smallest_refl.
      apply: (@fdownerr _ _ _ _ _ _ univ0_pic_UH).
    - move=> [|A0] [|A1] h00 h11 v; cbn -[ippo_at]; simp ElHead1_downcast.
      1,2: move=> _; apply: greater; apply: smallest_refl.
      apply: (@fdowndyn _ _ _ _ _ _ univ0_pic_UH).
    - move=> [|A0] h00 [|A1] h11 [|A2] h22 v; cbn -[ippo_at]; simp ElHead1_downcast.
      all: try solve [move=> ?; apply: smaller=> //; apply: smallest_refl].
      + move=> h ; unshelve apply: smaller => //.
        apply: (@fdownrel _ _ _ _ _ _ univ0_pic_UH)=> //.
        apply: (prec_iprec RHead1 _ _ _ _ h).
      + move=> h; apply: (@fdownerr _ _ _ _ _ _ univ0_pic_UH _ _ h11 h00).
        apply: smallest_refl; apply: el0_pic_smallest.
      + apply: (@fdowncomp _ _ _ _ _ _ univ0_pic_UH)=> //.
    - move=> [|A0]  [|B0] [|B1] hA00 hB01 v; cbn -[ippo_at]; simp ElHead1_downcast=> //.
      + move=> ?; apply: el0_iprec_err; apply: smallest_refl; apply: el0_pic_smallest.
      + move=> ?; apply prec_iprec; apply: smallest_refl.
      + apply: (@fdownmon_right _ _ _ _ _ _ univ0_pic_UH)=> //.
    -  move=> [|A0] X1 [|B0] hA00 hX11 hB00 ??; cbn -[ippo_at]; simp ElHead1_downcast=> //.
      + move=> h; exact (isrel_downcast h).
      + move=> h; apply: smaller.
        destruct X1; first apply: smallest_refl.
        apply: (@fdownrel _ _ _ _ _ _ univ0_pic_UH)=> //.
        apply: (prec_iprec RHead1 _ _ _ _ (isrel_urefl h))=> //.
      + move=> h; apply: smaller.
        destruct X1; last apply: smallest_refl.
        apply: (isrel_urefl h).
      + destruct X1; last apply: (@fdownrel _ _ _ _ _ _ univ0_pic_UH)=> //.
        move=> h; unshelve apply: (@fdownerr _ _ _ _ _ _ univ0_pic_UH).
        1: assumption.
        exact (isrel_downcast h).
    - move=> [|A0] [|A1] ?? h00 h11; cbn -[ippo_at]; simp ElHead1_downcast=> //.
      + move=> h _; exact (prec_iprec _ _ _ _ _ h).
      + move=> /wrefl ? ? h1 h2. repeat split=> //.
       exact (@smaller _ _ _ (el0_pic_smallest h11) _ h2).
      + move=> /wrefl ? ? h1 h2. repeat split=> //.
        by apply: smaller.
      + apply: (@isrel_RElH _ _ _ _ _ _ univ0_pic_UH).
    - move=> [|A] A00; apply: smaller; apply: greatest_refl.
  Qed.

  (** Instantiation of construction from unknown with Head1 *)

  Definition unk1_ppo : ppo@{v} unk1 := unk_ppo@{v v} (RElH := RHead1).

  Definition injunk1 (h : Head1) (x : ElHead1 h) : unk1 :=
    injUnkC FwCHead1 h x.

  #[global]
  Instance unk1_err : Err unk1 := err_unk FwCHead1.
  #[global]
  Instance unk1_smallest : SmallestElem unk1_ppo :=
    unk_smallest (RElH := RHead1).

  #[global]
  Instance unk1_dyn : Dyn unk1 := dyn_unk FwCHead1.
  #[global]
  Instance unk1_greatest : GreatestElem unk1_ppo :=
    unk_greatest (RElH := RHead1).

  Definition upcast_to_unk1 : forall h, ElHead1 h -> unk1 :=
    @inj_upcast Head1 FwCHead1.
  Definition downcast_from_unk1 : forall h, unk1 -> ElHead1 h :=
    @inj_downcast Head1 FwCHead1 _ _.

  (** Eppair with u0 *)

  Definition upcast_u0_to_unk1 : u0 -> unk1 :=
    upcast_to_unk1 hu01.

  Definition downcast_unk1_to_u0 : unk1 -> u0 :=
    downcast_from_unk1 hu01.

  Definition eppair_u0_to_unk1 : eppair u0_ppo unk1_ppo :=
    inj_eppair hu01 stt.

  (** Eppairs from types in universe 0 *)

  Definition upcast_el0_to_unk1 A : el0 A -> unk1 :=
    upcast_to_unk1 (hinj01 A).

  Definition downcast_unk1_to_el0 A : unk1 -> el0 A :=
    downcast_from_unk1 (hinj01 A).

  Definition eppair_el0_to_unk1 A (AA : u0_pic A A) : eppair (el0_pic_ppo AA) unk1_ppo :=
    inj_eppair (hinj01 A) AA.

  Lemma compepair_el0_to_unk1 A0 A1
        (A00 : u0_pic A0 A0)
        (A11 : u0_pic A1 A1)
        (A01 : u0_pic A0 A1) :
    compeppair (el0_pic_eppair A01) (eppair_el0_to_unk1 A11) ≃ eppair_el0_to_unk1 A00.
  Proof.
    apply: equiveppair_helper_up=> a aa; split; cbn.
    - apply: pu_injUnk=> //.
      pose proof (univ0_pic_upcastmon _ A11 _ _ aa).
      (unshelve apply: (el0_pic_upper_decomposition_isrel A11 A11 A01))=> //.
      apply: (equiv_upcast_l _ _ H (equiveppair_sym (el0_pic_id A11)) H).
    - apply: pu_injUnk=> //.
      pose proof (univ0_pic_upcastmon _ A11 _ _ aa).
      (unshelve apply: (el0_pic_upper_decomposition_isrel A11 A01 A11))=> //.
      apply: (equiv_upcast_r _ _ H (equiveppair_sym (el0_pic_id A11)) H).
  Qed.

  (** Eppairs from sigma in universe 1 *)
  (* unused *)

  Definition upcast_sigma_to_unk1 (A : Type@{v}) (B : A -> Type@{v})
             (upA : A -> unk1)
             (upB : forall a, B a -> unk1)
    : sigma@{v} B -> unk1
    :=
    @sigma_unk_upcast Head1 FwCHead1 A B upA upB.

  Definition downcast_unk1_to_sigma (A : Type@{v}) (B : A -> Type@{v})
             `{Err A} `{forall a, Err (B a)}
             `{Dyn A} `{forall a, Dyn (B a)}
             (downA : unk1 -> A)
             (downB : forall a, unk1 -> B a)
    : unk1 -> sigma@{v} B
    :=
    @sigma_unk_downcast Head1 FwCHead1 A B _ _ _ _ downA (fun _ => downB _).

  Definition eppair_sigma_to_unk1
             [A : Type@{v}] [B : fwc@{v v} A] [RA : ppo@{v} A] [RB : ippo@{v} RA B]
             [Aunk : eppair@{v} RA unk1_ppo]
             (Bunk : ieppair@{v} RB (ippo_from_ppo@{v} unk1_ppo unk1_ppo) Aunk)
             `{Err@{v} A} `{forall a : A, Err@{v} (B a)}
             `{Dyn@{v} A} `{forall a : A, Dyn@{v} (B a)}
             `{!SmallestElem@{v} RA}
             `{!forall (a : A) (aa : RA a a), SmallestElem@{v} (RB a aa)}
             `{!GreatestElem@{v} RA}
             `{!forall (a : A) (aa : RA a a), GreatestElem@{v} (RB a aa)} :
             eppair@{v} (sigma_ppo@{v v} RB) unk1_ppo :=
    sigma_unk_eppair Bunk.

End Unk1.
