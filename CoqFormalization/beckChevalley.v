From Coq Require Import ssreflect ssrfun.
From Equations Require Import Equations.
From PartialPreorder Require Import strictPropLogic.
From PartialPreorder Require Import prelude ppoDef.

Set Implicit Arguments.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Primitive Projections.
Import SPropNotations.

(** Statement of the Beck-Chevalley condition for an indexed partial preorder *)

Section Mate.
  Universe u.
  Context (A : Type@{u}) (RA : ppo@{u} A)
          (B : fwc@{u u} A)
          (RB : ippo@{u} RA B).

  Context (ax aa ab ay : A)
          (* Need to be explicit on the projection in order
             to prevent the generation of a fresh universe level *)
          (aaa : ppo_rel@{u} RA aa aa)
          (abb : ppo_rel@{u} RA ab ab)
          (axa : ppo_rel@{u} RA ax aa)
          (aay : ppo_rel@{u} RA aa ay)
          (axb : ppo_rel@{u} RA ax ab)
          (aby : ppo_rel@{u} RA ab ay).

  Let XB := ippo_on@{u} RB axb.
  Let XA := ippo_on@{u} RB axa.
  Let AY := ippo_on@{u} RB aay.
  Let BY := ippo_on@{u} RB aby.

  Lemma mate@{} :
    forall (ba : B aa) (bba : ippo_at@{u} RB aaa ba ba),
      RB ab abb
         (upcast XB (downcast XA ba))
         (downcast BY (upcast AY ba)).
  Proof.
    move=> ba bba.
    apply: iprec_refl@{u}.
    refine (eprel_equiveppair (equiveppair_sym (ippo_refl RB abb)) _).
    apply: eprel_down_right.
    change (compeppair _ _) with BY.
    apply: eprel_up_left.
    refine (eprel_equiveppair (equiveppair_sym (ippo_trans RB axb aby)) _).
    refine (eprel_equiveppair (ippo_trans RB axa aay) _).
    apply: eprel_down_left'.
    refine (eprel_up_right AY _ _ bba).
  Qed.


  (** Helper to state Beck-Chevalley condition
      If   ax ⊏ aa  is cartesian, that is if ax ≅ aa ⊓ ab
           ⊓    ⊓
           ab ⊏ ay
      then the mate of this square (induced by the ippo RB)
      is invertible
   *)
  Definition mate_invertible : SProp :=
    forall (ba : B aa) (bba : ippo_at@{u} RB aaa ba ba),
      RB ab abb
         (downcast BY (upcast AY ba))
         (upcast XB (downcast XA ba)).
End Mate.

Section MateDegenerateInv.
  Universe u.
  Context (A : Type@{u}) (RA : ppo@{u} A)
          (B : fwc@{u u} A)
          (RB : ippo@{u} RA B).
  Context (ax aa: A)
          (aaa : ppo_rel@{u} RA aa aa)
          (axa : ppo_rel@{u} RA ax aa)
          (axx : ppo_rel@{u} RA ax ax).

  Let XA := ippo_on@{u} RB axa.

  Lemma degen_left_mate_inv :
    mate_invertible@{u} RB ax aa ax aa aaa axx axa aaa axx axa.
  Proof.
    move=> ba baa.
    apply: (trans (downmon XA _ _ (equiv_upcast_app0 (ippo_refl RB aaa) _ _ baa))).

    apply: (trans _ (equiv_upcast_app0 (equiveppair_sym (ippo_refl RB axx)) _ _ (downmon XA _ _ baa))).
    by apply: downmon.
  Qed.

  Lemma degen_right_mate_inv :
    mate_invertible@{u} RB ax ax aa aa axx aaa axx axa axa aaa.
  Proof.
    move=> ba baa. apply: trans.
    1: move: (upmon XA _ _ baa); apply: equiv_downcast_app0; apply: (ippo_refl RB aaa).
    apply: (trans (upmon XA _ _ baa) _).
    apply: upmon; by apply: (equiv_downcast_app0 (equiveppair_sym (ippo_refl RB axx))).
  Qed.

End MateDegenerateInv.

Section MateInvPostcomp.
  Universe u.
  Context (A : Type@{u}) (RA : ppo@{u} A)
          (B : fwc@{u u} A)
          (RB : ippo@{u} RA B).
  Context (ax aa ab ay ay' : A)
          (aaa : ppo_rel@{u} RA aa aa)
          (abb : ppo_rel@{u} RA ab ab)
          (axa : ppo_rel@{u} RA ax aa)
          (aay : ppo_rel@{u} RA aa ay)
          (axb : ppo_rel@{u} RA ax ab)
          (aby : ppo_rel@{u} RA ab ay)
          (ayy' : ppo_rel@{u} RA ay ay').


  Let aay' := trans aay ayy'.
  Let aby' := trans aby ayy'.


  Lemma mate_inv_postcomp :
    mate_invertible@{u} RB ax aa ab ay aaa abb axa aay axb aby ->
    mate_invertible@{u} RB ax aa ab ay' aaa abb axa aay' axb aby'.
  Proof.
    move=> miy AX XA AY' BY' ba baa.
    apply: (trans _ (miy ba baa)).
    set (AY := ippo_on RB aay).
    set (YY' := ippo_on RB ayy').
    set (BY := ippo_on RB aby).
    apply: (trans _ (downmon BY _ _ (retract YY' _ (upmon AY _ _ baa)))).
    apply: (equiv_downcast_app0 (equiveppair_sym (ippo_trans RB aby ayy'))).
    by apply: (equiv_upcast_app0 (equiveppair_sym (ippo_trans RB aay ayy'))).
  Qed.

  Lemma mate_inv_postcomp_enough :
    mate_invertible@{u} RB ax aa ab ay' aaa abb axa aay' axb aby' ->
    mate_invertible@{u} RB ax aa ab ay aaa abb axa aay axb aby.
  Proof.
    move=> miy' AX XA AY BY ba baa.
    set (YY' := ippo_on RB ayy').
    apply: (trans (downmon BY _ _ (unit YY' _ (upmon AY _ _ baa)))).
    apply: (trans _ (miy' ba baa)).
    apply: (equiv_downcast_app0 (ippo_trans RB aby ayy')).
    by apply: (equiv_upcast_app0 (ippo_trans RB aay ayy')).
  Qed.

End MateInvPostcomp.
