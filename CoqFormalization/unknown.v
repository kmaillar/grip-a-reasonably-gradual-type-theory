From Coq Require Import ssreflect ssrfun.
From Equations Require Import Equations.
From PartialPreorder Require Import strictPropLogic.
From PartialPreorder Require Import prelude ppoDef sigma.

Set Implicit Arguments.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Primitive Projections.
Import SPropNotations.

Set Printing Universes.

(** Construction of Unknown, parametrized by a ppo H, and a family ElH *)
(** H correspond to types that ought to be below unknown in a fixed universe *)
(* This construction of unknown is closed under dependent sum. *)
(* With respect to the paper, the closure under other finitary W-types such as
   list is still missing (but see list.v) *)



Section Unknown.
  Universes u v.
  Context (H : Type@{u}) (ElH : fwc@{u v} H).

  Inductive unk : Type@{v} :=
  | errUnkC
  | unkUnkC
  | injUnkC (h : H) (v : ElH h)
  | sigmaUnkC (p1 : unk) (p2 : unk).
  (* Discrepancy with the paper: we treat sigma instead of list *)
  (* | listUnkC (l : list unk) *)


  Arguments injUnkC _ _: clear implicits.


  (* The properties on the relations are not strictly needed to define *)
  (* sp, is_unk_err and the precision relation on unknown but *)
  (* it will probably make things easier to prove *)
  Context (RH : ppo@{u} H) (RElH : ippo@{v} RH ElH).

  (** In order to simplify the presentation of precision on unknown
      we first define independently what it means to be self-precise (sp)
      and be a smallest element (is_unk_err) *)
  Fixpoint sp (u : unk) : SProp :=
    match u with
    | errUnkC => sUnit
    | unkUnkC => sUnit
    | injUnkC h v => sΣ (hh : RH h h), ippo_at@{v} RElH hh v v
    | sigmaUnkC p1 p2 => sp p1 s/\ sp p2
    end.

  Context `{forall h, Err@{v} (ElH h)}.

  Fixpoint is_unk_err (u : unk) : SProp :=
    match u with
    | errUnkC => sUnit
    | unkUnkC => sEmpty
    | injUnkC h v => sΣ (hh :  RH h h), ippo_at@{v} RElH hh v err
    | sigmaUnkC p1 p2 => is_unk_err p1 s/\ is_unk_err p2
    end.


  Lemma is_unk_err_sp u : is_unk_err u -> sp u.
  Proof.
    elim: u=> //[??[hh ?]|????[??]]; last (split; intuition).
    exists hh; apply: lower_refl; eassumption.
  Qed.

  Instance err_unk : Err@{v} unk := errUnkC.
  Instance dyn_unk : Dyn@{v} unk := unkUnkC.

  Context `{!forall h, Dyn (ElH h)}.

  Fixpoint dyn_bounded (u : unk) {struct u} : SProp :=
    match u with
    | errUnkC => sUnit
    | unkUnkC => sUnit
    | injUnkC h v => sΣ (hh :  RH h h), ippo_at@{v} RElH hh v dyn
    | sigmaUnkC u1 u2 => dyn_bounded u1 s/\ dyn_bounded u2
    end.

  Lemma dyn_bounded_sp u : dyn_bounded u -> sp u.
  Proof.
    elim: u=> // [h x [hh xd]|u1 ih1 u2 ih2 [/ih1 ? /ih2 ?]]; last by split.
    exists hh; exact ⌊xd⌋.
  Qed.


  Inductive prec_unk : srel unk :=
  | pu_any_unk u : dyn_bounded u -> prec_unk u dyn
  | pu_any_err (u u' : unk) : is_unk_err u -> sp u' -> prec_unk u u'
  (* Crucially, h1 and h2 are self-precise but not related by precision *)
  | pu_injUnk h1 (h11 : RH h1 h1) (x : ElH h1) h2 (h22: RH h2 h2) (y : ElH h2) :
    i[srel_at RElH] h1 h2 x y -> prec_unk (injUnkC h1 x) (injUnkC h2 y)
  | pu_sigmaUnkC (p1 p2 p1' p2' : unk) :
    prec_unk p1 p1' -> prec_unk p2 p2' -> prec_unk (sigmaUnkC p1 p2) (sigmaUnkC p1' p2')
  .

  (** Inversion lemmas for precision on unknown *)

  Lemma pu_err_r_inv u : prec_unk u err -> is_unk_err u.
  Proof. move: u=> [||??|??] h //; inversion h=> //. Qed.

  Lemma pu_unk_l_inv u : prec_unk dyn u -> u = dyn.
  Proof.
    move: u=> [||??|??] h.
    2: reflexivity.
    all: scontradiction (inversion h; contradiction).
  Qed.

  Inductive inj_l_inv h0 v0 : unk -> SProp :=
  | ili_unk : dyn_bounded (injUnkC h0 v0) -> inj_l_inv h0 v0 dyn
  | ili_err u : is_unk_err (injUnkC h0 v0) -> sp u -> inj_l_inv h0 v0 u
  | ili_inj h1 v1 : RH h0 h0 -> RH h1 h1 -> i[srel_at RElH] h0 h1 v0 v1 ->
                    inj_l_inv h0 v0 (injUnkC h1 v1).

  Context `{forall h (hh : RH h h), SmallestElem@{v} (ippo_at@{v} RElH hh)}.

  Lemma pu_inj_l_inv h v u :
    prec_unk (injUnkC h v) u -> inj_l_inv _ v u.
  Proof.
    move=> pf.
    refine (match pf in prec_unk u1 u2 return match u1 with
                                              | injUnkC h v => inj_l_inv h v u2
                                              | _ => sUnit
                                              end
            with
            | pu_any_err u0 u1 erru0 spu1 => _
            | pu_any_unk u0 spu0 => _
            | pu_injUnk _ _ _ => _
            | _ => stt end)=> //=.
    1: case: u0 spu0 => * ; by constructor.
    1: case: u0 erru0=> *; by constructor.
    by unshelve apply: ili_inj.
  Qed.

  Inductive inj_r_inv h1 v1 : unk -> SProp :=
  | iri_err u : is_unk_err u -> sp (injUnkC h1 v1) -> inj_r_inv h1 v1 u
  | iri_inj h0 v0 : RH h0 h0 -> RH h1 h1 -> i[srel_at RElH] h0 h1 v0 v1 -> inj_r_inv h1 v1 (injUnkC h0 v0).

  Lemma pu_inj_r_inv h v u :
    prec_unk u (injUnkC h v) -> inj_r_inv _ v u.
  Proof.
    move=> pf.
    refine (match pf in prec_unk u1 u2 return match u2 with
                                              | injUnkC h v => inj_r_inv h v u1
                                              | _ => sUnit
                                              end
            with
            | pu_any_err u0 u1 erru0 spu1 => _
            | pu_injUnk _ _ _ => _
            | _ => stt end)=> /=.
    1: case: u1 spu1=> *; by constructor.
    by unshelve apply: iri_inj.
  Qed.

  Inductive sigma_l_inv p1 p2 : unk -> SProp :=
  | sli_unk : dyn_bounded (sigmaUnkC p1 p2) -> sigma_l_inv p1 p2 dyn
  | sli_err u : is_unk_err (sigmaUnkC p1 p2) -> sp u -> sigma_l_inv p1 p2 u
  | sli_sig p1' p2' : prec_unk p1 p1' -> prec_unk p2 p2' -> sigma_l_inv p1 p2 (sigmaUnkC p1' p2').

  Lemma pu_sigma_l_inv p1 p2 u :
    prec_unk (sigmaUnkC p1 p2) u -> sigma_l_inv p1 p2 u.
  Proof.
    move: u=> [||??|??] h.
    2: inversion h; first by apply: sli_unk.
    1,2,3: apply: sli_err=> //; inversion h=> //.
    apply: sli_sig; inversion h=> //; cbn in *.
    1,2: repeat match goal with [ H : _ s/\ _ |- _ ] => destruct H end.
    1,2: by apply: pu_any_err.
  Qed.

  Inductive sigma_r_inv p1 p2 : unk -> Type :=
  | sri_err u : is_unk_err u -> sp (sigmaUnkC p1 p2) -> sigma_r_inv p1 p2 u
  | sri_sig p1' p2' : prec_unk p1' p1 -> prec_unk p2' p2 -> sigma_r_inv p1 p2 (sigmaUnkC p1' p2').

  Lemma pu_sigma_r_inv p1 p2 u :
    prec_unk u (sigmaUnkC p1 p2) -> sigma_r_inv p1 p2 u.
  Proof.
    move: u=> [||??|??] h.
    1-3: apply: sri_err; inversion h=> //.
    apply: sri_sig; inversion h; subst=> //; cbn in *.
    1,2: repeat match goal with [ H : _ s/\ _ |- _ ] => destruct H end.
    1,2: by apply: pu_any_err.
  Qed.

  (** Relation between self-precision and precision *)

  Lemma sp_prec_unk : forall u, sp u -> prec_unk u u.
  Proof.
    elim=> [||??|????[??]] ; try by (constructor; intuition).
    move=> [??]; unshelve apply: pu_injUnk=> //.
    by unshelve apply: prec_iprec.
  Qed.

  Lemma prec_unk_sp_l : forall u u', prec_unk u u' -> sp u.
  Proof.
    move=> ??; elim=> [? /dyn_bounded_sp //|?? /is_unk_err_sp //| ? h11 *|*]; last by split.
    exists h11; apply: hrel_lrefl; eassumption.
  Qed.

  Lemma prec_unk_sp_r : forall u u', prec_unk u u' -> sp u'.
  Proof.
    move=> ??; elim=> //???? h22 ??; exists h22; apply: hrel_urefl; eassumption.
  Qed.

  (** Additional assumptions on H/ElH in order to build unkwnon *)
  (* These conditions are necessary in the sense that assuming a
     construction of unknown in a fixed universe allow to derive these
     from the indexed partial preorder properties on the universe *)
  Class unknown_hypotheses : SProp :=
    mkUH {
        fdownerr : forall h0 h1 (h00 : RH h0 h0) (h11 : RH h1 h1) v,
          ippo_at@{v} RElH h00 v err ->
          ippo_at@{v} RElH h11 (fdowncast ElH h1 h0 v) err ;
        fdowndyn : forall h0 h1 (h00 : RH h0 h0) (h11 : RH h1 h1) v,
          ippo_at@{v} RElH h00 v dyn ->
          ippo_at@{v} RElH h11 (fdowncast ElH h1 h0 v) dyn ;
        fdowncomp : forall h0 (h00 : RH h0 h0)
                      h1 (h11 : RH h1 h1)
                      h2 (h22 : RH h2 h2) v,
          ippo_at@{v} RElH h22 v v ->
          ippo_at@{v} RElH h00
                 (fdowncast ElH h0 h1 (fdowncast ElH h1 h2 v))
                 (fdowncast ElH h0 h2 v) ;
        fdownmon_right : forall ha hb0 hb1 (haa : RH ha ha) (hb01 : RH hb0 hb1) v,
          ippo_at@{v} RElH haa v v ->
          iprec RElH hb01 (fdowncast ElH hb0 ha v) (fdowncast ElH hb1 ha v) ;
        fdownrel : forall ha0 ha1 hb (ha00 : RH ha0 ha0) (ha11 : RH ha1 ha1) (hbb : RH hb hb) v0 v1,
          i[srel_at RElH] ha0 ha1 v0 v1 ->
          ippo_at@{v} RElH hbb (fdowncast ElH hb ha0 v0) (fdowncast ElH hb ha1 v1) ;
        isrel_RElH : forall h0 h1 v0 v1 (h00 : RH h0 h0) (h11 : RH h1 h1),
          ippo_at@{v} RElH h00 v0 (fdowncast ElH h0 h1 v1) ->
          ippo_at@{v} RElH h11 v1 v1 ->
          i[srel_at RElH] h0 h1 v0 v1 ;
        ElH_err_unk : forall h (hh : RH h h), ippo_at@{v} RElH hh err dyn
      }.

  Context `{unknown_hypotheses}.

  (** Some consequences on the hypotheses to build unknown *)

  Lemma fdownmon ha0 ha1 hb0 hb1
        (ha01 : RH ha0 ha1) (hb01 : RH hb0 hb1)
        v0 v1 :
    iprec RElH ha01 v0 v1 ->
    iprec RElH hb01 (fdowncast ElH hb0 ha0 v0) (fdowncast ElH hb1 ha1 v1).
  Proof.
    move=> v01.
    apply: iprec_right.
    2: apply: (fdownrel hb1 ⌊ha01⌋ ⌈ha01⌉ ⌈hb01⌉ v01).
    unshelve apply: fdownmon_right.
    apply: lower_refl; eassumption.
    apply: eprel_lrefl; eassumption.
  Qed.

  Lemma fdowncast_comp
        h0 (h00 : RH h0 h0)
        h1 (h11 : RH h1 h1)
        h2 (h22 : RH h2 h2)
        v0 v1 v2 :
    ippo_at@{v} RElH h00 v0 (fdowncast ElH h0 h1 v1) ->
    ippo_at@{v} RElH h11 v1 (fdowncast ElH h1 h2 v2) ->
    ippo_at@{v} RElH h22 v2 v2 ->
    ippo_at@{v} RElH h00 v0 (fdowncast ElH h0 h2 v2).
  Proof.
    move=> v01 v12 v22.
    apply: (trans v01).
    apply: (trans _ (fdowncomp _ h00 _ h11 _ h22 _ v22)).
    apply: fdownrel=> //; apply: prec_iprec; eassumption.
  Qed.

  Lemma itrans_ElH h0 h1 h2
        (h00 : RH h0 h0) (h11 : RH h1 h1) (h22 : RH h2 h2)
        v0 v1 v2 :
    i[srel_at RElH] h0 h1 v0 v1 ->
    i[srel_at RElH] h1 h2 v1 v2 ->
    i[srel_at RElH] h0 h2 v0 v2 .
  Proof.
    move=> v01 v12.
    apply: isrel_RElH; last refine (hrel_urefl v12).
    apply: fdowncast_comp; last refine (hrel_urefl v12).
    1: exact (hrel_downcast v01).
    exact (hrel_downcast v12).
    Unshelve. all: assumption.
  Qed.


  Lemma is_unk_err_dyn_bounded u : is_unk_err u -> dyn_bounded u.
  Proof.
    elim: u=> // [h v [hh verr]|u1 ih1 u2 ih2 [/ih1 ? /ih2 ?]]; last by split.
    exists hh. exact (trans verr (ElH_err_unk h hh)).
  Qed.

  Lemma prec_unk_dyn_bounded u1 u2 : prec_unk u1 u2 -> dyn_bounded u2 -> dyn_bounded u1.
  Proof.
    elim=>{u1 u2}[//| ??/is_unk_err_dyn_bounded //
                | ? h11 ???? xy [h22 yd] | ????? ih1 ? ih2 [/ih1 ? /ih2 ?]].
    2: by split.
    exists h11. apply: (trans (hrel_downcast xy : RElH _ h11 _ _)).
    apply: fdowndyn; eassumption.
  Qed.

  Lemma is_unk_err_prec_unk_mon :
    forall u u', prec_unk u u' -> is_unk_err u' -> is_unk_err  u.
  Proof.
    move=> ??; elim=> // [? h11 ??? h22 xy [? yerr]|/=????????[??]]; last dintuition.
    exists h11. apply: trans; first refine (hrel_downcast xy). apply: fdownerr; eassumption.
  Qed.

  Lemma prec_unk_err_is_unk_err u : prec_unk u err -> is_unk_err u.
  Proof. move=> /is_unk_err_prec_unk_mon h; apply: h=> //. Qed.

  Lemma is_unk_err_prec_unk u : is_unk_err u -> prec_unk u err.
  Proof. move: u=> [?|[]|???|??[??]]; by constructor. Qed.


  (** Partial preorder structure *)

  Lemma prec_unk_urefl : upper_reflexive prec_unk.
  Proof.
    move=> ?? h; apply: sp_prec_unk; apply: prec_unk_sp_r; exact h.
  Qed.

  Lemma prec_unk_lrefl : lower_reflexive prec_unk.
  Proof.
    move=> ?? h; apply: sp_prec_unk; apply: prec_unk_sp_l; exact h.
  Qed.

  Lemma prec_unk_trans : transitive prec_unk.
  Proof.
    move=> x y z xy; elim: xy (xy) z=> [ux spux|ux uy errux spuy| hx hxx vx hy hyy vy vxy|????????] xy z.
    - move=> /pu_unk_l_inv ->; by constructor.
    - move=> /prec_unk_sp_r ?.
      by apply: pu_any_err.
    - move=> /pu_inj_l_inv [|*|*].
      + move=> [? vyd]; apply: pu_any_unk.
        exists hxx. apply: (trans (hrel_downcast vxy : RElH hx hxx _ _)).
        apply: fdowndyn; eassumption.
      + apply: pu_any_err=> //; apply: is_unk_err_prec_unk_mon; eassumption.
      + (unshelve apply: pu_injUnk)=> //.
        (unshelve apply: (itrans_ElH _ _ _ vxy))=> //.
    - move=> /pu_sigma_l_inv [].
      + move=> [??]; constructor; split; apply: prec_unk_dyn_bounded; eassumption.
      + move=> ? [??] ?; apply: pu_any_err=>//; split;
        apply: is_unk_err_prec_unk_mon; eassumption.
      + move=> *; apply: pu_sigmaUnkC; auto.
  Qed.

  Definition prec_unk_is_ppo :=
    mkIsPpo prec_unk prec_unk_trans prec_unk_urefl prec_unk_lrefl.

  Definition unk_ppo : ppo@{u} unk := mkPpo prec_unk_is_ppo.


  Instance unk_smallest : SmallestElem@{u} unk_ppo.
  Proof. do 3? constructor. apply: prec_unk_sp_l; eassumption. Qed.

  Instance unk_greatest `{!forall h (hh : RH h h), GreatestElem (ippo_at@{v} RElH hh)} : GreatestElem@{u} unk_ppo.
  Proof.
    constructor; first do 2 constructor.
    move=> a aa; constructor.
    elim: a aa=> // [].
    - move=> ?? /prec_unk_sp_l [hh vv]; exists hh; apply: greater; eassumption.
    - move=> ? ih1 ? ih2 /prec_unk_sp_l [/sp_prec_unk /ih1 ? /sp_prec_unk /ih2 ?] ; by split.
  Qed.

  (** Eppair from/to element of H *)

  Section InjEppair.

    Definition inj_upcast := injUnkC.
    Definition inj_downcast h0 : unk -> ElH h0 :=
      fun u => match u with
            | injUnkC h v => fdowncast ElH h0 h v
            | unkUnkC => dyn
            | _ => err
            end.

    Context `{!forall h (hh : RH h h), GreatestElem (ippo_at@{v} RElH hh)}.

    Lemma inj_downcast_sp h0 u  (h00 : RH h0 h0) :
      sp u -> ippo_at@{v} RElH h00 (inj_downcast h0 u) (inj_downcast h0 u).
    Proof.
      elim: u=> [?|?|h v [? ?]|? ih1 ? ih2 [??]].
      3: apply: iprec_refl; apply: fdownmon; apply: prec_iprec; eassumption.
      all: solve [apply: smallest_refl| apply: greatest_refl].
    Qed.

    Lemma inj_downcast_is_unk_err h0 (h00 : RH h0 h0) u :
      is_unk_err u -> ippo_at@{v} RElH h00 (inj_downcast h0 u) err.
    Proof.
      elim: u=> // [*|??[??]|*].
      2: apply: fdownerr; eassumption.
      all: apply: smallest_refl.
    Qed.

    Lemma pu_inj_diag_inv h v : prec_unk (injUnkC h v) (injUnkC h v) -> sp (injUnkC h v).
    Proof.
      move=> /pu_inj_r_inv /invert; subst=> //.
      unshelve eexists; first eassumption.
      apply: hrel_urefl; eassumption.
    Qed.


    Lemma inj_is_eppair h0 (h00 : RH h0 h0) :
      eppair_prop (RElH h0 h00) unk_ppo (@inj_upcast h0) (inj_downcast h0).
    Proof.
      constructor.
      - move=> ?? h; (unshelve apply: pu_injUnk)=> //.
        refine (prec_iprec _ _ _ _ _ h).
      - move=> ?? [] * //.
        + apply: greater; apply: inj_downcast_sp; by apply: dyn_bounded_sp.
        + apply: trans; first apply: inj_downcast_is_unk_err=> //.
          apply: smaller; by apply: inj_downcast_sp.
        + apply: iprec_refl.
          apply: prec_iprec.
          (unshelve apply: fdownrel)=> //.
        + apply: smallest_refl.
      - move=>  v u vv /pu_inj_l_inv [].
        + move=> ?; by apply: greater.
        + move=> ? [? verr] ?; apply: (trans verr); apply: smaller.
          by apply: inj_downcast_sp.
        + move=> ?? ? h11 v01; apply: (hrel_downcast v01).
      - move=> g [||h1 w|??] uu h.
        + constructor=> //=.
        + constructor; exists h00=> //.
        + move: (uu)=> /pu_inj_diag_inv [h11 ww].
          (unshelve apply: pu_injUnk)=> //.
          apply: isrel_RElH; eassumption.
        + apply: pu_any_err; first exists h00=> //.
          apply: prec_unk_sp_l; eassumption.
      - move=> a aa.
        by apply: (equiv_downcast_l a a _ (equiveppair_sym (ippo_refl RElH h00)) aa).
    Qed.

    Definition inj_eppair (h : H) (hh : RH h h) : eppair (ippo_at@{v} RElH hh) unk_ppo :=
      pack_eppair (inj_is_eppair h hh).
  End InjEppair.

  (** Eppair from/to Σ A B *)

  Section SigmaCast.
    Context (A : Type@{u}) (B : A -> Type@{u}).

    Section Upcast.
      Context (Aunk : A -> unk) (Bunk : forall a, B a -> unk).

      Definition sigma_unk_upcast : sigma B -> unk :=
        fun p => sigmaUnkC (Aunk (d1 p)) (Bunk (d2 p)).

    End Upcast.

    Section Downcast.
      Existing Instance err_sigma.
      Existing Instance dyn_sigma.
      Context `{Err A} `{forall a, Err (B a)} `{Dyn A} `{forall a, Dyn (B a)}
              (unkA : unk -> A) (unkB : forall u, unk -> B (unkA u)).

      Definition sigma_unk_downcast : unk -> sigma B :=
        fun u => match u return sigma@{u} B with
              | sigmaUnkC x y => {| d1 := unkA x ; d2 := unkB _ y |}
              | unkUnkC => dyn@{u}
              | _ => err
              end.

    End Downcast.
  End SigmaCast.

  Section SigmaEppair.
    Context (A : Type@{u}) (B : fwc@{u u} A).
    Context (RA : ppo@{u} A) (RB : ippo@{u} RA B).
    Context (Aunk : eppair RA unk_ppo)
            (Bunk : ieppair RB (ippo_from_ppo unk_ppo unk_ppo) Aunk).

    Let su_upcast :=
          @sigma_unk_upcast A B (upcast Aunk) (fun u => iupcast Bunk (upcast Aunk u)).

    Lemma sigma_unk_upcast_monotone : monotone (sigma_ppo RB) unk_ppo su_upcast.
    Proof.
      move=> ??[? /toiprec q]; apply: pu_sigmaUnkC.
      1: by refine (upmon Aunk _ _ _).
      refine (eprel_upcast (iupmon Bunk q)).
    Qed.

    Existing Instance err_sigma.
    Existing Instance dyn_sigma.
    Context `{Err A} `{forall a, Err (B a)} `{Dyn A} `{forall a, Dyn (B a)}.
    Context `{!SmallestElem RA} `{!forall a aa, SmallestElem (RB a aa)}.
    Context `{!GreatestElem RA} `{!forall a aa, GreatestElem (RB a aa)}.

    Existing Instance smallest_sigma.
    Existing Instance greatest_sigma.

    Let su_downcast :=
          @sigma_unk_downcast A B _ _ _ _ (downcast Aunk) (fun u => idowncast Bunk (ay :=  u) _).

    Lemma sigma_unk_downcast_sp u :
      sp u -> let x := su_downcast u in sigma_ppo RB x x.
    Proof.
      elim: u=> [_|_|???|p1 ???[??]].
      1-3: solve [apply: smallest_refl | apply: greatest_refl].
      assert (h1 : RA (downcast Aunk p1) (downcast Aunk p1)) by
        (apply: downmon; apply: sp_prec_unk=> //).
      exists h1; intro_iprec; unshelve apply: idownmon.
      1:by apply: sp_prec_unk.
      apply: prec_iprec; apply: sp_prec_unk=> //.
    Qed.

    Lemma sigma_unk_downcast_is_unk_err u :
      is_unk_err u -> sigma_ppo RB (su_downcast u) err.
    Proof.
      move: u=> [_|[]|???|??/=[h1 h2]].
      1,2: apply: smallest_refl.
      unshelve apply: intro_prec_sigma.
      - apply: (trans _ (downcast_err Aunk)).
        apply: downmon; by apply: is_unk_err_prec_unk.
      - unshelve apply: iprec_trans'.
        3: apply: (idowncast_err Bunk); [|apply: eprel_down_left
                                        |apply: eprel_smallest] ; apply: smallest_refl.
        unshelve apply: idownmon0; last unshelve apply: prec_iprec.
        4: apply: sp_prec_unk; by apply: is_unk_err_sp.
        1,4: by apply: is_unk_err_prec_unk.
        1,2: apply: eprel_down_left; apply: sp_prec_unk; by apply: is_unk_err_sp.
    Qed.


    Lemma sigma_unk_downcast_monotone : monotone unk_ppo (sigma_ppo RB) su_downcast.
    Proof.
      move=> ?? [??|????|????|??????].
      - apply: greater; apply: sigma_unk_downcast_sp; by apply: dyn_bounded_sp.
      - apply: (trans (sigma_unk_downcast_is_unk_err _ _))=> //.
        apply: smaller; apply: sigma_unk_downcast_sp=> //.
      - move=> *; apply: smallest_refl.
      - (unshelve apply: intro_prec_sigma); cbn; last by apply: downmon.
        apply: idownmon; unshelve apply: prec_iprec; last eassumption.
        apply: lower_refl; eassumption.
    Qed.


    Lemma sigma_unk_upcast_err (p : sigma B) :
      sigma_ppo RB p err -> unk_ppo (su_upcast p) err.
    Proof.
      move=> [h1 h2]; apply: pu_any_err=> //.
      assert (unk_ppo err err) as err_err by apply: smallest_refl.
      assert (part1 : unk_ppo (upcast Aunk (d1 p)) err).
      1: { apply: downtoup => //.
           apply: (trans h1); apply: smaller; by apply: downmon. }
      split; apply: prec_unk_err_is_unk_err=> //.
      set (x := iupcast _ _ _).
      assert (iprec (ippo_from_ppo unk_ppo unk_ppo) part1 x err) as ?%eprel_upcast=> //.
      rewrite /x; unshelve apply: idowntoup0.
      6: { unshelve apply: iprec_right; first eassumption.
           apply: smaller; unshelve apply: idownmon'; first unshelve apply: eprel_smallest.
           all: apply: smallest_refl. }
      1: assumption.
      1: apply: eprel_up_right; apply: lower_refl; eassumption.
      1,2: apply: eprel_smallest; apply: smallest_refl.
    Qed.

    Context `{!forall h (hh : RH h h), GreatestElem (ippo_at@{v} RElH hh)}.

    Lemma sigma_unk_is_eppair : eppair_prop (sigma_ppo RB) unk_ppo
                                            su_upcast su_downcast.
    Proof.
      constructor.
      - apply: sigma_unk_upcast_monotone.
      - unshelve apply: sigma_unk_downcast_monotone.
      - move=> a b aa; move: (aa)=> [aa1 /toiprec aa2] /pu_sigma_l_inv [].
        + move=> ?; by apply: greater.
        + move=> u []/is_unk_err_prec_unk h1 /is_unk_err_prec_unk h2 spu.
          move: (spu)=> /sigma_unk_downcast_sp [? /toiprec ?].
          assert (e1 : eprel Aunk (d1 a) err) by by apply: to_eprel.
          pose proof (e1' := eprel_up_right Aunk _ _ aa1).
          assert (e2 : heprel Bunk e1' (d2 a) err) by
            (apply: to_eprel=> //; apply: eprel_lrefl; eassumption).
          assert (part1 : RA (d1 a) (d1 (su_downcast u))).
          1:{
            apply: (trans (eprel_downcast e1)).
            apply:trans; first apply: downcast_err; by apply: smaller.
          }
          exists part1.
          unshelve apply: (iprec_trans _ (heprel_dr aa1 e1' e2)).
          1: {
            opacify.
            apply: (trans (eprel_downcast e1)).
            apply: trans; first apply: downcast_err.
            by apply: smaller.
          }
          1: assumption.
          apply: iprec_trans.
          apply: idowncast_err.
          4: apply: eprel_smallest.
          Unshelve. all: try eassumption.
          typeclasses eauto.
          apply: eprel_lrefl; eassumption.
        + move=> p1' p2' h1 h2.
          assert (e1 : RA (d1 a) (downcast Aunk p1')) by apply: uptodown@{u}=> //.
          exists e1; apply: iuptodown0.
          apply: eprel_up_right; exact aa1.
          apply: eprel_down_left. apply: upper_refl; eassumption.
          eassumption.
          apply: prec_iprec. refine h2.
          (* Where do these obligations come from... *)
          Unshelve. 1,2: assumption. by apply: upmon.
      - move=> a [||??|p1 p2] h ab; move: (ab)=> [h1 h2].
        + by apply: sigma_unk_upcast_err.
        + apply: greater. apply: sigma_unk_upcast_monotone ; apply: lower_refl; eassumption.
        + apply: trans; first by apply: sigma_unk_upcast_err.
          by apply: smaller.
        + move: (prec_unk_sp_l h)=> [/sp_prec_unk sp1 /sp_prec_unk sp2].
          apply: pu_sigmaUnkC.
          refine (downtoup Aunk _ _ _ h1)=> //.
          unshelve refine (eprel_upcast (idowntoup0 Bunk _ _ _ _ h2)).
          * refine (downtoup Aunk _ _ _ h1)=> //.
          * apply: eprel_up_right; apply: lower_refl; eassumption.
          * by apply: eprel_down_left.
          * assumption.
          * by apply: prec_iprec.
      - move=> a [? /toiprec ?]; unshelve apply: intro_prec_sigma.
        by apply: retract.
        by apply: iretract.
    Qed.

    Definition sigma_unk_eppair : eppair (sigma_ppo RB) unk_ppo :=
      pack_eppair sigma_unk_is_eppair.

  End SigmaEppair.

End Unknown.
