From Coq Require Import ssreflect ssrfun.
From Equations Require Import Equations.

From PartialPreorder Require Import strictPropLogic.
From PartialPreorder Require Import prelude.

Set Implicit Arguments.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Primitive Projections.
Import SPropNotations.

Set Printing Universes.


(** Partial preorders *)

(** A partial preorder (ppo) on a type A is a
    transitive, quasi reflexive relation *)
(** We choose to work with *definitionally proof-irrelevant*
    relations (SProp-valued) *)

(* We need to be careful to separate structure from properties.
   Concrete instances of ppos will be defined in a stratified fashion:
   1) the underlying type (and its structure, e.g. errors/unknown/casts)
   2) the precision relations
   3) the properties of these relations (ppo)
 *)

Notation srel A := (A -> A -> SProp).

Notation transitive R :=
  (forall x y z, R x y -> R y z -> R x z).
Notation upper_reflexive R :=
  (forall x y, R x y -> R y y).
Notation lower_reflexive R :=
  (forall x y, R x y -> R x x).
Notation quasi_reflexive R :=
  (forall x y, R x y -> R x x s/\ R y y).


Record is_ppo@{u} (A : Type@{u}) (RA : srel A) : SProp :=
  mkIsPpo {
      is_ppo_trans : transitive RA ;
      is_ppo_upper_refl : upper_reflexive RA ;
      is_ppo_lower_refl : lower_reflexive RA ;
    }.

Record ppo@{u} (A : Type@{u}) : Type@{u} :=
  mkPpo {
      ppo_rel :> srel A ;
      ppo_rel_is_ppo :> is_ppo@{u} ppo_rel
    }.

Section EqPpo.

  (* Equality is an equivalence relation,
     hence a partial preorder whenever it is
     equivalent to its SProp reflection.

     We use the particular case of types equipped with
     a decidable equality (EqDec A) *)

  Universe u.
  Context (A : Type@{u}) `{EqDec A}.

  Definition prec_eq : srel A := @sEq A.
  Lemma prec_eq_is_ppo : is_ppo prec_eq.
  Proof.
    constructor.
    move=> ??? ; apply: sEq_trans.
    all: move=> ???; apply: sEq_refl.
  Qed.

  Definition ppo_from_eq : ppo@{u} A := mkPpo prec_eq_is_ppo.

  Lemma prec_eqP `{EqDec A} (a0 a1 : A) : ppo_from_eq a0 a1 -> a0 = a1.
  Proof.
    case: (eq_dec a0 a1)=> // neq seq; sexfalso.
    move: seq. apply: sEq_to_eq_elim=> ?; contradiction.
  Qed.

End EqPpo.


(** Accessors, notations and tactics for ppos *)

Definition trans@{u} [A : Type@{u}] [RA : ppo@{u} A]  [x y z : A] xy yz : RA x z :=
  is_ppo_trans RA x y z xy yz.

Definition upper_refl@{u} [A : Type@{u}] [RA : ppo@{u} A]  [x y : A] xy : RA y y:=
  is_ppo_upper_refl RA x y xy.

Definition lower_refl@{u} [A : Type@{u}] [RA : ppo@{u} A]  [x y : A] xy : RA x x :=
  is_ppo_lower_refl RA x y xy.


Notation "⌈ w ⌉" := (upper_refl w).
Notation "⌊ w ⌋" := (lower_refl w).

Lemma with_refl_goal_sprop@{u} [A] (pA : ppo@{u} A) x y (G : pA x y -> SProp) :
  (forall (xx : pA x x) (yy : pA y y) (xy : pA x y), G xy) -> forall (xy : pA x y), G xy.
Proof. move=> H xy; apply: H; [apply: lower_refl| apply: upper_refl]; eassumption. Qed.

(* ssr intro pattern to strenghen hypothesis with reflexivities
eg turns a goal of the form forall (xy : pA x y), G with pA : ppo A
into forall (xx : pA x x) (yy : pA y y) (xy : pA x y), G
so can be used as move=> /wrefl xx yy xy.
 *)
Notation "'wrefl'" := ltac:(apply: with_refl_goal_sprop) (only parsing) : ssripat_scope.


(* Mostly unused *)
Record Ppo@{u} :=
  pack_ppo { carrier_ :> Type@{u} ; ppo_ :> ppo@{u} carrier_ }.



(** Partial equivalence relation (per) induced by a ppo *)

Definition sym@{u} (A : Type@{u}) (RA : srel A) : srel A :=
  fun x y => RA x y s/\ RA y x.

Notation "x ≈[ RA ] y" := (sym RA x y) (at level 70).

Section Sym.
  Universe u.
  Context (A : Type@{u}) (RA : ppo@{u} A).
  Lemma sym_refl x : RA x x -> x ≈[RA] x. Proof. by split. Qed.
  Lemma sym_trans : transitive (sym RA).
  Proof. move=> ??? [??][??]; split; apply: trans ; eassumption. Qed.
  Lemma sym_sym x y : x ≈[RA] y -> y ≈[RA] x.
  Proof. move=> []; by split. Qed.
  Lemma sym_refl' (a a' : A) :
    a = a' -> RA a' a' -> a ≈[RA] a'.
  Proof. move=> ->; apply: sym_refl. Qed.

  Definition sym_is_ppo : is_ppo (sym RA).
  Proof.
    constructor.
    - apply: sym_trans.
    - move=> ?? [??]; apply: sym_refl; apply: upper_refl; eassumption.
    - move=> ?? [??]; apply: sym_refl; apply: upper_refl; eassumption.
  Qed.

  Definition ppo_sym : ppo@{u} A := mkPpo sym_is_ppo.
End Sym.



(** Smallest & Greatest elements *)
(* We mostly consider ppos with specified
   smallest element err and greatest element dyn *)


Class Err@{u} (A : Type@{u}) := err : A.
#[global]
Hint Mode Err + : typeclass_instances.

Class SmallestElem {A} (RA : ppo A) `{Err A} : SProp :=
  mkSmallest {
      (* err is reflexive (self-precise) *)
    smallest_refl : RA err err ;
      (* and it is smaller than any other reflexive element *)
    smaller : forall a' , RA a' a' -> RA err a'
  }.
#[global]
Hint Mode SmallestElem + - + : typeclass_instances.

Definition is_smallest {A} RA `{SmallestElem A RA} : A -> SProp :=
  fun x => RA x err.

Lemma smaller' {A} (RA : ppo A) `{SmallestElem _ RA} (x : A) :
  is_smallest x -> forall y, RA y y -> RA x y.
Proof. move=> h ??; apply (trans h), smaller=> //. Qed.

Lemma smallest_refl' {A} (RA : ppo A) `{SmallestElem _ RA} (x : A) :
  is_smallest x -> RA x x.
Proof. move=> /wrefl//. Qed.

(* Will be used for unknown elements *)
Class Dyn A := dyn : A.
#[global]
Hint Mode Dyn + : typeclass_instances.

Class GreatestElem {A} (RA : ppo A) `{Dyn A} : SProp :=
  mkGreatest {
      (* dyn is reflexive (self-precise) *)
    greatest_refl : RA dyn dyn ;
      (* dyn is greater than any reflexive element *)
    greater : forall a' , RA a' a' -> RA a' dyn
  }.
#[global]
Hint Mode GreatestElem + - + : typeclass_instances.

Definition is_greatest {A} RA `{GreatestElem A RA} : A -> SProp :=
  fun x => RA dyn x.

Lemma greater' {A} (RA : ppo A) `{GreatestElem _ RA} (x : A) :
  is_greatest x -> forall y, RA y y -> RA y x.
Proof. move=> h ??; apply: trans; first apply greater; done. Qed.

Lemma greatest_refl' {A} (RA : ppo A) `{GreatestElem _ RA} (x : A) :
  is_greatest x -> RA x x.
Proof. move=> /wrefl//. Qed.


(** Meets and joins in a partial preorder *)
(* Meets and joins are not central to this development
   but they are useful to think about beck-checvalley style conditions *)

Section MeetAndJoin.
  Universe u.
  Context (A : Type@{u}).
  Class meetOp := meet : A -> A -> A.
  Notation "x ⊓ y" := (meet x y) (at level 65).
  Class joinOp := join : A -> A -> A.
  Notation "x ⊔ y" := (join x y) (at level 65).

  Context (RA : ppo@{u} A).

  Class isMeet `{meetOp} : SProp :=
    mkIsMeet {
        meet_smaller_l : forall a b, RA a a -> RA b b -> RA (a ⊓ b) a ;
        meet_smaller_r : forall a b, RA a a -> RA b b -> RA (a ⊓ b) b ;
        meet_smallest : forall a b c, RA c a -> RA c b -> RA c (a ⊓ b)
      }.

  Lemma meet_mon `{isMeet}:
    forall a a' b b', RA a a' -> RA b b' -> RA (a ⊓ b) (a' ⊓ b').
  Proof.
    move=> ????/wrefl ? _ ? /wrefl ? _ ?.
    apply: meet_smallest; apply: trans; try eassumption.
    all: by (apply meet_smaller_l + apply meet_smaller_r).
  Qed.

  Lemma meet_refl `{isMeet}: forall a b, RA a a -> RA b b -> RA (a ⊓ b) (a ⊓ b).
  Proof. move=> ??; apply: meet_mon. Qed.

  Class isJoin `{joinOp} : SProp :=
    mkIsJoin {
        join_greater_l : forall a b, RA a a -> RA b b -> RA a (a ⊔ b) ;
        join_greater_r : forall a b, RA a a -> RA b b -> RA b (a ⊔ b) ;
        join_greatest : forall a b c, RA a c -> RA b c -> RA (a ⊔ b) c
      }.

  Lemma join_mon `{isJoin}:
    forall a a' b b', RA a a' -> RA b b' -> RA (a ⊔ b) (a' ⊔ b').
  Proof.
    move=> ????/wrefl _ ? ? /wrefl _ ? ?.
    apply: join_greatest; apply: trans; try eassumption.
    all: by (apply join_greater_l + apply join_greater_r).
  Qed.

  Lemma join_refl `{isJoin}: forall a b, RA a a -> RA b b -> RA (a ⊔ b) (a ⊔ b).
  Proof. move=> ??; apply: join_mon. Qed.

End MeetAndJoin.

Notation "x ⊓ y" := (meet x y) (at level 65).
Notation "x ⊔ y" := (join x y) (at level 65).


Arguments meet_smaller_l [_ _] {_ _} [_ _] _ _.
Arguments meet_smaller_r [_ _] {_ _} [_ _] _ _.
Arguments meet_smallest [_ _] {_ _} [_ _ _] _ _.

Arguments join_greater_l [_ _] {_ _} [_ _] _ _.
Arguments join_greater_r [_ _] {_ _} [_ _] _ _.
Arguments join_greatest [_ _] {_ _} [_ _ _] _ _.



(** Ppo structure on simple (non-dependent) function type *)

(* Some parts of this section would better be located in arr.v
   but we use extensively the definition of monotone functions below
 *)
Section Arr.
  Universe u.

  (* Definition of the structure *)
  Section Bare.
    Context (A B : Type@{u}).

    #[global]
    Instance arr_err@{v | u <= v} (A B : Type@{u}) `{Err B} :
      Err@{v} (A -> B) := fun _ => err.

    #[global]
    Instance arr_dyn@{v | u <= v} `{Dyn B} : Dyn@{v} (A -> B) := fun _ => dyn.

    Context `{meetOp@{u} B}.
    #[global]
    Instance arr_meet : meetOp (A -> B) :=
      fun f g a => f a ⊓ g a.

    Context `{joinOp@{u} B}.

    #[global]
    Instance arr_join : joinOp (A -> B) :=
      fun f g a => f a ⊔ g a.

  End Bare.


  Section RelDef.
    Context (A B : Type@{u}) (RA : srel A) (RB : srel B).

    Definition arr_rel (RA : srel A) (RB : srel B) : srel (A -> B) :=
      fun f g => forall a a', RA a a' -> RB (f a) (g a').

    (* monotone functions ~ reflexive elements of arr_rel ~ self-precise functions *)
    Definition monotone (f : A -> B) := arr_rel RA RB f f.

    Lemma sym_mon f (mf : monotone f)
      : forall a a', a ≈[RA] a' -> f a ≈[RB] f a'.
    Proof. move=> ??[??]; split; apply: mf=> //. Qed.

    (** In order to ensure quasi-reflexivity of the relation on functions,
        we need to bake in monotonicity of the endpoints *)
    Definition arr_ppo_rel : srel (A -> B) :=
      fun f g => monotone f s/\ monotone g s/\ arr_rel RA RB f g.

  End RelDef.

  (* Properties of the relation *)
  Section Def.
    Context (A B : Type@{u}) (RA : ppo@{u} A) (RB : ppo@{u} B).

    Let arr_ppo_rel := (arr_ppo_rel RA RB).
    Let monotone := (monotone RA RB).

    Lemma arr_ppo_rel_trans : transitive arr_ppo_rel.
    Proof.
      move=> x y z [[??]xy] [[??] yz]; split=> // a a' /wrefl _ ? aa'.
      apply: trans; first apply: (xy _ _ aa'); by apply: yz.
    Qed.

    Lemma arr_ppo_rel_urefl : upper_reflexive arr_ppo_rel.
    Proof. move=> _ y [[_ yy]_] //. Qed.
    Lemma arr_ppo_rel_lrefl : lower_reflexive arr_ppo_rel.
    Proof. move=> x _ [[xx _] _] //. Qed.

    Definition arr_ppo_rel_is_ppo : is_ppo arr_ppo_rel :=
      mkIsPpo arr_ppo_rel arr_ppo_rel_trans arr_ppo_rel_urefl arr_ppo_rel_lrefl.

    Definition arr_ppo : ppo (A -> B) := mkPpo arr_ppo_rel_is_ppo.

    #[global,program]
    Instance arr_smallest@{v | u <= v} `{SmallestElem _ RB} : SmallestElem@{v} arr_ppo.
    Next Obligation.
      opacify; repeat split; move=> ???; apply: smallest_refl.
    Qed.
    Next Obligation.
      opacify; move=> ? ? f [[mf _]_]; repeat split=> //; move=> ?? /wrefl ???.
      1: apply: smallest_refl.
      apply smaller, mf=> //.
    Qed.

    #[global,program]
    Instance arr_greatest@{v | u <= v} `{GreatestElem _ RB} : GreatestElem@{v} arr_ppo.
    Next Obligation.
      opacify; repeat split; move=> ???; apply: greatest_refl.
    Qed.
    Next Obligation.
      opacify; move=> ? ? f [[mf _]_]; repeat split=> //; move=> ??/wrefl???.
      1: apply: greatest_refl.
      by apply greater, mf.
    Qed.


    Context `{meetOp@{u} B} `{!isMeet@{u} RB}.
    Lemma arr_meet_mon  (f g : A -> B) :
      monotone f -> monotone g -> monotone (f ⊓ g).
    Proof.
      move=> mf mg ?? aa' /=; apply: meet_mon; by (apply mf + apply mg).
    Qed.

    #[global]
    Instance arr_has_meet : isMeet arr_ppo.
    Proof.
      unshelve econstructor.
      - move=> f g [_ mf] [_ mg]; split; first (split=> //; by apply arr_meet_mon).
        move=> ?? /wrefl ? _ aa'.
        apply: trans _ (mf _ _ aa'); apply: meet_smaller_l.
        all: by (apply mf + apply mg).
      - move=> f g [_ mf] [_ mg]; split; first (split=> //; by apply arr_meet_mon).
        move=> ?? /wrefl ? _ aa'.
        apply: trans _ (mg _ _ aa'); apply: meet_smaller_r.
        all: by (apply mf + apply mg).
      - move=> f g h [[mh mf] hf] [[_ mg] hg].
        split; first (split=> //; by apply arr_meet_mon).
        move=> ?? /wrefl _ ? aa'; refine (trans (mh _ _ aa') _).
        apply: meet_smallest; by [apply: hf|apply: hg].
    Qed.

    Context `{joinOp@{u} B} `{!isJoin@{u} RB}.

    Lemma arr_join_mon (f g : A -> B) :
      monotone f -> monotone g -> monotone (f ⊔ g).
    Proof.
      move=> mf mg ?? aa' /=; apply: join_mon; by (apply mf + apply mg).
    Qed.

    #[global]
    Instance arr_has_join : isJoin arr_ppo.
    Proof.
      unshelve econstructor.
      - move=> f g [_ mf] [_ mg]; split; first (split=> //; by apply arr_join_mon).
        move=> ?? /wrefl _ ? aa'.
        refine (trans (mf _ _ aa') _); apply: join_greater_l.
        all: by (apply mf + apply mg).
      - move=> f g [_ mf] [_ mg]; split; first (split=> //; by apply arr_join_mon).
        move=> ?? /wrefl _ ? aa'.
        refine (trans (mg _ _ aa') _); apply: join_greater_r.
        all: by (apply mf + apply mg).
      - move=> f g h [[mf mh] fh] [[mg _] gh].
        split; first (split=> //; by apply arr_join_mon).
        move=> ?? /wrefl ? _ aa'; refine (trans _ (mh _ _ aa')).
        apply: join_greatest; by [apply: fh|apply: gh].
    Qed.



    (** Lemma on preservation of meet and join by monotone functions *)

    Lemma monotone_meet `{meetOp@{u} A} `{!isMeet@{u} RA} f :
      monotone f -> forall a b, RA a a -> RA b b -> RB (f (a ⊓ b)) (f a ⊓ f b).
    Proof.
      move=> mf ????; apply: meet_smallest; apply:mf; by [apply: meet_smaller_l| apply: meet_smaller_r].
    Qed.

    Lemma monotone_join `{joinOp@{u} A} `{!isJoin@{u} RA}  f :
      monotone f -> forall a b, RA a a -> RA b b -> RB (f a ⊔ f b) (f (a ⊔ b)).
    Proof.
      move=> mf ????; apply: join_greatest; apply:mf; by [apply: join_greater_l| apply: join_greater_r].
    Qed.

  End Def.

  Definition Arr@{v} (A B : Ppo@{u}) : Ppo@{v} := pack_ppo (arr_ppo A B).


  (** Categorical structure *)
  (* hthe composition compmon is associative and unital
     but we only use it implicitly *)

  (* These 2 lemmas would make sense with srel instead of ppo,
     but that might create troubles down the road *)
  Lemma idmon {A : Type@{u}} (RA : ppo@{u} A) : monotone RA RA (@id A).
  Proof. by []. Qed.

  Lemma compmon (A B C : Type@{u}) (RA : ppo@{u} A) (RB : ppo@{u} B) (RC : ppo@{u} C)
        f g (mf : monotone RA RB f) (mg : monotone RB RC g) :
    monotone RA RC (g \o f).
  Proof. move=> ???; apply: mg; apply: mf=>//. Qed.


  (** Equivalences of functions *)
  (* This will be used pervasively to state equiprecision relations
     as a replacement for function extensionality *)
  Section Equiv.
    Context (A B : Type@{u}) (RA : ppo@{u} A) (RB : ppo@{u} B).

    Definition equiv : srel (A -> B) := sym (arr_ppo RA RB).

    Lemma equiv_helper f g (mf : monotone RA RB f) (mg : monotone RA RB g)
          (fg : forall a, RA a a -> sym RB (f a) (g a)) :
      equiv f g.
    Proof.
      repeat split=> //; move=> a a' aa'.
      all: move: {fg}(fg _ (lower_refl aa')) => [fg gf]; apply: trans.
      - apply fg.
      - apply mg=> //.
      - apply gf.
      - apply mf=> //.
    Qed.

    Lemma equiv_prec_l f g a b (aa : RA a a) : equiv f g -> RB (f a) b -> RB (g a) b.
    Proof. move=> [_ [_ /(_ a a aa)]]; apply trans. Qed.

    Lemma equiv_prec_r f g a b (aa : RA a a) : equiv f g -> RB b (f a) -> RB b (g a).
    Proof. move=> [[_ /(_ a a aa)] ? _] ?; apply: trans; eassumption. Qed.

    Lemma equiv_app0 f g a a' :
      equiv f g -> RA a a' -> RB (f a) (g a').
    Proof.
      move=> [[_ fg] _] aa'; by apply: fg.
    Qed.

    Lemma equiv_app f g a a' :
      equiv f g -> sym RA a a' -> sym RB (f a) (g a').
    Proof.
      move=> [[_ fg] [_ gf]] [aa' a'a]; split; by [apply: fg| apply: gf].
    Qed.
  End Equiv.
End Arr.


(** Heterogeneous relation between two types accross a pair of cast *)
Section HeterogeneousRelation.
  Universe u.
  Context (A B : Type@{u}) (RA : srel A) (RB : srel B)
          (upcast : A -> B) (downcast : B -> A).

  (* We take a conservative approach with all necessary relations baked in.
     Later lemmas on concrete instances will show that some of these components
     are redundant (e.g. when upcast ⊣ downcast are adjoint) *)
  Definition hrel : A -> B -> SProp :=
    fun a b => RA a a s/\ RB (upcast a) b s/\ RA a (downcast b) s/\ RB b b.

  Notation "'hrel_split'" := ltac:(move=> [[[??]?]?]) (only parsing) : ssripat_scope.

  Lemma hrel_lrefl a b : hrel a b -> RA a a.
  Proof. by move=> [][][]. Qed.

  Lemma hrel_urefl a b : hrel a b -> RB b b.
  Proof. by move=>[][][]. Qed.

  Lemma hrel_upcast a b : hrel a b -> RB (upcast a) b.
  Proof. by move=>[][][]. Qed.

  Lemma hrel_downcast a b : hrel a b -> RA a (downcast b).
  Proof. by move=>[][][]. Qed.
End HeterogeneousRelation.

(** Embedding-Projection pairs *)
Section eppair.
  Universe u.

  Section Def.
    Context (A B : Type@{u}) (RA : ppo@{u} A) (RB : ppo@{u} B).

    Record eppair_prop (upcast : A -> B) (downcast : B -> A) : SProp :=
      mkEppairProp {
          (* Monotony properties *)
          upcastmon : monotone@{u} RA RB upcast ;
          downcastmon : monotone@{u} RB RA downcast ;

          (* Adjunction properties *)
          upcasttodowncast : forall a b, RA a a -> RB (upcast a) b -> RA a (downcast b) ;
          downcasttoupcast : forall a b, RB b b -> RA a (downcast b) -> RB (upcast a) b ;

          (* Retraction property *)
          downcast_upcast_retract : forall a, RA a a -> RA (downcast (upcast a)) a
        }.

    Record eppair :=
      pack_eppair {
      upcast : A -> B ;
      downcast : B -> A ;
      is_eppair :> eppair_prop upcast downcast
      }.

    Definition mkEppair f g monf mong u2d d2u r :=
         pack_eppair (@mkEppairProp f g monf mong u2d d2u r).

    Definition upmon (e : eppair) := upcastmon e.
    Definition downmon (e : eppair) := downcastmon e.
    Definition uptodown (e : eppair) := upcasttodowncast e.
    Definition downtoup (e : eppair) := downcasttoupcast e.
    Definition retract (e : eppair) := downcast_upcast_retract e.

    Lemma unit (ep : eppair) a : RA a a -> RA a (downcast ep (upcast ep a)).
    Proof. move=> ?; apply: uptodown=> //; by apply: upmon. Qed.

    Lemma counit (ep : eppair) b : RB b b -> RB (upcast ep (downcast ep b)) b.
    Proof. move=> ?; apply: downtoup=> //; by apply: downmon. Qed.


    (** Heterogeneous relations on top of an eppair satisfy useful properties *)
    Definition eprel (ep : eppair) (a : A) (b : B) : SProp :=
      hrel RA RB (upcast ep) (downcast ep) a b.

    Lemma eprel_lrefl (ep : eppair) a b : eprel ep a b -> RA a a.
    Proof. apply: hrel_lrefl. Qed.

    Lemma eprel_urefl (ep : eppair) a b : eprel ep a b -> RB b b.
    Proof. apply: hrel_urefl. Qed.

    Notation "⌞ w ⌟" := (eprel_lrefl w).
    Notation "⌜ w ⌝" := (eprel_urefl w).

    Lemma eprel_upcast (ep : eppair) a b : eprel ep a b -> RB (upcast ep a) b.
    Proof. apply: hrel_upcast. Qed.

    Lemma eprel_downcast (ep : eppair) a b : eprel ep a b -> RA a (downcast ep b).
    Proof. apply: hrel_downcast. Qed.

    Lemma to_eprel (ep : eppair) a b : RA a a -> RB (upcast ep a) b -> eprel ep a b.
    Proof.
      move=> aa ab; repeat split; move=> //.
      by apply: uptodown.
      apply: upper_refl; eassumption.
    Qed.

    Lemma to_eprel' (ep : eppair) a b : RA a (downcast ep b) -> RB b b -> eprel ep a b.
    Proof.
      move=> ab bb; repeat split; move=> //.
      apply: lower_refl; eassumption.
      by apply: downtoup.
    Qed.

    Lemma eprel_left (ep : eppair) a a' b : RA a a' -> eprel ep a' b -> eprel ep a b.
    Proof.
      move=> /wrefl ? _ ? a'b; apply: to_eprel=> //.
      apply: (trans _ (eprel_upcast a'b)); by apply: upmon.
    Qed.

    Lemma eprel_right (ep : eppair) a b b' : eprel ep a b -> RB b b' -> eprel ep a b'.
    Proof.
      move=> ab bb'; apply: to_eprel; first exact (eprel_lrefl ab).
      exact (trans (eprel_upcast ab) bb').
    Qed.

    Lemma eprel_up_right (ep : eppair) a a' : RA a a' -> eprel ep a (upcast ep a').
    Proof. move=> ?; apply: to_eprel; [apply: lower_refl| apply: upmon]; eassumption. Qed.

    Lemma eprel_down_left (ep : eppair) b b' : RB b b' -> eprel ep (downcast ep b) b'.
    Proof. move=> ?; apply: to_eprel'; [apply: downmon|apply: upper_refl]; eassumption. Qed.

    Lemma downcast_meet
           `{meetOp@{u} A} `{meetOp@{u} B} `{!isMeet@{u} RA} `{!isMeet@{u} RB}
           (ep : eppair) b b' :
      RB b b -> RB b' b' ->
      RA (downcast ep b ⊓ downcast ep b') (downcast ep (b ⊓ b')).
    Proof.
      move=> bb bb'; apply: uptodown.
      1: apply: meet_refl; by apply: downmon.
      apply: trans; last (apply: meet_mon; by apply: (counit ep)).
      apply: monotone_meet; first apply: upmon.
      all: by apply downmon.
    Qed.

    Lemma upcast_join
           `{joinOp@{u} A} `{joinOp@{u} B} `{!isJoin@{u} RA} `{!isJoin@{u} RB}
           (ep : eppair) a a' :
      RA a a -> RA a' a' ->
      RB (upcast ep (a ⊔ a')) (upcast ep a ⊔ upcast ep a').
    Proof.
      move=> ??; apply: downtoup.
      1: apply: join_refl; by apply: upmon.
      apply: trans; first (apply: join_mon; by apply: (unit ep)).
      apply: monotone_join; first apply: downmon.
      all: by apply upmon.
    Qed.

  End Def.

  Notation "⌞ w ⌟" := (eprel_lrefl w).
  Notation "⌜ w ⌝" := (eprel_urefl w).

  (** Partial preorders equipped with eppair form a category
      that can be moreover enriched over setoids using eppair equivalences *)

  Section Identity.
    Context (A : Type@{u}) (RA : ppo@{u} A).
    #[global,program]
    Definition ideppair : eppair RA RA :=
      mkEppair RA RA (idmon RA) (idmon RA) _ _ _.

    Lemma eprel_ideppair x y : eprel ideppair x y -> RA x y.
    Proof. apply: eprel_upcast. Qed.

    Lemma eprel_ideppair_inv x y : RA x y -> eprel ideppair x y.
    Proof. move=> /wrefl ? _ ?; by apply: to_eprel. Qed.

  End Identity.

  Section Composition.
    Context (A B C : Type@{u}) (RA : ppo@{u} A) (RB : ppo@{u} B) (RC : ppo@{u} C).

    Lemma compeppair_prop (uf : A -> B) (df : B -> A) (ug : B -> C) (dg : C -> B) :
      eppair_prop RA RB uf df ->
      eppair_prop RB RC ug dg ->
      eppair_prop RA RC (ug \o uf) (df \o dg).
    Proof.
      move=> Hf Hg ; constructor.
      1,2: apply: compmon; (apply: upcastmon + apply: downcastmon); eassumption.
      - move=> * /=. do 2 (apply: upcasttodowncast; try eassumption).
        apply: upcastmon; eassumption.
      - move=> * /=. do 2 (apply: downcasttoupcast; try eassumption).
        apply: downcastmon; eassumption.
      - move=> f * /=; apply: trans;
        last (apply: downcast_upcast_retract; eassumption).
        apply: downcastmon; first eassumption.
        apply: (downcast_upcast_retract Hg (uf f)).
        apply: upcastmon; eassumption.
    Qed.


    #[global,program]
    Definition compeppair (f : eppair RA RB) (g : eppair RB RC) :=
      pack_eppair (compeppair_prop f g).

    Lemma eprel_trans (f : eppair RA RB) (g : eppair RB RC) x y z :
      eprel f x y -> eprel g y z -> eprel (compeppair f g) x z.
    Proof.
      move=> xy yz; apply: to_eprel; first exact (eprel_lrefl xy).
      cbn; apply: (trans _ (eprel_upcast yz)).
      apply: upmon; by apply: eprel_upcast.
    Qed.

  End Composition.


  (* Equivalences of eppairs, builds on top of equiv *)
  Section Equiv.
    Context (A B : Type@{u}) (RA : ppo@{u} A) (RB : ppo@{u} B).

    Definition equiveppair (f g : eppair RA RB) :=
      equiv RA RB (upcast f) (upcast g) s/\
      equiv RB RA (downcast f) (downcast g).

    Lemma equiveppair_trans : transitive equiveppair.
    Proof.
      move=>??? [? ?] [? ?]; split; apply: sym_trans; eassumption.
    Qed.

    Lemma equiveppair_refl : forall f, equiveppair f f.
    Proof.
      move=> f; split; apply: sym_refl.
      - repeat split; apply: upmon.
      - repeat split; apply: downmon.
    Qed.

    Lemma retract_eqv (f : eppair RA RB) (a : A) :
      RA a a -> a ≈[RA] downcast f (upcast f a).
    Proof. move=> aa; split; by [apply: unit|apply: retract]. Qed.

    Lemma snake_up (f : eppair RA RB) (a : A) :
      RA a a -> upcast f a ≈[RB] upcast f (downcast f (upcast f a)).
    Proof.
      move=> aa; apply: equiv_app; last by apply: retract_eqv.
      repeat split; apply: upmon.
    Qed.

    Lemma snake_down (f : eppair RA RB) (b : B) :
      RB b b -> downcast f b ≈[RA] downcast f (upcast f (downcast f b)).
    Proof.
      move=> bb; apply: retract_eqv; by apply: downmon.
    Qed.

    Lemma upequiv_downequiv (f g : eppair RA RB)
          (upequiv : forall a : A, RA a a -> upcast f a ≈[RB] upcast g a) :
      forall a : B, RB a a -> downcast f a ≈[RA] downcast g a.
    Proof.
      move=> b bb; move: (downmon f _ _ bb)(downmon g _ _ bb)=> fbb gbb.
      split; apply: uptodown=> //.
      - refine (trans _ (counit f _ bb)); by case: (upequiv _ fbb).
      - refine (trans _ (counit g _ bb)); by case: (upequiv _ gbb).
    Qed.

    Lemma downequiv_upequiv (f g : eppair RA RB)
          (downequiv: forall a : B, RB a a -> downcast f a ≈[RA] downcast g a) :
      forall a : A, RA a a -> upcast f a ≈[RB] upcast g a.
    Proof.
      move=> a aa; move: (upmon f _ _ aa)(upmon g _ _ aa)=> faa gaa.
      split; apply: downtoup=> //.
      - apply: (trans (unit g _ aa)); by case: (downequiv _ gaa).
      - apply: (trans (unit f _ aa)); by case: (downequiv _ faa).
    Qed.

    Lemma equiveppair_helper (f g : eppair RA RB)
          (upequiv : forall a : A, RA a a -> upcast f a ≈[RB] upcast g a)
          (downequiv : forall a : B, RB a a -> downcast f a ≈[RA] downcast g a)
      : equiveppair f g.
    Proof.
      split; apply: equiv_helper => // ; apply upmon + apply downmon.
    Qed.

    Lemma equiveppair_helper_up (f g : eppair RA RB)
          (upequiv : forall a : A, RA a a -> upcast f a ≈[RB] upcast g a)
      : equiveppair f g.
    Proof.
      apply: equiveppair_helper=> //; by apply: upequiv_downequiv.
    Qed.

    Lemma equiveppair_helper_down (f g : eppair RA RB)
          (downequiv : forall a : B, RB a a -> downcast f a ≈[RA] downcast g a)
      : equiveppair f g.
    Proof.
      apply: equiveppair_helper=> //; by apply: downequiv_upequiv.
    Qed.

    Lemma equiveppair_sym : forall f g, equiveppair f g -> equiveppair g f.
    Proof. move=> f g [? ?]; split; by apply: sym_sym. Qed.

    Lemma equiv_upcast_l f g a b (aa : RA a a) :
      equiveppair f g -> RB (upcast f a) b -> RB (upcast g a) b.
    Proof. move=> [+ _]; apply equiv_prec_l=> //. Qed.

    Lemma eprel_equiveppair f g a b :
      equiveppair f g -> eprel f a b -> eprel g a b.
    Proof.
      move=> fg fab; pose proof (eprel_lrefl fab); apply: to_eprel=> //.
      apply: (equiv_upcast_l _ _ _ fg)=> //.
      by apply: eprel_upcast.
    Qed.

    Lemma eprel_equiveppair' f g a b :
      equiveppair f g -> eprel g a b -> eprel f a b.
    Proof. move/equiveppair_sym; apply: eprel_equiveppair. Qed.

    Lemma equiv_upcast_r f g a b (aa : RA a a) :
      equiveppair f g -> RB b (upcast f a) -> RB b (upcast g a).
    Proof. move=> [+ _]; apply equiv_prec_r=> //. Qed.

    Lemma equiv_upcast_app0 f g (fg : equiveppair f g) a a' :
      RA a a' -> RB (upcast f a) (upcast g a').
    Proof. apply: equiv_app0 (sprl fg). Qed.

    Lemma equiv_upcast_app f g (fg : equiveppair f g) a a' :
      sym RA a a' -> sym RB (upcast f a) (upcast g a').
    Proof. apply: equiv_app (sprl fg). Qed.

    Lemma equiv_downcast_l f g b a (bb : RB b b) :
      equiveppair f g -> RA (downcast f b) a -> RA (downcast g b) a.
    Proof. move=> [_]; apply equiv_prec_l=> //. Qed.

    Lemma equiv_downcast_r f g b a (bb : RB b b) :
      equiveppair f g -> RA a (downcast f b) -> RA a (downcast g b).
    Proof. move=> [_]; apply equiv_prec_r=> //. Qed.

    Lemma equiv_downcast_app0 f g (fg : equiveppair f g) b b' :
      RB b b' -> RA (downcast f b) (downcast g b').
    Proof. apply: equiv_app0 (sprr fg). Qed.

    Lemma equiv_downcast_app f g (fg : equiveppair f g) b b' :
      sym RB b b' -> sym RA (downcast f b) (downcast g b').
    Proof. apply: equiv_app (sprr fg). Qed.

  End Equiv.

  Section EquivComp.
    Context (A B C : Type@{u}) (RA : ppo@{u} A) (RB : ppo@{u} B) (RC : ppo@{u} C).

    Lemma equiveppair_comp_right (f g : eppair RA RB) (h : eppair RB RC) :
      equiveppair f g -> equiveppair (compeppair f h) (compeppair g h).
    Proof.
      move=> fg; move: (equiveppair_sym fg)=> gf.
      apply: equiveppair_helper_up=> a aa; split=> /=.
      all: apply: upmon; apply: equiv_upcast_l=> //; first eassumption; apply: upmon=> //.
    Qed.

    Lemma equiveppair_comp_left (h : eppair RA RB) (f g : eppair RB RC) :
      equiveppair f g -> equiveppair (compeppair h f) (compeppair h g).
    Proof.
      move=> fg; move: (equiveppair_sym fg)=> gf.
      apply: equiveppair_helper_up=> a aa; split=> /=.
      all: apply: equiv_upcast_l; [by apply: upmon|eassumption| by do 2 apply: upmon].
    Qed.

  End EquivComp.

  Lemma equiveppair_comp_assoc (A B C D : Type@{u})
        (RA : ppo@{u} A) (RB : ppo@{u} B) (RC : ppo@{u} C) (RD : ppo@{u} D)
        (f : eppair RA RB) (g : eppair RB RC) (h : eppair RC RD) :
    equiveppair (compeppair f (compeppair g h)) (compeppair (compeppair f g) h).
  Proof. apply: equiveppair_refl. Qed.

  Lemma equiveppair_left_unit (A B : Type@{u}) (RA : ppo@{u} A) (RB : ppo@{u} B)
        (f : eppair RA RB) (h : eppair RA RA) :
    equiveppair h (ideppair RA) -> equiveppair (compeppair h f) f.
  Proof.
    move: (equiveppair_refl f)=> [/= uf df] [/= uh dh].
    apply: equiveppair_helper_up=> x xx /=.
    apply: (equiv_app uf); apply: (equiv_app uh); by apply: sym_refl.
  Qed.

  Lemma equiveppair_right_unit (A B : Type@{u}) (RA : ppo@{u} A) (RB : ppo@{u} B)
        (f : eppair RA RB) (h : eppair RB RB) :
    equiveppair h (ideppair RB) -> equiveppair (compeppair f h) f.
  Proof.
    move: (equiveppair_refl f)=> [/= uf df] [/= uh dh].
    apply: equiveppair_helper_up=> x xx /=.
    apply: (equiv_app uh); apply: (equiv_app uf); by apply: sym_refl.
  Qed.

  Section EprelProperties.

    Context (AX AY : Type@{u}) (RAX : ppo@{u} AX) (RAY : ppo@{u} AY)
            (AXY : eppair RAX RAY) (ax : AX) (ay : AY).

    Lemma eprel_smallest `{Err AX} `{!SmallestElem RAX} : RAY ay ay -> eprel AXY err ay.
    Proof.
      move=> ?; apply: to_eprel'=> //; apply: smaller; by apply: downmon.
    Qed.

    Lemma eprel_greatest `{Dyn AY} `{!GreatestElem RAY} : RAX ax ax -> eprel AXY ax dyn.
    Proof.
      move=> ?; apply: to_eprel=> //; apply: greater; by apply: upmon.
    Qed.

    Lemma downcast_err `{Err AX} `{Err AY} `{!SmallestElem RAX} `{!SmallestElem RAY} : RAX (downcast AXY err) err.
    Proof.
      apply: trans; last (apply: (retract AXY); apply: smallest_refl).
      apply: downmon; apply: smaller; apply: upmon; apply: smallest_refl.
    Qed.

    Lemma eprel_err `{Err AX} `{Err AY} `{!SmallestElem RAX} `{!SmallestElem RAY} : eprel AXY ax err -> RAX ax err.
    Proof.
      move=> axerr; apply: (trans (eprel_downcast axerr)); apply: downcast_err.
    Qed.


    Context (AZ : Type@{u}) (RAZ : ppo@{u} AZ) (AYZ : eppair RAY RAZ) (az : AZ).

    Lemma eprel_down_left'@{}  :
      eprel AYZ ay az -> eprel (compeppair AXY AYZ) (downcast AXY ay) az.
    Proof.
      move=> ayz; apply: to_eprel.
      1: apply: downmon; exact (eprel_lrefl ayz).
      apply: (trans _ (eprel_upcast ayz))=> /=; apply: upmon.
      apply: counit; exact (eprel_lrefl ayz).
    Qed.

    Lemma eprel_down_right@{}  :
      eprel (compeppair AXY AYZ) ax az -> eprel AXY ax (downcast AYZ az).
    Proof.
      move=> axz; apply: to_eprel; first exact (eprel_lrefl axz).
      apply: uptodown; last exact (eprel_upcast axz).
      apply: upmon; exact (eprel_lrefl axz).
    Qed.

    Lemma eprel_up_right'@{}  :
      eprel AXY ax ay -> eprel (compeppair AXY AYZ) ax (upcast AYZ ay).
    Proof.
      move=> axy ; apply: to_eprel=> /= ; first exact (eprel_lrefl axy).
      apply: upmon; exact (eprel_upcast axy).
    Qed.

    Lemma eprel_up_left@{}  :
      eprel (compeppair AXY AYZ) ax az -> eprel AYZ (upcast AXY ax) az.
    Proof.
      move=> axz; apply:to_eprel; last exact (eprel_upcast axz).
      apply: upmon; exact (eprel_lrefl axz).
    Qed.
  End EprelProperties.
End eppair.

Notation "f ≃ g" := (equiveppair f g) (at level 70).

(* lower/upper reflexivity on eprel *)
Notation "⌞ w ⌟" := (eprel_lrefl w).
Notation "⌜ w ⌝" := (eprel_urefl w).



(** Indexed partial preorders (ippo) *)
(* The rest of this file lifts ppos at the level of type families *)

Section FamilyWithCast.
  Universe u v.
  Context (A : Type@{u}).

  (* fwc = Family With Cast *)
  (* Cast structure on a type family *)
  (* Note that the up/down part of the cast is purely conventional at this level *)
  (* This apparent duplication is necessary to have a correspondence
     between ippo (fwc + prop) over a ppo A and indexed eppair over the identity eppair on A *)
  Record fwc :=
    mkFwC {
        family :> A -> Type@{v} ;
        fupcast : forall ax ay, family ax -> family ay ;
        fdowncast : forall ax ay, family ay -> family ax ;
    }.

  (* If the base type A has decidable equality an each fibers
     B a have errors, we can equip B with a cast structure
   *)
  Definition fwc_dom_eq_dec `{EqDec A} (B : A -> Type@{v})
             `{forall a, Err (B a)} :
    fwc :=
    let cast ax ay (v : B ax) :=
      match eq_dec ax ay with
      | left e => transport B e v
      | right _ => err
      end
    in mkFwC B cast (fun ax ay => cast ay ax).


  (* indexed relation *)
  Definition isrel (B : fwc) :=
    (forall a a', B a -> B a' -> SProp).

  (* This definition contains redundancy in the well-behaved case
     where a ⊑ a' and B a ⊑ B a' induce an actual eppair
   *)
  Definition isrel_from_srel (B : fwc) (RB : forall a, srel (B a)) : isrel B :=
    fun a a' => hrel (RB a) (RB a') (fupcast B a a') (fdowncast B a a').

  Notation "'i[' RB ']'" := (isrel_from_srel _ RB).

  Context (B : fwc) (RB : forall a, srel (B a)) (a a' : A) (b : B a) (b' : B a').

  Lemma isrel_lrefl : i[ RB ] a a' b b' -> RB a b b.
  Proof. by move=>[][][]. Qed.

  Lemma isrel_urefl : i[ RB ] a a' b b' -> RB a' b' b'.
  Proof. by move=>[][][]. Qed.

  Lemma isrel_upcast : i[ RB ] a a' b b' -> RB a' (fupcast B a a' b) b'.
  Proof. by move=>[][][]. Qed.

  Lemma isrel_downcast : i[ RB ] a a' b b' -> RB a b (fdowncast B a a' b').
  Proof. by move=>[][][]. Qed.

End FamilyWithCast.

Notation "'i[' RB ']'" := (isrel_from_srel _ RB).

Section Ippo.
  Universe u.

  Context (A : Type@{u}) (RA : ppo@{u} A).

  Record ippo (B : fwc@{u u} A) : Type@{u} :=
    mkIppo {
        (* relation on the fiber at each index *)
        srel_at : forall a, srel (B a) ;

        (* At reflexive indexes, the relation is a partial preorder*)
        is_ppo_at : forall {a}, RA a a -> is_ppo (srel_at a) ;
        ippo_at := fun {a} (aa : RA a a) => mkPpo (is_ppo_at aa) ;

        (* On related indexes, the cast structure forms an eppair *)
        ippois_eppair_on : forall {ax ay} (axy : RA ax ay),
          eppair_prop (ippo_at (lower_refl axy))
                      (ippo_at (upper_refl axy))
                      (fupcast B ax ay)
                      (fdowncast B ax ay) ;
        ippo_on := fun {ax ay} (axy : RA ax ay) => pack_eppair@{u} (ippois_eppair_on axy) ;


        (* These assignments are functorial:
           transitivity maps to composition and relfexivity to identity *)
        ippo_trans : forall {ax ay az} (axy : RA ax ay) (ayz : RA ay az),
          equiveppair (compeppair (ippo_on axy) (ippo_on ayz))
                      (ippo_on (trans axy ayz)) ;
                      (* upper and lower reflexivity coincide *)
        ippo_refl : forall {ax} (axx : RA ax ax),
          equiveppair (ippo_on axx) (ideppair (ippo_at axx))
    }.

  Coercion ippo_at : ippo >-> Funclass.
  Arguments ippo_at : simpl never.


  Context {B : fwc@{u u} A} (RB : ippo B).

  Definition iprec@{} {a a' : A} (aa' : RA a a') (b : B a) (b' : B a') :=
    eprel (ippo_on RB aa') b b'.

  (* Just to make sure *)
  Lemma iprec_isrel a a' (aa' : RA a a') : i[ srel_at RB ] a a' = iprec aa'.
  Proof. reflexivity. Qed.

  Definition isp {a : A} (b : B a) := srel_at RB a b b.

  (* Passing back and forth between indexed precision and precision
     at reflexive indexes *)
  Lemma iprec_refl a (aa : RA a a) b b' : iprec aa b b' -> RB _ aa b b'.
  Proof.
    move/(eprel_equiveppair (ippo_refl RB aa)).
    apply: eprel_ideppair.
  Qed.

  Lemma prec_iprec a (aa : RA a a) b b' : RB _ aa b b' -> iprec aa b b'.
  Proof.
    move=>?; apply: (eprel_equiveppair' (ippo_refl RB aa)).
    by apply: eprel_ideppair_inv.
  Qed.

  (* Generalized adjunction yoga *)

  Lemma iprec_trans {a a' a'': A}
        (aa' : RA a a') (a'a'' : RA a' a'') (aa'' : RA a a'')
        (b : B a) (b' : B a') (b'' : B a'') :
    iprec aa' b b' -> iprec a'a'' b' b'' -> iprec aa'' b b''.
  Proof.
    move=> bb' b'b''; move: (eprel_trans bb' b'b'').
    apply: eprel_equiveppair; apply: ippo_trans.
  Qed.

  Lemma iprec_trans' {a a' a'': A}
        (aa' : RA a a') (a'a'' : RA a' a'')
        (b : B a) (b' : B a') (b'' : B a'') :
    iprec aa' b b' -> iprec a'a'' b' b'' -> iprec (trans aa' a'a'') b b''.
  Proof. apply: iprec_trans. Qed.

  Lemma iprec_left {a a' : A} (aa' : RA a a') (b0 b1 : B a)
    (b' : B a') :
    RB a (lower_refl aa') b0 b1 -> iprec aa' b1 b' -> iprec aa' b0 b'.
  Proof. apply: eprel_left. Qed.

  Lemma iprec_left' {a a' : A} (aa' : RA a a') (b0 b1 : B a)
    (b' : B a') :
    iprec (lower_refl aa') b0 b1 -> iprec aa' b1 b' -> iprec aa' b0 b'.
  Proof. move=> /iprec_refl; apply: iprec_left. Qed.

  Lemma iprec_right {a a' : A} (aa' : RA a a') (b : B a)
    (b0' b1' : B a') :
    iprec aa' b b0' -> RB a' (upper_refl aa') b0' b1' -> iprec aa' b b1'.
  Proof. apply: eprel_right. Qed.

  Lemma iprec_right' {a a' : A} (aa' : RA a a') (b : B a)
    (b0' b1' : B a') :
    iprec aa' b b0' -> iprec (upper_refl aa') b0' b1' -> iprec aa' b b1'.
  Proof. move=> + /iprec_refl; apply: iprec_right. Qed.

  Section UpDownGymkana.
    Context (a0 a1 a2 : A) (a01 : RA a0 a1) (a12 : RA a1 a2) (a02 : RA a0 a2).
    Context (b0 : B a0) (b1 : B a1) (b2 : B a2).

    Lemma iprec_dl : iprec a12 b1 b2 -> iprec a02 (fdowncast B a0 a1 b1) b2.
    Proof.
      move=> b12; refine (iprec_trans _ _ b12).
      exact (eprel_down_left (ippo_on RB a01) _ _ (eprel_lrefl b12)).
    Qed.

    Lemma iprec_dr : iprec a02 b0 b2 -> iprec a01 b0 (fdowncast B a1 a2 b2).
    Proof.
      move=> b02. change (fdowncast _ _ _) with (downcast (ippo_on RB a12)).
      refine (eprel_down_right _).
      apply: (eprel_equiveppair' _ b02).
      refine (ippo_trans _ a01 a12).
    Qed.

    Lemma iprec_ur : iprec a01 b0 b1 -> iprec a02 b0 (fupcast B a1 a2 b1).
    Proof.
      move=> b01 ; refine (iprec_trans _ b01 _).
      exact (eprel_up_right (ippo_on RB a12) _ _ (eprel_urefl b01)).
    Qed.

    Lemma iprec_ul : iprec a02 b0 b2 -> iprec a12 (fupcast B a0 a1 b0) b2.
    Proof.
      move=> b02. change (fupcast _ _ _) with (upcast (ippo_on RB a01)).
      refine (eprel_up_left _).
      apply: (eprel_equiveppair' _ b02).
      refine (ippo_trans _ a01 a12).
    Qed.

  End UpDownGymkana.


  Definition iequiv {a a' : A} (aa' : a ≈[ RA ] a') (b : B a) (b' : B a') :
    SProp :=
    iprec (sprl aa') b b' s/\ iprec (sprr aa') b' b.

  Notation " aa' ⊢ b ≈ b' " := (iequiv aa' b b') (at level 70).

  Lemma equiv_iequiv a (aa : RA a a) b b' :
    b ≈[RB a aa] b' -> spair aa aa ⊢ b ≈ b'.
  Proof. move=> [??]; split; by apply: prec_iprec. Qed.

  Lemma iequiv_equiv a (aa : RA a a) b b' :
    spair aa aa ⊢ b ≈ b' -> b ≈[RB a aa] b'.
  Proof. move=> [??]; split; by apply: iprec_refl. Qed.


  Lemma iequiv_helper {a a' : A} (aa' : a ≈[ RA ] a')
        (b : B a) (b' : B a') :
    RB a' ⌈sprl aa'⌉ (fupcast B a a' b) b' ->
    RB a ⌈sprr aa'⌉ (fupcast B a' a b') b ->
    aa' ⊢ b ≈ b'.
  Proof. move=> /wrefl ??? /wrefl ???; split; apply: to_eprel=> //. Qed.

  Lemma iequiv_helper_down {a a' : A} (aa' : sym RA a a')
        (b : B a) (b' : B a') :
    RB a ⌈sprr aa'⌉ b (fdowncast B a a' b') ->
    RB a' ⌈sprl aa'⌉ b' (fdowncast B a' a b) ->
    aa' ⊢ b ≈ b'.
  Proof.
    move=> /wrefl ??? /wrefl ???; split; apply: to_eprel=> //; by apply: downtoup.
  Qed.

  Lemma iequiv_helper' {a0 a1 : A}
        (a00 : RA a0 a0) (a01 : RA a0 a1)
        (a10 : RA a1 a0) (a11 : RA a1 a1)
        (b0 : B a0) (b1 : B a1) :
    RB a1 a11  (fupcast B a0 a1 b0) b1 ->
    RB a0 a00 (fupcast B a1 a0 b1) b0 ->
    spair a01 a10 ⊢ b0 ≈ b1.
  Proof. refine (iequiv_helper (spair a01 a10) b0 b1). Qed.

  Lemma iequiv_lower_refl {a a' : A} (aa' : sym RA a a')
        (b : B a) (b' : B a')
    : aa' ⊢ b ≈ b' -> RB a ⌊sprl aa'⌋ b b.
  Proof. move=> [] h _; apply: (eprel_lrefl h). Qed.

  Lemma iequiv_upper_refl {a a' : A} (aa' : sym RA a a')
        (b : B a) (b' : B a')
    : aa' ⊢ b ≈ b' -> RB a' ⌈sprl aa'⌉ b' b'.
  Proof. move=> [] h _; apply: (eprel_urefl h). Qed.

  Lemma iequiv_trans {ax ay az} (axy : sym RA ax ay) (ayz : sym RA ay az)
        (bx : B ax) (by_ : B ay) (bz : B az) :
    axy ⊢ bx ≈ by_ -> ayz ⊢ by_ ≈ bz -> sym_trans _ axy ayz ⊢ bx ≈ bz.
  Proof.
    move=> [??] [??]; split; apply: iprec_trans; eassumption.
  Qed.


  Context `{meetOp A} `{forall a, meetOp (B a)}.

  Definition dmeet {ax ay : A} (xb : B ax) (yb : B ay) : B (ax ⊓ ay) :=
    fdowncast B (ax ⊓ ay) ax xb ⊓ fdowncast B (ax ⊓ ay) ay yb.


  Context `{!isMeet RA} `{! forall a aa, isMeet (RB a aa)}.

  Definition dmeet_alt {ax ay : A}
             (axx : RA ax ax) (ayy : RA ay ay)
             (xb : B ax) (yb : B ay) :
    dmeet xb yb =
    downcast (ippo_on RB (meet_smaller_l axx ayy)) xb ⊓
             downcast (ippo_on RB (meet_smaller_r axx ayy)) yb.
  Proof. reflexivity. Defined.

  Lemma dmeet_smaller_l {ax ay : A} (xb : B ax) (yb : B ay) :
    forall (axx : RA ax ax) (ayy : RA ay ay),
    RB ax axx xb xb -> RB ay ayy yb yb ->
    iprec (meet_smaller_l axx ayy) (dmeet xb yb) xb.
  Proof.
    move=> axx ayy xxb yyb; apply: to_eprel'=> //.
    rewrite (dmeet_alt axx ayy).
    apply: meet_smaller_l; by apply: downmon.
  Qed.

  Lemma dmeet_smaller_r {ax ay : A} (xb : B ax) (yb : B ay) :
    forall (axx : RA ax ax) (ayy : RA ay ay),
    RB ax axx xb xb -> RB ay ayy yb yb ->
    iprec (meet_smaller_r axx ayy) (dmeet xb yb) yb.
  Proof.
    move=> axx ayy xxb yyb; apply: to_eprel'=> //.
    rewrite (dmeet_alt axx ayy).
    apply: meet_smaller_r; by apply: downmon.
  Qed.

  Lemma dmeet_smallest {ax ay az : A} (xb : B ax) (yb : B ay) (zb : B az) :
    forall (azx : RA az ax) (azy : RA az ay),
      iprec azx zb xb -> iprec azy zb yb ->
      iprec (meet_smallest azx azy) zb (dmeet xb yb).
  Proof.
    move=> azx azy bzx bzy.
    move: (eprel_urefl bzx) (eprel_urefl bzy) ⌈azx⌉ ⌈azy⌉=> bxx byy axx ayy.
    apply: to_eprel' ; rewrite (dmeet_alt axx ayy).
    2: apply: meet_refl; by apply: downmon.
    apply: trans; last (apply: downcast_meet; by apply: downmon).
    refine (meet_smallest _ _); set azmeet := meet_smallest _ _.
    - refine (trans (eprel_downcast bzx) _).
      set ameetx := meet_smaller_l _ _.
      by apply: (equiv_downcast_app0 (equiveppair_sym (ippo_trans RB azmeet ameetx))).
    - refine (trans (eprel_downcast bzy) _).
      set ameety := meet_smaller_r _ _.
      by apply: (equiv_downcast_app0 (equiveppair_sym (ippo_trans RB azmeet ameety))).
  Qed.

  Context `{joinOp A} `{forall a, joinOp (B a)}.

  Definition djoin {ax ay : A} (xb : B ax) (yb : B ay) : B (ax ⊔ ay) :=
    fupcast B ax (ax ⊔ ay) xb ⊔ fupcast B ay (ax ⊔ ay) yb.

  Context `{!isJoin RA} `{! forall a aa, isJoin (RB a aa)}.

  Definition djoin_alt {ax ay : A}
             (axx : RA ax ax) (ayy : RA ay ay)
             (xb : B ax) (yb : B ay) :
    djoin xb yb =
    upcast (ippo_on RB (join_greater_l axx ayy)) xb ⊔
             upcast (ippo_on RB (join_greater_r axx ayy)) yb.
  Proof. reflexivity. Defined.

  Lemma djoin_greater_l {ax ay : A} (xb : B ax) (yb : B ay) :
    forall (axx : RA ax ax) (ayy : RA ay ay),
    RB ax axx xb xb -> RB ay ayy yb yb ->
    iprec (join_greater_l axx ayy) xb (djoin xb yb).
  Proof.
    move=> axx ayy xxb yyb; apply: to_eprel=> //.
    rewrite (djoin_alt axx ayy).
    apply: join_greater_l; by apply: upmon.
  Qed.

  Lemma djoin_greater_r {ax ay : A} (xb : B ax) (yb : B ay) :
    forall (axx : RA ax ax) (ayy : RA ay ay),
    RB ax axx xb xb -> RB ay ayy yb yb ->
    iprec (join_greater_r axx ayy) yb (djoin xb yb).
  Proof.
    move=> axx ayy xxb yyb; apply: to_eprel=> //.
    rewrite (djoin_alt axx ayy).
    apply: join_greater_r; by apply: upmon.
  Qed.

  Lemma djoin_greatest {ax ay az : A} (xb : B ax) (yb : B ay) (zb : B az) :
    forall (axz : RA ax az) (ayz : RA ay az),
      iprec axz xb zb -> iprec ayz yb zb ->
      iprec (join_greatest axz ayz) (djoin xb yb) zb.
  Proof.
    move=> axz ayz bxz byz.
    move: (eprel_lrefl bxz) (eprel_lrefl byz) ⌊axz⌋ ⌊ayz⌋=> bxx byy axx ayy.
    rewrite (djoin_alt axx ayy); apply: to_eprel.
    1: apply: join_refl; by apply: upmon.
    apply: trans; first (apply: upcast_join; by apply: upmon).
    refine (join_greatest _ _); set ajoinz := join_greatest _ _.
    - refine (trans _ (eprel_upcast bxz)).
      set axjoin := join_greater_l _ _.
      by apply: (equiv_upcast_app0 (ippo_trans RB axjoin ajoinz)).
    - refine (trans _ (eprel_upcast byz)).
      set ayjoin := join_greater_r _ _.
      by apply: (equiv_upcast_app0 (ippo_trans RB ayjoin ajoinz)).
  Qed.


  (** upcast/downcast sequences through an upper-bound do not depend on the chosen upper-bound *)
  (* This lemma is crucial ! *)
  Lemma ippo_cast_equiv_above
        {a0 a1 ax ay : A}
        (ax0 : RA a0 ax) (ax1 : RA a1 ax)
        (ay0 : RA a0 ay) (ay1 : RA a1 ay)
        (axy : RA ax ay)
        (a00 : RA a0 a0) (a11 : RA a1 a1) :
    forall b (bb : RB a0 a00 b b),
      downcast (ippo_on RB ax1) (upcast (ippo_on RB ax0) b)
               ≈[RB a1 a11]
               downcast (ippo_on RB ay1) (upcast (ippo_on RB ay0) b).
  Proof.
    move=> b bb.
    pose proof (beqv := spair bb bb).
    pose proof (eqv_upA := equiv_app (sprl (ippo_trans RB ax0 axy)) beqv).
    pose proof (eqv_downB := equiv_app (sprr (ippo_trans RB ax1 axy)) eqv_upA).
    refine (sym_trans _ _ eqv_downB).
    split; refine (downmon (ippo_on RB ax1) _ _ _);
    [apply: unit| apply: (retract (ippo_on RB axy))];
      by apply: upmon.
  Qed.

End Ippo.

Arguments iprec_isrel [_ _ _ _ _ _] _.

(* Changes occurence of `i[srel_at RB] x1 x2 y1 y2`
   into `iprec RB x12 y1 y2` in the goal
   discharging the required relation `x12` with intuition *)
Ltac intro_iprec :=
  match goal with
  | [|- context c [ i[ @srel_at _ ?RA _ ?RB ] ?x1 ?x2 ?y1 ?y2 ]] =>
      let h := fresh "_h_" in
      assert (h : RA x1 x2) by intuition;
      change (i[ srel_at RB ] x1 x2 y1 y2) with (iprec RB h y1 y2)
  end.

(* Similar functionalities, as an ssr intro pattern *)
Notation "'toiprec'" := ltac:(intro_iprec) (only parsing) : ssripat_scope.


Notation " axy ⊢[ RB ] bx ≈ by_ " := (iequiv RB axy bx by_) (at level 70).


(** Indexed partial preorder with trivial casts
    over types with decidable equality *)
(* Employed for the definition of unknown in the universe 0 *)
Section IppoFromEqDec.
  Universe u.
  Context  (A : Type@{u}) `{EqDec A}
           (B : A -> Type@{u}) `{forall a, Err (B a)} (pB : forall a, ppo@{u} (B a)).

  Let RA := ppo_from_eq A.
  Let fB := fwc_dom_eq_dec B.

  #[program]
  Definition ippo_dom_eq_dec : ippo@{u} RA fB :=
    mkIppo (ppo_from_eq A) fB pB (fun a aa => pB a) _ _ _.
  Next Obligation.
    move=> ?? axy /=. induction (prec_eqP axy).
    rewrite eq_dec_refl /=; apply: ideppair.
  Qed.
  Next Obligation.
    move=> ??? axy ayz /=; induction (prec_eqP axy); induction (prec_eqP ayz).
    apply: equiveppair_helper_up=> /=; rewrite eq_dec_refl /=; apply: sym_refl.
  Qed.
  Next Obligation.
    move=> ??; apply: equiveppair_helper_up=> /=; rewrite eq_dec_refl /=; apply: sym_refl.
  Qed.

  Let RB := ippo_dom_eq_dec.

  Context  `{!forall a, SmallestElem (pB a)}.

  Lemma ippo_dom_eq_downerr
        (a0 a1 : A)
        (* a00 and a11 not needed in this case *)
        (a00 : RA a0 a0) (a11 : RA a1 a1)
        (b0 : fB a0) :
    RB a0 a00 b0 err -> RB a1 a11 (fdowncast fB a1 a0 b0) err.
  Proof.
    cbn; case: (eq_dec _ _)=> [e|ne]; first by depelim e.
    move=> ?; apply: smallest_refl.
  Qed.

  Lemma ippo_dom_eq_downdyn
        `{forall a, Dyn (B a)}
        `{forall a, GreatestElem (pB a)}
        (a0 a1 : A)
        (* a00 and a11 not needed in this case *)
        (a00 : RA a0 a0) (a11 : RA a1 a1)
        (b0 : fB a0) :
    RB a0 a00 b0 dyn -> RB a1 a11 (fdowncast fB a1 a0 b0) dyn.
  Proof.
    cbn; case: (eq_dec _ _)=> [e|ne]; first by depelim e.
    move=> ?; apply: smaller; apply: greatest_refl.
  Qed.

  Lemma ippo_dom_eq_fupcastmon a0 a1 a2 b0 b1 :
    i[ srel_at RB] a0 a1 b0 b1 ->
    pB a2 (fupcast fB a0 a2 b0) (fupcast fB a1 a2 b1).
  Proof.
    destruct (eq_dec a1 a0) as [e01|ne01] eqn:E01.
    - depelim e01=> /=.
      case: (eq_dec _ _)=> [e12|ne12] h; last apply: smallest_refl.
      move: (hrel_downcast h)=> /=; depelim e12; by rewrite E01.
    - move=> h; move: (hrel_downcast h); rewrite /= E01.
      case: (eq_dec a0 a2)=> [e02|ne02].
      + depelim e02=> /= b0err; by rewrite E01.
      + move=> _; case: (eq_dec _ _)=> [e12|_]; last apply: smallest_refl.
        depelim e12; apply: smaller; exact (hrel_urefl h).
  Qed.

  Lemma ippo_dom_eq_fdowncastmon a0 a1 a2 b0 b1 :
    i[ srel_at RB] a0 a1 b0 b1 ->
    pB a2 (fdowncast fB a2 a0 b0) (fdowncast fB a2 a1 b1).
  Proof. apply: ippo_dom_eq_fupcastmon. Qed.

  Lemma ippo_dom_eq_fdowncastcomp
        a0 (a00 : RA a0 a0)
        a1 (a11 : RA a1 a1)
        a2 (a22 : RA a2 a2)
        (b : B a2) :
    RB a2 a22 b b ->
    RB a0 a00 (fdowncast fB a0 a1 (fdowncast fB a1 a2 b))
       (fdowncast fB a0 a2 b).
  Proof.
    destruct (eq_dec a1 a0) as [e01|ne01] eqn:E01.
    - depelim e01. set (f := fdowncast _ _ a2 _). rewrite /= E01 /= /f=> bb.
      refine (ippo_dom_eq_fdowncastmon _ _).
      refine (prec_iprec RB _ _ _ _ bb)=> //.
    -  set (f := fdowncast _ a0 a2 _).
       rewrite /= E01 /= /f=> bb.
       apply: smaller.
      refine (ippo_dom_eq_fdowncastmon _ _).
      refine (prec_iprec RB _ _ _ _ bb)=> //.
  Qed.

  Lemma ippo_dom_eq_itrans a0 a1 a2 :
    RA a0 a0 ->
    RA a1 a1 ->
    RA a2 a2 ->
    forall (b0 : B a0) (b1 : B a1) (b2 : B a2),
      i[ srel_at RB] a0 a1 b0 b1 ->
      i[ srel_at RB] a1 a2 b1 b2 ->
      i[ srel_at RB] a0 a2 b0 b2.
  Proof.
    move=> a00 a11 a22 b0 b1 b2 b01 b12.
    destruct (eq_dec a1 a0) as [e01|ne01] eqn:E01.
    - depelim e01.
      move: (ippo_dom_eq_fdowncastmon a2 b01)=> db01.
      move: b01 b12=> [[[b00 bu01] b0d1] b11] [[[_ bu12] b1d2] b22].
      repeat split=> //; first by apply: (trans db01).
      move: bu01=> /=; rewrite /= E01=> bu01; by apply: (trans bu01).
    - move: (hrel_downcast b01)=> /=. rewrite E01=> b0err.
      repeat split=> //.
      + exact (hrel_lrefl b01).
      + apply: trans; first (unshelve apply: ippo_dom_eq_downerr)=> //.
        apply: smaller; apply: hrel_urefl; eassumption.
      + apply: (trans b0err); apply: smaller.
        apply: ippo_dom_eq_fdowncastmon.
        refine (prec_iprec RB _ _ _ _ (hrel_urefl b12))=> //.
      +  exact (hrel_urefl b12).
  Qed.

End IppoFromEqDec.


(** constant families can be equipped with identity casts *)
Definition const_fwc@{u} (A B : Type@{u}) : fwc@{u u} A :=
  mkFwC (fun _ => B) (fun _ _ => id) (fun _ _ => id).

(** Any partial preorder B induces a constant
    indexed partial preorder over an arbitrary ppo A*)
Section PpoIppo.
  Universe u.
  Context (A B : Type@{u}) (pA : ppo@{u} A) (pB : ppo@{u} B).

  #[program]
  Definition ippo_from_ppo : ippo@{u} pA (const_fwc A B) :=
    mkIppo pA (const_fwc A B) (fun _ => pB) (fun _ _ => pB)
           (fun _ _ _ => ideppair pB) _ _.
  Next Obligation.
    move=> ?????; apply: equiveppair_helper_up=> ??; split=> //.
  Qed.
  Next Obligation.
    move=> ??; apply: equiveppair_helper_up=> ??; split=> //.
  Qed.

End PpoIppo.


(** Indexed eppairs between indexed partial preorder over an eppair *)
Section Ieppair.
  Universe u.
  Context (AX AY : Type@{u}) (pAX : ppo@{u} AX) (pAY : ppo@{u} AY)
          (BX : fwc@{u u} AX) (pBX : ippo@{u} pAX BX)
          (BY : fwc@{u u} AY) (pBY : ippo@{u} pAY BY).
  Context (AXY : eppair@{u} pAX pAY).

  Record ieppair : Type@{u} :=
    mkIEppair {
        iupcast : forall {ax} ay, BX ax -> BY ay ;
        idowncast : forall ax {ay}, BY ay -> BX ax ;
        iis_eppair_on : forall {ax ay} (axy : eprel@{u} AXY ax ay),
            eppair_prop (pBX ax ⌞axy⌟)
                        (pBY ay ⌜axy⌝)
                        (iupcast ay) (idowncast ax) ;
        ieppair_on := fun {ax ay} (axy : eprel@{u} AXY ax ay) =>
                        pack_eppair (iis_eppair_on axy) ;
        ieppair_natural : forall {ax0 ax1 ay0 ay1}
                    (ax01 : pAX ax0 ax1) (ay01 : pAY ay0 ay1)
                    (axy0 : eprel AXY ax0 ay0) (axy1 : eprel AXY ax1 ay1),
            equiveppair (compeppair (ippo_on pBX ax01) (ieppair_on axy1))
                        (compeppair (ieppair_on axy0) (ippo_on pBY ay01))
      }.
  Context (BXY : ieppair).

  (** Indexed eppair adjunction yoga *)

  Lemma ieppair_comp_left ax0 ax1 ay1
        (ax01 : pAX ax0 ax1)
        (axy1 : eprel AXY ax1 ay1)
        (ax0y1 : eprel AXY ax0 ay1) :
    equiveppair (compeppair (ippo_on pBX ax01) (ieppair_on BXY axy1))
                (ieppair_on BXY ax0y1).
  Proof.
    apply: equiveppair_trans.
    2: apply: equiveppair_right_unit; apply: ippo_refl.
    apply: ieppair_natural.
  Qed.

  Lemma ieppair_comp_right ax0 ay0 ay1
        (axy0 : eprel AXY ax0 ay0)
        (ay01 : pAY ay0 ay1)
        (ax0y1 : eprel AXY ax0 ay1) :
    equiveppair (compeppair (ieppair_on BXY axy0) (ippo_on pBY ay01))
                (ieppair_on BXY ax0y1).
  Proof.
    apply: equiveppair_trans.
    2: apply: equiveppair_left_unit; apply: ippo_refl.
    apply: equiveppair_sym; apply: ieppair_natural.
  Qed.

  Definition heprel {ax ay} (axy : eprel AXY ax ay) (bx : BX ax) (by_ : BY ay) : SProp :=
    eprel (ieppair_on BXY axy) bx by_.


  Lemma heprel_left ax0 ax1 ay1 bx0 bx1 by1
        (ax01 : pAX ax0 ax1) (axy1 : eprel AXY ax1 ay1) (ax0y1 : eprel AXY ax0 ay1) :
    iprec pBX ax01 bx0 bx1 -> heprel axy1 bx1 by1 -> heprel ax0y1 bx0 by1.
  Proof.
    move=> bx01 bxy1 ; move: (eprel_trans bx01 bxy1).
    apply: eprel_equiveppair; apply: ieppair_comp_left.
  Qed.

  Lemma heprel_right ax0 ay0 ay1 bx0 by0 by1
        (axy0 : eprel AXY ax0 ay0)
        (ay01 : pAY ay0 ay1) (ax0y1 : eprel AXY ax0 ay1) :
    heprel axy0 bx0 by0 -> iprec pBY ay01 by0 by1 -> heprel ax0y1 bx0 by1.
  Proof.
    move=> bxy0 by01; move: (eprel_trans bxy0 by01).
    apply: eprel_equiveppair; apply: ieppair_comp_right.
  Qed.

  Lemma heprel_dl ax0 ay0 ay1 by0 by1
        (axy0 : eprel AXY ax0 ay0)
        (ay01 : pAY ay0 ay1)
        (ax0y1 : eprel AXY ax0 ay1) :
    iprec pBY ay01 by0 by1 ->
    heprel ax0y1 (idowncast BXY ax0 by0) by1.
  Proof.
    move=> by01.
    pose proof (bxy0 := eprel_down_left (ieppair_on BXY axy0) _ _ ⌞by01⌟).
    move: (eprel_trans bxy0 by01).
    apply: eprel_equiveppair; apply: ieppair_comp_right.
  Qed.

  Lemma heprel_dr ax0 ax1 ay1 bx0 by1
        (ax01 : pAX ax0 ax1)
        (axy1 : eprel AXY ax1 ay1)
        (ax0y1 : eprel AXY ax0 ay1) :
    heprel ax0y1 bx0 by1 -> iprec pBX ax01 bx0 (idowncast BXY ax1 by1).
  Proof.
    move=> /(eprel_equiveppair' (ieppair_comp_left ax01 axy1 ax0y1)).
    apply: eprel_down_right.
  Qed.

  Lemma heprel_ur ax0 ax1 ay1 bx0 bx1
        (ax01 : pAX ax0 ax1)
        (axy1 : eprel AXY ax1 ay1)
        (ax0y1 : eprel AXY ax0 ay1) :
    iprec pBX ax01 bx0 bx1 ->
    heprel ax0y1 bx0 (iupcast BXY ay1 bx1).
  Proof.
    move=> bx01.
    pose proof (bxy1 := eprel_up_right (ieppair_on BXY axy1) _ _ ⌜bx01⌝).
    move: (eprel_trans bx01 bxy1).
    apply: eprel_equiveppair; apply: ieppair_comp_left.
  Qed.

  Lemma heprel_ul ax0 ay0 ay1 bx0 by1
        (axy0 : eprel AXY ax0 ay0)
        (ay01 : pAY ay0 ay1)
        (ax0y1 : eprel AXY ax0 ay1) :
    heprel ax0y1 bx0 by1 -> iprec pBY ay01 (iupcast BXY ay0 bx0) by1.
  Proof.
    move=> /(eprel_equiveppair' (ieppair_comp_right axy0 ay01 ax0y1)).
    apply: eprel_up_left.
  Qed.

  Lemma iupmon0
        ax0 ax1 (ax01 : pAX ax0 ax1) bx0 bx1
        ay0 ay1 (ay01 : pAY ay0 ay1)
        (axy0 : eprel AXY ax0 ay0)
        (axy1 : eprel AXY ax1 ay1) :
    iprec pBX ax01 bx0 bx1 -> iprec pBY ay01 (iupcast BXY _ bx0) (iupcast BXY _ bx1).
  Proof.
    move=> ?; unshelve apply: heprel_ul=> //.
    1: apply: eprel_left; eassumption.
    unshelve apply: heprel_ur=> //.
    assumption.
  Qed.

  Lemma iupmon ax0 ax1 (ax01 : pAX ax0 ax1) bx0 bx1 :
    iprec pBX ax01 bx0 bx1 -> iprec pBY (upmon AXY _ _ ax01) (iupcast BXY _ bx0) (iupcast BXY _ bx1).
  Proof.
    apply: iupmon0; apply: eprel_up_right;
    [apply: lower_refl | apply: upper_refl] ; eassumption.
  Qed.

  Lemma iupmon' ax (axx : pAX ax ax) ay (ayy : pAY ay ay) :
    eprel AXY ax ay ->
    monotone (pBX ax axx) (pBY ay ayy) (iupcast BXY _).
  Proof.
    move=> ????; apply: iprec_refl.
    unshelve apply: iupmon0=> //; last by apply: prec_iprec.
    assumption.
  Qed.

  Lemma iupequiv
        ax0 ax1 (ax01 : ax0 ≈[pAX] ax1) bx0 bx1
        ay0 ay1 (ay01 : ay0 ≈[pAY] ay1)
        (axy0 : eprel AXY ax0 ay0)
        (axy1 : eprel AXY ax1 ay1) :
    iequiv pBX ax01 bx0 bx1 ->
    iequiv pBY ay01 (iupcast BXY _ bx0) (iupcast BXY _ bx1).
  Proof.
    move=> [xyb yxb]; split; [move: xyb|move: yxb]; apply: iupmon0=> //.
  Qed.

  Lemma idownmon0
        ay0 ay1 (ay01 : pAY ay0 ay1) by0 by1
        ax0 ax1 (ax01 : pAX ax0 ax1)
        (axy0 : eprel AXY ax0 ay0)
        (axy1 : eprel AXY ax1 ay1) :
    iprec pBY ay01 by0 by1 -> iprec pBX ax01 (idowncast BXY _ by0) (idowncast BXY _ by1).
  Proof.
    move=> ?; unshelve apply: heprel_dr=> //.
    1: apply: eprel_right; eassumption.
    unshelve apply: heprel_dl=> //.
    assumption.
  Qed.

  Lemma idownmon ay0 ay1 (ay01 : pAY ay0 ay1) by0 by1 :
    iprec pBY ay01 by0 by1 -> iprec pBX (downmon AXY _ _ ay01) (idowncast BXY _ by0) (idowncast BXY _ by1).
  Proof.
    apply: idownmon0; apply: eprel_down_left;
    [apply: lower_refl | apply: upper_refl] ; eassumption.
  Qed.

  Lemma idownmon' ax (axx : pAX ax ax) ay (ayy : pAY ay ay) :
    eprel AXY ax ay ->
    monotone (pBY ay ayy) (pBX ax axx) (idowncast BXY _).
  Proof.
    move=> ????; apply: iprec_refl.
    unshelve apply: idownmon0=> //; last by apply: prec_iprec.
    assumption.
  Qed.

  Lemma idownequiv
        ay0 ay1 (ay01 : ay0 ≈[pAY] ay1) by0 by1
        ax0 ax1 (ax01 : ax0 ≈[pAX] ax1)
        (axy0 : eprel AXY ax0 ay0)
        (axy1 : eprel AXY ax1 ay1) :
    ay01 ⊢[pBY] by0 ≈ by1 ->
    ax01 ⊢[pBX] idowncast BXY _ by0 ≈ idowncast BXY _ by1.
  Proof.
    move=> [xyb yxb]; split; [move: xyb|move: yxb]; apply: idownmon0=> //.
  Qed.

  Lemma iuptodown0
        ax0 ay0 ax1 ay1
        (axy0 : eprel AXY ax0 ay0)
        (axy1 : eprel AXY ax1 ay1)
        (ax01 : pAX ax0 ax1)
        (ay01 : pAY ay0 ay1)
        bx0 by1 :
    iprec@{u} pBX ⌊ax01⌋ bx0 bx0 ->
    iprec@{u} pBY ay01 (iupcast BXY _ bx0) by1 ->
    iprec@{u} pBX ax01 bx0 (idowncast BXY _ by1).
  Proof.
    move=> /(heprel_ur axy0 axy0) ??.
    unshelve apply: heprel_dr; first eassumption.
    1: apply: eprel_left; eassumption.
    apply: heprel_right; eassumption.
  Qed.

  Lemma idowntoup0
        ax0 ay0 ax1 ay1
        (axy0 : eprel AXY ax0 ay0)
        (axy1 : eprel AXY ax1 ay1)
        (ax01 : pAX ax0 ax1)
        (ay01 : pAY ay0 ay1)
        bx0 by1 :
    iprec@{u} pBY ⌈ay01⌉ by1 by1 ->
    iprec@{u} pBX ax01 bx0 (idowncast BXY _ by1) ->
    iprec@{u} pBY ay01 (iupcast BXY _ bx0) by1.
  Proof.
    move=> /(heprel_dl axy1 axy1) ??.
    unshelve apply: heprel_ul; first eassumption.
    1: apply: eprel_left; eassumption.
    apply: heprel_left; eassumption.
  Qed.

  Lemma iretract ax (axx : pAX ax ax) bx (bxx : iprec pBX axx bx bx) :
    iprec@{u} pBX (retract AXY ax axx)
         (idowncast BXY _ (iupcast BXY (upcast AXY ax) bx))
         bx.
  Proof.
    pose proof (ay := upcast AXY ax).
    pose proof (ayy := upmon AXY ax ax axx).
    pose proof (axy := eprel_up_right@{u} AXY ax ax axx).
    apply: (iprec_right _ _ (retract (ieppair_on BXY axy) bx (iprec_refl bxx))).
    unshelve apply: idownmon0=> //.
    1: assumption.
    1: by apply: eprel_down_left.
    unshelve apply: iupmon0=> //.
    assumption.
  Qed.

  Context `{forall a', Err@{u} (BY a')} `{forall a, Err@{u} (BX a)}.
  Context `{!forall ay ayy, SmallestElem (pBY ay ayy)}
          `{!forall ax axx, SmallestElem (pBX ax axx)}.

  Lemma idowncast_err ax0 ax1 (ax01 : pAX ax0 ax1)
        ay0 ay1 (ay01: pAY ay0 ay1)
        (axy0 : eprel AXY ax0 ay0)
        (axy1 : eprel AXY ax1 ay1) :
     iprec pBX ax01 (idowncast BXY (ay := ay0) ax0 err@{u}) err.
  Proof.
    apply: iprec_right.
    2: refine (downcast_err (ieppair_on BXY axy1)).
    apply: idownmon0=> //; apply: to_eprel.
    2: apply: downtoup; last apply: smaller; last apply: downmon.
    all: apply: smallest_refl.
    Unshelve. assumption.
  Qed.

End Ieppair.



(** Equivalences between indexed eppairs *)
Section Iequiveppair.
  Universe u.
  Context (AX AY : Type@{u}) (pAX : ppo@{u} AX) (pAY : ppo@{u} AY)
          (BX : fwc@{u u} AX) (pBX : ippo@{u} pAX BX)
          (BY : fwc@{u u} AY) (pBY : ippo@{u} pAY BY).
  Section Def.
    Context (AXY0 AXY1 : eppair@{u} pAX pAY).
    Context (BXY0 : ieppair@{u} pBX pBY AXY0) (BXY1 : ieppair@{u} pBX pBY AXY1).

    (* Not sure why I gave such a complicated definition,
       instead of the simpler expression below
     *)
    Definition iequiveppair : SProp :=
      forall ax0 ay0 ax1 ay1
        (ax01 : sym pAX ax0 ax1) (ay01 : sym pAY ay0 ay1)
        (axy0 : eprel AXY0 ax0 ay0) (axy1 : eprel AXY1 ax1 ay1),
        (forall bx0 bx1,
            ax01 ⊢[pBX] bx0 ≈ bx1 ->
            ay01 ⊢[pBY] iupcast BXY0 ay0 bx0 ≈ iupcast BXY1 ay1 bx1) s/\
          (forall by0 by1,
              ay01 ⊢[pBY] by0 ≈ by1 ->
              ax01 ⊢[pBX] idowncast BXY0 ax0 by0 ≈ idowncast BXY1 ax1 by1).

    (* Actually, this should be typically employed in a context
       where AXY0 ≃ AXY1 so one of axy_ is redundant *)
    Lemma to_iequiveppair (hequiv : forall ax ay (axy0 : eprel AXY0 ax ay)
                                 (axy1 : eprel AXY1 ax ay),
                         ieppair_on BXY0 axy0 ≃ ieppair_on BXY1 axy1):
      iequiveppair.
    Proof.
      move=> ax0 ay0 ax1 ay1 ax01 ay01 axy0 axy1.
      pose proof (axy1' := eprel_right _ (eprel_left _ (sprl ax01) axy1) (sprr ay01)).
      pose proof (axy0' := eprel_right _ (eprel_left _ (sprr ax01) axy0) (sprl ay01)).
      pose (h0 := hequiv ax0 ay0 axy0 axy1').
      pose (h1 := hequiv ax1 ay1 axy0' axy1).
      split.
      - move=> bx0 bx1 bx01.
        refine (iequiv_trans (iupequiv BXY0 ay01 axy0 axy0' bx01) _).
        apply: equiv_iequiv; refine (equiv_upcast_app h1 _).
        apply: sym_refl; apply: iequiv_upper_refl; eassumption.
      - move=> by0 by1 by01.
        refine (iequiv_trans (idownequiv BXY0 ax01 axy0 axy0' by01) _).
        apply: equiv_iequiv; refine (equiv_downcast_app h1 _).
        apply: sym_refl; apply: iequiv_upper_refl; eassumption.
    Qed.

  End Def.

  Section Refl.
    Context (AXY : eppair@{u} pAX pAY).
    Context (BXY : ieppair@{u} pBX pBY AXY).

    Lemma iequiveppair_refl : iequiveppair BXY BXY.
    Proof.
      move=> ???? ????; split=> ??; by [apply: iupequiv| apply: idownequiv].
    Qed.
  End Refl.

End Iequiveppair.
