From Coq Require Import ssreflect ssrfun.
From Equations Require Import Equations.
From PartialPreorder Require Import strictPropLogic.
From PartialPreorder Require Import prelude ppoDef bool prop box empty pi unk0 beckChevalley listwf.
From PartialPreorder Require unknown.

Set Implicit Arguments.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Primitive Projections.
Import SPropNotations.


Set Printing Universes.


Section U0.
  Universes u.

  (** Universe of codes *)
  (* Wrt the paper we do not show the closure under
     nat or list *)

  Inductive code0 : Type@{u} -> Type@{u+1} :=
  | cerr0 : code0 Empty@{u}
  | cunk0 : code0 unk0@{u}
  | cbool0 : code0 pbool@{u}
  | cprop0 : code0 SProp
  | cbox0 (p : SProp) : code0 (box_@{u} p)
  | carr0 (A : Type@{u})
          (cA : code0 A)
          (B : A -> Type@{u})
          (cB : forall a : A, code0 (B a))
    : code0 (forall (a : A), B a).

  (* An element of u0 is decoded to a type through el0 : u0 -> Type *)
  Record u0 := mkU0 { el0 : Type@{u} ; elcode0 : code0 el0 }.

  Definition erru0 := mkU0 cerr0.
  Definition unku0 := mkU0 cunk0.
  Definition boolu0 : u0 := mkU0 cbool0.
  Definition propu0 : u0 := mkU0 cprop0.
  Definition boxu0 (p : SProp) : u0 := mkU0 (cbox0 p).
  Definition piu0 (A : u0) (B : el0 A -> u0) : u0 :=
    mkU0 (carr0 (elcode0 A) (fun a => el0 (B a)) (fun a=> elcode0 (B a))).
  Definition arrU0 (A B : u0) := piu0 A (fun _ => B).

  #[global]
  Instance err_u0 : Err u0 := erru0.
  #[global]
  Instance dyn_u0 : Dyn u0 := unku0.

  (** Elimination principle on codes (targetting Type/SProp)*)

  Section U0elim.
    Universe w.
    Context (P : u0 -> Type@{w})
            (herr : P erru0)
            (hunk : P unku0)
            (hbool : P boolu0)
            (hprop : P propu0)
            (hbox : forall p, P (boxu0 p))
            (hpi : forall (A : u0) (B : el0 A -> u0),
                P A -> (forall a : el0 A, P (B a)) -> P (piu0 A B)).

    Definition u0elim (X : u0) : P X.
    Proof.
      move: X=> [X cX]; elim: cX=> [||||p|A cA ihA B cB ihB].
      - exact herr.
      - exact hunk.
      - exact hbool.
      - exact hprop.
      - exact (hbox p).
      - exact (hpi (fun a : el0 (mkU0 cA) => mkU0 (cB a)) ihA ihB).
    Defined.
  End U0elim.

  Section U0selim.
    Context (P : u0 -> SProp)
            (herr : P erru0)
            (hunk : P unku0)
            (hbool : P boolu0)
            (hprop : P propu0)
            (hbox : forall p, P (boxu0 p))
            (hpi : forall (A : u0) (B : el0 A -> u0),
                P A -> (forall a : el0 A, P (B a)) -> P (piu0 A B)).

    Definition u0selim (X : u0) : P X.
    Proof.
      move: X=> [X cX]; elim: cX=> [||||p|A cA ihA B cB ihB].
      - exact herr.
      - exact hunk.
      - exact hbool.
      - exact hprop.
      - exact (hbox p).
      - exact (hpi (fun a : el0 (mkU0 cA) => mkU0 (cB a)) ihA ihB).
    Defined.
  End U0selim.


  (** Exceptional terms on the decoding of codes *)

  Section ErrEl0.
    Universe u'.
    Constraint u <= u'.

    (* #[global] *)
    Instance err0_at (A : u0) : Err@{u'} (el0 A).
    Proof.
      elim/u0elim: A=> /=; typeclasses eauto.
    Defined.

    Instance dyn0_at (A : u0) : Dyn@{u'} (el0 A).
    Proof.
      elim/u0elim: A=> /=; typeclasses eauto.
    Defined.
  End ErrEl0.
  (* For some reason
     1) the #[global] on err0_at does not seem to be picked up
     2) just declaring Existing Instance err0_at employs simple apply which fails to unify with the goal (universe level troubles ?)
   So instead I issue an Hint Extern *)
  Hint Extern 0 (Err (el0 _)) => apply: err0_at : typeclass_instances.

  Hint Extern 0 (Dyn (el0 _)) => apply: dyn0_at : typeclass_instances.


  (** Implementation of cast between codes *)

  Definition cast_err (X Y : Type@{u}) `{Err@{u} X} `{Err@{u} Y} : (X -> Y) * (Y -> X) := (fun=> err, fun=> err).

  Definition cast_id (X : Type@{u}) : (X -> X) * (X -> X) := (id, id).

  Section El0Cast.

    Universe v.
    Constraint u < v.

    (* universe v needed for the induction hypothesis containing u0 : Type@{u} *)
    (** In the case of function types we need both directions for casts *)
    Definition el0_cast@{} (A B : u0) : (el0 A -> el0 B) * (el0 B -> el0 A).
    Proof.
      move: A B; elim/u0elim@{v}=> [||||P|A B ihA ihB].
      - move=> ?; split; [apply: empty_upcast| apply: empty_downcast].
      - move=> [_ [||||?|????]].
        + split; [apply: empty_downcast| apply: empty_upcast].
        + apply: cast_id.
        + split; [apply: (downcast_from_unk0 hbool0)|apply: (upcast_to_unk0 hbool0)].
        + split; [apply: (downcast_from_unk0 hprop0)|apply: (upcast_to_unk0 hprop0)].
        + split; [apply: (downcast_box_ _ (A:= _))| apply: upcast_box_].
        + exact cast_err.
      - move=> [_ [||||?|????]].
        + split; [apply: empty_downcast| apply: empty_upcast].
        + split; [apply: (upcast_to_unk0 hbool0)| apply:(downcast_from_unk0 hbool0)].
        + apply: cast_id.
        + exact cast_err.
        + exact cast_err.
        + exact cast_err.
      - move=> [_ [||||?|????]].
        + split; [apply: empty_downcast| apply: empty_upcast].
        + split; [apply: (upcast_to_unk0 hprop0)| apply:(downcast_from_unk0 hprop0)].
        + exact cast_err.
        + apply: cast_id.
        + exact cast_err.
        + exact cast_err.
      - move=> [_ [||||?|????]].
        + split; [apply: empty_downcast| apply: empty_upcast].
        + split; [apply: upcast_box_| apply:(downcast_box_ _ (A:= _))].
        + exact cast_err.
        + exact cast_err.
        + split; [apply: upcast_box_| apply:(downcast_box_ _ (A:= _))].
        + exact cast_err.
      - elim/u0elim=> [||||?|A' B' _ _].
        + split; [apply: empty_downcast| apply: empty_upcast].
        + exact cast_err.
        + exact cast_err.
        + exact cast_err.
        + exact cast_err.
        + split; apply: pi_cast.
          1,3: by case: (ihA A').
          * move=> a a'; by case (ihB a (B' a')).
          * move=> a' a; by case (ihB a (B' a')).
    Defined.

    Definition el0_upcast {A B : u0} := fst (el0_cast A B).
    Definition el0_downcast {A B : u0} := snd (el0_cast A B).

    Lemma el0_upcast_pi A A' B B' :
      @el0_upcast (piu0 A B) (piu0 A' B') =
      pi_cast (el0 \o B) (el0 \o B') (@el0_downcast A A') (fun a a' => @el0_upcast (B a) (B' a')).
    Proof. reflexivity. Qed.

    Lemma el0_downcast_pi A A' B B' :
      @el0_downcast (piu0 A' B') (piu0 A B) =
      pi_cast (el0 \o B) (el0 \o B') (@el0_upcast  A' A) (fun a a' => @el0_downcast (B' a') (B a)).
    Proof. reflexivity. Qed.


  End El0Cast.


  (** Term precision on the universe *)

  (* Any type but function types are self-precise for term precision at universe *)
  Definition sp_u0 (A : u0) : SProp :=
    match elcode0 A with
    | carr0 Ac B Bc => sEmpty
    | _ => sUnit
    end.

  Inductive prec_u0 : srel u0 :=
  | err_any A : sp_u0 A -> prec_u0 erru0 A
  | unk_any A : sp_u0 A -> prec_u0 A unku0
  | boolrefl0 : prec_u0 boolu0 boolu0
  | proprefl0 : prec_u0 propu0 propu0
  | boxcong0 (p q : SProp) : prec_prop p q -> prec_u0 (boxu0 p) (boxu0 q)
  (* Important: No case for function types in term precision *)
  .


  (** Lemmas on self-preciseness *)

  Lemma sp_prec_u0 A : sp_u0 A -> prec_u0 A A.
  Proof. elim/u0selim: A=> [||||P|A B ihA ihB []] ?; by constructor. Qed.

  Lemma prec_u0_sp_l AX AY : prec_u0 AX AY -> sp_u0 AX.
  Proof.
    elim/u0selim: AX => [||||P|A B ihA ihB] //=.
    move=> h ; inversion h; contradiction.
  Qed.

  Lemma prec_u0_sp_r AX AY : prec_u0 AX AY -> sp_u0 AY.
  Proof.
    elim/u0selim: AY => [||||P|A B ihA ihB] //=.
    move=> h ; inversion h; contradiction.
  Qed.

  (** Inversion lemmas for precisions *)

  Lemma prec_u0_unk_inv A : prec_u0 unku0 A -> A = unku0.
  Proof.
    elim/u0elim: A => [||||P|A B ihA ihB].
    2: reflexivity.
    all: move=> h; sexfalso; inversion h; contradiction.
  Qed.

  Lemma prec_u0_err_inv A : prec_u0 A err -> A = err.
  Proof.
    elim/u0elim: A => [||||P|A B ihA ihB]; first reflexivity.
    all: move=> h; sexfalso; inversion h; contradiction.
  Qed.

  (** Ppo structure for term precision *)

  Lemma prec_u0_trans : transitive prec_u0.
  Proof.
    move=> ???; elim=> [A spA|A spA|||p q pq] //.
    - move=> h; constructor; exact (prec_u0_sp_r h).
    - move=> /prec_u0_unk_inv ->; by constructor.
    - move=> h; inversion h; first by constructor.
      constructor; subst; move=> ? ; intuition.
  Qed.

  Lemma prec_u0_urefl : upper_reflexive prec_u0.
  Proof.
    move=> ??; elim ; first apply: sp_prec_u0; constructor=> //.
  Qed.

  Lemma prec_u0_lrefl : lower_reflexive prec_u0.
  Proof.
    move=> ??; elim; try constructor=> //.
    apply: sp_prec_u0.
  Qed.


  (* Introduction of a second universe level *)

  Universe v.
  Constraint u < v.

  Definition prec_u0_is_ppo@{} : is_ppo@{v} prec_u0 :=
    mkIsPpo prec_u0 prec_u0_trans prec_u0_urefl prec_u0_lrefl.

  Definition u0_ppo : ppo@{v} u0 := mkPpo prec_u0_is_ppo.

  Definition U0 : Ppo@{v} := pack_ppo@{v} u0_ppo.

  #[global]
  Instance u0_err : Err@{v} u0 := erru0.

  #[global,program]
  Instance u0_smallest : SmallestElem@{v} u0_ppo :=
    mkSmallest u0_ppo erru0 _ _.
  Next Obligation. constructor=> //. Qed.
  Next Obligation. opacify=> A /prec_u0_sp_l ? ; by constructor. Qed.

  #[global]
  Instance u0_dyn : Dyn@{v} u0 := unku0.
  #[global,program]
  Instance u0_greatest : GreatestElem@{v} u0_ppo :=
    mkGreatest u0_ppo unku0 _ _.
  Next Obligation. by constructor. Qed.
  Next Obligation. opacify=> A /prec_u0_sp_l; by constructor. Qed.

  (** Meet and join structure *)

  Set Equations Transparent.
  Equations u0_meet0 {A} (cA : code0 A) {B} (cB : code0 B) : u0 :=
  | cerr0, _ => erru0
  | _, cerr0 => erru0
  | carr0 _ _ _, _ => erru0
  | _, carr0 _ _ _ => erru0
  | cbox0 p, cbox0 q => boxu0 (p s/\ q)
  | cunk0, x => mkU0 x
  | x, cunk0 => mkU0 x
  | cbox0 _, _ => erru0
  | _, cbox0 _ => erru0
  | cbool0, cprop0 => erru0
  | cprop0, cbool0 => erru0
  | x, _ => mkU0 x.

  #[global]
  Instance u0_meet : meetOp u0 := fun A B => u0_meet0 (elcode0 A) (elcode0 B).


  #[global]
  Instance u0_has_meet : isMeet u0_ppo.
  Proof.
    constructor.
    1,2: (do 2 elim/u0selim=>[||||?|????])=> /invert /invert; cbn;
                                           try constructor; move=> // [//].
    (do 3 elim/u0selim=>[||||?|????])=> /invert /invert; cbn ; try constructor=> //.
    split; intuition.
  Qed.


  Equations u0_join0 {A} (cA : code0 A) {B} (cB : code0 B) : u0 :=
  | cunk0, _ => unku0
  | _, cunk0 => unku0
  | carr0 _ _ _, _ => unku0
  | _, carr0 _ _ _ => unku0
  | cbox0 p, cbox0 q => boxu0 (p s\/ q)
  | cerr0, x => mkU0 x
  | x, cerr0 => mkU0 x
  | cbox0 _, _ => unku0
  | _ , cbox0 _ => unku0
  | cbool0, cprop0 => unku0
  | cprop0, cbool0 => unku0
  | x, _ => mkU0 x.

  #[global]
  Instance u0_join : joinOp u0 := fun A B => u0_join0 (elcode0 A) (elcode0 B).

  #[global]
  Instance u0_has_join : isJoin u0_ppo.
  Proof.
    constructor.
    1,2: (do 2 elim/u0selim=>[||||?|????])=> /invert /invert; constructor=> // ; by constructor.
    (do 3 elim/u0selim=>[||||?|????])=> /invert /invert ; try constructor=> //; by case.
  Qed.


  (** Indexed partial preorder structure on el0 *)


  Definition fwc_u0@{} (A : Type@{u}) (B : A -> u0) : fwc@{v v} A :=
    mkFwC (fun a => el0 (B a))
          (fun ax ay => @el0_upcast@{v} (B ax) (B ay))
          (fun ax ay => @el0_downcast@{v} (B ax) (B ay)).

  Definition prec_el0@{} (A : u0) : srel (el0 A).
  Proof.
    elim/u0elim@{u}: A=> [||||P|A B ihA ihB].
    - exact prec_empty.
    - exact prec_unk0.
    - exact prec_bool.
    - exact prec_prop.
    - exact (@prec_box_ P).
    - refine (prec_pi0 ihA (fwc_u0 B) ihB).
  Defined.

  Definition el0_fwc : fwc@{v v} u0 :=
    mkFwC el0 (@el0_upcast) (@el0_downcast).

  Definition prec_el0' (A : u0) : srel (el0_fwc@{v} A) := prec_el0 A.

  Hint Extern 0 (Err (@family _ el0_fwc _)) => apply: err0_at : typeclass_instances.

  Hint Extern 0 (Dyn (@family _ el0_fwc _)) => apply: dyn0_at : typeclass_instances.


  Definition is_ppo_at (A : u0) (AA : prec_u0 A A) : is_ppo (prec_el0 A).
  Proof.
    elim/u0selim: A AA; cbn=> *.
    6: match goal with [ H : prec_u0 _ _ |- _ ] => try sabsurd H end.
    - exact empty_ppo.
    - exact unk0_ppo.
    - exact bool_ppo.
    - exact prop_ppo.
    - exact (box_ppo _).
  Defined.

  Let el0_at {A} AA := mkPpo (@is_ppo_at A AA).

  Definition smallest0_at@{} (A : u0) (AA : prec_u0 A A) :
    SmallestElem@{u} (el0_at AA).
  Proof.
    elim/u0selim: A AA => [||||?|????] /= H; last sabsurd H.
    all: cbn; typeclasses eauto.
  Qed.

  (* Alternative universe specification *)
  Definition smallest0_at'@{u' | u < u' +} (A : u0) (AA : prec_u0 A A) :
    SmallestElem@{u'} (el0_at AA).
  Proof.
    elim/u0selim: A AA => [||||?|????] /= H; last sabsurd H.
    all: cbn; try typeclasses eauto.
    apply: box_smallest. (* typeclass problem ? maybe put a hint by hand *)
  Qed.

  Definition greatest0_at@{} (A : u0) (AA : prec_u0 A A) :
    GreatestElem@{u} (el0_at AA).
  Proof.
    elim/u0selim: A AA => [||||?|????] /= H; last sabsurd H.
    all: cbn; typeclasses eauto.
  Qed.

  Definition el0_on_is_eppair@{} (A B : u0) (AB : u0_ppo A B) :
    eppair_prop@{u} (el0_at (prec_u0_lrefl AB)) (el0_at (prec_u0_urefl AB))
               el0_upcast@{v} el0_downcast@{v}.
  Proof.
    elim/u0selim: A B AB=> [||||?|?? _ _]; elim/u0selim=> [||||?|?? _ _] /= H.
    all: try sabsurd H.
    all: try refine (ideppair@{u} _).
    - exact (@from_empty_eppair@{u} _ _ _ empty_smallest).
    - exact (@from_empty_eppair@{u} _ _ _ unk0_smallest).
    - exact (@from_empty_eppair@{u} _ _ _ bool_smallest).
    - exact (@from_empty_eppair@{u} _ _ _ prop_smallest).
    - exact (@from_empty_eppair@{u} _ _ _ (box_smallest _)).
    - exact (eppair_to_unk0@{u} hbool0@{u}).
    - exact (eppair_to_unk0@{u} hprop0@{u}).
    - exact (cast_box_is_eppair@{u} _ (A:=_)).
    - cbn; exact (cast_box_is_eppair@{u} _ (A:=_)).
  Defined.

  Definition el0_on@{} (A B : u0) (AB : u0_ppo A B) : eppair@{u} (el0_at ⌊AB⌋) (el0_at ⌈AB⌉) :=
    pack_eppair (el0_on_is_eppair AB).


  Lemma el0_trans0 A B C (AB : u0_ppo A B) (BC : u0_ppo B C) (AC : u0_ppo A C) :
    compeppair (el0_on AB) (el0_on BC) ≃ el0_on AC.
  Proof.
    move: C BC AC; induction AB as [B0 spB0| A0 spA0 | | |?? pq]=> C BC AC.
    1:move=> *; apply: unicity_empty_eppair.
    2-3: inversion BC; apply: equiveppair_helper_up=> ??;
       apply: sym_refl; by apply: (upmon (el0_on BC)).
    - inversion BC; subst; apply: equiveppair_helper_up=> ??.
      apply: sym_refl; by apply: (upmon (el0_on AC)).
    - inversion BC; subst;apply: equiveppair_helper_up=> ??; cbn -[el0_at].
      all: apply: sym_refl=> // ; constructor=> //.
  Qed.

  Lemma el0_trans A B C (AB : u0_ppo A B) (BC : u0_ppo B C)  :
    equiveppair (compeppair (el0_on AB) (el0_on BC)) (el0_on (prec_u0_trans AB BC)).
  Proof. apply el0_trans0. Qed.

  Lemma el0_refl0 (A : u0) (AA : u0_ppo A A) :
    equiveppair (el0_on AA) (ideppair (el0_at AA)).
  Proof.
    elim/u0selim: A AA=> [||||?|????] AA; inversion AA.
    all: apply: equiveppair_refl.
  Qed.

  (** Term precision form an ippo structure on the first universe *)
  (* first part of Theorem 9 *)

  Definition El0_ippo : ippo U0 el0_fwc :=
    mkIppo u0_ppo el0_fwc prec_el0 is_ppo_at
           el0_on_is_eppair el0_trans el0_refl0.


  (** Properties of cast between self-precise types at u0 *)

  Lemma el0_up_down_sym_eq_sp (A B : u0) (AA : prec_u0 A A) (a : el0 A) :
    fupcast el0_fwc@{v} A B a = fdowncast el0_fwc@{v} B A a.
  Proof.
    elim/u0elim@{v}: A  B a AA=> [||||P|A B ihA ihB]; elim/u0elim=> [||||P'|A' B' _ _] //= ? h.
    sabsurd h.
  Qed.

  Lemma el0_upcast_join_eq (A B : u0) (AA : U0 A A) (BB : U0 B B)
        (a : el0 A)
        (X := A ⊔ B)
        (AX := join_greater_l AA BB)
        (BX := join_greater_r AA BB)
    :
      @el0_upcast A B a = downcast (el0_on BX) (upcast (el0_on AX) a).
  Proof.
    elim/u0elim: A B AA BB a {X}AX BX=> [||||P|Adom Acod ihAdom ihAcod];
    elim/u0elim=> [||||Q|Bdom Bcod ihBdom ihBcod]  //= AA BB a AX BX.
    sabsurd AA.
  Qed.

  Lemma el0_downcast_join_eq (A B : u0) (AA : U0 A A) (BB : U0 B B)
        (a : el0 A)
        (X := A ⊔ B)
        (AX := join_greater_l AA BB)
        (BX := join_greater_r AA BB)
    :
      @el0_downcast B A a = downcast (el0_on BX) (upcast (el0_on AX) a).
  Proof.
    elim/u0elim: A B AA BB a {X}AX BX=> [||||P|Adom Acod ihAdom ihAcod];
    elim/u0elim=> [||||Q|Bdom Bcod ihBdom ihBcod]  //= AA BB a AX BX.
    sabsurd AA.
  Qed.

  Lemma el0_cast_join (A B : u0) (AA : U0 A A) (BB : U0 B B)
        (a : el0 A) (aa : el0_at AA a a)
        (X := A ⊔ B)
        (AX := join_greater_l AA BB)
        (BX := join_greater_r AA BB)
    :
      @el0_upcast A B a ≈[el0_at BB] downcast (el0_on BX) (upcast (el0_on AX) a).
  Proof.
    rewrite (el0_upcast_join_eq AA BB) ; split; apply: downmon; apply: upmon=> //.
  Qed.


  Lemma el0_upcast_above (A B X : u0) (AX : U0 A X) (BX : U0 B X)
        (a : el0 A) (aa : el0_at ⌊AX⌋ a a) :
      @el0_upcast A B a ≈[el0_at ⌊BX⌋] downcast (el0_on BX) (upcast (el0_on AX) a).
  Proof.
    apply: sym_trans; first (apply: el0_cast_join; eassumption).
    pose proof (ABX := join_greatest AX BX).
    refine (ippo_cast_equiv_above El0_ippo _ _ _ _ ABX _ _ _ aa).
  Qed.

  Lemma el0_up_down_sym (A B : u0) (AA : prec_u0 A A) (BB : prec_u0 B B)
        (a : el0 A) (aa : el0_at AA a a) :
    @el0_downcast B A a ≈[el0_at BB] @el0_upcast A B a.
  Proof.
    rewrite (el0_upcast_join_eq AA BB) (el0_downcast_join_eq AA BB).
    apply: sym_refl; apply: downmon; by apply: upmon.
  Qed.

  Lemma el0_downcast_above (A B X : u0) (AX : U0 A X) (BX : U0 B X)
        (a : el0 A) (aa : el0_at ⌊AX⌋ a a) :
      @el0_downcast B A a ≈[el0_at ⌊BX⌋] downcast (el0_on BX) (upcast (el0_on AX) a).
  Proof.
    rewrite (el0_downcast_join_eq ⌊AX⌋ ⌊BX⌋).
    unshelve apply: (ippo_cast_equiv_above El0_ippo); last eassumption.
    by apply: join_greatest.
  Qed.


  (** Beck-Chevalley is satisfied for term precision in the first universe *)
  (* This hold only because we do not have dependent function type yet *)

  Theorem BeckChevalley_u0 :
    forall (A B Y : u0)
      (AY : u0_ppo A Y)
      (BY : u0_ppo B Y)
      (X := A ⊓ B)
      (AA := prec_u0_lrefl AY)
      (BB := prec_u0_lrefl BY)
      (XA := meet_smaller_l AA BB)
      (XB := meet_smaller_r AA BB),
    mate_invertible El0_ippo X A B Y AA BB XA AY XB BY.
  Proof.
    (do 3 elim/u0selim=>[||||?|????])=> /invert /invert; subst; red; cbn=> *.
    all: solve [contradiction|constructor=> //|intuition|case| apply: prec_bool_refl| apply: id].
  Qed.

  Lemma el0_upcast_unk_eq@{} [A B : u0] (AA : U0 A A) (BB : U0 B B) (a : el0 A) :
    let Aunk := unk_any _ (prec_u0_sp_l AA) in
    let Bunk := unk_any _ (prec_u0_sp_l BB) in
    el0_upcast@{v} a = downcast (el0_on Bunk) (upcast (el0_on Aunk) a).
  Proof.
    elim/u0elim: A B AA BB a => [||||P|Adom Acod ihAdom ihAcod];
    elim/u0elim=> [||||Q|Bdom Bcod ihBdom ihBcod]  //= AA BB a.
    sabsurd AA.
  Qed.

  Lemma el0_downcast_unk_eq@{} [A B : u0] (AA : U0 A A) (BB : U0 B B) (a : el0 A) :
    let Aunk := unk_any _ (prec_u0_sp_l AA) in
    let Bunk := unk_any _ (prec_u0_sp_l BB) in
    el0_downcast@{v} a = downcast (el0_on Bunk) (upcast (el0_on Aunk) a).
  Proof.
    elim/u0elim: A B AA BB a => [||||P|Adom Acod ihAdom ihAcod];
    elim/u0elim=> [||||Q|Bdom Bcod ihBdom ihBcod]  //= AA BB a.
    sabsurd AA.
  Qed.

  Existing Instance smallest0_at'.

  (** Term precision on the first universe satisfy the hypotheses to build
      an ? in the second universe *)
  (* Note that we actually want these hypotheses on type precision,
     see below (deifnition univ0_pic_UH) *)

  Definition univ0_UH : unknown.unknown_hypotheses El0_ippo.
  Proof.
    (* General idea: all heterogeneous precision relations between elements
       can be reduced to precision at unknown *)
    constructor.
    - move=> A0 A1 A00 A11 a aerr.
      cbn -[ippo_at]; rewrite (el0_downcast_join_eq A00 A11).
      apply: trans; last apply: downcast_err.
      3: apply: smallest0_at'.
      apply: downmon; apply: downtoup; first apply: smallest_refl.
      apply: (trans aerr); apply: smaller; apply: downmon; apply: smallest_refl.
    - move=> A0 A1 A00 A11 a adyn. apply: greater; first apply: greatest0_at.
      cbn -[ippo_at]; rewrite (el0_downcast_join_eq A00 A11).
      apply: downmon; apply: upmon; apply: lower_refl; eassumption.
    - move=> A0 A00 A1 A11 A2 A22 a2 a22.
      cbn -[ippo_at].
      rewrite (el0_downcast_unk_eq A22 A00)
              (el0_downcast_unk_eq A22 A11)
              (el0_downcast_unk_eq A11 A00).
      apply: downmon; apply: counit; by apply: upmon.
    - move=> A B0 B1 AA B01 a aa.
      cbn -[ippo_at].
      rewrite (el0_downcast_unk_eq AA ⌊B01⌋) (el0_downcast_unk_eq AA ⌈B01⌉).
      pose proof (unk_any _ (prec_u0_sp_l B01)).
      pose proof (unk_any _ (prec_u0_sp_r B01)).
      (unshelve apply: iprec_dr)=> //.
      (unshelve apply: iprec_dl)=> //.
      constructor=> //.
      apply: prec_iprec; by apply: upmon.
    - move=> A0 A1 B A00 A11 BB a0 a1 a01.
      cbn -[ippo_at].
      rewrite (el0_downcast_unk_eq A00 BB) (el0_downcast_unk_eq A11 BB).
      apply: downmon.
      refine (@iprec_refl _ _ _ El0_ippo _ (unk_any unku0 stt) _ _ _).
      unshelve apply: iprec_ul.
      1,2: constructor; apply: prec_u0_sp_l; eassumption.
      apply: to_eprel'.
      2: apply: upmon; apply: (hrel_urefl a01).
      move: (hrel_downcast a01); cbn -[ippo_at].
      by rewrite (el0_downcast_unk_eq A11 A00).
    - move=> A0 A1 a0 a1 A00 A11 a01 a11.
      repeat split=> //.
      1: exact (lower_refl a01).
      cbn; rewrite (el0_upcast_unk_eq A00 A11).
      change (prec_el0 A1 ?x ?y) with (El0_ippo A1 A11 x y).
      pose proof (A1unk := unk_any _ (prec_u0_sp_l A11)).
      apply: trans; last exact (retract (el0_on A1unk) _ a11).
      apply: downmon. apply: downtoup.
      1: by apply: upmon.
      move: a01; cbn; by rewrite (el0_downcast_unk_eq A11 A00).
    - move=> ??; apply: smaller; apply: greatest_refl; apply: greatest0_at.
  Qed.


  (** Type precision at level 0 *)
  (* The rest of this file is reserved to the definition type precision at level
  0 and the proof of its properties *)
  (* The dependency present in dependent products makes this process much more
     intricate and we need to prove most properties in a single involved
     well-founded induction. The intuition behind the chosen well-founded
     relation is that we want to consider order relationships between elements A
     of u0, pairs of elements (B1, B2) of u0 and triple (C1, C2, C3) of elements
     to define the properties of the relation on el0 A, on the pair of casts
     between B1 and B2 and on the composition of these casts between C1,C2 and
     C3. Hence, we use a custom well-founded relation than can be understood as
     a poor man multiset extension of the subterm ordering on codes. *)

  (** Helper lemmas for strong induction on singleton, pairs and triples
      of codes using a custom (well-founded) relation *)

  Section PackRel.

    (* Trying to do a proof by well founded induction below on a relation *)
    (*    relating elements from u0 with pairs and triples of elements *)
    (*    in order to prove properties on a single type as well as *)
    (*    eppairs and composition of these *)

    Context (A : Type@{v}) (ltA : A -> A -> Prop).


    Inductive PackUpTo3 : Type@{v} :=
    | packGround : A -> PackUpTo3
    | pack1 : A -> PackUpTo3
    | pack2 : A -> A -> PackUpTo3
    | pack3 : A -> A -> A -> PackUpTo3.

    (* This horrible relation packs up the dependency order used
       to prove properties on prec_u0_pic, the closure of prec_u0 under pi *)
    Inductive ltPackUpTo3 : PackUpTo3 -> PackUpTo3 -> Prop :=
    | put3_dec3 Y1 Y2 Y3 X : ltA Y1 X -> ltA Y2 X -> ltA Y3 X -> ltPackUpTo3 (pack3 Y1 Y2 Y3) (packGround X)
    | put3_dec1 Y1 X : ltA Y1 X -> ltPackUpTo3 (pack1 Y1) (packGround X)
    | put3_par_dec Y1 Y2 X1 X2 : ltA Y1 X1 -> ltA Y2 X2 ->
                                 ltPackUpTo3 (pack2 Y1 Y2) (pack2 X1 X2)
    | put2_par_dec_21 Y11 Y12 Y2 X1 X2 :
      ltA Y11 X1 -> ltA Y12 X1 -> ltA Y2 X2 ->
      ltPackUpTo3 (pack3 Y11 Y12 Y2) (pack2 X1 X2)
    | put2_par_dec_12 Y1 Y21 Y22 X1 X2 :
      ltA Y1 X1 -> ltA Y21 X2 -> ltA Y22 X2 ->
      ltPackUpTo3 (pack3 Y1 Y21 Y22) (pack2 X1 X2)
    | put3_par33_dec Y1 Y2 Y3 X1 X2 X3 :
      ltA Y1 X1 -> ltA Y2 X2 -> ltA Y3 X3 ->
      ltPackUpTo3 (pack3 Y1 Y2 Y3) (pack3 X1 X2 X3)
    (* Using simplicial conventions: the last index corresponds
       to the component *not* used *)
    | put3_face21_1 Y1 Y2 : ltPackUpTo3 (packGround Y2) (pack2 Y1 Y2)
    | put3_face21_2 Y1 Y2 : ltPackUpTo3 (packGround Y1) (pack2 Y1 Y2)
    | put3_degen12 Y1 : ltPackUpTo3 (pack2 Y1 Y1) (pack1 Y1)
    | put3_face32_1 Z1 Z2 Z3 : ltPackUpTo3 (pack2 Z2 Z3) (pack3 Z1 Z2 Z3)
    | put3_face32_2 Z1 Z2 Z3 : ltPackUpTo3 (pack2 Z1 Z3) (pack3 Z1 Z2 Z3)
    | put3_face32_3 Z1 Z2 Z3 : ltPackUpTo3 (pack2 Z1 Z2) (pack3 Z1 Z2 Z3)
    | put3_face31_23 Z1 Z2 Z3 : ltPackUpTo3 (pack1 Z1) (pack3 Z1 Z2 Z3)
    | put3_face31_13 Z1 Z2 Z3 : ltPackUpTo3 (pack1 Z2) (pack3 Z1 Z2 Z3)
    | put3_face31_12 Z1 Z2 Z3 : ltPackUpTo3 (pack1 Z3) (pack3 Z1 Z2 Z3)
    .

    Context `{wA : WellFounded _ ltA}.
    Context (transA : transitive ltA).

    Definition len123 (l : list A) :=
      let n := length l in match n with | 1 | 2 | 3 => sUnit | _ => sEmpty end.

    Definition list123 := blist A len123.

    Definition list123_to_pack : list123 -> PackUpTo3.
    Proof.
      move=> [[|a1 [| a2 [| a3 l ]]] h]; first sabsurd h.
      - exact (packGround a1).
      - exact (pack2 a1 a2).
      - exact (pack3 a1 a2 a3).
    Defined.

    Instance: WellFounded (bcart A ltA len123) := (bcart_wf A ltA len123).

    Open Scope list_scope.
    Import List.ListNotations.

    Arguments cart_cover2 [_ _] _ [_] _ [_] _ _.
    Arguments cart_cover3 [_ _] _ [_] _ [_] _ [_] _ _ _.


    Lemma ltPackUpTo3Acc_aux : forall bl, Acc ltPackUpTo3 (list123_to_pack bl).
    Proof.
      apply: FixWf=> -[[|a1 [| a2 [| a3 l ]]] h] ih /=; first sabsurd h.
      - constructor=> ? /invert; subst.
        + apply: (ih ⦑ _ :: _ :: _ :: nil ⦒).
          apply: cart_cover; do 3 (constructor=> //).
        + constructor=> ? /invert; subst.
          apply: (ih ⦑ _ :: _ :: nil ⦒).
          apply: cart_cover; do 2 (constructor=> //).
      - constructor=> ? /invert; subst.
        4,5: apply: (ih ⦑ _ :: nil ⦒); apply: cart_ssub;
             split=> //; repeat constructor.
        + apply: (ih ⦑ _ :: _ :: nil ⦒).
          apply: (cart_cover2 [_] [_]); by constructor.
        + apply: (ih ⦑ _ :: _ :: _ :: nil ⦒).
          apply: (cart_cover2 [_ ; _] [_]); repeat (constructor=> //).
        + apply: (ih ⦑ _ :: _ :: _ :: nil ⦒).
          apply: (cart_cover2 [_] [_;_]); repeat (constructor=> //).
      - destruct l; last sabsurd h.
        constructor=> ? /invert; subst.
        2-4: apply: (ih ⦑ _ :: _ :: nil ⦒); apply: cart_ssub;
            split=> //; repeat constructor.
        + apply: (ih ⦑ _ :: _ :: _ :: nil ⦒).
          apply: (cart_cover3 [_] [_] [_]); repeat (constructor=> //).
        + constructor=> ? /invert; subst.
          constructor=> ? /invert; subst.
          * apply: (ih ⦑ _ :: _ :: nil ⦒).
            apply: (cart_cover3 [_ ; _] [] []); repeat (constructor=> //).
          * apply: (ih ⦑ _ :: _ :: _ :: nil ⦒).
            apply: (cart_cover3 [_ ; _ ; _] [] []); repeat (constructor=> //).
          * apply: (ih ⦑ _ :: _ :: _ :: nil ⦒).
            apply: (cart_cover3 [_ ; _ ; _] [] []); repeat (constructor=> //).
          * apply: (ih ⦑ _ :: nil ⦒).
            apply: cart_ssub; split=> //; repeat constructor.
          * apply: (ih ⦑ _ :: nil ⦒).
            apply: cart_ssub; split=> //; repeat constructor.
        + constructor=> ? /invert; subst.
          constructor=> ? /invert; subst.
          * apply: (ih ⦑ _ :: _ :: nil ⦒).
            apply: (cart_cover3 [] [_ ; _] []); repeat (constructor=> //).
          * apply: (ih ⦑ _ :: _ :: _ :: nil ⦒).
            apply: (cart_cover3 [] [_ ; _ ; _] []); repeat (constructor=> //).
          * apply: (ih ⦑ _ :: _ :: _ :: nil ⦒).
            apply: (cart_cover3 [] [_ ; _ ; _] []); repeat (constructor=> //).
          * apply: (ih ⦑ _ :: nil ⦒).
            apply: cart_ssub; split=> //; repeat constructor.
          * apply: (ih ⦑ _ :: nil ⦒).
            apply: cart_ssub; split=> //; repeat constructor.
        + constructor=> ? /invert; subst.
          constructor=> ? /invert; subst.
          * apply: (ih ⦑ _ :: _ :: nil ⦒).
            apply: (cart_cover3 [] [] [_ ; _]); repeat (constructor=> //).
          * apply: (ih ⦑ _ :: _ :: _ :: nil ⦒).
            apply: (cart_cover3 [] [] [_ ; _ ; _]); repeat (constructor=> //).
          * apply: (ih ⦑ _ :: _ :: _ :: nil ⦒).
            apply: (cart_cover3 [] [] [_ ; _ ; _]); repeat (constructor=> //).
          * apply: (ih ⦑ _ :: nil ⦒).
            apply: cart_ssub; split=> //; repeat constructor.
          * apply: (ih ⦑ _ :: nil ⦒).
            apply: cart_ssub; split=> //; repeat constructor.
      Unshelve. all: done.
    Qed.

    Lemma ltPackUpTo3Acc : well_founded ltPackUpTo3.
    Proof.
      move=> [?|?|??|???].
      - apply: (ltPackUpTo3Acc_aux ⦑ _ :: nil ⦒)=> //.
      - constructor=> ? /invert; apply: (ltPackUpTo3Acc_aux ⦑ _ :: _ :: nil ⦒)=> //.
      - apply: (ltPackUpTo3Acc_aux ⦑ _ :: _ :: nil ⦒)=> //.
      - apply: (ltPackUpTo3Acc_aux ⦑ _ :: _ :: _ :: nil ⦒)=> //.
    Qed.

    Definition ltPackUpTo3ext := Relation_Operators.clos_trans PackUpTo3 ltPackUpTo3.

    #[global]
    Instance ltPackUpTo3ext_wf : WellFounded ltPackUpTo3ext.
    Proof. apply: WellFounded_trans_clos. apply: ltPackUpTo3Acc. Qed.

    Lemma put3_face31_23' Y1 Y2 Y3 : ltPackUpTo3ext (packGround Y1) (pack3 Y1 Y2 Y3).
    Proof.
      apply: Relation_Operators.t_trans; apply: Relation_Operators.t_step.
      1: exact (put3_face21_2 Y1 Y2).
      constructor.
    Qed.

    Lemma put3_face31_13' Y1 Y2 Y3 : ltPackUpTo3ext (packGround Y2) (pack3 Y1 Y2 Y3).
    Proof.
      apply: Relation_Operators.t_trans; apply: Relation_Operators.t_step.
      1: exact (put3_face21_1 Y1 Y2).
      constructor.
    Qed.

    Lemma put3_face31_12' Y1 Y2 Y3 : ltPackUpTo3ext (packGround Y3) (pack3 Y1 Y2 Y3).
    Proof.
      apply: Relation_Operators.t_trans; apply: Relation_Operators.t_step.
      1: exact (put3_face21_1 Y1 Y3).
      constructor.
    Qed.

    Lemma put3_face11 Y : ltPackUpTo3ext (packGround Y) (pack1 Y).
    Proof.
      apply: Relation_Operators.t_trans; apply: Relation_Operators.t_step.
      1: exact (put3_face21_1 Y Y).
      constructor.
    Qed.

    Lemma put3_ground X Y : ltA X Y -> ltPackUpTo3ext (packGround X) (packGround Y).
    Proof.
      move=> XY; apply: Relation_Operators.t_trans; first apply: put3_face11.
      by do 2 constructor.
    Qed.

    Lemma put3_dec2 Y1 Y2 X : ltA Y1 X -> ltA Y2 X -> ltPackUpTo3ext (pack2 Y1 Y2) (packGround X).
    Proof.
      move=> YX1 YX2 ; apply: Relation_Operators.t_trans.
      2: constructor; refine (put3_dec3 YX1 YX1 YX2).
      do 2 constructor.
    Qed.

  End PackRel.

  (* Inversion for dependent products *)
  Inductive u0_direct_subterm : u0 -> u0 -> Prop :=
  | u0_dir_sub_pi_dom A B : u0_direct_subterm A (piu0 A B)
  | u0_dir_sub_pi_cod A B (a : el0 A) : u0_direct_subterm (B a) (piu0 A B).

  Definition piu0_dom (A : u0) : u0 :=
    match elcode0 A with
    | carr0 Ac _ _ => mkU0 Ac
    | _ => A
    end.

  Definition piu0_cod (A : u0) : el0 (piu0_dom A) -> u0 :=
    match elcode0 A as Ac return el0 (piu0_dom (mkU0 Ac)) -> u0 with
    | carr0 _ _ Bc => fun a => mkU0 (Bc a)
    | _ => fun _ => A
    end.

  Lemma u0_direct_subterm_inv X Y :
    u0_direct_subterm X Y -> X = piu0_dom Y \/ exists a : el0 (piu0_dom Y), X = piu0_cod Y a.
  Proof.
    move=> /invert; subst; first by left.
    right; unshelve eexists; first eassumption; done.
  Qed.

  Lemma u0_direct_subterm_wf : well_founded u0_direct_subterm.
  Proof.
    elim/u0elim=>[||||?|A B ihA ihB]; constructor.
    1-5: move=> ? /invert.
    move=> ? /u0_direct_subterm_inv [-> | [a ->]] //.
  Qed.

  Definition u0_subterm :=
    Relation_Operators.clos_trans u0 u0_direct_subterm.

  (** The subterm relation on codes is well-founded *)
  Instance u0_subterm_wf : WellFounded u0_subterm.
  Proof. apply: WellFounded_trans_clos. apply: u0_direct_subterm_wf. Qed.


  (** SProp valued fixpoint by well-founded induction *)
  Definition sFixWf (A : Type@{v}) (R : A -> A -> Prop) `{WellFounded A R} (P : A -> SProp) (ih : forall x : A, (forall y : A, R y x -> P y) -> P x) : forall x : A, P x.
  Proof.
    move=> x; apply: unbox; move: x.
    apply: FixWf=> x ih'; constructor; apply: ih.
    move=> ??; apply: unbox; by apply: ih'.
  Qed.



  (** Type precision on the first universe *)
  (* Closure of term precision by dependent product *)

  Inductive prec_u0_pic@{} : srel u0 :=
  (* Err is smaller than dependent products *)
  | err_pi0 (A : u0) (B : el0 A -> u0) :
    prec_u0_pic A A ->
    monotone@{v} (prec_el0 A) prec_u0_pic B ->
    prec_u0_pic err (piu0 A B)

  (* Dependent products are congruent *)
  | picong0 (A A' : u0) (B : el0 A -> u0) (B' : el0 A' -> u0) :
    prec_u0_pic A A' ->
    monotone@{v} (prec_el0 A) prec_u0_pic B ->
    monotone@{v} (prec_el0 A') prec_u0_pic B' ->
    (forall a a', i[prec_el0'] A A' a a' -> prec_u0_pic (B a) (B' a')) ->
    prec_u0_pic (piu0 A B) (piu0 A' B')

  (* Injection of term precision in type precision *)
  | inj_prec_u0 A B : u0_ppo A B -> prec_u0_pic A B.

  Lemma prec_el0_pi_unfold A B :
    prec_el0 (piu0 A B) = prec_pi0 (prec_el0 A) (fwc_u0 B) (fun a => prec_el0 (B a)).
  Proof. reflexivity. Qed.

  (** Inversion lemmas on type precision *)

  Lemma prec_u0_pic_inv_pi_pi A0 A1 B0 B1 :
    prec_u0_pic (piu0 A0 B0) (piu0 A1 B1) ->
    prec_u0_pic A0 A1 s/\
      monotone (prec_el0 A0) prec_u0_pic B0 s/\
      monotone (prec_el0 A1) prec_u0_pic B1 s/\
      (forall a0 a1, i[prec_el0'] A0 A1 a0 a1 -> prec_u0_pic (B0 a0) (B1 a1)).
  Proof.
    move=> h;  split; first split; first split.
    - refine (match  h in prec_u0_pic X Y return
                   match elcode0 X, elcode0 Y with
                   | carr0 _ _ _, carr0 _ _ _ => prec_u0_pic (piu0_dom X) (piu0_dom Y)
                   | _, _ => sUnit
                   end
            with
            | err_pi0 _ _ => _
            | _ => _
            end)=> //.
      move: u u0 p; (do 2 elim/u0selim=> [||||?|????])=> //= /invert; contradiction.
    - refine (match  h in prec_u0_pic X Y return
                   match elcode0 X, elcode0 Y with
                   | carr0 _ _ _, carr0 _ _ _ =>
                       monotone (prec_el0 (piu0_dom X)) prec_u0_pic (piu0_cod X)
                   | _, _ => sUnit
                   end
            with
            | err_pi0 _ _ => _
            | _ => _
            end)=> //.
      move: u u0 p; (do 2 elim/u0selim=> [||||?|????])=> //= /invert; contradiction.
    - refine (match  h in prec_u0_pic X Y return
                   match elcode0 X, elcode0 Y with
                   | carr0 _ _ _, carr0 _ _ _ =>
                       monotone (prec_el0 (piu0_dom Y)) prec_u0_pic (piu0_cod Y)
                   | _, _ => sUnit
                   end
            with
            | err_pi0 _ _ => _
            | _ => _
            end)=> //.
      move: u u0 p; (do 2 elim/u0selim=> [||||?|????])=> //= /invert; contradiction.
    - refine (match  h in prec_u0_pic X Y return
                   match elcode0 X, elcode0 Y with
                   | carr0 _ _ _, carr0 _ _ _ =>
                       forall a0 a1, i[prec_el0'] (piu0_dom X) (piu0_dom Y) a0 a1 -> prec_u0_pic (piu0_cod X a0) (piu0_cod Y a1)
                   | _, _ => sUnit
                   end
            with
            | err_pi0 _ _ => _
            | _ => _
            end)=> //.
      move: u u0 p; (do 2 elim/u0selim=> [||||?|????])=> //= /invert; contradiction.
  Qed.

  Lemma prec_u0_pic_inv_pi_any A0 B0 X1 :
    prec_u0_pic (piu0 A0 B0) X1 ->
    { A1 : u0 & {
      B1 : el0 A1 -> u0 &
      ((X1 = piu0 A1 B1) *
      Box (
        prec_u0_pic A0 A1 s/\
        monotone (prec_el0 A0) prec_u0_pic B0 s/\
        monotone (prec_el0 A1) prec_u0_pic B1 s/\
        (forall a0 a1, i[prec_el0'] A0 A1 a0 a1 -> prec_u0_pic (B0 a0) (B1 a1))))%type }}.
  Proof.
    elim/u0elim: X1=> /=.
    6:{ move=> A1 B1 _ _; exists A1; exists B1; split=> //; constructor.
        by apply: prec_u0_pic_inv_pi_pi. }
    5: move=> ?.
    all: move=> h; sexfalso; inversion h;
               match goal with
               | [H : _ (piu0 _ _) _ |- _] => inversion H
               end; contradiction.
  Qed.

  Lemma prec_u0_pic_inv_err_pi A B :
    prec_u0_pic err (piu0 A B) ->
    prec_u0_pic A A s/\ monotone (prec_el0 A) prec_u0_pic B.
  Proof.
    move=> h; split.
    - refine (match  h in prec_u0_pic X Y return
                   match elcode0 X, elcode0 Y with
                   | cerr0, carr0 _ _ _ => prec_u0_pic (piu0_dom Y) (piu0_dom Y)
                   | _, _ => sUnit
                   end
            with
            | err_pi0 _ _ => _
            | _ => _
            end)=> //.
      move: u u0 p; (do 2 elim/u0selim=> [||||?|????])=> //= /invert; contradiction.
    - refine (match  h in prec_u0_pic X Y return
                   match elcode0 X, elcode0 Y with
                   | cerr0, carr0 _ _ _ =>
                       monotone (prec_el0 (piu0_dom Y)) prec_u0_pic (piu0_cod Y)
                   | _, _ => sUnit
                   end
            with
            | err_pi0 _ _ => _
            | _ => _
            end)=> //.
      move: u u0 p; (do 2 elim/u0selim=> [||||?|????])=> //= /invert; contradiction.
  Qed.

  Lemma prec_u0_pic_inv_diag (X : u0) :
    prec_u0_pic X X ->
    Box (prec_u0 X X) +
      { A : u0 &
      { B : el0 A -> u0 &
      ((X = piu0 A B) * Box (
        prec_u0_pic A A s/\
        monotone (prec_el0 A) prec_u0_pic B))%type }}.
  Proof.
    elim/u0elim: X => [||||?|A B ??].
    1-5: move=> h; left; constructor; by inversion h.
    move=> h; right; exists A; exists B; split=> //; constructor.
    move: (prec_u0_pic_inv_pi_pi h)=> [[? _] _] //.
  Qed.

  Definition is_el0_eppair [A B : u0] (pA : is_ppo (prec_el0' A))
             (pB : is_ppo (prec_el0' B)) :=
    eppair_prop (mkPpo pA) (mkPpo pB) el0_upcast@{v} el0_downcast@{v}.

  (* tbp = To Be Proven (Originality Inside®)
     Predicate for the main theorem proving that type precision
     underly an ippo *)
  Definition tbp (x : PackUpTo3 u0) : SProp :=
    match x with
    | packGround A =>
          prec_u0_pic A A ->
          sΣ (pA : is_ppo (prec_el0' A)), @SmallestElem _ (mkPpo pA) (err0_at A)
    | pack2 A0 A1 =>
        forall (A01 : prec_u0_pic A0 A1)
          (A00 : prec_u0_pic A0 A0)
          (A11 : prec_u0_pic A1 A1)
          (pA0 : is_ppo (prec_el0' A0))
          (pA1 : is_ppo (prec_el0' A1)),
          is_el0_eppair pA0 pA1
    | pack1 A0 =>
        forall (A00 : prec_u0_pic A0 A0)
          (pA0 : is_ppo (prec_el0' A0))
          (epA00 : is_el0_eppair pA0 pA0),
          pack_eppair epA00 ≃ ideppair (mkPpo pA0)
    | pack3 A0 A1 A2 =>
        forall (A01 : prec_u0_pic A0 A1)
          (A02 : prec_u0_pic A0 A2)
          (A12 : prec_u0_pic A1 A2)
          (A00 : prec_u0_pic A0 A0)
          (A11 : prec_u0_pic A1 A1)
          (A22 : prec_u0_pic A2 A2)
          (pA0 : is_ppo (prec_el0' A0))
          (pA1 : is_ppo (prec_el0' A1))
          (pA2 : is_ppo (prec_el0' A2))
          (epA01 : is_el0_eppair pA0 pA1)
          (epA02 : is_el0_eppair pA0 pA2)
          (epA12 : is_el0_eppair pA1 pA2),
          compeppair (pack_eppair epA01) (pack_eppair epA12) ≃ pack_eppair epA02
    end.

  Lemma u0_sub_dom A B : u0_subterm A (piu0 A B).
  Proof. do 2 constructor. Qed.

  Lemma u0_sub_cod A B a : u0_subterm (B a) (piu0 A B).
  Proof. constructor; by constructor. Qed.

  Notation "x ≺ y" := (ltPackUpTo3ext u0_subterm x y) (at level 80).


  (** Assuming that the induction hypothesis for the predicate tbp
      with respect to the well-founded relation ≺ holds for piu0 A B,
      build the ppo structure on A and ippo structure on B
   *)
  Section tbpIppo.

    Context (A : u0)
            (B : el0 A -> u0)
            (AA : prec_u0_pic A A)
            (BB : monotone (prec_el0 A) prec_u0_pic B)
            (ih : forall y : PackUpTo3 u0, y ≺ packGround (piu0 A B) -> tbp y).

    Lemma pA : is_ppo (prec_el0 A).
    Proof. by case: (ih (put3_ground _ _ _ (u0_sub_dom A B)) AA). Qed.

    Definition RA := mkPpo pA.

    Instance smallestA : SmallestElem RA.
    Proof. by case: (ih (put3_ground _ _ _ (u0_sub_dom A B)) AA). Qed.

    Lemma ppoB : forall (a : el0 A) (aa : RA a a), is_ppo (prec_el0 (B a)).
    Proof.
      move=> a aa.
      by case: ((ih (put3_ground _ _ _ (u0_sub_cod A B a))) (BB a a aa)).
    Qed.

    Let pB := fun a aa => mkPpo (ppoB a aa).

    Lemma smallestB : forall (a : el0 A) (aa : RA a a), SmallestElem (pB a aa).
    Proof.
      move=> a aa.
      by case: ((ih (put3_ground _ _ _ (u0_sub_cod A B a))) (BB a a aa)).
    Qed.

    Lemma pBB : forall (a0 a1 : el0 A) (a01 : RA a0 a1),
        is_el0_eppair (pB a0 ⌊a01⌋) (pB a1 ⌈a01⌉).
    Proof.
      move=> a0 a1 /wrefl a00 a11 a01.
      exact (ih (put3_dec2 _ _ _ _ (u0_sub_cod A B a0) (u0_sub_cod A B a1))
                (BB a0 a1 a01) (BB a0 a0 a00) (BB a1 a1 a11)
                (pB a0 a00) (pB a1 a11)).
    Qed.

    Let epB := fun a0 a1 a01 => pack_eppair (pBB a0 a1 a01).

    Lemma pBB_id : forall a (aa : RA a a), epB a a aa ≃ ideppair (pB a aa).
    Proof.
      move=> a aa.
      assert (dec : pack1 (B a) ≺ packGround (piu0 A B)) by
        (constructor; apply: put3_dec1; apply: u0_sub_cod).
      exact (ih dec (BB a a aa) (pB a aa) (pBB a a aa)).
    Qed.

    Lemma pBB_comp : forall a0 a1 a2 (a01 : RA a0 a1) (a12 : RA a1 a2),
        compeppair (epB a0 a1 a01) (epB a1 a2 a12) ≃
                   epB a0 a2 (trans a01 a12).
    Proof.
      move=> a0 a1 a2 /wrefl ?? a01 /wrefl ?? a12.
      assert (dec : pack3 (B a0) (B a1) (B a2) ≺ packGround (piu0 A B)) by
        (constructor; apply: put3_dec3; apply: u0_sub_cod).
      apply: (ih dec); apply: BB=> //.
      exact (trans a01 a12).
    Qed.

    Definition RB : ippo RA (fwc_u0 B).
    Proof.
      unshelve econstructor.
      - exact (fun a => prec_el0 (B a)).
      - apply: pB.
      - apply: pBB.
      - apply: pBB_comp.
      - apply: pBB_id.
    Defined.

  End tbpIppo.

  (** Assuming that the induction hypothesis for the predicate tbp
      with respect to the well-founded relation ≺ holds for
      the pair (piu0 A0 B0, piu0 A1 B1),
      build the eppair between the ppos corresponding to A0,A1 and
      the ieppair between the ippos corresponding to B0 and B1 *)
  Section tbpIeppair.
    Context (A0 : u0)
            (B0 : el0 A0 -> u0)
            (A00 : prec_u0_pic A0 A0)
            (B00 : monotone (prec_el0 A0) prec_u0_pic B0)
            (A1 : u0)
            (B1 : el0 A1 -> u0)
            (A11 : prec_u0_pic A1 A1)
            (B11 : monotone (prec_el0 A1) prec_u0_pic B1)
            (A01 : prec_u0_pic A0 A1)
            (B01 : forall (a0 : el0_fwc A0) (a1 : el0_fwc A1),
                i[ prec_el0'] A0 A1 a0 a1 -> prec_u0_pic (B0 a0) (B1 a1))
            (ih : forall y : PackUpTo3 u0, y ≺ pack2 (piu0 A0 B0) (piu0 A1 B1) -> tbp y).

    Lemma ih0 : forall y, y ≺ packGround (piu0 A0 B0) -> tbp y.
    Proof.
      move=> y py; apply: ih.
      apply: Relation_Operators.t_trans; first eassumption.
      constructor; apply: put3_face21_2.
    Qed.

    Lemma ih1 : forall y, y ≺ packGround (piu0 A1 B1) -> tbp y.
    Proof.
      move=> y py; apply: ih.
      apply: Relation_Operators.t_trans; first eassumption.
      constructor; apply: put3_face21_1.
    Qed.

    Let RA0 := RA A00 ih0.
    Let RA1 := RA A11 ih1.
    Let RB0 := RB A00 B00 ih0.
    Let RB1 := RB A11 B11 ih1.

    Lemma pA01 : is_el0_eppair RA0 RA1.
    Proof.
      assert (dec : pack2 A0 A1 ≺ pack2 (piu0 A0 B0) (piu0 A1 B1))
      by (constructor; apply: put3_par_dec; apply: u0_sub_dom).
      by apply: (ih dec).
    Qed.

    Let epA01 := pack_eppair pA01.

    Lemma pB01 : forall a0 a1 (a01 : eprel epA01 a0 a1),
        is_el0_eppair (RB0 a0 ⌞a01⌟) (RB1 a1 ⌜a01⌝).
    Proof.
      move=> a0 a1 a01.
      assert (dec : pack2 (B0 a0) (B1 a1) ≺ pack2 (piu0 A0 B0) (piu0 A1 B1))
      by (constructor; apply: put3_par_dec; apply: u0_sub_cod).
      apply: (ih dec); [apply: B01=> //| apply: B00; exact ⌞a01⌟ | apply: B11; exact ⌜a01⌝].
    Qed.

    Let epB01 := fun a0 a1 (a01 : eprel epA01 a0 a1) => pack_eppair (pB01 a01).

    Lemma naturalB01 (ax0 ay0 : el0 A0) (ax1 ay1 : el0 A1)
          (axy0 : RA0 ax0 ay0) (axy1 : RA1 ax1 ay1)
          (ax01 : eprel epA01 ax0 ax1)
          (ay01 : eprel epA01 ay0 ay1) :
      compeppair (ippo_on RB0 axy0) (epB01 ay01)
                 ≃ compeppair (epB01 ax01) (ippo_on RB1 axy1).
    Proof.
      assert (dec_x0y0y1 : pack3 (B0 ax0) (B0 ay0) (B1 ay1) ≺ pack2 (piu0 A0 B0) (piu0 A1 B1))
      by (constructor; apply: put2_par_dec_21; apply: u0_sub_cod).
      assert (dec_x0x1y1 : pack3 (B0 ax0) (B1 ax1) (B1 ay1) ≺ pack2 (piu0 A0 B0) (piu0 A1 B1))
      by (constructor; apply: put2_par_dec_12; apply: u0_sub_cod).
      pose proof (ax0y1 := eprel_left _ axy0 ay01).
      move: ⌊axy0⌋ ⌈axy0⌉ ⌊axy1⌋ ⌈axy1⌉=> ????.
      unshelve apply: equiveppair_trans; last apply: equiveppair_sym.
      apply: (epB01 ax0y1).
      apply: (ih dec_x0y0y1); try by [apply: B00|apply: B01|apply: B11].
      apply: (ih dec_x0x1y1); try by [apply: B00|apply: B01|apply: B11].
    Qed.

    Definition RB01 : ieppair RB0 RB1 epA01.
    Proof.
      unshelve econstructor.
      - move=> ??; apply: el0_upcast.
      - move=> ??; apply: el0_downcast.
      - apply: pB01.
      - apply: naturalB01.
    Defined.
  End tbpIeppair.

  Lemma iprec_el0_diag (A : u0)
        (pA : is_ppo (prec_el0 A))
        (pAA : is_el0_eppair (mkPpo pA) (mkPpo pA)):
    pack_eppair pAA ≃ ideppair (mkPpo pA) ->
    forall a0 a1, i[prec_el0'] A A a0 a1 -> prec_el0 A a0 a1.
  Proof.
    move=> ideqv ?? h; move: (hrel_upcast h).
    apply: (equiv_upcast_l _ _ _ ideqv).
    exact (hrel_lrefl h).
  Qed.

  Lemma weaken_ih a b : a ≺ b -> (forall y, y ≺ b -> tbp y) -> (forall y, y ≺ a -> tbp y).
  Proof.
    move=> ? ih ??; apply: ih; apply: Relation_Operators.t_trans; eassumption.
  Qed.

  Definition inject x y : ltPackUpTo3 u0_subterm x y -> x ≺ y.
  Proof. move=> ?; by constructor. Qed.


  (** Technical core of the main theorem on the first universe *)

  Lemma prec_u0_pic_valid : forall (x : PackUpTo3 u0), tbp x.
  Proof.
    apply (sFixWf (R := ltPackUpTo3ext u0_subterm)).
    move=> [A|A|A0 A1|A0 A1 A2] ih /=.
    - move=> /prec_u0_pic_inv_diag [[AA]| [A0 [B0 [eqA [[A00 B00]]]]]].
      + exists (is_ppo_at AA); apply: smallest0_at.
      + move: ih; rewrite eqA=> ih; clear A eqA.
        pose (RB := RB A00 B00 ih).
        exists (prec_pi_is_ppo RB).
        refine (@pi_smallest _ _ _ RB _ (smallestB A00 B00 ih)).
    - move=> /prec_u0_pic_inv_diag [[AA]| [A0 [B0 [eqA [[A00 B00]]]]]].
      + move=> ??; by apply: el0_refl0.
      + move: ih; rewrite eqA=> ih; clear A eqA.
        move=> ??.
        pose proof (ih' := weaken_ih (put3_face11 _ (piu0 A0 B0)) ih).
        pose proof (ih'' := weaken_ih (inject (put3_degen12 _ (piu0 A0 B0))) ih).
        pose (RB := RB A00 B00 ih').
        assert (dec : pack1 A0 ≺ packGround (piu0 A0 B0))
               by (do 2 constructor; apply: u0_sub_dom).
        pose proof (hA := (ih' _ dec A00 (pA A00 ih') (pA01 A00 A00 A00 ih''))).
        pose proof (h := iprec_el0_diag hA).
        pose (RBB := RB01 A00 B00 A00 B00 A00 (fun a0 a1 a01 => B00 a0 a1 (h a0 a1 a01)) ih'').
        assert (hB : iequiveppair RBB (id_ieppair RB))
               by (apply: to_iequiveppair=> *; apply: equiveppair_refl).
        (* Fail without the explicit RBB ? *)
        exact (@pi_eppair_id _ _ _ _ _ RBB hA hB).
    - move=> A01 A00 A11.
      move: {A00}(prec_u0_pic_inv_diag A00) A01 ih=> [[AA]| {A0}[A0 [B0 [-> [[A00 B00]]]]]] A01 ih.
      + (* This inversion works well but messes up all names... *)
        inversion A01; subst; [|inversion AA| move=> ??; by apply: el0_on].
        pose proof (ih' := weaken_ih (inject (put3_face21_1 _ _ _)) ih).
        pose (RB := RB H H0 ih').
        pose (smB := @pi_smallest _ _ _ RB _ (smallestB H H0 ih')).
        move=> ??; apply: (@from_empty_eppair _ _ _ smB).
      + move: {A01}(prec_u0_pic_inv_pi_any A01) ih A11
        => {A1}[A1 [B1 [-> [ [[[A01 _] B11] B01] ]]]] ih.
        move=> /prec_u0_pic_inv_pi_pi=> -[[[A11 _] _] _].
        move=> ??; apply: (pi_eppair (RB01 A00 B00 A11 B11 A01 B01 ih)).
    - move=> +++ A00.
      move: {A00}(prec_u0_pic_inv_diag A00) ih=> [[A00]| {A0}[A0 [B0 [-> [[A00 B00]]]]]] ih.
      +  move=> /invert; subst.
         1: move=> *; apply: unicity_empty_eppair.
         1: inversion A00.
         move=> _ A12; move: H; inversion A12; subst.
         1: move=> /invert *; apply: unicity_empty_eppair.
         1: move=> /invert; contradiction.
         move=> *; apply: el0_trans=> //.
      + move=> A01; move: {A01}(prec_u0_pic_inv_pi_any A01) ih
            => {A1}[A1 [B1 [-> [ [[[A01 _] B11] B01] ]]]] ih + A12.
        move: {A12}(prec_u0_pic_inv_pi_any A12) ih
        => {A2}[A2 [B2 [-> [ [[[A12 _] B22] B12] ]]]] ih.
        move=> /prec_u0_pic_inv_pi_pi=> -[[[A02 _] _] B02].
        move=> /prec_u0_pic_inv_pi_pi=> -[[[A11 _] _] _].
        move=> /prec_u0_pic_inv_pi_pi=> -[[[A22 _] _] _].
        pose proof (ih01 := weaken_ih (inject (put3_face32_3 _ _ _ _)) ih).
        pose proof (ih12 := weaken_ih (inject (put3_face32_1 _ _ _ _)) ih).
        pose proof (ih02 := weaken_ih (inject (put3_face32_2 _ _ _ _)) ih).
        pose (iRB01 := RB01 A00 B00 A11 B11 A01 B01 ih01).
        pose (iRB12 := RB01 A11 B11 A22 B22 A12 B12 ih12).
        pose (iRB02 := RB01 A00 B00 A22 B22 A02 B02 ih02).
        move=> ??????.
        apply: equiveppair_trans.
        refine (pi_eppair_comp iRB01 iRB12).
        set z := (z in _ ≃ z).
        change z with (pi_eppair iRB02).
        refine (@pi_eppair_equiv _ _ _ _ _ _ _ _ _ _ (_) iRB02 _ _).
        * apply: (ih (pack3 A0 A1 A2))=> //.
          1: constructor; apply: put3_par33_dec; apply: u0_sub_dom.
        * apply: to_iequiveppair=> ax az axz0 axz1.
          pose (epA12 := pack_eppair (pA01 A11 A22 A12 ih12)).
          pose (ay := downcast epA12 az).
          pose proof (axy0 := eprel_down_right axz0).
          pose proof (ayz0 := eprel_down_left epA12 _ _ ⌜axz0⌝).
          apply: (ih (pack3 (B0 ax) (B1 ay) (B2 az))).
          -- constructor; apply: put3_par33_dec; apply: u0_sub_cod.
          -- by apply B01.
          -- by apply B02.
          -- by apply B12.
          -- apply B00; exact ⌞axz0⌟.
          -- apply B11; exact ⌜axy0⌝.
          -- apply B22; exact ⌜axz0⌝.
          -- exact (ieppair_on iRB01 axy0).
          -- exact (ieppair_on iRB12 ayz0).
  Qed.



  Definition el0_pic_ppo (A : u0) : prec_u0_pic A A -> ppo (el0 A) :=
    fun AA => mkPpo (sdfst (prec_u0_pic_valid (packGround A) AA)).

  (** Ppo structure on type precision at level 0 *)

  Lemma prec_u0_pic_lrefl : lower_reflexive prec_u0_pic.
  Proof.
    move=> ??; elim=> [*|A _ B _ _ AA BB _ _ _ _ _|?? /wrefl ? _ _].
    - apply: inj_prec_u0; constructor=> //.
    - constructor=> // ?? aa'; apply: BB.
      unshelve apply: iprec_el0_diag=> //.
      apply: (el0_pic_ppo AA).
      apply: (prec_u0_pic_valid (pack2 A A) AA AA AA).
      apply: (prec_u0_pic_valid (pack1 A) AA).
    - by apply: inj_prec_u0.
  Qed.

  Lemma prec_u0_pic_urefl : upper_reflexive prec_u0_pic.
  Proof.
    move=> ??; elim=> [A B AA _ BB _|_ A _ B _ AA _ _ BB _ _ _|?? /wrefl _ ? _].
    - constructor=> //?? aa'; apply: BB.
      unshelve apply: iprec_el0_diag=> //.
      apply: (el0_pic_ppo AA).
      apply: (prec_u0_pic_valid (pack2 A A) AA AA AA).
      apply: (prec_u0_pic_valid (pack1 A) AA).
    - constructor=> // ?? aa'; apply: BB.
      unshelve apply: iprec_el0_diag=> //.
      apply: (el0_pic_ppo AA).
      apply: (prec_u0_pic_valid (pack2 A A) AA AA AA).
      apply: (prec_u0_pic_valid (pack1 A) AA).
    - by apply: inj_prec_u0.
  Qed.

  Definition el0_pic_eppair (A0 A1 : u0) (A01 : prec_u0_pic A0 A1) :
    eppair (el0_pic_ppo (prec_u0_pic_lrefl A01))
           (el0_pic_ppo (prec_u0_pic_urefl A01)) :=
    pack_eppair (prec_u0_pic_valid (pack2 A0 A1) A01
                                   (prec_u0_pic_lrefl A01)
                                   (prec_u0_pic_urefl A01) _ _).

  Definition el0_pic_id (A0 : u0) (A00 : prec_u0_pic A0 A0) :
    el0_pic_eppair A00 ≃ ideppair (el0_pic_ppo A00).
  Proof. apply: (prec_u0_pic_valid (pack1 A0))=> //. Qed.

  Definition el0_pic_comp0 (A0 A1 A2 : u0)
             (A01 : prec_u0_pic A0 A1)
             (A12 : prec_u0_pic A1 A2)
             (A02 : prec_u0_pic A0 A2) :
    compeppair (el0_pic_eppair A01) (el0_pic_eppair A12) ≃
               el0_pic_eppair A02.
  Proof.
    apply: (prec_u0_pic_valid (pack3 A0 A1 A2))=> //.
    1,2: apply: prec_u0_pic_lrefl; eassumption.
    apply: prec_u0_pic_urefl; eassumption.
  Qed.


  Lemma prec_u0_pic_trans : transitive prec_u0_pic.
  Proof.
    move=> ??+ XY; elim: XY=> [A B AA ihA BB ihB| A0 A1 B0 B1 A01 ihA01 B00 ihB00 B11 ihB11 B01 ihB01| A B AB] Z.
    - move=> /prec_u0_pic_inv_pi_any=> -[A2 [B2 [-> [ [ [[? ?] ?] ?]]]]].
      constructor=> //. apply: prec_u0_pic_urefl ; eassumption.
    - move=> /prec_u0_pic_inv_pi_any=> -[A2 [B2 [-> [ [ [[A12 _] B22] B12]]]]].
      constructor=> //.
      1: by apply: ihA01.
      pose proof (A02 := ihA01 _ A12).
      move=> ?? /(eprel_equiveppair' (el0_pic_comp0 A01 A12 A02)) a02.
      apply: ihB01; first apply: (eprel_down_right a02).
      apply: B12; apply: (eprel_down_left (el0_pic_eppair A12) _ _ ⌜a02⌝).
    - move=> BZ; move: BZ AB=> {B}[A2 B2 A22 B22|????????|???].
      + move=> /invert; constructor=> //.
      + move=> /invert; contradiction.
      + move=> ?; constructor; apply: trans; eassumption.
  Qed.

  Lemma prec_u0_pic_is_ppo : is_ppo@{v} prec_u0_pic.
  Proof.
    constructor; [ apply: prec_u0_pic_trans
                 | apply: prec_u0_pic_urefl
                 | apply: prec_u0_pic_lrefl ].
  Qed.

  (** The first universe equipped with type precision is a partial preorder *)

  Definition u0_pic : ppo@{v} u0 := mkPpo prec_u0_pic_is_ppo.

  Instance u0_pic_smallest : SmallestElem u0_pic.
  Proof.
    constructor; first by do 2 constructor.
    move=> ? /prec_u0_pic_inv_diag=> -[[?]|[A [B [-> [ [??]]]]]].
    - apply: inj_prec_u0; by apply: smaller.
    - by constructor.
  Qed.

  (* No greatest element for u0_pic !!! *)

  Lemma u0_pic_no_greatest `{GreatestElem u0 u0_pic} : False.
  Proof.
    assert (u0_pic unku0 unku0) by by do 2 constructor.
    assert (H2: u0_pic unku0 dyn) by by apply: greater.
    assert (u0_pic (arrU0 unku0 unku0) (arrU0 unku0 unku0)) by by constructor.
    assert (H4 : u0_pic (arrU0 unku0 unku0) dyn) by by apply: greater.
    move: (prec_u0_pic_inv_pi_any H4)=> [? [? [e _]]].
    rewrite e in H2. clear -H2.
    sexfalso. inversion H2. inversion H.
  Qed.

  (** The decoding on the first universe equipped with type precision is an indexed partial preorder *)

  #[program]
  Definition el0_pic_ippo : ippo u0_pic el0_fwc :=
    mkIppo u0_pic el0_fwc prec_el0' _ _ _ _.
  Next Obligation. apply: el0_pic_ppo. Qed.
  Next Obligation. apply: el0_pic_eppair. Qed.
  Next Obligation.
    move=> ?????; apply: el0_pic_comp0=> //.
    apply: prec_u0_pic_trans; eassumption.
  Qed.
  Next Obligation. apply: el0_pic_id. Qed.

  Instance el0_pic_smallest (A : u0) (AA : u0_pic A A) :
    SmallestElem (el0_pic_ippo _ AA).
  Proof. apply: (sdsnd (prec_u0_pic_valid (packGround A) AA)). Qed.


  #[program]
  Definition el0_pic_to_ippo (A : u0) (B : el0 A -> u0)
             (AA : u0_pic A A)
             (BB : monotone (el0_pic_ippo _ AA) u0_pic B)
    : ippo (el0_pic_ippo _ AA) (fwc_u0 B) :=
    mkIppo (el0_pic_ippo _ AA) (fwc_u0 B) (fun a => prec_el0' (B a)) _ _ _ _.
  Next Obligation.
    move=> A B AA BB a aa; apply: (el0_pic_ippo _ (BB _ _ aa)).
  Qed.
  Next Obligation.
    move=> A B AA BB ax ay axy.
    apply: (ippo_on el0_pic_ippo (BB _ _ axy)).
  Qed.
  Next Obligation.
    move=> A B AA BB ax ay az axy ayz.
    apply: (ippo_trans el0_pic_ippo (BB _ _ axy) (BB _ _ ayz)).
  Qed.
  Next Obligation.
    move=> A B AA BB ax axx.
    apply: (ippo_refl el0_pic_ippo (BB _ _ axx)).
  Qed.

  Instance el0_pic_greatest (A : u0) (AA : u0_pic A A) :
    GreatestElem (el0_pic_ippo _ AA).
  Proof.
    elim/u0selim: A AA=> [||||?|A0 B0 ihA0 ihB0].
    1-4: move=> ?; try typeclasses eauto.
    - move=> ?; apply: box_greatest.
    - move=> AA ; move: AA (AA)=> /prec_u0_pic_inv_pi_pi [[[A00 _] B00] B00'] ?.
      apply: (@pi_greatest _ _ _ (el0_pic_to_ippo A00 B00) _).
      move=> ? aa; apply: ihB0. apply: B00; exact aa.
  Qed.



  #[program]
  Definition el0_pic_to_ieppair (A0 A1 : u0) (B0 : el0 A0 -> u0) (B1 : el0 A1 -> u0)
             (A01 : u0_pic A0 A1)
             (B00 : monotone (el0_pic_ippo _ ⌊A01⌋) u0_pic B0)
             (B11 : monotone (el0_pic_ippo _ ⌈A01⌉) u0_pic B1)
             (B01 : forall a0 a1 (a01 : iprec el0_pic_ippo A01 a0 a1),
                 u0_pic (B0 a0) (B1 a1))
    : ieppair (el0_pic_to_ippo ⌊A01⌋ B00)
              (el0_pic_to_ippo ⌈A01⌉ B11)
              (el0_pic_eppair A01).
  Proof.
    unshelve econstructor.
    - move=> ??; apply: (fupcast el0_fwc@{v}).
    - move=> ??; apply: (fdowncast el0_fwc@{v}).
    - move=> ?? a01. apply: (el0_pic_eppair (B01 _ _ a01)).
    - opacify. move=> *.
      apply: equiveppair_trans; last apply: equiveppair_sym; apply: (ippo_trans el0_pic_ippo).
      by apply: B00.
      by apply: B01.
      by apply: B01.
      by apply: B11.
  Defined.


  (** The rest of this file is dedicated to the proof
      of univ0_pic_UH in order to build the unknown type in universe 1 *)


  Lemma el0_pic_fdowncast_err :
    forall A0 A1 B1 (X := piu0 A1 B1) (A00 : u0_ppo A0 A0) f,
    fdowncast el0_fwc@{v} X A0 f = err.
  Proof.
    elim/u0elim=> [||||P|Adom Acod ihAdom ihAcod] //= ?? A00; sabsurd A00.
  Qed.

  Lemma el0_pic_fdowncast_err' :
    forall A0 B0 (X := piu0 A0 B0) A1 (A11 : u0_ppo A1 A1) a1,
    fdowncast el0_fwc@{v} A1 X a1 = err.
  Proof.
    move=> ???.
    elim/u0elim=> [||||P|Adom Acod ihAdom ihAcod] //= A11; sabsurd A11.
  Qed.

  Lemma el0_pic_fupcast_err :
    forall A0 A1 B1 (X := piu0 A1 B1) (A00 : u0_ppo A0 A0) f,
    fupcast el0_fwc@{v} X A0 f = err.
  Proof.
    elim/u0elim=> [||||P|Adom Acod ihAdom ihAcod] //= ?? A00; sabsurd A00.
  Qed.

  Lemma el0_pic_fupcast_err' :
    forall A0 B0 (X := piu0 A0 B0) A1 (A11 : u0_ppo A1 A1) a1,
    fupcast el0_fwc@{v} A1 X a1 = err.
  Proof.
    move=> ???.
    elim/u0elim=> [||||P|Adom Acod ihAdom ihAcod] //= A11; sabsurd A11.
  Qed.

  Lemma el0_fdowncast_pi@{} A A' B B' :
    fdowncast el0_fwc@{v} (piu0 A' B') (piu0 A B) =
      pi_cast@{v} (fun x => el0 (B x)) (fun x => el0 (B' x))
             (fupcast el0_fwc@{v} A' A)
              (fun a a' => fdowncast el0_fwc@{v} (B' a') (B a)).
  Proof. reflexivity. Qed.

  Lemma el0_fupcast_pi@{} A A' B B' :
    fupcast el0_fwc@{v} (piu0 A B) (piu0 A' B') =
      pi_cast@{v} (fun x => el0 (B x)) (fun x => el0 (B' x))
              (fdowncast el0_fwc@{v} A A')
              (fun a a' => fupcast el0_fwc@{v} (B a) (B' a')).
  Proof. reflexivity. Qed.


  Lemma el0_iprec_err A0 A1 (A01 : u0_pic A0 A1) a1 :
    el0_pic_ippo A1 ⌈A01⌉ a1 a1 -> iprec el0_pic_ippo A01 err a1.
  Proof. move=> ?; apply: to_eprel'=> //; apply: smaller; by apply: downmon. Qed.

  Notation "[monotone f | RA ⤳ RB ]" :=
    (forall a0 a1, RA a0 a1 -> RB (f a0) (f a1)).

  Lemma fupcast_pi_sp AX0 BX0 AY0 BY0
        (AX00 : u0_pic AX0 AX0)
        (AY00 : u0_pic AY0 AY0)
        (BX00 : monotone (el0_pic_ppo AX00) u0_pic BX0)
        (BY00 : monotone (el0_pic_ppo AY00) u0_pic BY0)
        (ABX00 : u0_pic (piu0 AX0 BX0) (piu0 AX0 BX0))
        (ABY00 : u0_pic (piu0 AY0 BY0) (piu0 AY0 BY0))
        (downMon : monotone (el0_pic_ppo AY00) (el0_pic_ppo AX00) (fdowncast el0_fwc@{v} AX0 AY0))
        (upMon : forall ay ay' (ayy : el0_pic_ppo AY00 ay ay')
                   bx bx' (bxx : iprec el0_pic_ippo (BX00 _ _ (downMon ay ay' ayy)) bx bx'),
            iprec el0_pic_ippo (BY00 _ _ ayy)
                  (fupcast el0_fwc@{v} (BX0 _) (BY0 _) bx)
                  (fupcast el0_fwc@{v} (BX0 _) (BY0 _) bx'))
        f g :
    el0_pic_ppo ABX00 f g ->
    el0_pic_ppo ABY00
                (fupcast el0_fwc@{v} (piu0 AX0 BX0) (piu0 AY0 BY0) f)
                (fupcast el0_fwc@{v} (piu0 AX0 BX0) (piu0 AY0 BY0) g).
  Proof.
    move=> fg; rewrite el0_fupcast_pi.
    pose (RX := el0_pic_to_ippo AX00 BX00).
    pose (RY := el0_pic_to_ippo AY00 BY00).
    set (h := fun _ _ => fupcast _ _ _).
    unshelve refine (pi_cast_mon' (B:=fwc_u0 BX0) (B':=fwc_u0 BY0) h (RB:=RX) (RB':=RY) _ _ _).
    + apply: downMon.
    + apply: upMon.
    + exact fg.
  Qed.

  Lemma fdowncast_pi_sp AX0 BX0 AY0 BY0
        (AX00 : u0_pic AX0 AX0)
        (AY00 : u0_pic AY0 AY0)
        (BX00 : monotone (el0_pic_ppo AX00) u0_pic BX0)
        (BY00 : monotone (el0_pic_ppo AY00) u0_pic BY0)
        (ABX00 : u0_pic (piu0 AX0 BX0) (piu0 AX0 BX0))
        (ABY00 : u0_pic (piu0 AY0 BY0) (piu0 AY0 BY0))
        (downMon : monotone (el0_pic_ppo AX00) (el0_pic_ppo AY00) (fupcast el0_fwc@{v} AX0 AY0))
        (upMon : forall ay ay' (ayy : el0_pic_ppo AX00 ay ay')
                   bx bx' (bxx : iprec el0_pic_ippo (BY00 _ _ (downMon ay ay' ayy)) bx bx'),
            iprec el0_pic_ippo (BX00 _ _ ayy)
                  (fdowncast el0_fwc@{v} (BX0 _) (BY0 _) bx)
                  (fdowncast el0_fwc@{v} (BX0 _) (BY0 _) bx'))
        f g :
    el0_pic_ppo ABY00 f g ->
    el0_pic_ppo ABX00
                (fdowncast el0_fwc@{v} (piu0 AX0 BX0) (piu0 AY0 BY0) f)
                (fdowncast el0_fwc@{v} (piu0 AX0 BX0) (piu0 AY0 BY0) g).
  Proof.
    move=> fg; rewrite el0_fdowncast_pi.
    pose (RX := el0_pic_to_ippo AX00 BX00).
    pose (RY := el0_pic_to_ippo AY00 BY00).
    set (h := fun _ _ => fdowncast _ _ _).
    unshelve refine (pi_cast_mon' (B:=fwc_u0 BY0) (B':=fwc_u0 BX0) h (RB:=RY) (RB':=RX) _ _ _).
    + apply: downMon.
    + apply: upMon.
    + exact fg.
  Qed.

  Lemma fupcast_pi_mon AX0 BX0 AY0 BY0 AX1 BX1 AY1 BY1
        (AX01 : u0_pic AX0 AX1)
        (AY01 : u0_pic AY0 AY1)
        (BX00 : monotone (el0_pic_ppo ⌊AX01⌋) u0_pic BX0)
        (BX11 : monotone (el0_pic_ppo ⌈AX01⌉) u0_pic BX1)
        (BX01 : forall ax0 ax1 (ax01 : iprec el0_pic_ippo AX01 ax0 ax1),
            u0_pic (BX0 ax0) (BX1 ax1))
        (BY00 : monotone (el0_pic_ppo ⌊AY01⌋) u0_pic BY0)
        (BY11 : monotone (el0_pic_ppo ⌈AY01⌉) u0_pic BY1)
        (BY01 : forall ay0 ay1 (ay01 : iprec el0_pic_ippo AY01 ay0 ay1),
            u0_pic (BY0 ay0) (BY1 ay1))
        (ABX01 : u0_pic (piu0 AX0 BX0) (piu0 AX1 BX1))
        (ABY01 : u0_pic (piu0 AY0 BY0) (piu0 AY1 BY1))
        (downMon0 : monotone (el0_pic_ppo ⌊AY01⌋) (el0_pic_ppo ⌊AX01⌋) (fdowncast el0_fwc@{v} AX0 AY0))
        (upMon0 : forall ay ay' (ayy : el0_pic_ppo ⌊AY01⌋ ay ay')
                   bx bx' (bxx : iprec el0_pic_ippo (BX00 _ _ (downMon0 ay ay' ayy)) bx bx'),
            iprec el0_pic_ippo (BY00 _ _ ayy)
                  (fupcast el0_fwc@{v} (BX0 _) (BY0 _) bx)
                  (fupcast el0_fwc@{v} (BX0 _) (BY0 _) bx'))
        (downMon1 : monotone (el0_pic_ppo ⌈AY01⌉) (el0_pic_ppo ⌈AX01⌉) (fdowncast el0_fwc@{v} AX1 AY1))
        (upMon1 : forall ay ay' (ayy : el0_pic_ppo ⌈AY01⌉ ay ay')
                   bx bx' (bxx : iprec el0_pic_ippo (BX11 _ _ (downMon1 ay ay' ayy)) bx bx'),
            iprec el0_pic_ippo (BY11 _ _ ayy)
                  (fupcast el0_fwc@{v} (BX1 _) (BY1 _) bx)
                  (fupcast el0_fwc@{v} (BX1 _) (BY1 _) bx'))
        (downMon01 : forall ay0 ay1 (ay01 : iprec el0_pic_ippo AY01 ay0 ay1),
            iprec el0_pic_ippo AX01 (fdowncast el0_fwc@{v} AX0 AY0 ay0)
                  (fdowncast el0_fwc@{v} AX1 AY1 ay1))
        (upMon01 : forall ay0 ay1 (ay01 : iprec el0_pic_ippo AY01 ay0 ay1)
                   bx0 bx1 (bx01 : iprec el0_pic_ippo (BX01 _ _ (downMon01 ay0 ay1 ay01)) bx0 bx1),
            iprec el0_pic_ippo (BY01 _ _ ay01)
                  (fupcast el0_fwc@{v} (BX0 _) (BY0 _) bx0)
                  (fupcast el0_fwc@{v} (BX1 _) (BY1 _) bx1))
        f0 f1 :
  iprec el0_pic_ippo ABX01 f0 f1 ->
  iprec el0_pic_ippo ABY01
    (fupcast el0_fwc@{v} (piu0 AX0 BX0) (piu0 AY0 BY0) f0)
    (fupcast el0_fwc@{v} (piu0 AX1 BX1) (piu0 AY1 BY1) f1).
  Proof.
    by unshelve apply: (pi_cast_hmon (BX0 := fwc_u0 BY0) (BX1 := fwc_u0 BY1)
                                     (BY0 := fwc_u0 BX0) (BY1 := fwc_u0 BX1)
                                     (RBX0 := el0_pic_to_ippo ⌊AY01⌋ BY00)
                                     (RBX1 := el0_pic_to_ippo ⌈AY01⌉ BY11)
                                     (RBY0 := el0_pic_to_ippo ⌊AX01⌋ BX00)
                                     (RBY1 := el0_pic_to_ippo ⌈AX01⌉ BX11)
                                     (BX01 := el0_pic_to_ieppair BY00 BY11 BY01)
                                     (BY01 := el0_pic_to_ieppair BX00 BX11 BX01)).
  Qed.

  Lemma fdowncast_pi_mon AX0 BX0 AY0 BY0 AX1 BX1 AY1 BY1
        (AX01 : u0_pic AX0 AX1)
        (AY01 : u0_pic AY0 AY1)
        (BX00 : monotone (el0_pic_ppo ⌊AX01⌋) u0_pic BX0)
        (BX11 : monotone (el0_pic_ppo ⌈AX01⌉) u0_pic BX1)
        (BX01 : forall ax0 ax1 (ax01 : iprec el0_pic_ippo AX01 ax0 ax1),
            u0_pic (BX0 ax0) (BX1 ax1))
        (BY00 : monotone (el0_pic_ppo ⌊AY01⌋) u0_pic BY0)
        (BY11 : monotone (el0_pic_ppo ⌈AY01⌉) u0_pic BY1)
        (BY01 : forall ay0 ay1 (ay01 : iprec el0_pic_ippo AY01 ay0 ay1),
            u0_pic (BY0 ay0) (BY1 ay1))
        (ABX01 : u0_pic (piu0 AX0 BX0) (piu0 AX1 BX1))
        (ABY01 : u0_pic (piu0 AY0 BY0) (piu0 AY1 BY1))
        (upMon0 : monotone (el0_pic_ppo ⌊AY01⌋) (el0_pic_ppo ⌊AX01⌋) (fupcast el0_fwc@{v} AY0 AX0))
        (downMon0 : forall ay ay' (ayy : el0_pic_ppo ⌊AY01⌋ ay ay')
                   bx bx' (bxx : iprec el0_pic_ippo (BX00 _ _ (upMon0 ay ay' ayy)) bx bx'),
            iprec el0_pic_ippo (BY00 _ _ ayy)
                  (fdowncast el0_fwc@{v} (BY0 _) (BX0 _) bx)
                  (fdowncast el0_fwc@{v} (BY0 _) (BX0 _) bx'))
        (upMon1 : monotone (el0_pic_ppo ⌈AY01⌉) (el0_pic_ppo ⌈AX01⌉) (fupcast el0_fwc@{v} AY1 AX1))
        (downMon1 : forall ay ay' (ayy : el0_pic_ppo ⌈AY01⌉ ay ay')
                   bx bx' (bxx : iprec el0_pic_ippo (BX11 _ _ (upMon1 ay ay' ayy)) bx bx'),
            iprec el0_pic_ippo (BY11 _ _ ayy)
                  (fdowncast el0_fwc@{v} (BY1 _) (BX1 _) bx)
                  (fdowncast el0_fwc@{v} (BY1 _) (BX1 _) bx'))
        (upMon01 : forall ay0 ay1 (ay01 : iprec el0_pic_ippo AY01 ay0 ay1),
            iprec el0_pic_ippo AX01 (fupcast el0_fwc@{v} AY0 AX0 ay0)
                  (fupcast el0_fwc@{v} AY1 AX1 ay1))
        (downMon01 : forall ay0 ay1 (ay01 : iprec el0_pic_ippo AY01 ay0 ay1)
                   bx0 bx1 (bx01 : iprec el0_pic_ippo (BX01 _ _ (upMon01 ay0 ay1 ay01)) bx0 bx1),
            iprec el0_pic_ippo (BY01 _ _ ay01)
                  (fdowncast el0_fwc@{v} (BY0 _) (BX0 _) bx0)
                  (fdowncast el0_fwc@{v} (BY1 _) (BX1 _) bx1))
        f0 f1 :
  iprec el0_pic_ippo ABX01 f0 f1 ->
  iprec el0_pic_ippo ABY01
    (fdowncast el0_fwc@{v} (piu0 AY0 BY0) (piu0 AX0 BX0) f0)
    (fdowncast el0_fwc@{v} (piu0 AY1 BY1) (piu0 AX1 BX1) f1).
  Proof.
    by unshelve apply: (pi_cast_hmon (BX0 := fwc_u0 BY0) (BX1 := fwc_u0 BY1)
                                     (BY0 := fwc_u0 BX0) (BY1 := fwc_u0 BX1)
                                     (RBX0 := el0_pic_to_ippo ⌊AY01⌋ BY00)
                                     (RBX1 := el0_pic_to_ippo ⌈AY01⌉ BY11)
                                     (RBY0 := el0_pic_to_ippo ⌊AX01⌋ BX00)
                                     (RBY1 := el0_pic_to_ippo ⌈AX01⌉ BX11)
                                     (BX01 := el0_pic_to_ieppair BY00 BY11 BY01)
                                     (BY01 := el0_pic_to_ieppair BX00 BX11 BX01)).
  Qed.


  Lemma el0_fdowncast_err A0 x : fdowncast el0_fwc@{v} A0 err@{v} x = err.
  Proof. elim/u0elim: A0=> //. Qed.

  Lemma el0_fupcast_err A0 x : fupcast el0_fwc@{v} err@{v} A0 x = err.
  Proof. elim/u0elim: A0=> //. Qed.



  Definition univ0_pic_castmon : forall A0 A1 B0 B1
    (A01 : u0_pic A0 A1) (B01 : u0_pic B0 B1),
    (forall a0 a1 (a01 : iprec el0_pic_ippo A01 a0 a1),
        iprec el0_pic_ippo B01
              (fupcast el0_fwc@{v} A0 B0 a0)
              (fupcast el0_fwc@{v} A1 B1 a1)) s/\
      (forall b0 b1 (b01 : iprec el0_pic_ippo B01 b0 b1),
          iprec el0_pic_ippo A01
                (fdowncast el0_fwc@{v} A0 B0 b0)
                (fdowncast el0_fwc@{v} A1 B1 b1)).
  Proof.
    pose (pair_to_list := fun (p : u0 * u0) => (fst p :: snd p :: nil)%list).
    pose (R := MR (cart u0 u0_subterm) pair_to_list).
    move=> A0 A1; set p := (A0,A1). change A0 with (fst p). change A1 with (snd p). move: {A0 A1}p.
    refine (sFixWf (R:=R) _ _).
    1: apply: wf_MR; apply: Transitive_Closure.wf_clos_trans; apply: cart_direct_wf.
    move=> [A0 A1] ih B0 B1 /wrefl A00 A11 A01 /wrefl B00 B11 B01.
    move: A01 B01 A00 A11 B00 B11 ih. cbn -[fupcast fdowncast el0_fwc u0_pic].
    move=> {A0 A1}[Adom1 Acod1 Adom11 Acod11
                 |Adom0 Adom1 Acod0 Acod1 Adom01 Acod00 Acod11 Acod01
                 |A0 A1 A01]
          {B0 B1}[Bdom1 Bcod1 Bdom11 Bcod11
                 |Bdom0 Bdom1 Bcod0 Bcod1 Bdom01 Bcod00 Bcod11 Bcod01
                 |B0 B1 B01] A00 A11 B00 B11 ih.
    - assert (H1 : R (Adom1, Adom1) (err@{v}, piu0 Adom1 Acod1)).
      { apply: (cart_cover2 _ _ nil (Adom1 :: Adom1 :: nil))=> //.
        by do 2 (constructor; first apply: u0_sub_dom). }
      case: (ih (Adom1,Adom1) H1 Bdom1 Bdom1 Adom11 Bdom11)=> Adomup Adomdown.
      split.
      + move=> a0 a1 a01; apply: el0_iprec_err.
        (unshelve apply: fupcast_pi_sp)=> //; last exact ⌜a01⌝.
        * move=> ???. apply: (iprec_refl (RB:=el0_pic_ippo) (aa:=Adom11)).
          apply: Adomdown. by apply: prec_iprec.
        * move=> bx1 by1 bxy1.
          refine (sprl (ih (Acod1 _, Acod1 _) _ (Bcod1 bx1) (Bcod1 by1)
                           (Acod11 _ _ _) (Bcod11 _ _ bxy1))).
          apply: (cart_cover2 _ _ nil (_ :: _ :: nil))=> //.
          by do 2 (constructor; first apply: u0_sub_cod).
      + move=> b0 b1 b01; apply: el0_iprec_err.
        (unshelve apply: fdowncast_pi_sp)=> //; last exact ⌜b01⌝.
        * opacify. move=> ???. apply: (iprec_refl (RB:=el0_pic_ippo) (aa:=Bdom11)).
          apply: Adomup. by apply: prec_iprec.
        * move=> ax1 ay1 axy1.
          refine (sprr (ih (Acod1 ax1, Acod1 ay1) _ (Bcod1 _) (Bcod1 _)
                           (Acod11 _ _ axy1) (Bcod11 _ _ _))).
          apply: (cart_cover2 _ _ nil (_ :: _ :: nil))=> //.
          by do 2 (constructor; first apply: u0_sub_cod).
    - pose proof (Bdom11 := ⌈Bdom01: u0_pic _ _⌉).
      assert (H1 : R (Adom1, Adom1) (err@{v}, piu0 Adom1 Acod1)).
      { apply: (cart_cover2 _ _ nil (Adom1 :: Adom1 :: nil))=> //.
        by do 2 (constructor; first apply: u0_sub_dom). }
      case: (ih (Adom1, Adom1) H1 Bdom1 Bdom1 Adom11 Bdom11)=> Adomup Adomdown.
      split.
      + move=> a0 a1 a01; apply: el0_iprec_err.
        (unshelve apply: fupcast_pi_sp)=> //; last exact ⌜a01⌝.
        * opacify. move=> ???. apply: (iprec_refl (RB:=el0_pic_ippo) (aa:=Adom11)).
          apply: Adomdown. by apply: prec_iprec.
        * move=> bx1 by1 bxy1.
          refine (sprl (ih (Acod1 _, Acod1 _) _ (Bcod1 bx1) (Bcod1 by1)
                           (Acod11 _ _ _) (Bcod11 _ _ bxy1))).
          apply: (cart_cover2 _ _ nil (_ :: _ :: nil))=> //.
          by do 2 (constructor; first apply: u0_sub_cod).
      + move=> b0 b1 b01; apply: el0_iprec_err.
        (unshelve apply: fdowncast_pi_sp)=> //; last exact ⌜b01⌝.
        * opacify. move=> ???. apply: (iprec_refl (RB:=el0_pic_ippo) (aa:=Bdom11)).
          apply: Adomup. by apply: prec_iprec.
        * move=> ax1 ay1 axy1.
          refine (sprr (ih (Acod1 ax1, Acod1 ay1) _ (Bcod1 _) (Bcod1 _)
                    (Acod11 _ _ axy1) (Bcod11 _ _ _))).
          apply: (cart_cover2 _ _ nil (_ :: _ :: nil))=> //.
          by do 2 (constructor; first apply: u0_sub_cod).
    - split; move=> *; apply: el0_iprec_err.
      1: rewrite (el0_pic_fupcast_err ⌈B01⌉); apply: smallest_refl.
      rewrite (el0_pic_fdowncast_err _ _ ⌈B01⌉); apply: smallest_refl.
    - pose proof (Adom11 := ⌈Adom01: u0_pic _ _⌉).
      assert (H1 : R (Adom1, Adom1) (piu0 Adom0 Acod0, piu0 Adom1 Acod1)).
      { apply: (cart_cover2 _ _ nil (Adom1 :: Adom1 :: nil))=> //.
        by do 2 (constructor; first apply: u0_sub_dom). }
      case: (ih (Adom1, Adom1) H1 Bdom1 Bdom1 Adom11 Bdom11)=> Adomup Adomdown.
      split.
      + move=> a0 a1 a01; apply: el0_iprec_err.
        (unshelve apply: fupcast_pi_sp)=> //; last exact ⌜a01⌝.
        * opacify. move=> ???. apply: (iprec_refl (RB:=el0_pic_ippo) (aa:=Adom11)).
          apply: Adomdown. by apply: prec_iprec.
        * move=> bx1 by1 bxy1.
          refine (sprl (ih (Acod1 _, Acod1 _) _ (Bcod1 bx1) (Bcod1 by1)
                           (Acod11 _ _ _) (Bcod11 _ _ bxy1))).
          apply: (cart_cover2 _ _ nil (_ :: _ :: nil))=> //.
          by do 2 (constructor; first apply: u0_sub_cod).
      + move=> b0 b1 b01; apply: el0_iprec_err.
        (unshelve apply: fdowncast_pi_sp)=> //; last exact ⌜b01⌝.
        * opacify. move=> ???. apply: (iprec_refl (RB:=el0_pic_ippo) (aa:=Bdom11)).
          apply: Adomup. by apply: prec_iprec.
        * move=> ax1 ay1 axy1.
          refine (sprr (ih (Acod1 ax1, Acod1 ay1) _ (Bcod1 _) (Bcod1 _)
                           (Acod11 _ _ axy1) (Bcod11 _ _ _))).
          apply: (cart_cover2 _ _ nil (_ :: _ :: nil))=> //.
          by do 2 (constructor; first apply: u0_sub_cod).
    - pose proof (Adom00 := ⌊Adom01: u0_pic _ _⌋).
      pose proof (Bdom00 := ⌊Bdom01: u0_pic _ _⌋).
      pose proof (Adom11 := ⌈Adom01: u0_pic _ _⌉).
      pose proof (Bdom11 := ⌈Bdom01: u0_pic _ _⌉).
      assert (H0 : R (Adom0, Adom0) (piu0 Adom0 Acod0, piu0 Adom1 Acod1)).
      { apply: (cart_cover2 _ _ (Adom0 :: Adom0 :: nil) nil)=> //.
        by do 2 (constructor; first apply: u0_sub_dom). }
      assert (H01 : R (Adom0, Adom1) (piu0 Adom0 Acod0, piu0 Adom1 Acod1)).
      { apply: (cart_cover2 _ _ (Adom0 :: nil) (Adom1 :: nil));
        constructor=> // ; apply: u0_sub_dom. }
      assert (H1 : R (Adom1, Adom1) (piu0 Adom0 Acod0, piu0 Adom1 Acod1)).
      { apply: (cart_cover2 _ _ nil (Adom1 :: Adom1 :: nil))=> //.
        by do 2 (constructor; first apply: u0_sub_dom). }
      case: (ih (Adom0,Adom0) H0 Bdom0 Bdom0 Adom00 Bdom00)=> Adom0up Adom0down.
      case: (ih (Adom0, Adom1) H01 Bdom0 Bdom1 Adom01 Bdom01)=> Adom01up Adom01down.
      case: (ih (Adom1,Adom1) H1 Bdom1 Bdom1 Adom11 Bdom11)=> Adom1up Adom1down.
      split.
      + (unshelve apply: fupcast_pi_mon)=> //.
        * opacify. move=> ???. apply: (iprec_refl (RB:=el0_pic_ippo) (aa:=Adom00)).
          apply: Adom0down. by apply: prec_iprec.
        * opacify. move=> ???. apply: (iprec_refl (RB:=el0_pic_ippo) (aa:=Adom11)).
          apply: Adom1down. by apply: prec_iprec.
        * move=> bx1 by1 bxy1.
          refine (sprl (ih (Acod0 _, Acod0 _) _ (Bcod0 bx1) (Bcod0 by1)
                           (Acod00 _ _ _) (Bcod00 _ _ bxy1))).
          apply: (cart_cover2 _ _ (_ :: _ :: nil) nil)=> //.
          by do 2 (constructor; first apply: u0_sub_cod).
        * move=> bx1 by1 bxy1.
          refine (sprl (ih (Acod1 _, Acod1 _) _ (Bcod1 bx1) (Bcod1 by1)
                           (Acod11 _ _ _) (Bcod11 _ _ bxy1))).
          apply: (cart_cover2 _ _ nil (_ :: _ :: nil))=> //.
          by do 2 (constructor; first apply: u0_sub_cod).
        * move=> bx1 by1 bxy1.
          refine (sprl (ih (Acod0 _ , Acod1 _) _ (Bcod0 bx1) (Bcod1 by1)
                    (Acod01 _ _ _) (Bcod01 _ _ bxy1))).
          apply: (cart_cover2 _ _ (_ :: nil) (_ :: nil)); constructor=> //; apply: u0_sub_cod.
      + (unshelve apply: fdowncast_pi_mon)=> //.
        * opacify. move=> ???. apply: (iprec_refl (RB:=el0_pic_ippo) (aa:=Bdom00)).
          apply: Adom0up. by apply: prec_iprec.
        * opacify. move=> ???. apply: (iprec_refl (RB:=el0_pic_ippo) (aa:=Bdom11)).
          apply: Adom1up. by apply: prec_iprec.
        * move=> bx1 by1 bxy1.
          refine (sprr (ih (Acod0 _, Acod0 _) _ (Bcod0 _) (Bcod0 _)
                           (Acod00 _ _ _) (Bcod00 _ _ _))).
          apply: (cart_cover2 _ _ (_ :: _ :: nil) nil)=> //.
          by do 2 (constructor; first apply: u0_sub_cod).
        * move=> bx1 by1 bxy1.
          refine (sprr (ih (Acod1 bx1, Acod1 by1) _ (Bcod1 _) (Bcod1 _)
                           (Acod11 _ _ bxy1) (Bcod11 _ _ _))).
          apply: (cart_cover2 _ _ nil (_ :: _ :: nil))=> //.
          by do 2 (constructor; first apply: u0_sub_cod).
        * move=> bx1 by1 bxy1.
          refine (sprr (ih (Acod0 bx1, Acod1 by1) _ (Bcod0 _) (Bcod1 _)
                    (Acod01 _ _ bxy1) (Bcod01 _ _ _))).
          apply: (cart_cover2 _ _ (_ :: nil) (_ :: nil)); constructor=> //; apply: u0_sub_cod.
    - split; move=> *.
      1: rewrite (el0_pic_fupcast_err ⌊B01⌋) (el0_pic_fupcast_err ⌈B01⌉).
      2: rewrite (el0_pic_fdowncast_err _ _ ⌊B01⌋) (el0_pic_fdowncast_err _ _ ⌈B01⌉).
      all: apply: el0_iprec_err; apply: smallest_refl.
    - split; move=> *.
      + case: (fupcast _ _ _ _); apply: el0_iprec_err.
        rewrite (el0_pic_fupcast_err' _ _ ⌈A01⌉); apply: smallest_refl.
      + rewrite el0_fdowncast_err; apply: el0_iprec_err.
        rewrite (el0_pic_fdowncast_err' ⌈A01⌉); apply: smallest_refl.
    - split=> *.
      1: rewrite (el0_pic_fupcast_err' _ _ ⌊A01⌋) (el0_pic_fupcast_err' _ _ ⌈A01⌉).
      2: rewrite (el0_pic_fdowncast_err' ⌊A01⌋) (el0_pic_fdowncast_err' ⌈A01⌉).
      all: apply: el0_iprec_err; apply: smallest_refl.
    - clear ih A00 A11 B00 B11; split.
      1: move: A01=> /wrefl ???; move=> ?? a01; rewrite 2?el0_up_down_sym_eq_sp //; move: a01.
      all: by apply: (@unknown.fdownmon _ _ _ _ _ _ univ0_UH).
  Qed.


  Lemma univ0_pic_upcasthmon : forall A0 A1 B0 B1
                                (A01 : u0_pic A0 A1) (B01 : u0_pic B0 B1)
                                a0 a1 (a01 : iprec el0_pic_ippo A01 a0 a1),
      iprec el0_pic_ippo B01 (fupcast el0_fwc@{v} A0 B0 a0) (fupcast el0_fwc@{v} A1 B1 a1).
  Proof. move=> ???? A01 B01. by case: (univ0_pic_castmon A01 B01). Qed.

  Lemma univ0_pic_upcasthequiv :
    forall A0 A1 B0 B1
      (A01 : A0 ≈[u0_pic] A1)
      (B01 : B0 ≈[u0_pic] B1)
      a0 a1
      (a01 : A01 ⊢[el0_pic_ippo] a0 ≈ a1),
      B01 ⊢[el0_pic_ippo] fupcast el0_fwc@{v} A0 B0 a0 ≈ fupcast el0_fwc@{v} A1 B1 a1.
  Proof.
    move=> ???? [A01 A10] [B01 B10] ?? [b01 b10]; split.
    all: apply: univ0_pic_upcasthmon; eassumption.
  Qed.

  Lemma univ0_pic_downcasthmon : forall A0 A1 B0 B1
                                (A01 : u0_pic A0 A1) (B01 : u0_pic B0 B1)
                                b0 b1 (b01 : iprec el0_pic_ippo B01 b0 b1),
      iprec el0_pic_ippo A01 (fdowncast el0_fwc@{v} A0 B0 b0) (fdowncast el0_fwc@{v} A1 B1 b1).
  Proof. move=> ???? A01 B01. by case: (univ0_pic_castmon A01 B01). Qed.

  Lemma univ0_pic_downcasthequiv :
    forall A0 A1 B0 B1
      (A01 : A0 ≈[u0_pic] A1)
      (B01 : B0 ≈[u0_pic] B1)
      b0 b1
      (b01 : B01 ⊢[el0_pic_ippo] b0 ≈ b1),
      A01 ⊢[el0_pic_ippo] fdowncast el0_fwc@{v} A0 B0 b0 ≈ fdowncast el0_fwc@{v} A1 B1 b1.
  Proof.
    move=> ???? [A01 A10] [B01 B10] ?? [b01 b10]; split.
    all: apply: univ0_pic_downcasthmon; eassumption.
  Qed.

  Lemma univ0_pic_upcastmon A0 B0 A00 B00 :
    monotone (el0_pic_ppo A00) (el0_pic_ppo B00) (fupcast el0_fwc@{v} A0 B0).
  Proof.
    move=> ???.
    apply: (iprec_refl (RB:=el0_pic_ippo) (aa:=B00)).
    (unshelve apply: univ0_pic_upcasthmon)=> //.
    by apply: prec_iprec.
  Qed.

  Lemma univ0_pic_downcastmon A0 B0 A00 B00 :
    monotone (el0_pic_ppo A00) (el0_pic_ppo B00) (fdowncast el0_fwc@{v} A0 B0).
  Proof.
    move=> ???.
    apply: (iprec_refl (RB:=el0_pic_ippo) (aa:=B00)).
    (unshelve apply: univ0_pic_downcasthmon)=> //.
    by apply: prec_iprec.
  Qed.


  Lemma eprel_err_inv@{w}
        [AX AY : Type@{w}] [RAX : ppo@{w} AX]
        [RAY : ppo@{w} AY] [AXY : eppair@{w} RAX RAY]
        [ax : AX] `{H : Err@{w} AX} `{H0 : Err@{w} AY}
        `{!SmallestElem@{w} RAX}
        `{!SmallestElem@{w} RAY} :
    RAX ax err@{w} -> eprel@{w} AXY ax err@{w}.
  Proof.
    move=> axerr; apply: to_eprel'.
    1: apply: (trans axerr); apply: smaller; apply: downmon.
    all: apply: smallest_refl.
  Qed.

  Lemma el0_pic_downerr : forall (A0 A1 : u0) (A00 : u0_pic A0 A0) (A11 : u0_pic A1 A1)
                            (v : el0_fwc@{v} A0),
      el0_pic_ippo A0 A00 v err@{v} ->
      el0_pic_ippo A1 A11 (fdowncast el0_fwc@{v} A1 A0 v) err@{v}.
  Proof.
    move=> /[swap]; apply: (sFixWf (R:=u0_subterm))=> A1 ih A0 A00 A11.
    move: (prec_u0_pic_inv_diag A00) (prec_u0_pic_inv_diag A11) ih.
    move=> [[A00']|[Adom0 [Acod0 [eq0 [[Adom00 Acod00]]]]]]
          [[A11']|[Adom1 [Acod1 [eq1 [[Adom11 Acod11]]]]]] ih.
    + refine (@unknown.fdownerr _ _ _ _ _ _ univ0_UH _ _ A00' A11').
    + move=> v verr; apply eq_sym in eq1; induction eq1.
      rewrite el0_pic_fdowncast_err //.
      apply: smallest_refl.
    + move=> v verr; apply eq_sym in eq0; induction eq0.
      rewrite el0_pic_fdowncast_err' //.
      apply: smallest_refl.
    + move=> f ferr; apply eq_sym in eq0; apply eq_sym in eq1; induction eq0; induction eq1.
      rewrite el0_fdowncast_pi.
      pose proof (h := (univ0_pic_downcastmon A00 A11 _ _ ferr)).
      apply: (trans h).
      change (el0_pic_ppo _ ?x ?y) with (pi_ppo (el0_pic_to_ippo Adom11 Acod11) x y).
      split; first split.
      - move: ⌈h⌉=> [_ //].
      - move: (@smallest_refl _ _ _ (el0_pic_smallest A11))=> [_ //].
      - move=> a ? aa' ; unshelve apply: eprel_err_inv.
        2:apply: (el0_pic_smallest (Acod11 _ _ ⌊aa'⌋)).
        1: apply: (el0_pic_smallest (Acod11 _ _ ⌈aa'⌉)).
        rewrite el0_fdowncast_pi /pi_cast.
        unshelve apply: (ih (Acod1 a) (u0_sub_cod _ _ _) (Acod0 _)).
        * apply: Acod00; move: ⌊aa'⌋; apply: univ0_pic_upcastmon=> //.
        * apply: (Acod11 _ _ ⌊aa'⌋).
        * apply: smallest_refl.
  Qed.

  Lemma el0_pic_uperr : forall (A0 A1 : u0) (A00 : u0_pic A0 A0) (A11 : u0_pic A1 A1)
                            (v : el0_fwc@{v} A0),
      el0_pic_ippo A0 A00 v err@{v} ->
      el0_pic_ippo A1 A11 (fupcast el0_fwc@{v} A0 A1 v) err@{v}.
  Proof.
    move=> /[swap]; apply: (sFixWf (R:=u0_subterm))=> A1 ih A0 A00 A11.
    move: (prec_u0_pic_inv_diag A00) (prec_u0_pic_inv_diag A11) ih.
    move=> [[A00']|[Adom0 [Acod0 [eq0 [[Adom00 Acod00]]]]]]
          [[A11']|[Adom1 [Acod1 [eq1 [[Adom11 Acod11]]]]]] ih.
    + move=> v vv.
      rewrite el0_up_down_sym_eq_sp=> //. move: v vv.
      refine (@unknown.fdownerr _ _ _ _ _ _ univ0_UH _ _ A00' A11').
    + move=> v verr; apply eq_sym in eq1; induction eq1.
      rewrite el0_pic_fupcast_err' //.
      apply: smallest_refl.
    + move=> v verr; apply eq_sym in eq0; induction eq0.
      rewrite el0_pic_fupcast_err //.
      apply: smallest_refl.
    + move=> f ferr; apply eq_sym in eq0; apply eq_sym in eq1; induction eq0; induction eq1.
      rewrite el0_fupcast_pi.
      pose proof (h := (univ0_pic_upcastmon A00 A11 _ _ ferr)).
      apply: (trans h).
      change (el0_pic_ppo _ ?x ?y) with (pi_ppo (el0_pic_to_ippo Adom11 Acod11) x y).
      split; first split.
      - move: ⌈h⌉=> [_ //].
      - move: (@smallest_refl _ _ _ (el0_pic_smallest A11))=> [_ //].
      - move=> a ? aa' ; unshelve apply: eprel_err_inv.
        2:apply: (el0_pic_smallest (Acod11 _ _ ⌊aa'⌋)).
        1: apply: (el0_pic_smallest (Acod11 _ _ ⌈aa'⌉)).
        rewrite el0_fupcast_pi /pi_cast.
        unshelve apply: (ih (Acod1 a) (u0_sub_cod _ _ _) (Acod0 _)).
        * apply: Acod00; move: ⌊aa'⌋; apply: univ0_pic_downcastmon=> //.
        * apply: (Acod11 _ _ ⌊aa'⌋).
        * apply: smallest_refl.
  Qed.

  Lemma el0_pic_cast_comp
        A0 (A00 : u0_pic A0 A0)
        A1 (A11 : u0_pic A1 A1)
        A2 (A22 : u0_pic A2 A2) :
    (forall v, el0_pic_ippo A2 A22 v v ->
    el0_pic_ippo A0 A00
                 (fdowncast el0_fwc@{v} A0 A1 (fdowncast el0_fwc@{v} A1 A2 v))
                 (fdowncast el0_fwc@{v} A0 A2 v))
      s/\
      (forall v, el0_pic_ippo A0 A00 v v ->
            el0_pic_ippo A2 A22
                         (fupcast el0_fwc@{v} A1 A2 (fupcast el0_fwc@{v} A0 A1 v))
                         (fupcast el0_fwc@{v} A0 A2 v)).
  Proof.
    move: A0 A00 A1 A11 A2 A22.
    apply: (sFixWf (R:=u0_subterm))=> A0 ih A00 A1 A11 A2 A22.
    move: A00 A11 A22 (A00) (A11) (A22)=> +++ X00 X11 X22.
    move=> /prec_u0_pic_inv_diag [[Ap00]|[Adom0 [Acod0 [eq0 [[Adom00 Acod00]]]]]];
    move=> /prec_u0_pic_inv_diag [[Ap11]|[Adom1 [Acod1 [eq1 [[Adom11 Acod11]]]]]].
    1,4: move=> /prec_u0_pic_inv_diag [[Ap22]|[Adom2 [Acod2 [eq2 [[Adom22 Acod22]]]]]].
    - split.
      2: move=> ??; rewrite !el0_up_down_sym_eq_sp //.
      all: by apply: (@unknown.fdowncomp _ _ _ El0_ippo _ _ univ0_UH).
    - apply eq_sym in eq2; induction eq2.
      split=> v.
      + rewrite (el0_pic_fdowncast_err' Ap11).
        move=> vv. apply: trans.
        unshelve apply: el0_pic_downerr.
        1: constructor; assumption.
        1: by apply: smallest_refl.
        apply: smaller; move: vv; apply: univ0_pic_downcastmon=> //.
      + rewrite (el0_pic_fupcast_err' _ _ Ap11).
        move=> vv.
        apply: smaller; move: vv; apply: univ0_pic_upcastmon=> //.
    - apply eq_sym in eq1; induction eq1.
      split=> v.
      + rewrite (el0_pic_fdowncast_err _ _ Ap22).
        move=> vv. apply: trans.
        unshelve apply: el0_pic_downerr.
        1: assumption.
        1: by apply: smallest_refl.
        apply: smaller; move: vv; apply: univ0_pic_downcastmon=> //.
      + rewrite (el0_pic_fupcast_err Ap22).
        move=> vv; apply: smaller; move: vv; apply: univ0_pic_upcastmon=> //.
    - apply eq_sym in eq0; induction eq0.
      apply eq_sym in eq1; induction eq1.
      apply eq_sym in eq2; induction eq2.
      rewrite 3!el0_fdowncast_pi 3!el0_fupcast_pi.
      move: (ih Adom0 (u0_sub_dom _ _) Adom00 Adom1 Adom11 Adom2 Adom22) => [hdomdown hdomup].
      split=> v.
      + rewrite (pi_cast_comp (fwc_u0 Acod2) (fwc_u0 Acod1) (fwc_u0 Acod0)).
        unshelve apply: (pi_cast_hmon_diag
                           (BX0 := fwc_u0 Acod0) (BY0 := fwc_u0 Acod2)
                           (RBX0 := el0_pic_to_ippo Adom00 Acod00)
                           (RBY0 := el0_pic_to_ippo Adom22 Acod22)).
        * opacify; apply: compmon; apply: univ0_pic_upcastmon=> //.
        * opacify; apply: univ0_pic_upcastmon=> //.
        * opacify; move=> ax0 ax1 ax01.
          apply: (trans (hdomup ax0 ⌊ax01⌋)).
          move: ax01; apply: univ0_pic_upcastmon=> //.
        * move=> ax0 ax1 ax01 by0 by1 by01.
          apply: univ0_pic_downcasthmon; first by apply: Acod00.
          apply: univ0_pic_downcasthmon.
          eassumption.
          Unshelve.
          1: assumption.
          1: apply: Acod11; (unshelve apply: univ0_pic_upcastmon)=> //.
          apply Acod22; do 2 (unshelve apply: univ0_pic_upcastmon)=> //.
        * move=> ????? by01.
          unshelve (apply: univ0_pic_downcasthmon); first by apply: Acod00.
          2: eassumption.
          apply Acod22; (unshelve apply: univ0_pic_upcastmon)=> //.
        * move=> ax0 ax1 /wrefl ? ? ax01 by0 by1 by01.
          apply: iprec_left.
          2:{ apply: (univ0_pic_downcasthmon (Acod00 _ _ ax01) by01).
              apply Acod22.
              refine (trans _ (univ0_pic_upcastmon Adom00 Adom22 _ _ ax01)).
              apply hdomup. exact ⌊ax01⌋.
          }
          (unshelve refine (sprl (ih (Acod0 ax0) (u0_sub_cod _ _ _) _ _ _ _ _) _ _))=> //.
          apply Acod22; do 2 (unshelve apply: univ0_pic_upcastmon)=> //.
          by apply: Acod00.
          apply Acod11; (unshelve apply: univ0_pic_upcastmon)=> //.
          exact ⌞by01⌟.
      + rewrite (pi_cast_comp (fwc_u0 Acod0) (fwc_u0 Acod1) (fwc_u0 Acod2)).
        unshelve apply: (pi_cast_hmon_diag
                           (BX0 := fwc_u0 Acod2) (BY0 := fwc_u0 Acod0)
                           (RBX0 := el0_pic_to_ippo Adom22 Acod22)
                           (RBY0 := el0_pic_to_ippo Adom00 Acod00)).
        * opacify; apply: compmon; apply: univ0_pic_downcastmon=> //.
        * opacify; apply: univ0_pic_downcastmon=> //.
        * opacify; move=> ax0 ax1 ax01.
          apply: (trans (hdomdown ax0 ⌊ax01⌋)).
          move: ax01; apply: univ0_pic_downcastmon=> //.
        * move=> ax0 ax1 ax01 by0 by1 by01.
          apply: univ0_pic_upcasthmon; first by apply: Acod22.
          apply: univ0_pic_upcasthmon.
          eassumption.
          Unshelve.
          1: apply: Acod11; (unshelve apply: univ0_pic_downcastmon)=> //.
          apply Acod00; do 2 (unshelve apply: univ0_pic_downcastmon)=> //.
        * move=> ????? by01.
          unshelve (apply: univ0_pic_upcasthmon); first by apply: Acod22.
          2: eassumption.
          apply Acod00; (unshelve apply: univ0_pic_downcastmon)=> //.
        * move=> ax0 ax1 /wrefl ? ? ax01 by0 by1 by01.
          apply: iprec_left.
          2:{ apply: (univ0_pic_upcasthmon (Acod22 _ _ ax01) by01).
              apply Acod00.
              refine (trans _ (univ0_pic_downcastmon Adom22 Adom00 _ _ ax01)).
              by apply hdomdown.
          }
          (unshelve refine (sprr (ih (Acod0 _) (u0_sub_cod _ _ _) _ _ _ _ _) _ _))=> //.
          apply Acod00; do 2 (unshelve apply: univ0_pic_downcastmon)=> //.
          apply Acod11; (unshelve apply: univ0_pic_downcastmon)=> //.
          by apply: Acod22.
          exact ⌞by01⌟.
    - apply eq_sym in eq1; induction eq1.
      move=> A22 ; split=> v vv.
      + rewrite (el0_pic_fdowncast_err' Ap00).
        apply: smaller; by unshelve apply: univ0_pic_downcastmon.
      + rewrite (el0_pic_fupcast_err' _ _ Ap00).
        apply: trans.
        (unshelve apply: el0_pic_uperr)=> //.
        1: by apply: smallest_refl.
        apply: smaller; move: vv; apply: univ0_pic_upcastmon=> //.
    - apply eq_sym in eq0; induction eq0.
      move=> A22; split=> v vv.
      + rewrite (el0_pic_fdowncast_err _ _ Ap11).
        apply: smaller; move: vv; apply: univ0_pic_downcastmon=> //.
      + rewrite (el0_pic_fupcast_err Ap11).
        apply: trans.
        (unshelve apply: el0_pic_uperr)=> //.
        1: by apply: smallest_refl.
        apply: smaller; move: vv; apply: univ0_pic_upcastmon=> //.
  Qed.



  Lemma el0_pic_fdowncast_comp
        A0 (A00 : u0_pic A0 A0)
        A1 (A11 : u0_pic A1 A1)
        A2 (A22 : u0_pic A2 A2) v :
    el0_pic_ippo A2 A22 v v ->
    el0_pic_ippo A0 A00
                 (fdowncast el0_fwc@{v} A0 A1 (fdowncast el0_fwc@{v} A1 A2 v))
                 (fdowncast el0_fwc@{v} A0 A2 v).
  Proof. apply: (sprl (el0_pic_cast_comp A00 A11 A22)). Qed.


  Lemma el0_pic_fupcast_comp
        A0 (A00 : u0_pic A0 A0)
        A1 (A11 : u0_pic A1 A1)
        A2 (A22 : u0_pic A2 A2) v :
    el0_pic_ippo A0 A00 v v ->
    el0_pic_ippo A2 A22
                 (fupcast el0_fwc@{v} A1 A2 (fupcast el0_fwc@{v} A0 A1 v))
                 (fupcast el0_fwc@{v} A0 A2 v).
  Proof. apply: (sprr (el0_pic_cast_comp A00 A11 A22)). Qed.


  (** The first universe equipped with type precision satisfy the adequate hypotheses to build unknown in the second universe *)

  Definition univ0_pic_UH : unknown.unknown_hypotheses el0_pic_ippo.
  Proof.
    constructor.
    - apply: el0_pic_downerr.
    - move=> ????? vv; apply: trans.
      move: vv. apply: univ0_pic_downcastmon=> //.
      apply: greater. apply: univ0_pic_downcastmon=> //.
      apply: greatest_refl.
      Unshelve. assumption.
    - apply: el0_pic_fdowncast_comp.
    - move=> ?????? vv.
      (unshelve apply: univ0_pic_downcasthmon)=> //.
      by apply: prec_iprec.
    - move=> A0 A1 B A00 A11 BB a0 a1 a01.
      apply: trans.
      (apply: univ0_pic_downcastmon)=> //.
      apply: (hrel_downcast a01).
      apply: el0_pic_fdowncast_comp=> //.
      exact (hrel_urefl a01).
      Unshelve. all:assumption.
    - move=> A0 A1; move: A1 A0.
      apply: (sFixWf (R:=u0_subterm))=> A1 ih A0 a0 a1 A00 A11.
      move: (prec_u0_pic_inv_diag A00) (prec_u0_pic_inv_diag A11) =>
            [[Ap00]|[Adom0 [Acod0 [eq0 [[Adom00 Acod00]]]]]]
            [[Ap11]|[Adom1 [Acod1 [eq1 [[Adom11 Acod11]]]]]].
      + apply: (@unknown.isrel_RElH _ _ _ _ _ _ univ0_UH)=> //.
      + apply eq_sym in eq1; induction eq1=> a01 a11.
        do 3 split=> //; first exact ⌊a01⌋.
        rewrite (el0_pic_fupcast_err' _ _ Ap00).
        apply: (smaller _ a11).
      + apply eq_sym in eq0; induction eq0=> a01 a11.
        do 3 split=> //; first exact ⌊a01⌋.
        rewrite (el0_pic_fupcast_err Ap11).
        apply: (smaller _ a11).
      + apply eq_sym in eq0; induction eq0;
        apply eq_sym in eq1; induction eq1=> a01 a11.
        do 3 split=> //; first exact ⌊a01⌋.
        change (srel_at _ _ ?x ?y) with (pi_ppo (el0_pic_to_ippo Adom11 Acod11) x y).
        split; first split.
        * by case: (univ0_pic_upcastmon A00 A11 _ _ ⌊a01⌋).
        * by case: a11.
        * move=> x0 x1 x01.
          set y0 := (fdowncast el0_fwc@{v} Adom0 Adom1 x0).
          set y1 := (fdowncast el0_fwc@{v} Adom0 Adom1 x1).
          pose proof (y01 := univ0_pic_downcastmon Adom11 Adom00 _ _ x01).
          pose proof (hdomih := ih Adom1 (u0_sub_dom _ _) Adom0 y1 x1 Adom00 Adom11
                             (univ0_pic_downcastmon Adom11 Adom00 _ _ ⌈x01⌉)
                             ⌈x01⌉).
          pose proof (hdom := hrel_upcast hdomih : el0_pic_ppo Adom11 _ _).

          set z1 := (fupcast el0_fwc@{v} Adom0 Adom1 y1).
          pose proof (z01 := trans (univ0_pic_upcastmon Adom00 Adom11 _ _ y01) hdom).
          move: a01.
          change (el0_pic_ippo _ _ ?x ?y) with (pi_ppo (el0_pic_to_ippo Adom00 Acod00) x y).
          move=> [[af00 _] af01].
          pose proof (h := af01 _ _ y01).
          move: a11.
          change (el0_pic_ippo _ _ ?x ?y) with (pi_ppo (el0_pic_to_ippo Adom11 Acod11) x y).
          move=> [_ af11].

          pose proof (h' := (af11 _ _ hdom)).
          unshelve epose (i := iprec_trans _ h (univ0_pic_downcasthmon _ h')).
          2: apply: univ0_pic_downcastmon=> //; exact ⌈x01⌉.
          1: exact y01.
          1: exact (Acod00 _ _ ⌈y01⌉).
          1: exact (Acod11 _ _ hdom).
          unshelve epose (ih (Acod1 x1) (u0_sub_cod _ _ _) (Acod0 y0) _ _
                    (Acod00 _ _ ⌊y01⌋)
                    (Acod11 _ _ ⌈x01⌉)
                    (trans (eprel_downcast i) _)
                    ⌜af11 _ _ ⌈x01⌉⌝
                ).
          1:{ move: ⌜af11 _ _ ⌈x01⌉⌝; apply: el0_pic_fdowncast_comp.
              exact (Acod00 _ _ ⌊y01⌋).
              exact (Acod00 _ _ ⌈y01⌉).
              exact (Acod11 _ _ ⌈x01⌉). }
          apply: to_eprel.
          1:{
            pose (a00 := spair (spair af00 af00) af00 : pi_ppo _ _ _).
            move: (univ0_pic_upcastmon A00 A11 _ _ a00).
            change (el0_pic_ippo _ _ ?x ?y) with (pi_ppo (el0_pic_to_ippo Adom11 Acod11) x y).
            move=> [_ uaf00].
            exact (hrel_urefl (uaf00 _ _ ⌊x01⌋)).
          }
          refine (trans _ (hrel_upcast i0 : el0_pic_ppo (Acod11 _ _ ⌈x01⌉) _ _)).
          move: (iprec_refl (af00 _ _ ⌊y01⌋)).
          apply: el0_pic_fupcast_comp.
          exact (Acod00 _ _ ⌊y01⌋).
          exact (Acod11 _ _ ⌊x01⌋).
          exact (Acod11 _ _ ⌈x01⌉).
          Unshelve. assumption.
    - move=> ??. apply: smaller; apply: greatest_refl.
  Qed.


  (** Upcast and the symmetric downcast between types related
      by precision are extensionally equal *)

  (* On universes, the conventional distinction between upcast and downcast
     is extensionally irrelevant *)
  (* Note that as a consequence of upper decomposition below,
     a similar theorem would hold with A and B merely self-precise
     rather than related by precision *)

  Lemma el0_pic_up_down_sym (A B : u0) (AB : u0_pic A B) :
    (forall (a : el0 A) (aa : el0_pic_ppo ⌊AB⌋ a a),
        fdowncast el0_fwc@{v} B A a ≈[el0_pic_ppo ⌈AB⌉] fupcast el0_fwc@{v} A B a) s/\
      (forall (b : el0 B) (bb : el0_pic_ppo ⌈AB⌉ b b),
    fupcast el0_fwc@{v} B A b ≈[el0_pic_ppo ⌊AB⌋] fdowncast el0_fwc@{v} A B b).
  Proof.
    pattern A, B, AB. set G := (fun _ => _).
    assert (H : forall A B (AB : u0_ppo A B), G A B (inj_prec_u0 AB)).
    { move=> ??/wrefl ???; split; first by apply: el0_up_down_sym.
      move=> ??; apply:sym_sym; unshelve apply: el0_up_down_sym; eassumption. }
    unfold G; elim/u0selim: B A AB=> [||||?|A1 B1 ihA1 ihB1].
    1-5: move=> ? /invert; apply: H=> //.
    clear G H.
    elim/u0selim=> [||||?|A0 B0 _ _].
    2-5: move=> /invert; inversion H.
    - move=> h; split=> a aa //.
      rewrite el0_fdowncast_err el0_fupcast_err.
      apply: sym_refl; apply: smallest_refl.
      refine (el0_pic_smallest ⌈h⌉).
    - move=> h; move: (prec_u0_pic_inv_pi_pi h)=> [[[A01 B00] B11] B01].
      pose proof (A11 := prec_u0_pic_urefl A01);
      pose proof (A00 := prec_u0_pic_lrefl A01); split.
      + move=> f; rewrite el0_fdowncast_pi el0_fupcast_pi.
        pose (R0 := el0_pic_to_ippo (prec_u0_pic_lrefl A01) B00).
        pose (R1 := el0_pic_to_ippo (prec_u0_pic_urefl A01) B11).
        change (el0_pic_ppo _ f f) with (pi_ppo@{v v} R0 f f)=> ff.
        change (el0_pic_ppo _) with (pi_ppo@{v v} R1).
        unshelve refine (pi_cast_equiv@{v} (RBX:=R0) _ _ _ _ _ (sprr ff)).
        * by apply: univ0_pic_downcastmon.
        * apply: equiv_helper.
          1: by apply: univ0_pic_upcastmon.
          1: by apply: univ0_pic_downcastmon.
          exact (sprr (ihA1 A0 A01)).
        * move=> ?? a01 ?? b01.
          refine (univ0_pic_upcasthmon _ b01).
          exact (B00 _ _ (univ0_pic_downcastmon A11 A00 _ _ a01)).
          exact (B11 _ _ a01).
        * move=> a aa b0 b1 b01.
          pose proof (Baa := B11 _ _ aa).
          apply: iequiv_equiv.
          apply: iequiv_trans.
          1: apply: (univ0_pic_downcasthequiv (spair Baa Baa) b01).
          apply: (sym_mon B00).
          apply: (sprr (ihA1 A0 A01) _ aa).
          apply: equiv_iequiv.
          pose proof (daa := iprec_dl A0 (A01 : u0_pic _ _) A01 (prec_iprec el0_pic_ippo _ _ _ _ aa)).
          apply: (sprl (ihB1 a _ (B01 _ _ daa))).
          exact (eprel_urefl (sprl b01)).
          Unshelve. by apply: sym_refl. assumption.

      (* symmetric case *)
      + move=> f; rewrite el0_fdowncast_pi el0_fupcast_pi.
        pose (R0 := el0_pic_to_ippo (prec_u0_pic_lrefl A01) B00).
        pose (R1 := el0_pic_to_ippo (prec_u0_pic_urefl A01) B11).
        change (el0_pic_ppo _ f f) with (pi_ppo@{v v} R1 f f)=> ff.
        change (el0_pic_ppo _) with (pi_ppo@{v v} R0).
        unshelve refine (pi_cast_equiv@{v} (RBX:=R1) _ _ _ _ _ (sprr ff)).
        * by apply: univ0_pic_upcastmon.
        * apply: equiv_helper.
          2: by apply: univ0_pic_upcastmon.
          1: by apply: univ0_pic_downcastmon.
          exact (sprl (ihA1 A0 A01)).
        * move=> ?? a01 ?? b01.
          refine (univ0_pic_downcasthmon _ b01).
          exact (B00 _ _ a01).
          exact (B11 _ _ (univ0_pic_upcastmon A00 A11 _ _ a01)).
        * move=> a aa b0 b1 b01.
          pose proof (Baa := B00 _ _ aa).
          apply: iequiv_equiv; apply: iequiv_trans.
          1: apply: (univ0_pic_upcasthequiv (spair Baa Baa) b01).
          apply: (sym_mon B11).
          apply: (sprl (ihA1 A0 A01) _ aa).
          apply: equiv_iequiv.
          pose proof (daa := iprec_ur A1 (A01 : u0_pic _ _) A01 (prec_iprec el0_pic_ippo _ _ _ _ aa)).
          apply: (sprr (ihB1 _ _ (B01 _ _ daa))).
          exact (eprel_urefl (sprl b01)).
          Unshelve. by apply: sym_refl. assumption.
  Qed.

  Lemma el0_pic_down_up_eqv (A B : u0) (AB : u0_pic A B)
        (a : el0 A) (aa : el0_pic_ppo ⌊AB⌋ a a) :
    fdowncast el0_fwc@{v} B A a ≈[el0_pic_ppo ⌈AB⌉] fupcast el0_fwc@{v} A B a.
  Proof. exact (sprl (@el0_pic_up_down_sym A B AB) a aa). Qed.

  Lemma el0_pic_up_down_eqv (A B : u0) (AB : u0_pic A B)
        (b : el0 B) (bb : el0_pic_ppo ⌈AB⌉ b b) :
    fupcast el0_fwc@{v} B A b ≈[el0_pic_ppo ⌊AB⌋] fdowncast el0_fwc@{v} A B b.
  Proof. exact (sprr (@el0_pic_up_down_sym A B AB) b bb). Qed.


  Lemma el0_pic_isrel_upper_decomposition A0 A1 X
        (XX : prec_u0_pic X X)
        (A0X : u0_pic A0 X)
        (A1X : u0_pic A1 X)
        a0 a1 :
    i[srel_at el0_pic_ippo] A0 A1 a0 a1 ->
      el0_pic_ippo X XX (fupcast el0_fwc@{v} A0 X a0) (fupcast el0_fwc@{v} A1 X a1).
  Proof.
    move=> a01.
    epose (@unknown.fdownrel _ _ _ _ _ _
                              univ0_pic_UH _ _
                              X ⌊A0X⌋ ⌊A1X⌋ XX _ _ a01).
    apply: trans; last (apply: trans; first exact p).
    exact (sprr (el0_pic_down_up_eqv A0X a0 (isrel_lrefl a01))).
    exact (sprl (el0_pic_down_up_eqv A1X a1 (isrel_urefl a01))).
  Qed.

  Lemma el0_pic_upper_decomposition_isrel A0 A1 X
        (XX : prec_u0_pic X X)
        (A0X : u0_pic A0 X)
        (A1X : u0_pic A1 X)
        a0 a1 :
    el0_pic_ppo ⌊A0X⌋ a0 a0 ->
    el0_pic_ppo ⌊A1X⌋ a1 a1 ->
    el0_pic_ippo X XX (fupcast el0_fwc@{v} A0 X a0) (fupcast el0_fwc@{v} A1 X a1) ->
    i[srel_at el0_pic_ippo] A0 A1 a0 a1.
  Proof.
    move=> a00 a11 a01.
    unshelve apply: (@unknown.isrel_RElH _ _ _ _ _ _
                                          univ0_pic_UH _ _ _ _ ⌊A0X⌋ ⌊A1X⌋ _ a11).
    apply: trans.
    2: apply: (el0_pic_fdowncast_comp ⌊A0X⌋ ⌈A0X⌉ ⌊A1X⌋ a1 a11).
    apply: iprec_refl.
    (unshelve apply: iprec_dr)=> //.
    apply: iprec_right; last exact (sprr (el0_pic_down_up_eqv A1X a1 a11)).
    apply: to_eprel=> //.
  Qed.

  (** Upper decomposition for universe 0 (section 3 of the paper) *)

  Lemma isrel_univ0_decomposition A0 A1 X (XX : prec_u0_pic X X) a0 a1
        (A0X : u0_pic A0 X)
        (A1X : u0_pic A1 X) :
    i[srel_at el0_pic_ippo] A0 A1 a0 a1 s<->
    el0_pic_ppo ⌊A0X⌋ a0 a0 s/\
      el0_pic_ippo X XX (fupcast el0_fwc@{v} A0 X a0)
                   (fupcast el0_fwc@{v} A1 X a1) s/\
      el0_pic_ppo ⌊A1X⌋ a1 a1.
  Proof.
    split.
    - move=> h; repeat split.
      + exact (isrel_lrefl h).
      + exact (el0_pic_isrel_upper_decomposition XX A0X A1X h).
      + exact (isrel_urefl h).
    - move=> [[??]?]; apply: el0_pic_upper_decomposition_isrel; eassumption.
  Qed.

  Lemma upper_decomposition_helper
    A0 A1 X (XX : prec_u0_pic X X) a0
    (A0X : u0_pic A0 X)
    (A1X : u0_pic A1 X) :
    el0_pic_ppo ⌊A0X⌋ a0 a0 ->
    fdowncast el0_fwc@{v} A1 X (fdowncast el0_fwc@{v} X A0 a0)
              ≈[ el0_pic_ppo ⌊A1X⌋ ]
              fdowncast el0_fwc@{v} A1 A0 a0.
  Proof.
    move=> a00; split.
    apply: (@unknown.fdowncomp _ _ _ _ _ _ univ0_pic_UH A1 ⌊A1X⌋ X XX A0 ⌊A0X⌋ a0 a00).
    apply: (iprec_refl@{v} (RB:=el0_pic_ippo) (aa:=⌊A1X⌋)).
    (unshelve apply: (@unknown.fdownmon _ _ _ _ _ _ univ0_pic_UH))=> //.
    apply: iprec_right.
    2: apply: (sprr (el0_pic_down_up_eqv A0X a0 a00)).
    apply: eprel_up_right. assumption.
  Qed.

  Lemma upper_decomposition
    A0 A1 X (XX : prec_u0_pic X X) a0
    (A0X : u0_pic A0 X)
    (A1X : u0_pic A1 X) :
    el0_pic_ppo ⌊A0X⌋ a0 a0 ->
    fdowncast el0_fwc@{v} A1 X (fupcast el0_fwc@{v} A0 X a0)
              ≈[ el0_pic_ppo ⌊A1X⌋ ]
              fdowncast el0_fwc@{v} A1 A0 a0.
  Proof.
    move=> a00. unshelve apply: sym_trans.
    3: by unshelve apply: upper_decomposition_helper.
    unshelve apply: (sym_mon (univ0_pic_downcastmon XX ⌊A1X⌋)).
    unshelve apply: sym_sym.
    apply: (el0_pic_down_up_eqv A0X a0 a00).
  Qed.

  (** Failure of threesomes (Example 4 of the paper) *)
  Section U0PicBeckChevalleyCounterExample.

    Definition X1 := arrU0 propu0 propu0.

    Equations B2 (b : pbool@{u}) : u0 :=
    | ptrue => propu0
    | pfalse => propu0
    | punkbool => unku0
    | perrbool => erru0.

    Lemma B2mon0 : monotone bool_ppo u0_ppo B2.
    Proof.
      move=> ?? []; cbn; try do 2 constructor=> //.
      case; constructor=> //.
    Qed.

    Lemma B2mon : monotone bool_ppo u0_pic B2.
    Proof. move=> ?? []; cbn; try do 2 constructor=> //; case: b=> //. Qed.

    Definition X2 := piu0 boolu0 B2.

    Lemma X1self : u0_pic X1 X1.
    Proof. do 3 constructor. Qed.

    Lemma X2self : u0_pic X2 X2.
    Proof.
      assert (u0_pic boolu0 boolu0) by do 2 constructor.
      constructor=> //.
      1-2: apply: B2mon.
      move=> ??; change (i[_] _ _ ?a ?a') with (iprec@{v} el0_pic_ippo H a a').
      move=> h; move: (iprec_refl h); apply: B2mon.
    Qed.

    Definition X1meetX2 := arrU0 erru0 erru0.
    Lemma X1meetX2self : u0_pic X1meetX2 X1meetX2.
    Proof. by do 3 constructor. Qed.

    Lemma X1meetX2toX1 : u0_pic X1meetX2 X1.
    Proof. by do 3 constructor. Qed.

    Lemma X1meetX2toX2 : u0_pic X1meetX2 X2.
    Proof.
      constructor=> //.
      3: apply: B2mon.
      1-2: by do 2 constructor.
      constructor; apply: smaller.
      exact (B2mon0  (isrel_urefl H)).
    Qed.

    Lemma downcast_err_eq A x : fdowncast el0_fwc@{v} A erru0 x = err.
    Proof.
      elim/u0elim: A=> [||||?|A B??]; reflexivity.
    Qed.

    Lemma X1meetX2_min X : u0_pic X X1 -> u0_pic X X2 -> u0_pic X X1meetX2.
    Proof.
      elim/u0selim: X=> [||||?|A B??].
      1: repeat constructor.
      1-4: move=> /invert; inversion H.
      move=> /prec_u0_pic_inv_pi_pi [[[AA1 ?]?] BB1].
      move=> /prec_u0_pic_inv_pi_pi [[[AA2 ?]?] BB2].
      constructor=> //.
      + inversion AA1. inversion AA2. constructor.
        exact (meet_smallest H H2).
      + by do 2 constructor.
      + move=> ?? aa'; move: (isrel_downcast aa').
        rewrite downcast_err_eq=> aerr.
        pose proof (aerr1 := BB1 _ _ (eprel_err_inv (AXY:= ippo_on el0_pic_ippo AA1) aerr)).
        pose proof (aerr2 := BB2 _ _ (eprel_err_inv (AXY:= ippo_on el0_pic_ippo AA2) aerr)).
        inversion aerr1; inversion aerr2; subst.
        constructor; exact (meet_smallest H H2).
    Qed.

    Definition f : el0 X1 := fun _ => sUnit.
    Lemma fmon : el0_pic_ppo X1self f f.
    Proof.
      cbn; red. set lemma := (pi_rel0 _ _ _ _ _).
      enough lemma; first by (split; first split).
      move=> ???; repeat split; constructor.
    Qed.

    Definition X1joinX2 := arrU0 unku0 unku0.
    Lemma X1joinX2self : u0_pic X1joinX2 X1joinX2.
    Proof. by do 3 constructor. Qed.

    Lemma X1toX1joinX2 : u0_pic X1 X1joinX2.
    Proof. by do 3 constructor. Qed.

    Lemma X2toX1joinX2 : u0_pic X2 X1joinX2.
    Proof.
      constructor=> //.
      2: apply: B2mon.
      1-2: by do 2 constructor.
      constructor; apply: greater.
      exact (B2mon0  (isrel_lrefl H)).
    Qed.

    (* Not literally needed since all upper decompositions of casts
       are equivalent by ippo_cast_equiv_above *)
    (* Lemma X1meetX2_max X : u0_pic X1 X -> u0_pic X2 X -> u0_pic X1joinX2 X. *)


    Lemma cast_unk_ftrue :
      fdowncast el0_fwc@{v} X2 X1joinX2
                    (fupcast el0_fwc@{v} X1 X1joinX2 f) ptrue = sUnit.
    Proof. reflexivity. Qed.

    Lemma cast_meet_ftrue :
      fupcast el0_fwc@{v} X1meetX2 X2
                  (fdowncast el0_fwc@{v} X1meetX2 X1 f) ptrue = err.
    Proof. reflexivity. Qed.

    Lemma BC_u0_counterexample :
      snot (mate_invertible el0_pic_ippo X1meetX2 X1 X2 X1joinX2
                            X1self X2self
                            X1meetX2toX1 X1toX1joinX2
                            X1meetX2toX2 X2toX1joinX2).
    Proof.
      move=> /(_ f fmon) [_] /(_ ptrue ptrue truerefl)[[_ /(_ stt) + ] _] //.
    Qed.

  End U0PicBeckChevalleyCounterExample.



End U0.
