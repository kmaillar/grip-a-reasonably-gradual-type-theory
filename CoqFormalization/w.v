From Coq Require Import ssreflect ssrfun.
From Equations Require Import Equations.
From PartialPreorder Require Import strictPropLogic.
From PartialPreorder Require Import prelude ppoDef.

Set Implicit Arguments.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Primitive Projections.
Import SPropNotations.


Inductive W@{u} (A : Type@{u}) (B : A -> Type@{u}) : Type@{u} :=
| wunk : W B
| werr : W B
| wsup (a : A) (k : B a -> W B) : W B.

Arguments wunk {_ _}.
Arguments werr {_ _}.

#[global]
Instance err@{u} (A : Type@{u}) (B : A -> Type@{u}) : Err@{u} (W B) := werr.

#[global]
Instance dyn@{u} (A : Type@{u}) (B : A -> Type@{u}) : Dyn@{u} (W B) := wunk.

Section CastW.
  Universe u.
  Context (A0 A1 : Type@{u}) (castA01 : A0 -> A1)
          (B0 : A0 -> Type@{u}) (B1 : A1 -> Type@{u})
          (castB01 : forall a0, B1 (castA01 a0) -> B0 a0).

  Equations cast  (w : W B0) : W B1 :=
  | wunk => wunk
  | werr => werr
  | wsup a0 k => wsup (castA01 a0) (fun b1 => cast (k (castB01 b1))).

End CastW.

Section Rel.
  Universe u.
  Context (A : Type@{u}) (RA : srel A)
          (B : fwc@{u u} A) (RB : forall a, srel (B a)).

  Inductive wprec : W B -> W B -> SProp :=
  | werrrefl : wprec werr werr
  | werrany w : wprec w w -> wprec werr w
  | wunkrefl : wprec wunk wunk
  | wanyunk w : wprec w w -> wprec w wunk
  | wsupcong a a' k k' :
    RA a a' ->
    (forall b0 b1, i[RB] a a b0 b1 -> wprec (k b0) (k b1)) ->
    (forall b0 b1, i[RB] a' a' b0 b1 -> wprec (k' b0) (k' b1)) ->
    (forall b b', i[RB] a a' b b' -> wprec (k b) (k' b')) ->
    wprec (wsup a k) (wsup a' k').


  Lemma wprec_sup_inv a (k : B a -> W B) w :
    wprec (wsup a k) w ->
    (w = @wunk _ B) +
          { a' : A & { k' : B a' -> W B &
                                   ((w = wsup a' k') *
                                   Box (
                                       RA a a' s/\
                                         (forall b0 b1, i[RB] a a b0 b1 -> wprec (k b0) (k b1)) s/\
    (forall b0 b1, i[RB] a' a' b0 b1 -> wprec (k' b0) (k' b1)) s/\
    (forall b b', i[RB] a a' b b' -> wprec (k b) (k' b'))))%type

              }}.
  Proof.
    destruct w.
    - by left.
    - move=> h; sexfalso; inversion h.
    - move=> h; right; exists a0, k0; split=> //.
      constructor.
      pattern a, k, a0, k0.
      set P := fun _ => _.
      refine match h in wprec w1 w2 return
                 match w1, w2 with
                 | wsup a k, wsup a' k' => P a k a' k'
                 | _, _ => sUnit
                 end with
           | wsupcong _ _ _ _ _ _ => _
           | _ => _
           end.
      4: destruct w.
      1-6: exact stt.
      unfold P; clear P; repeat split=> //.
  Qed.

End Rel.

Section WPpo.
  Universe u.
  Context (A : Type@{u}) (RA : ppo A)
          (B : fwc@{u u} A) (RB : ippo RA B).

  Let wprec := wprec RA B (srel_at RB).

  Lemma wprec_upper_refl : upper_reflexive wprec.
  Proof.
    move=> w1 w2; elim=> //.
    1-3: constructor.
    move=> a a' k k' aa' kk ihkk k'k' ihk'k' kk' ihkk'.
    constructor=> //.
    apply: upper_refl; eassumption.
  Qed.

  Lemma wprec_lower_refl : lower_reflexive wprec.
  Proof.
    move=> w1 w2; elim=> //.
    1-3: constructor.
    move=> a a' k k' aa' kk ihkk k'k' ihk'k' kk' ihkk'.
    constructor=> //.
    apply: lower_refl; eassumption.
  Qed.

  Lemma wprec_trans : transitive wprec.
  Proof.
    move=> w1 w2 w3 h; elim: h w3 (h)=> //.
    - move=> ? _ _ ? _ /wprec_upper_refl; by constructor.
    - move=> ? ? _ ? _ h; inversion h; by constructor.
    - move=> a0 a1 k0 k1 a01 k00 ihk00 k11 ihk11 k01 ihk01 ? /wprec_lower_refl ? h.
      move: (wprec_sup_inv h)=> [-> |[a2 [k2 [-> [[[[a12 _] k22] k12]]]]]].
      1: by constructor.
      assert (a02 : RA a0 a2) by (apply: trans; eassumption).
      constructor=> //.
      move=> b0 b2; intro_iprec=> b02.
      assert (b01 : iprec RB a01 b0 (fupcast B a0 a1 b0)).
      { apply: iprec_ur; first eassumption.
        apply: prec_iprec; apply: eprel_lrefl; eassumption. }
      unshelve refine (ihk01 b0 _ b01 _ _ _).
      + by apply: k01.
      + apply k12; intro_iprec; (unshelve apply: iprec_ul)=> //.
  Qed.

End WPpo.



(* Section HRel. *)
(*   Universe u. *)
(*   Context (A0 A1 : Type@{u}) (RA01 : A0 -> A1 -> SProp) *)
(*           (B0 : A0 -> Type@{u}) (B1 : A1 -> Type@{u}) *)
(*           (RB01 : forall a0 a1, B0 a0 -> B1 a1 -> SProp). *)

(*   Inductive whprec : W B0 -> W B1 -> SProp := *)
(*   | werrrefl : wprec werr werr *)
(*   | werrany w : wprec w w -> wprec werr w *)
(*   | wunkrefl : wprec wunk wunk *)
(*   | wanynk w : wprec w w -> wprec w wunk *)
(*   | wsupcong a a' k k' : *)
(*     RA a a' -> *)
(*     (forall b b', i[RB] a a' b b' -> wprec (k b) (k' b')) -> *)
(*     wprec (wsup a k) (wsup a' k'). *)

(* End Rel. *)
