From Coq Require Import ssreflect ssrfun.
From Equations Require Import Equations.
From PartialPreorder Require Import strictPropLogic.
From PartialPreorder Require Import prelude ppoDef.

Set Implicit Arguments.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Primitive Projections.
Import SPropNotations.


(** Cast operations between non-dependent function types and corresponding eppairs *)

Section ArrCast.
  Universe u.
  Context (A A' B B' : Type@{u}) (castA : A' -> A) (castB : B -> B').
  Definition arr_cast (f : A -> B) : A' -> B' :=
    fun a' => castB (f (castA a')).
End ArrCast.

Section ArrEppair.
  Universe u.
  Context (A A' B B' : Type@{u})
          (pA : ppo@{u} A)
          (pA' : ppo@{u} A')
          (pB : ppo@{u} B)
          (pB' : ppo@{u} B')
          (AA' : eppair@{u} pA pA') (BB' : eppair@{u} pB pB').

  Definition arr_upcast : (A -> B) -> (A' -> B') :=
    arr_cast (downcast AA') (upcast BB').

  Lemma arr_upcast_preserves_mon f : monotone pA pB f -> monotone pA' pB' (arr_upcast f).
  Proof. move=> monf ???; by apply upmon, monf, downmon. Qed.

  Lemma arr_upcast_mon : monotone (arr_ppo pA pB) (arr_ppo pA' pB') arr_upcast.
  Proof.
    move=> f g [[??] fg]; split.
    1: split; by apply arr_upcast_preserves_mon.
    move=> ???; by apply upmon, fg, downmon.
  Qed.

  Definition arr_downcast : (A' -> B') -> (A -> B) :=
    arr_cast (upcast AA') (downcast BB').

  Lemma arr_downcast_preserves_mon f : monotone pA' pB' f -> monotone pA pB (arr_downcast f).
  Proof. move=> monf ???; by apply downmon, monf, upmon. Qed.

  Lemma arr_downcast_mon : monotone (arr_ppo pA' pB') (arr_ppo pA pB) arr_downcast.
  Proof.
    move=> f g [[??] fg]; split.
    1: split; by apply arr_downcast_preserves_mon.
    move=> ???; by apply downmon, fg, upmon.
  Qed.

  #[program]
  Definition arr_eppair : eppair (arr_ppo pA pB) (arr_ppo pA' pB') :=
    mkEppair _ _ arr_upcast_mon arr_downcast_mon _ _ _.
  Next Obligation.
    move=> f g [[monf _] _] [[_ mong] ufg]; split; first split=> //.
    1: by apply arr_downcast_mon.
    move=> a a' aa'; assert (aa : pA a a) by (apply: lower_refl ; eassumption).
    apply: uptodown; first apply monf=> //.
    apply: trans; last (apply: ufg; apply: upmon; eassumption).
    by apply upmon, monf, unit.
  Qed.
  Next Obligation.
    move=> f g [[mong _] _] [[monf _] ufg]; split; first split=> //.
    1: by apply arr_upcast_mon.
    move=> a a' aa'; assert (aa : pA' a' a') by (apply: upper_refl ; eassumption).
    apply: downtoup; first apply mong=> //.
    apply: trans.
    1: apply: ufg; apply: downmon; eassumption.
    by apply downmon, mong, counit.
  Qed.
  Next Obligation.
    move=> f [[monf _] _]; split; first split=> //.
    1: by apply arr_downcast_mon, arr_upcast_mon.
    move=> ???; apply: trans.
    1: apply retract, monf, downmon, upmon; apply: lower_refl; eassumption.
    apply monf. apply: trans; first (apply: retract; apply: lower_refl).
    all: eassumption.
  Qed.

End ArrEppair.
