From Coq Require Import ssreflect ssrfun.
From Equations Require Import Equations.
From PartialPreorder Require Import strictPropLogic.
From PartialPreorder Require Import prelude ppoDef.

Set Implicit Arguments.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Primitive Projections.
Import SPropNotations.

(** Partial preorder on non-dependent cartesian product *)

Section CartesianProduct.
  Universe u.
  Context (A B : Type@{u}) (RA : ppo@{u} A) (RB : ppo@{u} B).

  Record prod : Type@{u} := { p1 : A ; p2 :  B }.
  Definition prec_prod : srel prod :=
    fun x y => RA (p1 x) (p1 y) s/\ RB (p2 x) (p2 y).

  Lemma prec_prod_trans : transitive prec_prod.
  Proof. move=> ??? [??] [??]; split; apply: trans; eassumption. Qed.

  Lemma prec_prod_urefl : upper_reflexive prec_prod.
  Proof. move=> ?? [??]; split; apply: upper_refl; eassumption. Qed.

  Lemma prec_prod_lrefl : lower_reflexive prec_prod.
  Proof. move=> ?? [??]; split; apply: lower_refl; eassumption. Qed.

  Definition prec_prod_is_ppo : is_ppo@{u} prec_prod :=
    mkIsPpo prec_prod prec_prod_trans prec_prod_urefl prec_prod_lrefl.

  Definition prod_ppo := mkPpo prec_prod_is_ppo.

  Context `{Err A} `{Err B}.
  Instance err_prod : Err prod := {| p1 := err ; p2 := err |}.

  Context `{!SmallestElem RA} `{!SmallestElem RB}.
  Instance smallest_prod : SmallestElem prod_ppo.
  Proof.
    constructor.
    + split; apply: smallest_refl.
    + move=> ? [??]; split; apply: smaller; eassumption.
  Qed.

  Context `{Dyn A} `{Dyn B}.
  Instance dyn_prod : Dyn prod := {| p1 := dyn ; p2 := dyn |}.

  Context `{!GreatestElem RA} `{!GreatestElem RB}.
  Instance greatest_prod : GreatestElem prod_ppo.
  Proof.
    constructor.
    + split; apply: greatest_refl.
    + move=> ? [??]; split; apply: greater; eassumption.
  Qed.

End CartesianProduct.

(** Partial preorder on dependent sum *)

Section SigmaDef.
  Universe u.
  Context (A : Type@{u}) (B : A -> Type@{u}).

  Record sigma : Type@{u} := { d1 : A ; d2 :  B d1  }.

  Context `{Err A} `{forall a, Err (B a)}.
  #[global]
  Instance err_sigma : Err sigma := {| d1 := err ; d2 := err |}.

  Context `{Dyn A} `{forall a, Dyn (B a)}.
  #[global]
  Instance dyn_sigma : Dyn sigma := {| d1 := dyn ; d2 := dyn |}.
End SigmaDef.


Section Sigma.
  Universe u.
  Context (A : Type@{u}) (B : fwc@{u u} A).

  Let sigma := (sigma@{u} B).

  Definition prec_sigma (RA : srel A) (RB : forall a, srel (B a)) : srel sigma :=
    fun x y => sΣ (xy1 : RA (d1 x) (d1 y)), i[RB] _ _ (d2 x) (d2 y).

  Context (RA : ppo@{u} A) (RB : ippo@{u} RA B).

  Let prec_sigma := prec_sigma RA (srel_at RB).

  Lemma intro_prec_sigma x y (xy1 : RA (d1 x) (d1 y))
        (xy2 : iprec RB xy1 (d2 x) (d2 y)) : prec_sigma x y.
  Proof. exists xy1; assumption. Qed.

  Lemma prec_sigma_trans : transitive prec_sigma.
  Proof.
    move=> ??? [??] [??]; unshelve eexists.
    - apply: trans; eassumption.
    - intro_iprec; unshelve apply: iprec_trans; eassumption.
  Qed.

  Lemma prec_sigma_urefl : upper_reflexive prec_sigma.
  Proof.
    move=> ?? [/wrefl ??? /toiprec ?] ; unshelve eexists=> //.
    intro_iprec ; apply: prec_iprec; apply: eprel_urefl; eassumption.
  Qed.

  Lemma prec_sigma_lrefl : lower_reflexive prec_sigma.
  Proof.
    move=> ?? [/wrefl ??? /toiprec]; unshelve eexists=> //.
    intro_iprec; apply: prec_iprec; apply: eprel_lrefl; eassumption.
  Qed.

  Definition prec_sigma_is_ppo : is_ppo@{u} prec_sigma :=
    mkIsPpo prec_sigma prec_sigma_trans prec_sigma_urefl prec_sigma_lrefl.

  Definition sigma_ppo := mkPpo prec_sigma_is_ppo.

  Context `{Err A} `{forall a, Err (B a)}.
  Context `{!SmallestElem RA} `{!forall a aa, SmallestElem (RB a aa)}.
  Instance smallest_sigma : SmallestElem sigma_ppo.
  Proof.
    constructor.
    + unshelve eexists; first apply: smallest_refl.
      intro_iprec; apply: prec_iprec; apply: smallest_refl.
    + move=> ? [??]; unshelve eexists.
      1: apply: smaller; eassumption.
      intro_iprec; apply: eprel_smallest; by apply: iprec_refl.
  Qed.

  Context `{Dyn A} `{forall a, Dyn (B a)}.
  Context `{!GreatestElem RA} `{!forall a aa, GreatestElem (RB a aa)}.
  Instance greatest_sigma : GreatestElem sigma_ppo.
  Proof.
    constructor.
    + unshelve eexists; first apply: greatest_refl.
      intro_iprec; apply: prec_iprec; apply: greatest_refl.
    + move=> ? [??]; unshelve eexists.
      1: apply: greater; eassumption.
      intro_iprec; apply: eprel_greatest; by apply: iprec_refl.
  Qed.

End Sigma.


(** Casts and eppairs between dependent sums *)

Definition cast_sigma@{u}
           (AX AY : Type@{u})
           (BX : AX -> Type@{u})
           (BY : AY -> Type@{u})
           (fXY : AX -> AY)
           (gXY : forall ax, BX ax -> BY (fXY ax))
  : sigma@{u} BX -> sigma BY :=
  fun p => {| d1 := fXY (d1 p) ; d2 := gXY _ (d2 p) |}.

Section SigmaCast.
  Universe u.
  Context (AX AY : Type@{u})
          (BX : fwc@{u u} AX)
          (BY : fwc@{u u} AY)
          (fXY : AX -> AY) (gXY : forall ax, BX ax -> BY (fXY ax)).


  Let cast_sigma := @cast_sigma AX AY BX BY fXY gXY.

  Context (RAX : ppo@{u} AX) (RAY : ppo@{u} AY)
          (RBX : ippo@{u} RAX BX) (RBY : ippo@{u} RAY BY)
          (fmon : monotone RAX RAY fXY)
          (gmon : forall ax ax' (axx' : RAX ax ax') bx bx',
              iprec RBX axx' bx bx' -> iprec RBY (fmon ax ax' axx') (gXY _ bx) (gXY _ bx')).

  Lemma cast_sigma_monotone : monotone (sigma_ppo RBX) (sigma_ppo RBY) cast_sigma.
  Proof.
    move=> ?? [? /toiprec ?]; unshelve eexists; first by apply: fmon.
    intro_iprec; by unshelve apply: gmon.
  Qed.

End SigmaCast.

Section SigmaEppair.
  Universe u.
  Context (AX AY : Type@{u})
          (RAX : ppo@{u} AX) (RAY : ppo@{u} AY)
          (BX : fwc@{u u} AX)
          (BY : fwc@{u u} AY)
          (RBX : ippo@{u} RAX BX) (RBY : ippo@{u} RAY BY)
          (AXY : eppair RAX RAY)
          (BXY : ieppair RBX RBY AXY).

  Definition upcast_sigma :=
    @cast_sigma _ _ BX BY (upcast AXY) (fun ax => iupcast BXY (ax := ax) (upcast AXY ax)).
  Definition downcast_sigma :=
    @cast_sigma _ _ BY BX (downcast AXY) (fun ay => idowncast BXY (ay := ay) (downcast AXY ay)).

  Lemma cast_sigma_is_eppair :
    eppair_prop (sigma_ppo RBX) (sigma_ppo RBY) upcast_sigma downcast_sigma.
  Proof.
    constructor.
    - apply: (cast_sigma_monotone _ (upmon AXY)); apply: iupmon.
    - apply: (cast_sigma_monotone _ (downmon AXY)); apply: idownmon.
    - move=> ?? [? /toiprec ?] [? /toiprec ?]; unshelve eexists;
            first by apply: uptodown.
      intro_iprec; unshelve apply: iuptodown0=> //; last eassumption.
      1: by apply: eprel_up_right.
      apply: eprel_down_left; apply: upper_refl; eassumption.
    - move=> ?? [? /toiprec ?] [? /toiprec ?]; unshelve eexists; first by apply: downtoup.
      intro_iprec; unshelve apply: idowntoup0=> //; last eassumption.
      1: apply: eprel_up_right; apply: lower_refl; eassumption.
      by apply: eprel_down_left.
    - move=> ? [? /toiprec ?]; unshelve eexists; first by apply: retract@{u}.
      by unshelve apply: iretract.
  Qed.

  Definition sigma_eppair := pack_eppair cast_sigma_is_eppair.

End SigmaEppair.
