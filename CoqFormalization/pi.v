From Coq Require Import ssreflect ssrfun.
From Equations Require Import Equations.
From PartialPreorder Require Import strictPropLogic.
From PartialPreorder Require Import prelude ppoDef.

Set Implicit Arguments.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Primitive Projections.
Import SPropNotations.

(** Error, unknown and cast for dependent products *)
Section Bare.
  Universe u.
  Context (A : Type@{u}) (B : A -> Type@{u}).
  #[global]
  Instance pi_err@{} `{forall a, Err (B a)} : Err@{u} (forall a, B a) := fun=> err.
  #[global]
  Instance pi_dyn@{} `{forall a, Dyn (B a)} : Dyn (forall a, B a) := fun=> dyn.

  Context (A' : Type@{u}) (B' : A' -> Type@{u})
          (castA : A' -> A) (castB : forall {a} a', B a -> B' a').
  Definition pi_cast (f : forall a : A, B a) : forall a' : A', B' a' :=
    fun a' => castB _ (f (castA a')).
End Bare.

(** Precision relation on dependent products *)

Section Precision.
  Universe u.
  Context (A : Type@{u}) (RA : srel A)
          (B : fwc@{u u} A) (RB : forall a, srel (B a)).

  Definition pi_rel0 : srel (forall a, B a) :=
      fun f g => forall a a' (aa' : RA a a'), i[ RB ] a a' (f a) (g a').

  Definition prec_pi0 (f g : forall (a : A), B a) :=
    pi_rel0 f f s/\ pi_rel0 g g s/\ pi_rel0 f g.

End Precision.

(** Partial preorder structure on dependent products *)

Section Pi.
  Universe u.
  Context (A : Type@{u}) (RA : ppo@{u} A) (B : fwc@{u u} A) (RB : ippo@{u} RA B).

  Definition pi_rel : srel (forall a, B a) :=
      fun f g => forall a a' (aa' : RA a a'), iprec RB aa' (f a) (g a').

  Lemma pi_rel_eq : pi_rel = pi_rel0 RA B (srel_at RB).
  Proof. reflexivity. Defined.

  Definition prec_pi (f g : forall (a : A), B a) :=
    pi_rel f f s/\ pi_rel g g s/\ pi_rel f g.

  Lemma prec_pi_eq : prec_pi = prec_pi0 RA B (srel_at RB).
  Proof. reflexivity. Defined.

  Lemma prec_pi_trans : transitive prec_pi.
  Proof.
    move=> f g h [[ff gg] fg] [[_ hh] gh]; split=> // a ? /wrefl aa _ aa'.
    refine (iprec_trans _ (fg _ _ aa) (gh _ _ aa'))=> //.
  Qed.

  Lemma prec_pi_urefl : upper_reflexive prec_pi.
  Proof. move=> f g [[_ gg] _]; by split. Qed.

  Lemma prec_pi_lrefl : lower_reflexive prec_pi.
  Proof. move=> f g [[ff _] _]; by split. Qed.

  Definition prec_pi_is_ppo : is_ppo prec_pi :=
    mkIsPpo prec_pi prec_pi_trans prec_pi_urefl prec_pi_lrefl.

  Definition pi_ppo : ppo (forall a, B a) := mkPpo prec_pi_is_ppo.

  Lemma pi_ppo_sym_helper (f g : forall (a : A), B a) :
    pi_rel f f -> pi_rel g g ->
    (forall a (aa : sym RA a a), aa ⊢[ RB ] f a ≈ g a) ->
    f ≈[pi_ppo] g.
  Proof.
    move=> ff gg fg; split; (split; first by split).
    all: move=> a a' /wrefl _ a'a' aa'; case: (fg _ (spair a'a' a'a'))=> /= ??.
    - refine (iprec_trans _ (ff _ _ aa') _); eassumption.
    - refine (iprec_trans _ (gg _ _ aa') _); eassumption.
  Qed.

  Lemma pi_mon_equiv (f g : forall a, B a) :
    pi_rel f f ->
    (forall a (aa : RA a a), f a ≈[RB a aa] g a) ->
    pi_rel g g.
  Proof.
    move=> mf efg x y xy.
    apply: iprec_left; first (apply: sprr; apply: efg).
    apply: iprec_right; last (apply: sprl; apply: efg).
    by apply: mf.
  Qed.

  Lemma pi_ppo_sym_helper' (f g : forall (a : A), B a) :
    pi_rel f f ->
    (forall a (aa : RA a a), f a ≈[RB a aa] g a) ->
    f ≈[pi_ppo] g.
  Proof.
    move=> ff fg; apply: pi_ppo_sym_helper=> //.
    1: exact (pi_mon_equiv _ ff fg).
    move=> ? [aa ?]; split; apply: prec_iprec; by case: (fg _ aa).
  Qed.

  Lemma pi_rel_iequiv0 f g (fg : pi_rel f g) (gf : pi_rel g f) :
    forall a a' (aa' : a ≈[RA] a'), aa' ⊢[RB] f a ≈ g a'.
  Proof.
    move=> ?? [aa' a'a]; split; [apply: fg|apply: gf].
  Qed.

  Lemma pi_rel_iequiv f g (fg : sym pi_ppo f g) :
    forall a a' (aa' : a ≈[RA] a'), aa' ⊢[RB] f a ≈ g a'.
  Proof. case: fg=> [[_ fg] [_ gf]]; by apply: pi_rel_iequiv0. Qed.

  Definition Pi : Ppo := pack_ppo pi_ppo.


  Context `{forall a, Err@{u} (B a)} `{forall a (aa : RA a a), SmallestElem@{u} (RB a aa) }.

  (* This lemma should not be visible outside this section *)
  Lemma pi_smallest_rel_refl : pi_rel err err.
  Proof.
    move=> ?? axy; apply: to_eprel'.
    2: apply: smallest_refl.
    apply smaller, downmon, smallest_refl.
  Qed.

  #[global,program]
  Instance pi_smallest@{} : SmallestElem@{u} pi_ppo :=
    mkSmallest _ err _ _.
  Next Obligation.
    opacify ; pose proof pi_smallest_rel_refl; repeat split=> //.
  Qed.
  Next Obligation.
    opacify.
    move=> f [[_ _] ff];
          split; first (split=> // ; apply pi_smallest_rel_refl).
    move=> ?? aa'; move: (⌜ff _ _ ⌈aa'⌉⌝)=> ?; apply: to_eprel'=> //.
    by apply smaller, downmon.
  Qed.

  Context `{forall a, Dyn@{u} (B a)} `{forall a (aa : RA a a), GreatestElem@{u} (RB a aa) }.

  (* This lemma should not be visible outside this section *)
  Lemma pi_dyn_rel_refl : pi_rel dyn dyn.
  Proof.
    move=> ?? axy; apply: to_eprel.
    1: apply: greatest_refl.
    apply greater, upmon, greatest_refl.
  Qed.

  #[global,program]
   Instance pi_greatest : GreatestElem pi_ppo :=
    mkGreatest _ dyn _ _.
  Next Obligation.
    opacify ; pose proof pi_dyn_rel_refl; repeat split=> //.
  Qed.
  Next Obligation.
    opacify; move=> f [[_ _] ff];
                   split; first (split=> // ; apply pi_dyn_rel_refl).
    move=> ?? aa'. move: ⌜ff _ _ ⌊aa'⌋⌝=> ?.
    apply: to_eprel=> //; by apply greater, upmon.
  Qed.

  Context `{forall a, meetOp@{u} (B a)}.

  #[global]
  Instance pi_meet : meetOp (forall a, B a) :=
    fun f g a => f a ⊓ g a.

  Context `{!forall a aa, isMeet@{u} (RB a aa)}.

  Lemma pi_meet_mon  (f g : forall a, B a) :
    pi_rel f f -> pi_rel g g -> pi_rel (f ⊓ g) (f ⊓ g).
  Proof.
    move=> mf mg ?? /wrefl aa _ aa' /=.
    apply: to_eprel.
    1: apply: meet_refl; apply: iprec_refl; apply: mf + apply: mg.
    apply: trans.
    1:{
      apply: monotone_meet; first apply: upmon.
      all: by (apply mf + apply mg).
    }
    apply: meet_mon; by (apply mf + apply mg).
  Qed.

  #[global]
  Instance pi_has_meet : isMeet pi_ppo.
  Proof.
    unshelve econstructor.
    - move=> f g [_ mf] [_ mg]; split; first (split=> //; by apply pi_meet_mon).
      move=> ?? /wrefl ? _ aa'.
      refine (iprec_left _ _ (mf _ _ aa')).
      apply: meet_smaller_l.
      all: by (apply mf + apply mg).
    - move=> f g [_ mf] [_ mg]; split; first (split=> //; by apply pi_meet_mon).
      move=> ?? /wrefl ? _ aa'.
      refine (iprec_left _ _ (mg _ _ aa')).
      apply: meet_smaller_r.
      all: by (apply mf + apply mg).
    - move=> f g h [[mh mf] hf] [[_ mg] hg].
      split; first (split=> //; by apply pi_meet_mon).
      move=> ?? /wrefl _ ? aa'.
      refine (iprec_right _ (mh _ _ aa') _).
      apply: meet_smallest;
      apply: iprec_refl; by [apply: hf|apply: hg].
  Qed.

  Context `{forall a, joinOp@{u} (B a)}
          `{! forall a aa, isJoin@{u} (RB a aa)}.

  #[global]
  Instance pi_join : joinOp (forall a, B a) :=
    fun f g a => f a ⊔ g a.

  Lemma pi_join_mon (f g : forall a, B a) :
    pi_rel@{u} f f -> pi_rel@{u} g g -> pi_rel (f ⊔ g) (f ⊔ g).
  Proof.
    move=> mf mg ?? /wrefl _ ? aa' /=.
    apply: to_eprel'.
    2:apply: join_refl; apply: iprec_refl;
         (apply mf + apply mg).
    apply: trans.
    2:{
      apply: monotone_join; first apply: downmon.
      all: apply: iprec_refl; by (apply mf + apply mg).
    }
    apply: join_mon; apply: eprel_downcast; apply mf + apply mg.
  Qed.

  #[global]
  Instance pi_has_join : isJoin pi_ppo.
  Proof.
    unshelve econstructor.
    - move=> f g [_ mf] [_ mg]; split; first (split=> //; by apply pi_join_mon).
      move=> ?? /wrefl _ ? aa'.
      refine (iprec_right _ (mf _ _ aa') _); apply: join_greater_l.
      all: apply: iprec_refl; by (apply mf + apply mg).
    - move=> f g [_ mf] [_ mg]; split; first (split=> //; by apply pi_join_mon).
      move=> ?? /wrefl _ ? aa'.
      refine (iprec_right _ (mg _ _ aa') _); apply: join_greater_r.
      all: apply: iprec_refl; by (apply mf + apply mg).
    - move=> f g h [[mf mh] fh] [[mg _] gh].
      split; first (split=> //; by apply pi_join_mon).
      move=> ?? /wrefl _ ? aa'.
      refine (iprec_left _ _ (mh _ _ aa')).
      apply: join_greatest; apply: iprec_refl ; by [apply: fh|apply: gh].
  Qed.

  (** Lemma on preservation of meet and join by monotone functions *)

  Lemma pi_rel_meet `{meetOp@{u} A} `{!isMeet@{u} RA} f :
    pi_rel f f ->
    forall a b (aa : RA a a) (bb : RA b b),
      iprec RB (meet_mon _ _ _ _ aa bb) (f (a ⊓ b))
            (dmeet (f a) (f b)).
  Proof.
    move=> mf ?? aa bb.
    change (meet_mon _ _ _ _ _ _) with
        (meet_smallest (meet_smaller_l aa bb)
                       (meet_smaller_r aa bb)).
    apply: dmeet_smallest; apply: mf.
  Qed.

  Lemma pi_rel_join `{joinOp@{u} A} `{!isJoin@{u} RA} f :
    pi_rel f f ->
    forall a b (aa : RA a a) (bb : RA b b),
      iprec RB (join_mon _ _ _ _ aa bb)
            (djoin (f a) (f b)) (f (a ⊓ b)).
  Proof.
    move=> mf ?? aa bb.
    change (join_mon _ _ _ _ _ _) with
        (join_greatest (join_greater_l aa bb)
                       (join_greater_r aa bb)).
    apply: djoin_greatest; apply: mf.
  Qed.

End Pi.

Section PiCast.
  Universe u.
  Context (A A' : Type@{u})
          (B : fwc@{u u} A) (B' : fwc@{u u} A')
          (castA : A' -> A) (castB : forall {a} a', B a -> B' a').

  Let pi_cast := pi_cast B B' castA castB.

  Lemma pi_cast_mon
        (RA : ppo@{u} A) (RA' : ppo@{u} A')
        (RB : ippo RA B) (RB' : ippo RA' B')
    (sp_castA : arr_rel RA' RA castA castA)
    (sp_castB : forall a'0 a'1 (a'01 : RA' a'0 a'1)
       b0 b1 (b01 : iprec RB (sp_castA _ _ a'01) b0 b1),
        iprec RB' a'01 (castB _ a'0 b0) (castB _ a'1 b1))
    (f g : forall a, B a) :
    pi_rel RB f g ->
    pi_rel RB' (pi_cast f) (pi_cast g).
  Proof.
    move=> mfg a'0 a'1 a'01.
    apply: sp_castB.
    apply: mfg.
  Qed.

  Lemma pi_cast_mon'
        (RA : ppo@{u} A) (RA' : ppo@{u} A')
        (RB : ippo RA B) (RB' : ippo RA' B')
        (sp_castA : arr_rel RA' RA castA castA)
        (sp_castB : forall a'0 a'1 (a'01 : RA' a'0 a'1)
                      b0 b1 (b01 : iprec RB (sp_castA _ _ a'01) b0 b1),
            iprec RB' a'01 (castB _ a'0 b0) (castB _ a'1 b1))
        (f g : forall a, B a) :
    prec_pi RB f g -> prec_pi RB' (pi_cast f) (pi_cast g).
  Proof.
    move=> [[ff gg] fg]; split; first split; [move: ff| move: gg| move: fg];
          (unshelve apply: pi_cast_mon)=> //.
  Qed.
End PiCast.



Section PiCastProp.
  Universe u.
  Lemma pi_cast_comp (AX AY AZ : Type@{u})
        (BX : fwc@{u u} AX)
        (BY : fwc@{u u} AY)
        (BZ : fwc@{u u} AZ)
        (castAYX : AY -> AX)
        (castAZY : AZ -> AY)
        castBXY castBYZ f :
    pi_cast BY BZ castAZY castBYZ (pi_cast BX BY castAYX castBXY f) =
    pi_cast BX BZ (castAYX \o castAZY)
            (fun ax az bx => castBYZ (castAZY az) az (castBXY ax (castAZY az) bx)) f.
  Proof. reflexivity. Defined.

  Context (AX AY : Type@{u})
          (RAX : ppo@{u} AX) (RAY : ppo@{u} AY)
          (BX : fwc@{u u} AX)
          (BY : fwc@{u u} AY)
          (RBX : ippo RAX BX) (RBY : ippo RAY BY)
          (castA0 castA1 : AY -> AX)
          (castB0 castB1 : forall {ax} ay, BX ax -> BY ay)
          (sp_castA1 : monotone RAY RAX castA1)
          (sp_castB1 :
            forall (a'0 a'1 : AY) (a'01 : RAY a'0 a'1) (b0 : BX (castA1 a'0))
              (b1 : BX (castA1 a'1)),
              iprec RBX (sp_castA1 a'0 a'1 a'01) b0 b1 ->
              iprec RBY a'01 (@castB1 (castA1 a'0) a'0 b0)
                    (@castB1 (castA1 a'1) a'1 b1))
          (eqv_castA : equiv RAY RAX castA0 castA1)
          (eqv_castB : forall ay (ayy : RAY ay ay)
                         b0 b1
                         (b01 : equiv_app eqv_castA (spair ayy ayy) ⊢[RBX] b0 ≈ b1),
              castB0 ay b0 ≈[RBY ay ayy] castB1 ay b1).

  Let sp_pi_cast1 : forall h (hh : pi_rel RBX h h),
      pi_rel RBY
             (pi_cast BX BY castA1 (@castB1) h)
             (pi_cast BX BY castA1 (@castB1) h) :=
        fun f ff => pi_cast_mon (@castB1) sp_castA1 sp_castB1 ff.


  Lemma pi_cast_equiv h (hh : pi_rel@{u u} RBX h h) :
    pi_cast BX BY castA0 castB0 h ≈[pi_ppo RBY] pi_cast BX BY castA1 castB1 h.
  Proof.
    apply: sym_sym; apply: pi_ppo_sym_helper'=> //.
    1: by apply: sp_pi_cast1.
    move=> a aa; split;
    [apply: sprr|apply: sprl]; apply: eqv_castB;
    split; apply: hh.
  Qed.

End PiCastProp.


(** Eppairs between dependent products *)

Section PiEppair.
  Universe u.
  Context (A A' : Type@{u}) (pA : ppo@{u} A) (pA' : ppo@{u} A')
          (B : fwc@{u u} A) (pB : ippo@{u} pA B)
          (B' : fwc@{u u} A') (pB' : ippo@{u} pA' B').
  Context (AA' : eppair@{u} pA pA').
  Context (BB' : ieppair@{u} pB pB' AA').

  Definition pi_upcast : (forall a, B a) -> (forall a', B' a') :=
    pi_cast B B' (downcast AA') (fun a a' => iupcast BB' a').

  Lemma pi_rel_upcast (f : forall a, B a) :
    pi_rel pB f f -> pi_rel pB' (pi_upcast f) (pi_upcast f).
  Proof.
    unshelve apply: pi_cast_mon.
    1: move=> ???; by apply: downmon.
    move=> ??/wrefl *; apply: iupmon0; last eassumption.
    1-2: apply: eprel_down_left=> //.
  Qed.

  Lemma pi_upcast_mon : monotone (pi_ppo pB) (pi_ppo pB') pi_upcast.
  Proof.
    move=> f g /wrefl -[_ /pi_rel_upcast ff] [_ /pi_rel_upcast gg] [_ fg]; split; first by split.
    move=> x y /wrefl xx yy xy.
    unshelve apply: iupmon0; last apply: fg.
    1: apply: downmon=> //.
    1-2: apply: eprel_down_left=> //.
  Qed.


  Definition pi_downcast : (forall a', B' a') -> (forall a, B a) :=
    pi_cast B' B (upcast AA') (fun a a' => idowncast BB' a').

  Lemma pi_rel_downcast (f : forall a', B' a') :
    pi_rel pB' f f -> pi_rel pB (pi_downcast f) (pi_downcast f).
  Proof.
    move=> monf x y /wrefl=> ?? xy; unshelve apply: idownmon0; last apply: monf.
    1: apply: upmon=> //.
    1-2: apply: eprel_up_right=> //.
  Qed.


  Lemma pi_downcast_mon : monotone (pi_ppo pB') (pi_ppo pB) pi_downcast.
  Proof.
    move=> f g /wrefl -[_ /pi_rel_downcast ff] [_ /pi_rel_downcast gg] [_ fg]; split; first by split.
    move=> x y /wrefl xx yy xy.
    unshelve apply: idownmon0; last apply: fg.
    1: apply: upmon=> //.
    1-2: apply: eprel_up_right=> //.
  Qed.


  #[program]
   Definition pi_eppair : eppair (pi_ppo pB) (pi_ppo pB') :=
    mkEppair _ _ pi_upcast_mon pi_downcast_mon _ _ _.
  Next Obligation.
    move=> f g [_ ff] [[_ gg] ufg]; split; first (split=> // ; by apply: pi_rel_downcast).
    move=> x y /wrefl xx yy xy.
    unshelve apply: (iprec_trans _ (ff _ _ (unit@{u} AA' _ xx))).
    - apply: trans; last exact (retract AA' _ yy).
      by apply downmon, upmon.
    - move: (ufg _ _ (upmon@{u} AA' _ _ xy)); apply: iuptodown0.
      + apply: eprel_down_left; by apply upmon.
      + by apply: eprel_up_right.
      + apply: ff.
  Qed.
  Next Obligation.
    move=> f g [_ gg] [[ff _] fdg]; split; first (split=> // ; by apply: pi_rel_upcast).
    move=> x y /wrefl xx yy xy.
    assert (x'y : pA' (upcast AA' (downcast AA' x)) y) by
        apply (trans (counit AA' _ xx))=> //.
    unshelve apply: idowntoup0=> //.
    2: apply downmon; exact xx.
    - by apply: eprel_down_left.
    - apply: eprel_down_left; exact xy.
    - unshelve refine (iprec_trans _ (fdg _ _ (downmon@{u} AA' _ _ xx)) _).
      1: by apply downmon.
      unshelve apply: idownmon0=> //.
      1: assumption.
      + apply: eprel_up_right; by apply downmon.
      + apply: to_eprel'=> //; by apply downmon.
  Qed.
  Next Obligation.
    move=> f [_ ff]; split; first split=> //.
    1: by apply pi_rel_downcast, pi_rel_upcast.
    unfold pi_downcast, pi_upcast.
    move=> x y /wrefl xx yy xy.

    pose proof (udxx := retract AA' _ xx).
    pose proof (uxux := upmon AA' _ _ xx).
    assert (udxux : eprel AA' (downcast AA' (upcast AA' x)) (upcast AA' x))
      by by apply eprel_down_left, upmon.
    assert (xux : eprel AA' x (upcast AA' x))
      by by apply eprel_up_right.
    pose proof (ff _ _ (retract AA' _ xx)).
    unshelve eapply (iupmon0 BB' uxux udxux xux) in H.
    eapply (idownmon0 BB' xx xux xux) in H.
    unshelve refine (iprec_trans _ H _); first assumption.
    apply: iprec_left.
    1: apply (retract (ieppair_on BB' xux)); by case: (ff _ _ xx).
    by apply: ff.
  Qed.

  Let RB := pi_ppo pB.
  Let RB' := pi_ppo pB'.

  Lemma pi_eppair_eprel f g :
    eprel pi_eppair f g ->
    RB f f s/\ RB' g g s/\
      (forall a a' (aa' : eprel AA' a a'), eprel (ieppair_on BB' aa') (f a) (g a')).
  Proof.
    move=> fg; split; first (split; [exact ⌞fg⌟|exact ⌜fg⌝]).
    move: ⌞fg⌟ => [_ ff].
    move=> a a' aa'; unshelve apply: to_eprel.
    + unshelve apply: iprec_refl.
      exact (ff _ _ ⌞aa'⌟).
    + move: (eprel_upcast aa') (eprel_upcast fg)=> uaa' [_ /(_ _ _ uaa') ufg].
      apply: (trans _ (eprel_upcast ufg)).
      apply: trans.
      apply: iprec_refl. apply: iupmon0.
      3: exact (ff _ _ (unit AA' a ⌞aa'⌟)).
      1: assumption.
      1: apply: (eprel_left _ _ aa'); apply: retract; exact ⌞aa'⌟.
      pose proof (dua := retract AA' _ ⌞aa'⌟).
      pose proof (duaua := eprel_up_right AA' _ _ dua).
      epose (ieppair_comp_right BB' duaua uaa' (eprel_left _ dua aa')).
      refine (equiv_upcast_l _ _ _ e _).
      2: apply: upmon.
      all: apply: iprec_refl; apply: ff.
  Qed.

  Lemma pi_eppair_eprel_intro f g :
    RB f f ->
    RB' g g ->
    (forall a a' (aa' : eprel AA' a a'), eprel (ieppair_on BB' aa') (f a) (g a')) ->
    eprel pi_eppair f g.
  Proof.
    move=> ff [_ gg] fg; apply: to_eprel=> //.
    split; first split=> //.
    1: move: (pi_upcast_mon ff)=> [//].
    move=> ?? aa'.
    pose (daa := eprel_down_left AA' _ _ ⌊aa'⌋).
    pose (daa' := eprel_down_left AA' _ _ aa').
    pose (eqv := ieppair_comp_right BB' daa aa' daa').
    exact (eprel_up_left (eprel_equiveppair' eqv (fg _ _ daa'))).
  Qed.

End PiEppair.

Section PiEppairEquiv.
  Universe u v.
  Context (AX AY : Type@{u}) (pAX : ppo@{u} AX) (pAY : ppo@{u} AY)
          (BX : fwc@{u u} AX) (pBX : ippo@{u} pAX BX)
          (BY : fwc@{u u} AY) (pBY : ippo@{u} pAY BY).
  Context (AXY0 AXY1 : eppair@{u} pAX pAY).
  Context (BXY0 : ieppair@{u} pBX pBY AXY0)
          (BXY1 : ieppair@{u} pBX pBY AXY1).

  Lemma pi_eppair_equiv (AXY01 : equiveppair AXY0 AXY1)
        (BXY01 : iequiveppair BXY0 BXY1) :
    equiveppair@{v} (pi_eppair@{u} BXY0) (pi_eppair@{u} BXY1).
  Proof.
    apply: equiveppair_helper.
    - move=> f [_ ff]; apply: pi_ppo_sym_helper.
      1,2: by apply: pi_rel_upcast.
      move=> a aa.
      pose proof (aa0 := eprel_down_left@{u} AXY0 _ _ (sprl aa)).
      pose proof (aa1 := eprel_down_left@{u} AXY1 _ _ (sprl aa)).
      pose proof (a01 := equiv_app (sprr AXY01) aa).
      pose proof (dXY01 := sprl (BXY01 (downcast AXY0 a) a (downcast AXY1 a) a
                              a01 aa aa0 aa1)).
      apply: dXY01; apply: pi_rel_iequiv0=> //.
    - move=> f [_ ff]; apply: pi_ppo_sym_helper.
      1,2: by apply: pi_rel_downcast.
      move=> a aa.
      pose proof (aa0 := eprel_up_right@{u} AXY0 _ _ (sprl aa)).
      pose proof (aa1 := eprel_up_right@{u} AXY1 _ _ (sprl aa)).
      pose proof (a01 := equiv_app (sprl AXY01) aa).
      pose proof (dXY01 := sprr (BXY01 a (upcast AXY0 a) a (upcast AXY1 a)
                              aa a01 aa0 aa1)).
      apply: dXY01; apply: pi_rel_iequiv0=> //.
  Qed.
End PiEppairEquiv.

Section PiEppairId.
  Universe u.
  Context (AX : Type@{u}) (pAX : ppo@{u} AX)
          (BX : fwc@{u u} AX) (pBX : ippo@{u} pAX BX).
  Context (AXX : eppair@{u} pAX pAX).
  Context (BXX : ieppair@{u} pBX pBX AXX).

  #[program]
  Definition id_ieppair : ieppair@{u} pBX pBX (ideppair pAX) :=
    mkIEppair (fupcast BX) (fdowncast BX) _ _.
  Next Obligation.
    move=> ?? aa'; exact (ippois_eppair_on pBX (eprel_upcast aa')).
  Qed.
  Next Obligation.
    move=> ax0 ax1 ay0 ay1 ax01 ay01 axy0 axy1.
    move: (eprel_upcast axy0) (eprel_upcast axy1)=> axy0' axy1'.
    rewrite -/(ippo_on@{u} pBX axy0') -/(ippo_on@{u} pBX axy1').
    apply: equiveppair_trans; first apply: ippo_trans.
    apply: equiveppair_sym; exact (ippo_trans pBX axy0' ay01).
  Qed.

  Lemma pi_eppair_id :
    equiveppair AXX (ideppair pAX) ->
    iequiveppair BXX id_ieppair ->
    equiveppair (pi_eppair BXX) (ideppair (pi_ppo pBX)).
  Proof.
    move=> eAXX eBXX; apply: equiveppair_helper_up=> f [_ ff] /=;
          unshelve apply: pi_ppo_sym_helper=> //.
    1: by apply: pi_rel_upcast.
    move=> a eaa.
    move: (sprl eaa) (equiv_downcast_app eAXX eaa)=> aa daa.
    move: (eprel_down_left AXX _ _ aa) (eprel_ideppair_inv pAX _ _ aa)=> e e'.
    move: (eBXX _ a a a daa eaa e e')=> [eBXXup _].
    apply: (iequiv_trans (eBXXup _ _ (pi_rel_iequiv0 ff ff daa))).
    pose proof (h := iequiv_equiv (aa:=aa) (pi_rel_iequiv0 ff ff eaa)).
    apply: equiv_iequiv; apply: (equiv_upcast_app (ippo_refl pBX aa) h).
  Qed.
End PiEppairId.



Section PiEppairComp.
  Universe u.
  Context (AX AY AZ : Type@{u})
          (pAX : ppo@{u} AX) (pAY : ppo@{u} AY) (pAZ : ppo@{u} AZ)
          (BX : fwc@{u u} AX) (pBX : ippo@{u} pAX BX)
          (BY : fwc@{u u} AY) (pBY : ippo@{u} pAY BY)
          (BZ : fwc@{u u} AZ) (pBZ : ippo@{u} pAZ BZ).
  Context (AXY : eppair@{u} pAX pAY) (AYZ : eppair@{u} pAY pAZ).
  Context (BXY : ieppair@{u} pBX pBY AXY) (BYZ : ieppair@{u} pBY pBZ AYZ).

  #[program]
  Definition comp_ieppair : ieppair@{u} pBX pBZ (compeppair AXY AYZ) :=
    mkIEppair (* pBX pBZ *)
      (fun ax az => let ay := downcast AYZ az in iupcast BYZ az \o iupcast BXY ay)
      (fun ax az => let ay := downcast AYZ az in
                 idowncast BXY ax \o idowncast BYZ ay)
      _ _.
  Next Obligation.
    move=> ax az axz /=.
    pose proof (axy := eprel_down_right axz).
    pose proof (ayz := eprel_down_left AYZ az az (eprel_urefl axz)).
    refine (compeppair_prop@{u} (iis_eppair_on BXY axy) (iis_eppair_on BYZ ayz)).
  Qed.
  Next Obligation.
    move=> ax0 ax1 az0 az1 ax01 az01 axz0 axz1 /=.
    set ay0 := downcast AYZ az0.
    set ay1 := downcast AYZ az1.
    pose proof (axy0 := eprel_down_right axz0).
    pose proof (ayz0 := eprel_down_left AYZ _ _ (eprel_urefl axz0)).
    pose proof (axy1 := eprel_down_right axz1).
    pose proof (ayz1 := eprel_down_left AYZ _ _ (eprel_urefl axz1)).
    pose proof (ay01 := downmon AYZ _ _ az01).
    pose (natxy := ieppair_natural BXY ax01 ay01 axy0 axy1).
    pose (natyz := ieppair_natural BYZ ay01 az01 ayz0 ayz1).
    pose (wiskerr := equiveppair_comp_right (ieppair_on BYZ ayz1) natxy).
    pose (wiskerl := equiveppair_comp_left (ieppair_on BXY axy0) natyz).
    refine (equiveppair_trans wiskerr wiskerl).
  Qed.

  Lemma pi_eppair_comp :
    equiveppair (compeppair (pi_eppair BXY) (pi_eppair BYZ))
                (pi_eppair comp_ieppair).
  Proof.
    apply: equiveppair_helper_up=> f [_ ff].
    unshelve apply: pi_ppo_sym_helper.
    + by do 2 apply pi_rel_upcast.
    + apply: pi_rel_upcast=> //.
    + move=> a aa /=; move: (sprl aa) => ? ; unfold pi_upcast, pi_cast=> /=; split.
      * unshelve apply: iupmon0.
        1: by apply: downmon.
        1,2: by apply: eprel_down_left.
        unshelve apply: iupmon0.
        1: by do 2 apply: downmon.
        1,2: apply: eprel_down_left; by apply: downmon.
        apply: ff.
      * unshelve apply: iupmon0.
        1: by apply: downmon.
        1,2: by apply: eprel_down_left.
        unshelve apply: iupmon0.
        1: by do 2 apply: downmon.
        1,2: apply: eprel_down_left; by apply: downmon.
        apply: ff.
  Qed.
End PiEppairComp.


(** Preservation of heterogeneous precision relation by pi_cast *)

Section PiCastHmon.
  Universe u.
  Context (AX0 AX1 AY0 AY1 : Type@{u})
          (RAX0 : ppo@{u} AX0)
          (RAX1 : ppo@{u} AX1)
          (RAY0 : ppo@{u} AY0)
          (RAY1 : ppo@{u} AY1)
          (AX01 : eppair RAX0 RAX1)
          (AY01 : eppair RAY0 RAY1)
          (BX0 : fwc@{u u} AX0)
          (BX1 : fwc@{u u} AX1)
          (BY0 : fwc@{u u} AY0)
          (BY1 : fwc@{u u} AY1)
          (RBX0 : ippo@{u} RAX0 BX0)
          (RBX1 : ippo@{u} RAX1 BX1)
          (RBY0 : ippo@{u} RAY0 BY0)
          (RBY1 : ippo@{u} RAY1 BY1)
          (BX01 : ieppair RBX0 RBX1 AX01)
          (BY01 : ieppair RBY0 RBY1 AY01)
          (castA0 : AX0 -> AY0)
          (castA1 : AX1 -> AY1)
          (castB0 : forall {ay0} ax0, BY0 ay0 -> BX0 ax0)
          (castB1 : forall {ay1} ax1, BY1 ay1 -> BX1 ax1)
          (castA00 : monotone RAX0 RAY0 castA0)
          (castA11 : monotone RAX1 RAY1 castA1)
          (castA01 : forall ax0 ax1 (ax01 : eprel AX01 ax0 ax1),
              eprel AY01 (castA0 ax0) (castA1 ax1))
          (castB00 : forall ax0 ax0' (ax00 : RAX0 ax0 ax0')
                       by0 by0' (by00 : iprec RBY0 (castA00 _ _ ax00) by0 by0'),
              iprec RBX0 ax00 (castB0 ax0 by0) (castB0 ax0' by0'))
          (castB11 : forall ax1 ax1' (ax11 : RAX1 ax1 ax1')
                       by1 by1' (by11 : iprec RBY1 (castA11 _ _ ax11) by1 by1'),
              iprec RBX1 ax11 (castB1 ax1 by1) (castB1 ax1' by1'))
          (castB01 : forall ax0 ax1 (ax01 : eprel AX01 ax0 ax1)
                       by0 by1 (by01 : eprel (ieppair_on BY01 (castA01 _ _ ax01)) by0 by1),
              eprel (ieppair_on BX01 ax01) (castB0 ax0 by0) (castB1 ax1 by1)).

  Let RX0 := pi_ppo RBX0.
  Let RX1 := pi_ppo RBX1.
  Let RY0 := pi_ppo RBY0.
  Let RY1 := pi_ppo RBY1.
  Let RX01 := pi_eppair BX01.
  Let RY01 := pi_eppair BY01.

  Let cast0 := pi_cast BY0 BX0 castA0 castB0.
  Let cast1 := pi_cast BY1 BX1 castA1 castB1.

  Lemma pi_cast_hmon f0 f1 :
    eprel RY01 f0 f1 -> eprel RX01 (cast0 f0) (cast1 f1).
  Proof.
    move=> /pi_eppair_eprel [[f00 f11] f01].
    unshelve apply: pi_eppair_eprel_intro.
    - move: f00; (unshelve apply: pi_cast_mon')=> //.
    - move: f11; (unshelve apply: pi_cast_mon')=> //.
    - move=> ???; apply: castB01; apply: f01.
  Qed.

End PiCastHmon.

Section PiCastHmonDiag.
  Universe u.
  Context (AX0 AY0 : Type@{u})
          (RAX0 : ppo@{u} AX0)
          (RAY0 : ppo@{u} AY0)
          (BX0 : fwc@{u u} AX0)
          (BY0 : fwc@{u u} AY0)
          (RBX0 : ippo@{u} RAX0 BX0)
          (RBY0 : ippo@{u} RAY0 BY0)
          (castA0 : AX0 -> AY0)
          (castA1 : AX0 -> AY0)
          (castB0 : forall {ay0} ax0, BY0 ay0 -> BX0 ax0)
          (castB1 : forall {ay0} ax0, BY0 ay0 -> BX0 ax0)
          (castA00 : monotone RAX0 RAY0 castA0)
          (castA11 : monotone RAX0 RAY0 castA1)
          (castA01 : forall ax0 ax1 (ax01 : RAX0 ax0 ax1),
              RAY0 (castA0 ax0) (castA1 ax1))
          (castB00 : forall ax0 ax0' (ax00 : RAX0 ax0 ax0')
                       by0 by0' (by00 : iprec RBY0 (castA00 _ _ ax00) by0 by0'),
              iprec RBX0 ax00 (castB0 ax0 by0) (castB0 ax0' by0'))
          (castB11 : forall ax1 ax1' (ax11 : RAX0 ax1 ax1')
                       by1 by1' (by11 : iprec RBY0 (castA11 _ _ ax11) by1 by1'),
              iprec RBX0 ax11 (castB1 ax1 by1) (castB1 ax1' by1'))
          (castB01 : forall ax0 ax1 (ax01 : RAX0 ax0 ax1)
                       by0 by1 (by01 : iprec RBY0 (castA01 _ _ ax01) by0 by1),
              iprec RBX0 ax01 (castB0 ax0 by0) (castB1 ax1 by1)).

  Let RX0 := pi_ppo RBX0.
  Let RY0 := pi_ppo RBY0.
  Let RX01 := pi_eppair (id_ieppair RBX0).
  Let RY01 := pi_eppair (id_ieppair RBY0).

  Let cast0 := pi_cast BY0 BX0 castA0 castB0.
  Let cast1 := pi_cast BY0 BX0 castA1 castB1.

  Lemma pi_cast_hmon_diag f0 f1 :
    RY0 f0 f1 -> RX0 (cast0 f0) (cast1 f1).
  Proof.
    move=> f01.
    apply: eprel_ideppair.
    apply: eprel_equiveppair.
    apply: pi_eppair_id.
    2: apply: iequiveppair_refl.
    1: apply: equiveppair_refl.
    move: (eprel_equiveppair' (pi_eppair_id (equiveppair_refl _) (iequiveppair_refl _)) (eprel_ideppair_inv _ _ _ f01)).
    (unshelve apply: pi_cast_hmon)=> //.
    - move=> ?? ax01. apply: eprel_ideppair_inv. move: (eprel_ideppair ax01).
      apply: castA01.
    - move=> ?? ax01; move: (eprel_ideppair ax01). apply: castB01.
  Qed.

End PiCastHmonDiag.
