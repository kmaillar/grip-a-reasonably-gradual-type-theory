From Coq Require Import ssreflect ssrfun.
From Equations Require Import Equations.
From PartialPreorder Require Import strictPropLogic.
From PartialPreorder Require Import prelude ppoDef.

Set Implicit Arguments.
Set Universe Polymorphism.
Set Polymorphic Inductive Cumulativity.
Set Primitive Projections.
Import SPropNotations.

(** Partial preorder structure on boxed propositions *)

Section BoxProp.
  Universe u.
  Context (P : SProp).
  Inductive box_ : Type@{u} :=
  | errBox
  | boxBox : P -> box_
  | unkBox.

  (* The precision on boxed elements is trivial *)
  Definition prec_box_ : srel box_ := fun _ _ => sUnit.

  Lemma prec_box_trans : transitive prec_box_. Proof. by []. Qed.
  Lemma prec_box_urefl : upper_reflexive prec_box_. Proof. by []. Qed.
  Lemma prec_box_lrefl : lower_reflexive prec_box_. Proof. by []. Qed.

  Definition prec_box_is_ppo : is_ppo@{u} prec_box_ :=
    mkIsPpo prec_box_ prec_box_trans prec_box_urefl prec_box_lrefl.

  #[program]
  Definition box_ppo : ppo box_ := mkPpo prec_box_is_ppo.
  Definition Box_ : Ppo := pack_ppo box_ppo.

  #[global]
  Instance box_err : Err box_ := errBox.
  #[global]
  Instance box_smallest : SmallestElem box_ppo :=
    mkSmallest box_ppo errBox stt (fun _ _=> stt).

  #[global]
  Instance box_dyn : Dyn box_ := unkBox.
  #[global]
  Instance box_greatest : GreatestElem box_ppo :=
    mkGreatest box_ppo unkBox stt (fun _ _=> stt).

  (* Box_ types are trivial wrt their precision
  so any element serve as a join/meet *)
  #[global]
  Instance box_meet : meetOp box_ := fun _ _=> err.
  #[global]
  Instance box_has_meet : isMeet box_ppo.
  Proof. unshelve econstructor=> * //=. Qed.

  #[global]
  Instance box_join : joinOp box_ := fun _ _=> err.
  #[global]
  Instance box_has_join : isJoin box_ppo.
  Proof. unshelve econstructor=> * //=. Qed.

  (* Box P is equivalent to Empty as long as precision is concerned,
     so it is also initial in the category of ppos and eppairs *)
  Context (A : Type@{u}) `{Err@{u} A}.
  Definition upcast_box_ (p : box_) :  A := err.
  Definition downcast_box_ (a : A) : box_ := err.
  Lemma cast_box_is_eppair (RA : ppo A) `{!SmallestElem RA}
    : eppair_prop@{u} box_ppo RA upcast_box_ downcast_box_.
  Proof.
    unshelve econstructor=> //.
    - move=> ?*; apply: smallest_refl.
    - move=> *; by apply: smaller.
  Qed.
End BoxProp.
(* Problem to instantiate below so Hint Extern instead of #[global] *)
#[global]
Hint Extern 0 (Err (box_@{u} ?x)) => apply: box_err@{u} : typeclass_instances.
#[global]
Hint Extern 0 (SmallestElem (box_ppo ?x)) => apply: box_smallest : typeclass_instances.
