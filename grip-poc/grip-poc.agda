{-# OPTIONS --rewriting --prop --confluence-check #-}

-- --confluence-check

open import Agda.Primitive
open import Agda.Builtin.Bool
open import Agda.Builtin.Nat
open import Agda.Builtin.List
open import Agda.Builtin.Equality
open import Agda.Builtin.Equality.Rewrite
open import Agda.Builtin.Sigma
open import Agda.Builtin.Unit
open import Data.Bool
open import Data.Sum
open import ett-rr
open ExcTT

open import setoid-rr using (Tel ; _×_ ; _,_ ; _~_ ; iP ; iS ; inj ; uninj ; fstC ; sndC ; ⊤P ; ttP ; ⊥ ; Id ; Id-refl ; concatId ; ap ; transport ; inverse ; _⊎⊎_ ; inj₁ ; inj₂ ; exfalso; exfalsoP)

{- 
 we encode error and unkown as exceptions with a two-valued parameter
-}

data ExcVal : Set where
  ✠val : ExcVal
  ¿val : ExcVal

✠ : (A : Set ℓ) → A
✠ A = exc _ ✠val A

¿ : (A : Set ℓ) → A
¿ A = exc _ ¿val A

¿¿ : ∀ {ℓ} → Set ℓ
¿¿ {ℓ} = ¿ (Set ℓ)

{- 
 Axiomatisation of type precision ⊑, term precision ≼, and cast
-}

postulate
  _⊑_ : Set ℓ → Set ℓ → Prop (lsuc ℓ)
  _≼_ : {A B : Set ℓ} → A → B → Prop ℓ
  cast : (A B : Set ℓ) → A → B 

-- equiprecision on terms

_≈_ : {A : Set ℓ} (a a' : A) → Prop (lsuc ℓ)
a ≈ a' = (a ≼ a') × (a' ≼ a)

-- self-precision on types

sp : (A : Set ℓ) → Prop (lsuc ℓ)
sp A = A ⊑ A

-- self-precision on terms

⊢sp : {A : Set ℓ} (a : A) → Prop ℓ
⊢sp a = a ≼ a

-- definition of an ep-pair

record ep-pair (A B : Set ℓ) : Set ℓ where 
  constructor _,,_,,_
  field
    {- Monotonicity -}
    cast-mon-up : {a a' : A} → a ≼ a' → cast A B a ≼ cast A B a'
    cast-mon-down : {b b' : B} → b ≼ b' → cast B A b ≼ cast B A b'
    {- Adjunction -}
    cast-up   : {a : A} {b : B} → ⊢sp b → a ≼ cast B A b → cast A B a ≼ b
    cast-down : {a : A} {b : B} → ⊢sp a → cast A B a ≼ b → a ≼ cast B A b
    {- Retraction -}
    cast-retr : {a : A} → ⊢sp a → cast B A (cast A B a) ≼ a

open ep-pair public

-- reflexivity and transitivity of term and type precision

postulate
  ⊑reflL : (A B : Set ℓ) → A ⊑ B → sp A
  ≼reflL : (A B : Set ℓ) {a : A} {b : B} → sp A → sp B → a ≼ b → ⊢sp a
  ⊑reflR : (A B : Set ℓ) → A ⊑ B → sp B
  ≼reflR : (A B : Set ℓ) {a : A} {b : B} → sp A → sp B → a ≼ b → ⊢sp b
  ⊑trans : (A B C : Set ℓ) → A ⊑ B → B ⊑ C → A ⊑ C
  ≼trans : {A B C : Set ℓ} {a : A} {b : B} {c : C} → sp A → sp B → sp C → a ≼ b → b ≼ c → a ≼ c

-- precision on the universe

  ⊑Set : Set ℓ ⊑ Set ℓ

-- precision on types, when seen as terms of the universe, is the restriction of type precision
-- to types that are more precise than unknown (¿_Type_l)

  ≼Set⊑ : (A B : Set ℓ) → A ≼ B ≡ _×_ {ℓ₂ = ℓ} (A ⊑ B) (B ⊑ ¿ _)

-- axiomatization of bottom and top elements 

  ¿top : {A B : Set ℓ} (a : A) → sp A → sp B → ⊢sp a → a ≼ ¿ B
  ¿Nat : Nat ⊑ ¿ _
  ¿Bool : Bool ⊑ ¿ _
  ¿iS : {A : Set ℓ} → sp A → iS {ℓ₁ = lsuc ℓ} A ⊑ ¿¿
  ✠bot : {A B : Set ℓ} (b : B) → ⊢sp b → ✠ A ≼ b
  ¿refl : (A : Set ℓ) (e : sp A) → ¿ A ≼ ¿ A
  ✠refl : (A : Set ℓ) (e : sp A) → ✠ A ≼ ✠ A

-- any cast decomposes through a less precise type

  UPdec : (A B X : Set ℓ) (eAX : A ⊑ X) (eBX : B ⊑ X) {a : A} → ⊢sp a →
          cast X B (cast A X a) ≈ cast A B a

-- characterization of heterogeneous precision through a less precise type

  ⊑het-hom : {A B X : Set ℓ} {a : A} {b : B} → (eAX : A ⊑ X) → (eBX : B ⊑ X) →                 a ≼ b → cast A X a ≼ cast B X b
  ⊑hom-het : {A B X : Set ℓ} {a : A} {b : B} → (eAX : A ⊑ X) → (eBX : B ⊑ X) → ⊢sp b → ⊢sp a → cast A X a ≼ cast B X b → a ≼ b

-- graduality: A ⊑ B gives rise to an ep-pair
                    
  ⊑ep-pair : (A B : Set ℓ) (e : A ⊑ B) → ep-pair A B

{-# REWRITE ≼Set⊑ #-}

¿¿-refl : ¿¿ {ℓ = ℓ} ⊑ ¿¿
¿¿-refl {ℓ} = fstC (¿refl (Set ℓ) ⊑Set)

✠✠refl : ✠ (Set ℓ) ⊑ ✠ (Set ℓ)
✠✠refl {ℓ} = fstC (✠refl (Set ℓ) ⊑Set)

-- precision for injection for Set (iS) and Prop (iP) 

postulate
  ⊑-Type-iS : {ℓ ℓ₁ : Level} {A A' : Set ℓ} → iS {ℓ₁ = ℓ₁} A ⊑ iS {ℓ₁ = ℓ₁} A' ≡ iP {ℓ₁ = lsuc ℓ₁} (A ⊑ A')
  ≼-inj : {ℓ ℓ₁ : Level} {A A' : Set ℓ} (a : A) (a' : A') →
          _≼_ {A = iS A} {B = iS A'} (inj {ℓ₁ = ℓ₁} a) (inj {ℓ₁ = ℓ₁} a') ≡ iP {ℓ₁ = ℓ₁} (a ≼ a')
  ≼-uninj : {ℓ ℓ₁ : Level} {A A' : Set ℓ} (a : iS A) (a' : iS A') → a ≼ a' →
            (uninj {ℓ₁ = ℓ₁} a) ≼ (uninj {ℓ₁ = ℓ₁} a')
  ≈-uninj : {ℓ ℓ₁ : Level} {A : Set ℓ} (a a' : iS A) → a ≈ a' →
            (uninj {ℓ₁ = ℓ₁} a) ≈ (uninj {ℓ₁ = ℓ₁} a')
  cast-iS : (A A' : Set ℓ) (a : A) →
            cast (iS {ℓ₁ = ℓ₁} A) (iS {ℓ₁ = ℓ₁} A') (inj a) ≡
            inj (cast A A' a)

{-# REWRITE ⊑-Type-iS ≼-inj cast-iS #-}

-- Derivable properties of ep-pairs

⊑het-hom-down : {A B : Set ℓ} {a : A} {b : B} → sp A → sp B → a ≼ b → a ≼ cast B A b
⊑het-hom-down {ℓ} {A} {B} eA eB eab = 
  let eA¿ = ¿iS eA
      eB¿ = ¿iS eB
      ea = ≼reflL A B eA eB eab
      eb = ≼reflR A B eA eB eab
      epA = ⊑ep-pair (iS {ℓ₁ = lsuc ℓ} A) ¿¿ eA¿
      epB = ⊑ep-pair (iS {ℓ₁ = lsuc ℓ} B) ¿¿ eB¿
      inj eb¿ , _ = UPdec (iS {ℓ₁ = lsuc ℓ} B) (iS {ℓ₁ = lsuc ℓ} A) ¿¿ eB¿ eA¿ (inj eb)
      upa≼upb = ⊑het-hom {A = iS {ℓ₁ = lsuc ℓ} A} {B = iS {ℓ₁ = lsuc ℓ} B} {X = ¿¿} eA¿ eB¿ (inj eab)
      inj a≼upb = cast-down epA (inj ea) upa≼upb
  in ≼trans {A = A} {B = A} {C = A} eA eA eA a≼upb eb¿

⊑hom-het-down : {A B : Set ℓ} {a : A} {b : B} → sp A → sp B → ⊢sp b → a ≼ cast B A b → a ≼ b
⊑hom-het-down {ℓ} {A} {B} eA eB eb eab =
  let eA¿ = ¿iS eA
      eB¿ = ¿iS eB
      ea = ≼reflL A A eA eA eab
      epA = ⊑ep-pair (iS {ℓ₁ = lsuc ℓ} A) ¿¿ eA¿
      epB = ⊑ep-pair (iS {ℓ₁ = lsuc ℓ} B) ¿¿ eB¿
      _ , inj eb¿ = UPdec (iS {ℓ₁ = lsuc ℓ} B) (iS {ℓ₁ = lsuc ℓ} A) ¿¿ eB¿ eA¿ (inj eb)
      ebb = cast-mon-up epB (inj eb)
  in uninj (⊑hom-het {A = iS {ℓ₁ = lsuc ℓ} A} {B = iS {ℓ₁ = lsuc ℓ} B} {X = ¿¿} eA¿ eB¿ (inj eb) (inj ea)
                     (cast-up epA ebb (inj ( ≼trans {A = A} {B = A} {C = A} eA eA eA eab eb¿))))

ep-unit : (A B : Set ℓ) (eAB : A ⊑ B) {a : A} → ⊢sp a → a ≼ cast B A (cast A B a)
ep-unit A B eAB ea = let eA = ⊑reflL A B eAB
                         eB = ⊑reflR A B eAB
                         ep = ⊑ep-pair A B eAB
                     in cast-down ep ea (cast-mon-up ep ea)

ep-counit : (A B : Set ℓ) (eAB : A ⊑ B) {b : B} → ⊢sp b → cast A B (cast B A b) ≼ b
ep-counit A B eAB eb = let eA = ⊑reflL A B eAB
                           eB = ⊑reflR A B eAB
                           ep = ⊑ep-pair A B eAB
                       in cast-up ep eb (cast-mon-down ep eb)

cast-retr≈ : (A B : Set ℓ) (eAB : A ⊑ B) {a : A} → ⊢sp a → cast B A (cast A B a) ≈ a
cast-retr≈ A B eAB ea = let ep = ⊑ep-pair A B eAB
                        in cast-retr ep ea , ep-unit A B eAB ea

⊑het-hom-up : (A B : Set ℓ) (eAB : A ⊑ B) {a : A} {b : B} → a ≼ b → cast A B a ≼ b
⊑het-hom-up A B eAB eab = let ep = ⊑ep-pair A B eAB
                              eA = ⊑reflL A B eAB
                              eB = ⊑reflR A B eAB
                              edown = ⊑het-hom-down {A = A} {B = B} eA eB eab
                              eb = ≼reflR A B eA eB eab
                          in cast-up ep eb edown 

⊑hom-het-up : (A B : Set ℓ) (eAB : A ⊑ B) {a : A} {b : B} → ⊢sp a → cast A B a ≼ b → a ≼ b 
⊑hom-het-up A B eAB ea eab = let ep = ⊑ep-pair A B eAB
                                 eA = ⊑reflL A B eAB
                                 eB = ⊑reflR A B eAB
                                 edown = cast-down ep ea eab
                                 eb = ≼reflR B B eB eB eab
                             in ⊑hom-het-down {A = A} {B = B} eA eB eb edown 


-- General cast composition is derivable from the one define on types related by precision

up-cast-comp : {A B C : Set ℓ} (eAB : A ⊑ B) (eBC : B ⊑ C) {a : A} → ⊢sp a → cast B C (cast A B a) ≈ cast A C a
up-cast-comp {ℓ} {A} {B} {C} eAB eBC ea =
  let epAB = ⊑ep-pair A B eAB
      epBC = ⊑ep-pair B C eBC
      eAC = ⊑trans A B C eAB eBC
      epAC = ⊑ep-pair A C eAC
      eA = ⊑reflL A B eAB
      eB = ⊑reflR A B eAB
      eC = ⊑reflR B C eBC
      ea1 , ea2 = UPdec A B C eAC eBC ea
      eb1 , eb2 = UPdec B A C eBC eAC (cast-mon-up epAB ea)
  in cast-up epBC (cast-mon-up epAC ea) ea2 ,
     cast-up epAC (cast-mon-up epBC (cast-mon-up epAB ea)) (≼trans {A = A} {B = A} {C = A} eA eA eA (ep-unit A B eAB ea) eb2) 


down-cast-comp : {A B C : Set ℓ} (eAB : A ⊑ B) (eBC : B ⊑ C) {c : C} → ⊢sp c → cast B A (cast C B c) ≈ cast C A c
down-cast-comp {ℓ} {A} {B} {C} eAB eBC ec =
  let epAB = ⊑ep-pair A B eAB
      epBC = ⊑ep-pair B C eBC
      eAC = ⊑trans A B C eAB eBC
      epAC = ⊑ep-pair A C eAC
      eA = ⊑reflL A B eAB
      eB = ⊑reflR A B eAB
      eC = ⊑reflR B C eBC
      eb1 , eb2 = UPdec  B A C eBC eAC (cast-mon-down epBC ec)
      eb2c = cast-mon-up epAC eb2
      ea1 , ea2 = UPdec A B C eAC eBC (cast-mon-down epAC ec)
  in (cast-down epAC (cast-mon-down epAB (cast-mon-down epBC ec))
                (≼trans {A = C} {B = C} {C = C} eC eC eC (≼trans {A = C} {B = C} {C = C} eC eC eC eb2c
                (ep-counit A C eAC (cast-mon-up epBC (cast-mon-down epBC ec)))) (ep-counit B C eBC ec))) ,
     cast-down epAB (cast-mon-down epAC ec) (≼trans {A = B} {B = B} {C = B} eB eB eB ea2 (cast-mon-down epBC (ep-counit A C eAC ec)))

cast-mon-het : {A A' B B' : Set ℓ} (eA : A ⊑ A') (eB : B ⊑ B') {a : A} {a' : A'} → a ≼ a' → cast A B a ≼ cast A' B' a'
cast-mon-het {ℓ} {A} {A'} {B} {B'} eAA' eBB' eaa' =
  let eA = ⊑reflL A A' eAA'
      eA' = ⊑reflR A A' eAA'
      eB = ⊑reflL B B' eBB'
      eB' = ⊑reflR B B' eBB'
      ea = ≼reflL A A' eA eA' eaa'
      ea' = ≼reflR A A' eA eA' eaa'
      eA¿ = ¿iS eA
      eA'¿ = ¿iS eA'
      eB¿ = ¿iS eB
      eB'¿ = ¿iS eB'
      epA = ⊑ep-pair (iS {ℓ₁ = lsuc ℓ} A) ¿¿ eA¿
      epA' = ⊑ep-pair (iS {ℓ₁ = lsuc ℓ} A') ¿¿ eA'¿
      epAA' = ⊑ep-pair A A' eAA'
      epB = ⊑ep-pair (iS {ℓ₁ = lsuc ℓ} B) ¿¿ eB¿
      epB' = ⊑ep-pair (iS {ℓ₁ = lsuc ℓ} B') ¿¿ eB'¿
      epBB' = ⊑ep-pair B B' eBB'
      ea¿refl = cast-mon-up epA' (inj (cast-mon-up epAA' ea))
      _ , inj ea¿ = UPdec (iS {ℓ₁ = lsuc ℓ} A) (iS {ℓ₁ = lsuc ℓ} B) ¿¿ eA¿ eB¿ (inj ea)
      inj ea'¿ , _ = UPdec (iS {ℓ₁ = lsuc ℓ} A') (iS {ℓ₁ = lsuc ℓ} B') ¿¿ eA'¿ eB'¿ (inj ea')
      inj eaa'¿ = cast-mon-down epB' (cast-mon-up epA' (inj (cast-up epAA' ea' (⊑het-hom-down {A = A} {B = A'} eA eA' eaa'))))
      ea1 , ea2 = up-cast-comp {A = iS {ℓ₁ = lsuc ℓ} A} {B = iS {ℓ₁ = lsuc ℓ} A'} {C = ¿ _} (inj eAA') eA'¿ (inj ea)
      eb1 , inj eb2 = down-cast-comp {A = iS {ℓ₁ = lsuc ℓ} B} {B = iS {ℓ₁ = lsuc ℓ} B'} {C = ¿ _} (inj eBB') eB'¿ ea¿refl
  in ≼trans {A = B} {B = B} {C = B'} eB eB eB' ea¿
    (≼trans {A = B} {B = B'} {C = B'} eB eB' eB'
    (≼trans {A = B} {B = B'} {C = B'} eB eB' eB' (≼-uninj (cast ¿¿ (iS B) (cast (iS A) ¿¿ (inj _))) (cast ¿¿ (iS B') (cast (iS A') ¿¿ (inj (cast A A' _))))
    (≼trans {A = iS {ℓ₁ = lsuc ℓ} B} {B = iS {ℓ₁ = lsuc ℓ} B} {C = iS {ℓ₁ = lsuc ℓ} B'} (inj eB) (inj eB) (inj eB') (cast-mon-down epB ea2)
            (inj (⊑hom-het-down {A = B} {B = B'} eB eB'
                            (≼-uninj (cast ¿¿ (iS B') (cast (iS A') ¿¿ (inj (cast A A' _)))) (cast ¿¿ (iS B') (cast (iS A') ¿¿ (inj (cast A A' _)))) (cast-mon-down epB' ea¿refl))
                            eb2))))
     eaa'¿) ea'¿)

cast-mon : {A B : Set ℓ} {a a' : A} → sp A → sp B → a ≼ a' → cast A B a ≼ cast A B a'
cast-mon {ℓ} {A} {B} eA eB eaa' = cast-mon-het {A = A} {A' = A} {B = B} {B' = B} eA eB eaa'

{- precision and cast for dependent product -}

Π : (A : Set ℓ) (B : A → Set ℓ₁) → Set (ℓ ⊔ ℓ₁)
Π {ℓ} {ℓ₁} A B = (a : A) → B a

postulate
  ⊑-Type-Pi : (A A' : Set ℓ) (B : A → Set ℓ₁) (B' : A' → Set ℓ₁) →
              ((a : A) → B a) ⊑ ((a' : A') → B' a') ≡
              Tel {ℓ₂ = lsuc (ℓ ⊔ ℓ₁)} (A ⊑ A')
                  (λ Aε → _×_ {ℓ₂ = ℓ ⊔ ℓ₁}  ((a : A) (a' : A')  → a ≼ a' → B a ⊑ B' a')
                  (_×_ {ℓ₂ = ℓ ⊔ ℓ₁} ((a : A) (a' : A)   → a ≼ a' → B a ⊑ B a')
                                     ((a : A') (a' : A') → a ≼ a' → B' a ⊑ B' a')))

  ⊑-Pi : (A A' : Set ℓ) (B : A → Set ℓ₁) (B' : A' → Set ℓ₁) (f : (a : A) → B a) (g : (a' : A') → B' a') →
         f ≼ g ≡
         _×_ {ℓ₂ = ℓ ⊔ ℓ₁} ((a : A) (a' : A') (ea : a ≼ a') → (f a) ≼ (g a'))
        (_×_ {ℓ₂ = ℓ ⊔ ℓ₁} ((a : A) (a' : A) (ea : a ≼ a') → (f a) ≼ (f a'))
                           ((a : A') (a' : A') (ea : a ≼ a') → (g a) ≼ (g a')))
  cast-Pi : (A A' : Set ℓ) (B : A → Set ℓ₁) (B' : A' → Set ℓ₁) (f : (a : A) → B a) →
            cast ((a : A) → B a) ((a' : A') → B' a') f ≡
            λ (a' : A') → let a = cast A' A a'
                          in cast (B a) (B' a') (f a)
          
{-# REWRITE ⊑-Type-Pi ⊑-Pi cast-Pi #-}

postulate cast-Pi-Prop : (A A' : Prop ℓ) (B : Set ℓ₁) (B' : Set ℓ₁) (f : (a : A) → B) →
                         cast ((a : A) → B) ((a' : A') → B') f ≡ ✠ ((a' : A') → B')

{-# REWRITE cast-Pi-Prop #-}

{- casts from Π to ¿ -}

postulate
  cast-Π¿ : (A : Set ℓ) (B : A → Set ℓ₁) (f : iS (Π A B)) (X : Set (lsuc (ℓ ⊔ ℓ₁))) →
            cast (¿ (Set (lsuc (ℓ ⊔ ℓ₁)))) X (cast (iS (Π A B)) (¿ (Set (lsuc (ℓ ⊔ ℓ₁)))) f) ≡
            cast (iS (Π A B)) X f
  cast-Π✠ : (A : Set ℓ) (B : A → Set ℓ₁) (f : (a : A) → B a) (X : Set (ℓ ⊔ ℓ₁)) →
            cast (¿ (Set (ℓ ⊔ ℓ₁))) X (cast ((a : A) → B a) (¿ (Set (ℓ ⊔ ℓ₁))) f) ≡
            ✠ X

{-# REWRITE cast-Π¿ cast-Π✠ #-}

{- precision and cast for Prop -}

postulate
  ⊑-Type-Prop : Prop ℓ ⊑ Prop ℓ ≡ ⊤P
  cast-prop : (A : Prop ℓ) → cast (Prop ℓ) (Prop ℓ) A ≡ A

{-# REWRITE ⊑-Type-Prop cast-prop #-}

{- precision and cast for Set -}

postulate
  ⊑-Type-Set : Set ℓ₁ ⊑ Set ℓ₁ ≡ ⊤P
  cast-set : (A : Set ℓ) → cast (Set ℓ) (Set ℓ) A ≡ A

{-# REWRITE ⊑-Type-Set cast-set #-}

-- axiom for the identity type on Prop : Prop ext 

postulate ⊑-prop : (P Q : Prop ℓ) → P ≼ Q ≡ ⊤P

{-# REWRITE ⊑-prop #-}

sp-prop : (P : Prop ℓ) → ⊢sp P
sp-prop P = ttP


-- Now for positive / inductive types, we first start with inductive types without indices

-- Sigma types

postulate
  ⊑-Type-Sigma : (A A' : Set ℓ) (B : A → Set ℓ₁) (B' : A' → Set ℓ₁) →
                 Σ A B ⊑ Σ A' B' ≡
                 Tel {ℓ₂ = ℓ} (A ⊑ A') (λ e → (a : A) (a' : A') → a ≼ a' → B a ⊑ B' a')
  cast-pair : (A A' : Set ℓ) (B : A → Set ℓ₁) (B' : A' → Set ℓ₁) (x : A) (y : B x) →
              cast (Σ A B) (Σ A' B') (x , y) ≡
              (cast A A' x , cast (B x) (B' (cast A A' x)) y)

{-# REWRITE ⊑-Type-Sigma cast-pair #-}

-- Box types

postulate
  ⊑-Type-Box : (P Q : Prop ℓ) → Box P ⊑ Box Q ≡ P ≼ Q
  cast-Box : (A A' : Prop ℓ) (a : A) →
             cast (Box A) (Box A') (box a) ≡ ✠ (Box A')

{-# REWRITE ⊑-Type-Box cast-Box #-}

-- Unit

postulate
  ⊑-Type-Unit : ⊤ ⊑ ⊤ ≡ ⊤P
  cast-Unit : (t : ⊤) → cast ⊤ ⊤ t ≡ t

{-# REWRITE ⊑-Type-Unit cast-Unit #-}

-- List

postulate
  ⊑-Type-List : {A A' : Set ℓ} → List A ⊑ List A' ≡ A ⊑ A' 
  cast-List-nil : (A A' : Set ℓ) →
                  cast (List A) (List A') [] ≡ []
  cast-List-cons : (A A' : Set ℓ) (a : A) (l : List A) →
                   cast (List A) (List A') (a ∷ l) ≡
                   cast A A' a ∷ cast (List A) (List A') l

{-# REWRITE ⊑-Type-List cast-List-nil cast-List-cons #-}

-- Nat

postulate
  ⊑-Type-Nat : Nat ⊑ Nat ≡ ⊤P
  cast-zero : cast Nat Nat zero ≡ zero
  cast-suc : (n : Nat) → cast Nat Nat (suc n) ≡ suc (cast Nat Nat n)

{-# REWRITE ⊑-Type-Nat cast-zero cast-suc #-}

-- Bool 

postulate
  ⊑-Type-Bool : Bool ⊑ Bool ≡ ⊤P
  cast-true : cast Bool Bool true ≡ true
  cast-false : cast Bool Bool false ≡ false

{-# REWRITE ⊑-Type-Bool cast-true cast-false #-}

postulate
  cast-NatBool : (n : Nat)  → cast Nat Bool n ≡ ✠ Bool
  cast-BoolNat : (b : Bool) → cast Bool Nat b ≡ ✠ Nat

{-# REWRITE cast-NatBool cast-BoolNat #-}

-- Sum types

postulate
  ⊑-Type-sum : {A A' : Set ℓ} {B B' : Set ℓ₁} →
               (A ⊎ B) ⊑ (A' ⊎ B') ≡ Tel {ℓ₂ = ℓ} (A ⊑ A') (λ _ → B ⊑ B')
  cast-Sum-inj₁ : (A A' : Set ℓ) (B B' : Set ℓ₁) (a : A) →
                  cast (A ⊎ B) (A' ⊎ B') (inj₁ a) ≡
                  inj₁ (cast A A' a)
  cast-Sum-inj₂ : (A A' : Set ℓ) (B B' : Set ℓ₁) (b : B) →
                  cast (A ⊎ B) (A' ⊎ B') (inj₂ b) ≡
                  inj₂ (cast B B' b)

{-# REWRITE ⊑-Type-sum cast-Sum-inj₁ cast-Sum-inj₂ #-}


{-
   Definition of precision on inductive types.

   Compare to the definition on lists in the paper, we use an inductive presentation instead of a presentation with fixpoint
-}


ΣR : (A A' : Set ℓ) (B : A → Set ℓ₁) (B' : A' → Set ℓ₁) 
     (ab : Σ A B) (ab' : Σ A' B') → Prop (ℓ ⊔ ℓ₁)
ΣR {ℓ} A A' B B' (a , b) (a' , b') = Tel {ℓ₂ = ℓ} (a ≼ a') (λ ea → b ≼ b')

postulate ⊑-Σ : (A A' : Set ℓ) (B : A → Set ℓ₁) (B' : A' → Set ℓ₁)  (ab : Σ A B) (ab' : Σ A' B') →
                 ab ≼ ab' ≡ ΣR A A' B B' ab ab'
                     
{-# REWRITE ⊑-Σ #-}

data sumR {A A' : Set ℓ} {B B' : Set ℓ} : A ⊎ B → A' ⊎ B' → Prop ℓ where
  sumR₁₁  : (a : A) (a' : A') → a ≼ a' → sumR (inj₁ a) (inj₁ a') 
  sumR₂₂  : (b : B) (b' : B') → b ≼ b' → sumR (inj₂ b) (inj₂ b')
  sumR✠   : (x : A' ⊎ B') → sumR (✠ (A ⊎ B)) x 
  sumR¿   : (x : A ⊎ B) → sumR x (¿ (A' ⊎ B'))  

postulate ⊑-sum : (A A' : Set ℓ) (B B' : Set ℓ) (ab : A ⊎ B) (ab' : A' ⊎ B') →
                   ab ≼ ab' ≡ sumR ab ab'
                     
{-# REWRITE ⊑-sum #-}

data listR {A A' : Set ℓ} : List A → List A' → Prop ℓ where
  []R      : listR [] []
  _∷R_     : ∀ (a : A) (a' : A') (l : List A) (l' : List A') →
               a ≼ a' → listR l l' → listR (a ∷ l) (a' ∷ l')
  listR✠   : (l : List A') → listR (✠ (List A)) l 
  listR¿   : (l : List A)  → listR l (¿ (List A'))

postulate ⊑-List : (A A' : Set ℓ) (l : List A) (l' : List A') →
                   l ≼ l' ≡ listR l l'

{-# REWRITE ⊑-List #-}

data NatR : Nat → Nat → Prop where
  zeroR : NatR 0 0
  sucR  : ∀ {n n' : Nat} → NatR n n' → NatR (suc n) (suc n')
  N✠    : (n : Nat) → NatR (✠ Nat) n
  N¿    : (n : Nat) → NatR n (¿ Nat)

postulate ⊑-Nat : (n n' : Nat) → n ≼ n' ≡ NatR n n'

{-# REWRITE ⊑-Nat #-}

-- the two lemma below cannot be proven because Agda
-- cannot do inversion on NatR because ✠ Nat and ¿ Nat
-- are encoded using axioms

postulate ≼✠eq : (n : Nat) → n ≼ ✠ Nat → n ≡ ✠ Nat
postulate ≼¿eq : (n : Nat) → ¿ Nat ≼ n → n ≡ ¿ Nat

reflNat : (n : Nat) → n ≼ n
reflNat zero = zeroR
reflNat (suc n) = sucR (reflNat n)

data BoolR : Bool → Bool → Prop where
  Btrue   : BoolR true true
  Bfalse  : BoolR false false
  B✠      : (b : Bool) → BoolR (✠ Bool) b 
  B¿      : (b : Bool) → BoolR b (¿ Bool)
  
postulate ⊑-Bool : (b b' : Bool) → b ≼ b' ≡ BoolR b b'

{-# REWRITE ⊑-Bool #-}

-- precision on Box 

postulate ⊑-Box : (A A' : Prop ℓ) (l : Box A) (l' : Box A') →
                   l ≼ l' ≡ ⊤P

{-# REWRITE ⊑-Box #-}

