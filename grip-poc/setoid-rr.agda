{-# OPTIONS --rewriting --prop --confluence-check #-}

-- --confluence-check

open import Agda.Primitive
open import Agda.Builtin.Bool
open import Agda.Builtin.Nat
open import Agda.Builtin.List
open import Agda.Builtin.Equality
open import Agda.Builtin.Equality.Rewrite
open import Agda.Builtin.Sigma
open import Agda.Builtin.Unit
open import Data.Vec.Base
open import Data.Bool
open import Data.Sum

open import ett-rr
open ExcTT

{-
  Agda uses explicit cumulativity, o we define iS and iP to make use of it
-}

record iS (A : Set ℓ) : Set (ℓ ⊔ ℓ₁) where
  constructor inj
  field
    uninj : A

open iS public

record prod {a b} (A : Set a) (B : Set b) : Set (a ⊔ b) where
  constructor _,_
  field
    fstprod : A
    sndprod : B 

open prod public

record iP (A : Prop ℓ) : Prop (ℓ ⊔ ℓ₁) where
  constructor inj
  field
    uninj : A

open iP public

-- a bit of boilerplate to deal with Prop

data ⊥ : Prop where

record ⊤P : Prop ℓ where
  constructor ttP
  
-- sigma type in Prop used to handle telescopes. 

infixr 4 _,_

record Tel (A : Prop ℓ) (B : A → Prop ℓ₁) : Prop (ℓ ⊔ ℓ₁ ⊔ ℓ₂) where
  constructor _,_
  field
    fstC : A
    sndC : B fstC

open Tel public

_×_ : ∀ (A : Prop ℓ) (B : Prop ℓ₁) → Prop (ℓ ⊔ ℓ₁ ⊔ ℓ₂)
_×_ {ℓ₂ = ℓ₂} A B = Tel {ℓ₂ = ℓ₂} A (λ _ → B)

infixr 4 _⊎⊎_

data _⊎⊎_ (A : Prop) (B : Prop) : Prop where
  inj₁ : (x : A) → A ⊎⊎ B
  inj₂ : (y : B) → A ⊎⊎ B

exfalso : {A : Set ℓ} → ⊥ → A 
exfalso ()

exfalsoP : {A : Prop ℓ} → ⊥ → A 
exfalsoP ()

{- 
 Axiomatisation of Id, Id-refl, transport (for proposition), cast 

 Note that Id-refl, transport are axioms in Prop, 
 so we don't need to give them a computation content. 

 Also transport-refl is useless for transport on Prop
-}

postulate
  IdU : Set ℓ → Set ℓ → Prop (lsuc ℓ)
  IdU-Set : IdU (Set ℓ) (Set ℓ)
  IdOver : (A B : Set ℓ) → (e : IdU A B) → A → B → Prop ℓ
  IdOverU : (A B : Set ℓ) → IdOver (Set ℓ) (Set ℓ) IdU-Set A B ≡ IdU A B
  Id-refl-gen : {A : Set ℓ} {e : _} (x : A) → IdOver A A e x x

{-# REWRITE IdOverU #-}

infixr 3 _~_

_~_ : {A : Set ℓ} → A → A → Prop ℓ
_~_ {ℓ} {A} = IdOver A A (Id-refl-gen {e = IdU-Set} A) 

Id : (A : Set ℓ) → A → A → Prop ℓ
Id {ℓ} A = _~_ {ℓ} {A} 

Id-refl : {A : Set ℓ} (x : A) → x ~ x
Id-refl {A = A} x = Id-refl-gen {e = Id-refl-gen {e = IdU-Set} A} x

postulate
  transport : {A : Set ℓ} (x : A) (P : (a : A) → x ~ a → Prop ℓ₁) (t : P x (Id-refl x)) (y : A) (e : x ~ y) → P y e
  cast : (A B : Set ℓ) (e : IdU A B) → A → B 
  cast-refl : {A : Set ℓ} (a : A) → a ~ cast A A (Id-refl A) a

-- Additional structure on equality

ap : {A : Set ℓ} {B : Set ℓ₁} {x y : A} (f : A → B) (e : x ~ y) →
     f x ~ f y
ap {ℓ} {ℓ₁} {A} {B} {x} {y} f e = transport x (λ z _ → f x ~ f z) (Id-refl (f x)) y e

ap2 : {A : Set ℓ} {B : A → Set ℓ₁} {C : Set ℓ₂} {x y : A} {x' : B x} {y' : B y}
      (f : (a : A) → B a → C) (e : x ~ y) (e' : IdOver (B x) (B y) (ap B e) x' y') →
      f x x' ~ f y y'
ap2 {ℓ} {ℓ₁} {ℓ₂} {A} {B} {C} {x} {y} {x'} {y'} f e e' =
  transport x (λ z ez → (z' : B z) (e' : IdOver (B x) (B z) (ap B ez) x' z') → f x x' ~ f z z')
            (λ y' e' → ap (f x) e') y e y' e'

transportOver : {A B : Set ℓ} (eAB : IdU A B) (x : A) (P : (A' : Set ℓ) (e : IdU A A') (a : A') → IdOver A A' e x a → Prop ℓ₁)
                              (t : P A (Id-refl A) x (Id-refl x)) (y : B) (e : IdOver A B eAB x y) → P B eAB y e
transportOver {ℓ} {ℓ₁} {A} {B} eAB x P t = transport A (λ A' eAA' → (a : A') → (e : IdOver A A' eAA' x a) → P A' eAA' a e) (transport x (P A (Id-refl A)) t) B eAB 

apOver : {A : Set ℓ} {B : A → Set ℓ₁} {C : A → Set ℓ₂} {x y : A} (e : x ~ y) (f : (a : A) → C a → B a) {c : C x} {c' : C y} →
         IdOver (C x) (C y) (ap C e) c c' → IdOver (B x) (B y) (ap B e) (f x c) (f y c')
apOver {ℓ} {ℓ₁} {ℓ₂} {A} {B} {C} {x} {y} e f {c} {c'} =
       transport x (λ y e →  (c : C x) (c' : C y) →
                      IdOver (C x) (C y) (ap C e) c c' → IdOver (B x) (B y) (ap B e) (f x c) (f y c'))
                 (λ c → transport c (λ c' ec → IdOver (B x) (B x) (ap B (Id-refl x)) (f x c) (f x c')) (Id-refl (f x c))) y e c c'

cast-reflOver : {A A' : Set ℓ} (e : IdU A A') (a : A) → IdOver A A' e a (cast A A' e a)
cast-reflOver {A = A} {A' = A'} e a = transport A (λ A' e →  IdOver A A' e a (cast A A' e a)) (cast-refl a) A' e 

apOver-simpl' : {A : Set ℓ} {B : A → Set ℓ₁} {x y : A} (e : x ~ y) (f : (a : A) (e : x ~ a) → B a) →
               IdOver (B x) (B y) (ap B e) (f x (Id-refl x)) (f y e)
apOver-simpl' {ℓ} {ℓ₁} {A} {B} {x} {y} e f =
       transport x (λ y e →  IdOver (B x) (B y) (ap B e) (f x (Id-refl x)) (f y e)) (Id-refl (f x (Id-refl x))) y e


inverse : (A : Set ℓ) {x y : A} (p : x ~ y) → y ~ x
inverse A {x} {y} p = transport x (λ z _ → z ~ x) (Id-refl x) y p

inverseOver  : (A A' : Set ℓ) {x : A} (e : IdU A A') {y : A'} (p : IdOver {ℓ} A A' e x y) → IdOver A' A (inverse (Set ℓ) {A} {A'} e) y x
inverseOver {ℓ} A A' {x} e {y} = transport A (λ B e → (y : B) → IdOver A B e x y → IdOver B A (inverse (Set ℓ) {A} {B} e) y x) (λ y p → inverse A p ) A' e y

concatId : (A : Set ℓ) {x y z : A} (p : x ~ y)
           (q : y ~ z) → x ~ z
concatId A {x} {y} {z} p q = transport y (λ t _ → x ~ t) p z q

concatOver : (A B C : Set ℓ) (e : IdU A B) (e' : IdU B C) {x : A} {y : B} {z : C} (p : IdOver A B e x y)
           (q : IdOver B C e' y z) → IdOver A C (concatId (Set ℓ) {A} {B} {C} e e') x z
concatOver {ℓ} A B C e e' {x} {y} {z} p q = transport B (λ C ee → (z : C) (q : IdOver B C ee y z) → IdOver A C (concatId (Set ℓ) {A} {B} {C} e ee) x z)
                                                      (λ z q → transport B (λ A e → (x : A) (p : IdOver A B (inverse (Set ℓ) {B} {A} e) x y) → IdOver A B (inverse (Set ℓ) {B} {A} e) x z)
                                                                         (λ x p → concatId B p q) A (inverse (Set ℓ) {A} {B} e) x p)
                                                      C e' z q 

transport-Id : {A : Set ℓ} (P : A → Set ℓ₁) (x : A) (t : P x) (y : A) (e : x ~ y) → P y
transport-Id P x t y e = cast (P x) (P y) (ap P e) t

transport2-Id : {A : Set ℓ} (P : A → Set ℓ₁) (Q : (a : A) → P a → Set ℓ₂)
               (x : A) (t : P x) (u : Q x t) (y : A) (e : x ~ y) → Q y (transport-Id P x t y e)
transport2-Id P Q x t u y e = cast (Q x t) (Q y (transport-Id P x t y e))
                                   (transport x (λ y e → IdU (Q x t) (Q y (transport-Id P x t y e)))
                                              (ap (Q x) (cast-refl t)) y e)
                                   u

cast-reflOver-concat : {A A' : Set ℓ} {a : A} {a' : A'} (e : IdU A A') (ea : IdOver A A' e a a') →
                       Id A' a' (cast A A' e a)
cast-reflOver-concat {ℓ} {A} {A'} {a} {a'} e ea = concatOver A' A A' (inverse (Set ℓ) {x = A} {y = A'} e) e (inverseOver A A' e ea) (cast-reflOver e a) 

castP : (A A' : Prop ℓ) (e : _) (a : A) → A'
castP A A' e a = transport A (λ P _ → P) a A' e

-- we now state axioms for the identity type

-- Dependent product

postulate Id-Type-Pi : (A A' : Set ℓ) (B : A → Set ℓ₁) (B' : A' → Set ℓ₁) →
                       IdU ((a : A) → B a) ((a' : A') → B' a') ≡
                       Tel {ℓ₂ = ℓ} (IdU A A') (λ e → (a : A) (a' : A') → IdOver A A' e a a' → IdU (B a) (B' a'))

{-# REWRITE Id-Type-Pi #-}

PiR : (A A' : Set ℓ) (B : A → Set ℓ₁) (B' : A' → Set ℓ₁) (f : (a : A) → B a) (g : (a' : A') → B' a')
      (e : IdU ((a : A) → B a) ((a' : A') → B' a')) → Prop (ℓ ⊔ ℓ₁)      
PiR A A' B B' f g e = (a : A) (a' : A') (ea : IdOver A A' (fstC e) a a') → IdOver (B a) (B' a') (sndC e a a' ea) (f a) (g a')

postulate Id-Pi : (A A' : Set ℓ) (B : A → Set ℓ₁) (B' : A' → Set ℓ₁) (f : (a : A) → B a) (g : (a' : A') → B' a')
                  (e : IdU ((a : A) → B a) ((a' : A') → B' a')) →
                  IdOver ((a : A) → B a) ((a' : A') → B' a') e f g ≡
                  PiR A A' B B' f g e 

{-# REWRITE Id-Pi #-}

Pi-fst' : {A A' : Set ℓ} {B : A → Set ℓ₁} {B' : A' → Set ℓ₁} →
           IdU ((a : A) → B a) ((a' : A') → B' a') →
           IdU A' A
Pi-fst' {ℓ} {ℓ₁} {A} e = inverse (Set ℓ) {x = A} (fstC e)

Pi-snd' : {A A' : Set ℓ} {B : A → Set ℓ₁} {B' : A' → Set ℓ₁} →
          (e : IdU ((a : A) → B a) ((a' : A') → B' a')) →
          (a' : A') → IdU (B (cast A' A (Pi-fst' {B = B} e) a')) (B' a')
Pi-snd' {ℓ} {ℓ₁} {A} {A'} {B} {B'} e a' =
  let fste = Pi-fst' {B = B} e
  in sndC e (cast A' A fste a') a' (inverseOver A' A fste (cast-reflOver fste a'))

postulate cast-Pi : (A A' : Set ℓ) (B : A → Set ℓ₁) (B' : A' → Set ℓ₁) (f : (a : A) → B a) (e : Id _ ((a : A) → B a) ((a' : A') → B' a')) →
                    cast ((a : A) → B a) ((a' : A') → B' a') e f ≡
                    λ (a' : A') → let a = cast A' A (Pi-fst' {B = B} e) a'
                                  in cast (B a) (B' a') (Pi-snd' {B = B} e a') (f a)

{-# REWRITE cast-Pi #-}

postulate
  Id-Type-Prop : IdU (Prop ℓ) (Prop ℓ) ≡ ⊤P
  cast-prop : (A : Prop ℓ) (e : _) → cast (Prop ℓ) (Prop ℓ) e A ≡ A

{-# REWRITE Id-Type-Prop cast-prop #-}

postulate
  Id-Type-Set : IdU (Set ℓ₁) (Set ℓ₁) ≡ ⊤P
  cast-set : (A : Set ℓ) (e : _) → cast (Set ℓ) (Set ℓ) e A ≡ A

{-# REWRITE Id-Type-Set cast-set #-}

-- axiom for the identity type on Prop : Prop ext 

postulate Id-prop : (P Q : Prop ℓ) (e : _) → IdOver (Prop ℓ) (Prop ℓ) e P Q ≡ (P → Q) × (Q → P)

{-# REWRITE Id-prop #-}


-- Now for positive types, we first start with inductive types without indices

-- Sigma types

postulate Id-Type-Sigma : (A A' : Set ℓ) (B : A → Set ℓ₁) (B' : A' → Set ℓ₁) →
                       IdU (Σ A B) (Σ A' B') ≡
                       Tel {ℓ₂ = ℓ} (IdU A A') (λ e → (a : A) (a' : A') → IdOver A A' e a a' → IdU (B a) (B' a'))

{-# REWRITE Id-Type-Sigma #-}

postulate cast-pair : (A A' : Set ℓ) (B : A → Set ℓ₁) (B' : A' → Set ℓ₁) (x : A) (y : B x) (e : IdU (Σ A B) (Σ A' B')) →
                    let eA = fstC e in
                    let x' = cast A A' eA x in 
                    let eB = sndC e x x' (cast-reflOver eA x) in
                    cast (Σ A B) (Σ A' B') e (x , y) ≡
                    (cast A A' eA x , cast (B x) (B' x') eB y)

{-# REWRITE cast-pair #-}

-- Box types

postulate Id-Type-Box : (P Q : Prop ℓ) → IdU (Box P) (Box Q) ≡ Id (Prop ℓ) P Q

{-# REWRITE Id-Type-Box #-}

postulate cast-Box : (A A' : Prop ℓ) (a : A) (e : IdU (Box A) (Box A')) →
                    cast (Box A) (Box A') e (box a) ≡ box (castP A A' e a)

{-# REWRITE cast-Box #-}

-- Unit

postulate
  Id-Type-Unit : IdU ⊤ ⊤ ≡ ⊤P
  cast-Unit : (e : _) (t : ⊤) → cast ⊤ ⊤ e t ≡ t

{-# REWRITE Id-Type-Unit cast-Unit #-}

-- List

postulate Id-Type-List : {A A' : Set ℓ} → IdU (List A) (List A') ≡ IdU A A' 

{-# REWRITE Id-Type-List #-}

postulate
  cast-List-nil : (A A' : Set ℓ) (e : _) →
                   cast (List A) (List A') e [] ≡ []
  cast-List-cons : (A A' : Set ℓ) (e : _) (a : A) (l : List A) →
                   cast (List A) (List A') e (a ∷ l) ≡
                   cast A A' e a ∷ cast (List A) (List A') e l

{-# REWRITE cast-List-nil cast-List-cons #-}

-- Nat

postulate
  Id-Type-Nat : IdU Nat Nat ≡ ⊤P
  cast-zero : (e : _) → cast Nat Nat e zero ≡ zero
  cast-suc : (e : _) (n : Nat) → cast Nat Nat e (suc n) ≡ suc (cast Nat Nat e n)

{-# REWRITE Id-Type-Nat cast-zero cast-suc #-}

-- Bool 

postulate
  Id-Type-Bool : IdU Bool Bool ≡ ⊤P
  cast-true : (e : _) → cast Bool Bool e true ≡ true
  cast-false : (e : _) → cast Bool Bool e false ≡ false

{-# REWRITE Id-Type-Bool cast-true cast-false #-}

-- Sum types

postulate Id-Type-sum : {A A' : Set ℓ} {B B' : Set ℓ₁} →
                       IdU (A ⊎ B) (A' ⊎ B') ≡ Tel {ℓ₂ = ℓ} (IdU A A') (λ _ → IdU B B')

{-# REWRITE Id-Type-sum #-}


postulate
  cast-Sum-inj₁ : (A A' : Set ℓ) (B B' : Set ℓ₁) (a : A) (e : _) →
                  let eA = fstC e in
                  cast (A ⊎ B) (A' ⊎ B') e (inj₁ a) ≡
                  inj₁ (cast A A' eA a)
  cast-Sum-inj₂ : (A A' : Set ℓ) (B B' : Set ℓ₁) (b : B) (e : _) →
                  let eB = sndC e in
                  cast (A ⊎ B) (A' ⊎ B') e (inj₂ b) ≡
                  inj₂ (cast B B' eB b)

{-# REWRITE cast-Sum-inj₁ cast-Sum-inj₂ #-}


-- Equality on positive types can be characterized

Id-Box : (P : Prop ℓ) (p p' : P) → Id (Box P) (box p) (box p')
Id-Box P p p' = Id-refl (box p)

Id-Unit : (p q : ⊤) → Id ⊤ p q
Id-Unit tt tt = Id-refl tt

ΣR : (A A' : Set ℓ) (B : A → Set ℓ₁) (B' : A' → Set ℓ₁) (e : IdU (Σ A B) (Σ A' B')) 
     (ab : Σ A B) (ab' : Σ A' B') → Prop (ℓ ⊔ ℓ₁)
ΣR {ℓ} A A' B B' (eA , eB) (a , b) (a' , b') = Tel {ℓ₂ = ℓ} (IdOver A A' eA a a') (λ ea → IdOver (B a) (B' a') (eB a a' ea) b b')

postulate Id-Σ : (A A' : Set ℓ) (B : A → Set ℓ₁) (B' : A' → Set ℓ₁) (e : IdU (Σ A B) (Σ A' B')) (ab : Σ A B) (ab' : Σ A' B') →
                 IdOver (Σ A B) (Σ A' B') e ab ab' ≡ ΣR A A' B B' e ab ab'
                     
{-# REWRITE Id-Σ #-}

sumR : (A A' : Set ℓ) (B B' : Set ℓ) (e : IdU (A ⊎ B) (A' ⊎ B')) → A ⊎ B → A' ⊎ B' → Prop ℓ 
sumR A A' B B' (eA , eB) (inj₁ a) (inj₁ a') = IdOver A A' eA a a'
sumR A A' B B' (eA , eB) (inj₁ a) (inj₂ b') = iP ⊥
sumR A A' B B' (eA , eB) (inj₂ b) (inj₁ a') = iP ⊥
sumR A A' B B' (eA , eB) (inj₂ b) (inj₂ b') = IdOver B B' eB b b'

postulate Id-sum : (A A' : Set ℓ) (B B' : Set ℓ) (e : IdU (A ⊎ B) (A' ⊎ B')) (ab : A ⊎ B) (ab' : A' ⊎ B') →
                   IdOver (A ⊎ B) (A' ⊎ B') e ab ab' ≡ sumR A A' B B' e ab ab'
                     
{-# REWRITE Id-sum #-}

data listR {A A' : Set ℓ} (e : IdU (List A) (List A')) : List A → List A' → Prop ℓ where
  []R  : listR e [] []
  _∷R_ : ∀ (a : A) (a' : A') (l : List A) (l' : List A') →
           IdOver A A' e a a' → listR e l l' → listR e (a ∷ l) (a' ∷ l')

Id-list : (A : Set ℓ)  (l l' : List A) → listR (Id-refl A) l l' → Id (List A) l l'
Id-list A .[] .[] []R = Id-refl {A = List A} []
Id-list A .(a ∷ l) .(a' ∷ l') ((a ∷R a') l l' x el) = ap2 {A = A} {B = λ _ → List A} {C = List A} _∷_ x (Id-list A l l' el)

data NatR : Nat → Nat → Prop where
  zeroR  : NatR 0 0
  sucR : ∀ (n n' : Nat) → NatR n n' → NatR (suc n) (suc n')

Id-nat : (n n' : Nat) → NatR n n' → Id Nat n n'
Id-nat .0 .0 zeroR = Id-refl 0
Id-nat .(suc n) .(suc n') (sucR n n' en) = ap suc (Id-nat n n' en)

data BoolR : Bool → Bool → Prop where
  trueR  : BoolR true true
  falseR  : BoolR false false

Id-bool : (b b' : Bool) → BoolR b b' → Id Bool b b'
Id-bool .true .true trueR = Id-refl true
Id-bool .false .false falseR = Id-refl false

postulate Id-bool' : (b b' : Bool) → IdOver Bool Bool _ b b' ≡ BoolR b b'
{-# REWRITE Id-bool' #-}

≡is~ : (A : Set ℓ) (a b : A) → a ≡ b → a ~ b
≡is~ A a b refl = Id-refl a

postulate
  noconftrueexc : (E : Set) (e : E) → exc E e Bool ~ true → ⊥
  noconffalseexc : (E : Set) (e : E) → exc E e Bool ~ false → ⊥
  noconf0exc : (E : Set) (e : E) → exc E e Nat ~ 0 → ⊥
  noconfsucexc : (E : Set) (e : E) (n : Nat) → exc E e Nat ~ suc n → ⊥

-- definition of catch of boolean for predicate in Prop

postulate catch-Bool : (E : Set) (P : Bool → Prop ℓ) → P true → P false → ((e : E) → P (exc E e Bool)) → (b : Bool) → P b 

catch-Bool✠ : (E : Set) (e : E) → (P : Bool → Prop ℓ) → (Pt : P true) → (Pf : P false) → (Pexc : (e : E) → P (exc E e Bool)) →
              box (catch-Bool E P Pt Pf Pexc (exc E e Bool)) ~ box (Pexc e)
catch-Bool✠ E e P Pt Pf Pexc = Id-refl (box (Pexc e))

catch-Bool-true : (E : Set) (P : Bool → Prop ℓ) → (Pt : P true) → (Pf : P false) → (Pexc : (e : E) → P (exc E e Bool)) →
              box (catch-Bool E P Pt Pf Pexc true) ~ box Pt
catch-Bool-true E P Pt Pf Pexc = Id-refl (box Pt)

catch-Bool-false : (E : Set) (P : Bool → Prop ℓ) → (Pt : P true) → (Pf : P false) → (Pexc : (e : E) → P (exc E e Bool)) →
              box (catch-Bool E P Pt Pf Pexc false) ~ box Pf
catch-Bool-false E P Pt Pf Pexc = Id-refl (box Pf)

