# Prerequisite

- Agda >= 2.6.2.1
- Agda standard library >= 1.7.1

# Compilation

From this directory, issue:
```
  $ make
```
or, alternatively:
```
  $ agda grip-poc-examples.agda
```

# Files organisation

- `ett-rr.agda`: rewrite rules for exceptions (following [Pédrot and Tabareau 2018])
- `setoid-rr.agda`: rewrite rules for observational type theory (following [Pujet and Tabareau 2022])
- `grip-poc.agda`: axiomatization of the primitives and reduction rules of GRIP, and derivation of general properties of precision and casts for these
- `grip-poc-examples.agda`: examples of Section 2


# Pragmas and postulates

This development pass the following options to agda
```
  {-# OPTIONS --rewriting --prop --confluence-check #-}
```

The first activates rewrite rules as used to extend the computational content of
agda, and the third activates the confluence checker for these rules.
The second flag activates the predicatve hierarchy of definitionally proof irrelevant
propositions.

The building blocks for GRIP are given through `postulate` instances and their
computational content is specified by equations flagged with a `{-# REWRITE ... #-}` pragma.

