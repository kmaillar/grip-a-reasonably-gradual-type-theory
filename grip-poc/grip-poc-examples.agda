{-# OPTIONS --rewriting --prop --confluence-check #-}

-- --confluence-check

open import Agda.Primitive
open import Agda.Builtin.Bool
open import Agda.Builtin.Nat
open import Agda.Builtin.List
open import Agda.Builtin.Equality
open import Agda.Builtin.Equality.Rewrite
open import Agda.Builtin.Sigma
open import Agda.Builtin.Unit
open import Data.Bool
open import Data.List

open import ett-rr
open ExcTT

open import setoid-rr using (Tel ; _×_ ; _,_ ; _~_ ; iP ; iS ; inj ; uninj ; fstC ; sndC ; ⊤P ; ttP ; ⊥ ; Id ; Id-refl ; concatId ; ap ; ap2 ; transport ; inverse ; _⊎⊎_ ; inj₁ ; inj₂ ; exfalso; exfalsoP; ≡is~; noconfsucexc; noconftrueexc)

open import grip-poc

-- convenience
triple : {A : Prop ℓ} → A → _×_ {ℓ₂ = ℓ₂} A (_×_ {ℓ₂ = ℓ₂} A A)
triple a = (a , a , a)

----------------------------------
-- example: Ω raises an error  
----------------------------------

ω : ¿¿ {ℓ = ℓ} → ¿¿ {ℓ = ℓ}
ω X = cast ¿¿ (¿¿ → ¿¿) X X

Ω : ¿¿ {ℓ = ℓ}
Ω = ω (cast (¿¿ → ¿¿) (¿¿) ω)

checkΩ : Ω ≡ ✠ (¿¿ {ℓ = ℓ})
checkΩ = refl

----------------------------------
-- basic properties on Nat and List
-----------------------------------

Nat-refl : (n : Nat) → n ≼ n
Nat-refl = catch-Nat ExcVal _ zeroR (λ n → sucR) (λ { ¿val → ¿refl Nat ttP ; ✠val → ✠refl Nat ttP})  

⊢sp-suc : ⊢sp suc
⊢sp-suc = triple λ a a' ea → sucR ea

spNat : sp Nat
spNat = ⊑reflL Nat ¿¿ ¿Nat

List-refl : (l : List Nat) → l ≼ l
List-refl = catch-List ExcVal Nat _ []R (λ n l H → (_ ∷R _) _ _ (Nat-refl n) H) (λ { ¿val → ¿refl (List Nat) ttP ; ✠val → ✠refl (List Nat) ttP})

----------------------------------
-- examples of proof of precision
-----------------------------------

-- Nat → Nat is internally provable to not more precise than ¿¿

notNatarrowNat⊑ : (Nat → Nat) ⊑ ¿¿ → ⊥
notNatarrowNat⊑ absurd = let ep-unit-absurd = fstC (ep-unit (Nat -> Nat) (¿ Set) absurd ⊢sp-suc)
                             ep-at-0 = ep-unit-absurd 0 0 zeroR
                             abs≼ = ≼✠eq 1 ep-at-0
                         in noconfsucexc _ ✠val 0 (inverse Nat (≡is~ Nat _ _ abs≼)) 

-- the lifting of Nat → Nat to the level is more precise than ¿¿

iSNatarrowNat⊑ : iS (Nat → Nat) ⊑ ¿¿ {lsuc lzero}
iSNatarrowNat⊑ = ¿iS {A = Nat -> Nat} (ttP , triple λ a a' x → ttP)

-- similarly, the lifting of ¿¿ → ¿¿ to the level is more precise than ¿¿

¿arrow¿⊑ : iS (¿ (Set ℓ) → ¿ (Set ℓ)) ⊑ ¿ (Set (lsuc ℓ))
¿arrow¿⊑ {ℓ} = ¿iS {A = ¿ (Set ℓ) -> ¿ (Set ℓ)} (¿¿-refl {ℓ} , triple (λ a a' x → ¿¿-refl {ℓ}))

----------------------------------
-- some basic properties of nat, plus, prod and lists
----------------------------------

-- checking that a natural number is 0

is-zero : Nat -> Bool
is-zero = catch-Nat-Set ExcVal _ true  (λ _ _  → false) (λ { ¿val → ¿ _ ; ✠val → ✠ _})

-- property asserting that a natural number is not an exception

valid-Nat : Nat → Prop 
valid-Nat = catch-Nat-Set ExcVal _ ⊤P (λ n Hn → Hn) (λ { ¿val → iP ⊥ ; ✠val → iP ⊥})

-- property asserting that a natural number is not an error

not✠-Nat : Nat → Prop 
not✠-Nat = catch-Nat-Set ExcVal _ ⊤P (λ n Hn → Hn) (λ { ¿val → ⊤P ; ✠val → iP ⊥})

-- property asserting that a list is not an exception

valid-List : List Nat → Prop 
valid-List = catch-List-Set ExcVal Nat _ ⊤P (λ a l Hl → valid-Nat a × Hl) (λ { ¿val → iP ⊥ ; ✠val → iP ⊥})

-- property asserting that a list is not an error

not✠-List : List Nat → Prop 
not✠-List = catch-List-Set ExcVal Nat _ ⊤P (λ a l Hl → not✠-Nat a × Hl) (λ { ¿val → ⊤P ; ✠val → iP ⊥})

-- the function _+_ does not return an error when the inputs are not errors

not✠-plus : (n n' : Nat) → not✠-Nat n → not✠-Nat n' → not✠-Nat (n + n') 
not✠-plus = catch-Nat ExcVal _ (λ n _ en → en) (λ n H n' en en' → H n' en en') (λ b → λ n' x x₁ → x)

-- the function _*_ does not return an error when the inputs are not errors

not✠-prod : (n n' : Nat) → not✠-Nat n → not✠-Nat n' → not✠-Nat (n * n') 
not✠-prod = catch-Nat ExcVal _ (λ n' x x₁ → ttP) (λ n H n' en en' → not✠-plus n' (n * n') en' (H n' en en')) (λ b → λ n' x x₁ → x)

-- a natural number less precise that non-error natural number is not an error

not✠-Nat≼ : (n n' : Nat) → not✠-Nat n → n ≼ n' → not✠-Nat n'
not✠-Nat≼ .0 .0 en zeroR = en
not✠-Nat≼ .(suc _) .(suc _) en (sucR enn') = not✠-Nat≼ _ _ en enn'
not✠-Nat≼ n .(¿ Nat) en (N¿ .n) = ttP

-- a list less precise that non-error list is not an error

not✠-List≼ : (l l' : List Nat) → l ≼ l' → not✠-List l → not✠-List l'
not✠-List≼ .[] .[] []R el = ttP
not✠-List≼ .(a ∷ l) .(a' ∷ l') ((a ∷R a') l l' x ell') (ea , el) = not✠-Nat≼ _ _ ea x , not✠-List≼ l l' ell' el
not✠-List≼ l .(¿ (List Nat)) (listR¿ .l) el = ttP

-- plus preserves validity

valid-plus :  (n n' : Nat) → valid-Nat n → valid-Nat n' → valid-Nat (n + n')
valid-plus zero n' vn vn' = vn'
valid-plus (suc n) n' vn vn' = valid-plus n n' vn vn'

----------------------------------
-- nArrow example
----------------------------------

nArrow : Nat → Set
nArrow = catch-Nat-Set ExcVal _ Nat (λ n H → Nat → H) (λ b → exc ExcVal b Set)

-- for any n, nArrow n is self precise as a type

nArrowSp : (n : Nat) → nArrow n ⊑ nArrow n
nArrowSp = catch-Nat ExcVal _ ttP (λ n Hn → ttP , triple (λ a a' x → Hn)) (λ { ¿val → ¿¿-refl {ℓ = lzero} ; ✠val → ✠✠refl {ℓ = lzero}})

nArrowSp¿ : nArrow (¿ Nat) ⊑ nArrow (¿ Nat)
nArrowSp¿ = ¿¿-refl {ℓ = lzero}

-- for any n, the lifting of nArrow n is self precise as a term

nArrow-below-¿ : (n : Nat) → iS (nArrow n) ⊑ ¿ Set₁
nArrow-below-¿ n = ¿iS {A = nArrow n} (nArrowSp n)

nArrow-≼ : (n : Nat) → iS {ℓ₁ = lsuc lzero} (nArrow n) ≼ iS (nArrow n)
nArrow-≼ n = (inj (nArrowSp n)) , (nArrow-below-¿ n)

------------------------------------
-- gradual guarantee for a monotonous context
-- is a direct consequence of the framework
-----------------------------------

mon : {A : Set ℓ} {B : Set ℓ₁} (eA : A ⊑ A) (eB : B ⊑ B) (P : A → B) → Prop (ℓ ⊔ ℓ₁)
mon eA eB P = P ≼ P

true≼ : (b b' : Bool) → b ~ true → b ≼ b' → (b' ~ true) ⊎⊎ (b' ~ ¿ Bool)
true≼ .true .true eb Btrue = inj₁ (Id-refl true)
true≼ .(✠ Bool) b' eb (B✠ .b') = exfalsoP (noconftrueexc _ ✠val eb)
true≼ b .(¿ Bool) eb (B¿ _) = inj₂ (Id-refl (¿ Bool))

-- generic version of gradual guarantee using ≼

gradual-guarantee : (A : Set ℓ) (eA : sp A) (x y : A) (exy : x ≼ y) (P : A → Bool) (eP : mon eA ttP P) → (P x) ≼ (P y)
gradual-guarantee A eA x y exy P eP = fstC eP _ _ exy

-- specific version of gradual guarantee when P x returns true

gradual-guarantee-true : (A : Set ℓ) (eA : sp A) (x y : A) (exy : x ≼ y) (P : A → Bool) (eP : mon eA ttP P) →
                          (P x) ~ true → (P y ~ true) ⊎⊎ (P y ~ ¿ Bool)
gradual-guarantee-true A eA x y exy P eP ePx = true≼ _ _ ePx (gradual-guarantee A eA x y exy P eP)

----------------------------------
-- monotonicity of plus
----------------------------------

plus-mon-aux : {n n' m m' : Nat} → n ≼ n' → m ≼ m' → (n + m) ≼ (n' + m')
plus-mon-aux zeroR em = em
plus-mon-aux (sucR en) em = sucR (plus-mon-aux en em)
plus-mon-aux (N✠ _) em = N✠ _
plus-mon-aux (N¿ _) em = N¿ _

plus-mon : _+_ ≼ _+_
plus-mon = triple λ a a' ea → (λ a₁ a'' ea' → plus-mon-aux ea ea') , (λ a₁ a'' ea' → plus-mon-aux (Nat-refl a) ea') , λ a₁ a'' ea' → plus-mon-aux (Nat-refl a') ea'

----------------------------------
-- monotonicity of map on lists
----------------------------------

spArrow : (A : Set ℓ) → (B : Set ℓ₁) → sp A → sp B →  sp (A → B)
spArrow A B eA eB = eA , triple (λ a a' x → eB)

map-mon-aux : {A A' : Set ℓ} {B B' : Set ℓ₁} → A ≼ A' → B ≼ B' → 
               (f : A → B) → (f' : A' → B') → f ≼ f' →
               (l : List A) → (l' : List A') → l ≼ l' →
               map f l ≼ map f' l'
map-mon-aux eAA' eBB' f f' eff' .[] .[] []R = []R
map-mon-aux eAA' eBB' f f' eff' .(a ∷ l) .(a' ∷ l') ((a ∷R a') l l' x ell') =
  (f a ∷R f' a') (map f l) (map f' l')
  (fstC eff' a a' x) (map-mon-aux eAA' eBB' f f' eff' l l' ell')
map-mon-aux eAA' eBB' f f' eff' .(✠ (List _)) l' (listR✠ .l') = listR✠ (map f' l')
map-mon-aux eAA' eBB' f f' eff' l .(¿ (List _)) (listR¿ _) = listR¿ (map f l)

map-mon' : {A A' : Set ℓ} {B B' : Set ℓ₁} → A ≼ A' → B ≼ B' → map {A = A} {B = B} ≼ map {A = A'} {B = B'}
map-mon' {ℓ} {ℓ₁} {A} {A'} {B} {B'} eAA' eBB' =
  let eA = ≼reflL (Set ℓ) (Set ℓ) {a = A} {b = A'} ⊑Set ⊑Set eAA'
      eA' = ≼reflR (Set ℓ) (Set ℓ) {a = A} {b = A'} ⊑Set ⊑Set eAA'
      eB = ≼reflL (Set ℓ₁) (Set ℓ₁) {a = B} {b = B'} ⊑Set ⊑Set eBB'
      eB' = ≼reflR (Set ℓ₁) (Set ℓ₁) {a = B} {b = B'} ⊑Set ⊑Set eBB'
      eAB = spArrow A B (fstC eA) (fstC eB)
      eA'B' = spArrow A' B' (fstC eA') (fstC eB')
  in 
  (λ f f' eff' → (map-mon-aux eAA' eBB' f f' eff') ,
                 ((map-mon-aux eA eB f f (≼reflL (A → B) (A' → B') eAB eA'B' eff')) ,
                  map-mon-aux eA' eB' f' f' (≼reflR (A → B) (A' → B') eAB eA'B' eff'))) ,
  ((λ f f' eff' → map-mon-aux eA eB f f' eff' ,
                  (map-mon-aux eA eB f f (≼reflL (A → B) (A → B) eAB eAB eff')) ,
                   map-mon-aux eA eB f' f' (≼reflR (A → B) (A → B) eAB eAB eff')) ,
  λ f f' eff' → (map-mon-aux eA' eB' f f' eff') ,
                ((map-mon-aux eA' eB' f f (≼reflL (A' → B') (A' → B') eA'B' eA'B' eff')) ,
                map-mon-aux eA' eB' f' f' (≼reflR (A' → B') (A' → B') eA'B' eA'B' eff'))) 

map' : (A : Set ℓ) (B : Set ℓ₁) → (A → B) → List A → List B
map' A B = map {A = A} {B = B}

map-mon : map' {ℓ} {ℓ₁} ≼ map' {ℓ} {ℓ₁}
map-mon {ℓ} {ℓ₁} = triple λ A A' eAA' →
  let eA = ≼reflL (Set ℓ) (Set ℓ) {a = A} {b = A'} ⊑Set ⊑Set eAA'
      eA' = ≼reflR (Set ℓ) (Set ℓ) {a = A} {b = A'} ⊑Set ⊑Set eAA'
  in (λ B B' eBB' → map-mon' eAA' eBB') , (λ B B' eBB' → map-mon' eA eBB') , λ B B' eBB' → map-mon' eA' eBB'

-- example of use of monotoniticity on the add1 / add¿ example

add1 : Nat → Nat
add1 = λ x → x + 1

add¿ : ¿¿ → Nat
add¿ = λ x → cast ¿¿ Nat x + 1

not✠-Natadd1 : (n : Nat) → not✠-Nat n → not✠-Nat (add1 n)
not✠-Natadd1 = catch-Nat ExcVal _ (λ x → ttP) (λ n x x₁ → x x₁) λ e x → x

-- add1 is more precise than add¿

add1≼add¿ : add1 ≼ add¿
add1≼add¿ = (λ a a' ea → fstC (fstC plus-mon a (cast ¿¿ Nat a') (⊑het-hom-down {A = Nat} {B = ¿¿} ttP (¿¿-refl {lzero}) ea)) 1 1 (Nat-refl 1)) ,
            (λ a a' ea → fstC (fstC plus-mon a a' ea) 1 1 (Nat-refl 1)) ,
            (λ a a' ea → fstC (fstC plus-mon (cast ¿¿ Nat a) (cast ¿¿ Nat a') (cast-mon {A = ¿¿} {B = Nat} (¿¿-refl {lzero}) ttP ea)) 1 1 (Nat-refl 1))

-- for any self-precise correction property, map add¿ is correct as soon as map add1 is correct

map-add¿-correct : (P : List Nat → Prop)
                   (P≼ : (l l' : List Nat) → l ≼ l' → P l → P l') →
                   (map1correct : (l : List Nat) → P l → P (map add1 l)) →
                   (l : List Nat) → P l → P (map add¿ (cast (List Nat) (List ¿¿) l))
map-add¿-correct P P≼ map1correct l el = P≼ (map add1 l) (map add¿ (cast (List Nat) (List ¿¿) l))
                                             ((fstC (fstC (map-mon' (¿Nat , ¿¿-refl {lzero}) (ttP , ¿Nat)) add1 add¿ add1≼add¿) l (cast (List Nat) (List ¿¿) l) (⊑hom-het-up (List Nat) (List ¿¿) ¿Nat (List-refl l) (cast-mon {A = List Nat} {B = List ¿¿} ttP (¿¿-refl {lzero}) (List-refl l))))) (map1correct l el)


-- specific instance for not✠-List as the correction property

map-add1-correct : (l : List Nat) → not✠-List l → not✠-List (map add1 l)
map-add1-correct = catch-List ExcVal Nat _ (λ p → p)
                              (λ n l ind (pn , pl) → not✠-Natadd1 n pn , ind pl) λ e p → p

map-add¿-correct-not✠ : (l : List Nat) → not✠-List l → not✠-List (map add¿ (cast (List Nat) (List ¿¿) l))
map-add¿-correct-not✠ l = map-add¿-correct not✠-List not✠-List≼ map-add1-correct l

----------------------------------
-- monotonicity of prod
----------------------------------

prod-mon-aux : {n n' m m' : Nat} → n ≼ n' → m ≼ m' → (n * m) ≼ (n' * m')
prod-mon-aux zeroR em = zeroR
prod-mon-aux (sucR en) em = plus-mon-aux em (prod-mon-aux en em)
prod-mon-aux (N✠ _) em = N✠ _
prod-mon-aux (N¿ _) em = N¿ _ 

prod-mon : _*_ ≼ _*_
prod-mon = triple λ a a' ea → (λ a₁ a'' ea' → prod-mon-aux ea ea') , (λ a₁ a'' ea' → prod-mon-aux (Nat-refl a) ea') , λ a₁ a'' ea' → prod-mon-aux (Nat-refl a') ea'

----------------------------------
-- monotonicity of multiplication on lists example
----------------------------------

-- optimized definition of multiplication on lists

mult-✠ : (l : List Nat) → Nat
mult-✠ = catch-List-Set ExcVal Nat _ 1
                        (λ n l H → catch-Bool-Set ExcVal _ (✠ Nat) (n * H) (λ { ¿val → ¿ _ ; ✠val → ✠ _}) (is-zero n))
                        (λ { ¿val → ¿ Nat ; ✠val → ✠ Nat}) 

mult : (l : List Nat) → Nat
mult l = catch-Nat-Set ExcVal _ 0 (λ n Hn → suc n) (λ { ¿val → ¿ Nat ; ✠val → 0}) (mult-✠ l)

-- helper functions

mult-✠-mon-helper1 : (n n' : Nat) → n ≡ ✠ Nat → (n + n') ≼ ✠ Nat
mult-✠-mon-helper1 n n' refl = N✠ _

mult-✠-mon-helper2 : (n n' : Nat) → n ≡ ¿ Nat → ¿ Nat ≼ (n + n')  
mult-✠-mon-helper2 n n' refl = N¿ _

mult-✠-mon-helper3 : (n n' : Nat) → not✠-Nat n → not✠-Nat n' → not✠-Nat (n + n' * n) 
mult-✠-mon-helper3 n n' en en' = not✠-plus n (n' * n) en (not✠-prod n' n en' en)

-- auxiliary technical lemma describing in detailed behaviour of mult 

mult-✠-mon : (l l' : List Nat) → not✠-List l → l ≼ l' →
             ((mult-✠ l ≼ ✠ Nat) × ((mult-✠ l' ≼ ✠ Nat) ⊎⊎ (¿ Nat ≼ mult-✠ l'))) ⊎⊎
             ((not✠-Nat (mult-✠ l)) × (mult-✠ l ≼ mult-✠ l'))
mult-✠-mon .[] .[] vl []R = inj₂ (vl , sucR zeroR)
mult-✠-mon .(0 ∷ l) .(0 ∷ l') (va , vl) ((.0 ∷R .0) l l' zeroR ell') = inj₁ ((N✠ (✠ Nat)) , (inj₁ (N✠ (✠ Nat))))
mult-✠-mon (suc n ∷ l) (suc n' ∷ l') (va , vl) ((.(suc _) ∷R .(suc _)) l l' (sucR x) el) with mult-✠-mon l l' vl el
... | inj₁ (x' , inj₁ y) =
  inj₁ (mult-✠-mon-helper1 _ (n * mult-✠ l) (≼✠eq _ x') , inj₁ (mult-✠-mon-helper1 _ (n' * mult-✠ l') (≼✠eq _ y)))
... | inj₁ (x' , inj₂ y) =
  inj₁ (mult-✠-mon-helper1 _ (n * mult-✠ l) (≼✠eq _ x') , inj₂ (mult-✠-mon-helper2 _ (n' * mult-✠ l') (≼¿eq _ y)))
... | inj₂ (x' , vll') =
  inj₂ (mult-✠-mon-helper3 (mult-✠ l) n x' va , fstC (fstC plus-mon _ _ vll') _ _ (fstC (fstC prod-mon _ _ x) _ _ vll')  )
mult-✠-mon (suc n ∷ l) .(¿ Nat ∷ l') (va , vl) ((suc n ∷R .(¿ Nat)) l l' (N¿ _) el) with mult-✠-mon l l' vl el
... | inj₁ (x' , inj₁ y) =
  inj₁ (mult-✠-mon-helper1 _ (n * mult-✠ l) (≼✠eq _ x') , inj₂ (N¿ (¿ Nat)))
... | inj₁ (x' , inj₂ y) =
  inj₁ (mult-✠-mon-helper1 _ (n * mult-✠ l) (≼✠eq _ x') , inj₂ (N¿ (¿ Nat)))
... | inj₂ (x' , vll') =
  inj₂ (mult-✠-mon-helper3 (mult-✠ l) n x' va , ¿top {B = Nat} (mult-✠ (suc n ∷ l)) spNat spNat (Nat-refl (mult-✠ (suc n ∷ l))))
mult-✠-mon (zero ∷ l) .(¿ Nat ∷ l') (va , vl) ((zero ∷R .(¿ Nat)) l l' (N¿ _) el)  = inj₁ (N✠ (✠ Nat) , inj₂ (N¿ (¿ Nat))) 
mult-✠-mon [] .(¿ (List Nat)) vl (listR¿ .[]) = inj₂ (vl , N¿ (suc 0))
mult-✠-mon (zero ∷ l) .(¿ (List Nat)) (va , vl) (listR¿ .(zero ∷ l)) = inj₁ (N✠ (✠ Nat) , inj₂ (N¿ (¿ Nat))) 
mult-✠-mon (suc n ∷ l) .(¿ (List Nat)) (va , vl) (listR¿ .(suc n ∷ l)) with mult-✠-mon l (¿ (List Nat)) vl (¿top {B = List Nat} l spNat spNat (List-refl l))
... | inj₁ (x' , inj₁ y) =
  inj₁ (mult-✠-mon-helper1 _ (n * mult-✠ l) (≼✠eq _ x')  , inj₂ (N¿ (¿ Nat)))
... | inj₁ (x' , inj₂ y) =
  inj₁ (mult-✠-mon-helper1 _ (n * mult-✠ l) (≼✠eq _ x')  , inj₂ (N¿ (¿ Nat)))
... | inj₂ (x' , vll') =
  inj₂ (mult-✠-mon-helper3 (mult-✠ l) n x' va , ¿top {B = Nat} (mult-✠ (suc n ∷ l)) spNat spNat (Nat-refl (mult-✠ (suc n ∷ l))))


mult-mon-aux : (n n' : Nat) → (((n ≼ ✠ Nat) × ((n' ≼ ✠ Nat) ⊎⊎ (¿ Nat ≼ n'))) ⊎⊎ ((not✠-Nat n) × (n ≼ n'))) →
               catch-Nat-Set ExcVal (λ _ → Nat) 0 (λ n Hn → suc n) (λ { ¿val → ¿ Nat ; ✠val → 0}) n ≼
               catch-Nat-Set ExcVal (λ _ → Nat) 0 (λ n Hn → suc n) (λ { ¿val → ¿ Nat ; ✠val → 0}) n'
mult-mon-aux n n' (inj₁ (vn , inj₁ vn')) with ≼✠eq _ vn | ≼✠eq _ vn'
... | refl | refl = zeroR
mult-mon-aux n n' (inj₁ (vn , inj₂ vn')) with ≼✠eq _ vn | ≼¿eq _ vn'
... | refl | refl = N¿ 0
mult-mon-aux .0 .0 (inj₂ (vn , zeroR)) = zeroR
mult-mon-aux .(suc _) .(suc _) (inj₂ (vn , sucR enn')) = sucR enn'
mult-mon-aux x .(¿ Nat) (inj₂ (vn , N¿ _)) = N¿ _

-- monotonicity of mult

mult-mon : (l l' : List Nat) → not✠-List l → l ≼ l' → mult l ≼ mult l'
mult-mon .[] .[] vl []R = sucR zeroR
mult-mon .(0 ∷ l) .(0 ∷ l') vl ((.0 ∷R .0) l l' zeroR ell') = zeroR
mult-mon (suc n ∷ l) (suc n' ∷ l') vl ((.(suc _) ∷R .(suc _)) l l' (sucR x) el) =
  mult-mon-aux (mult-✠ (suc n ∷ l)) (mult-✠ (suc n' ∷ l'))
               (mult-✠-mon (suc n ∷ l) (suc n' ∷ l') vl (((suc n) ∷R (suc n')) l l' (sucR x) el))
mult-mon .(a ∷ l) .(¿ Nat ∷ l') vl ((a ∷R .(¿ Nat)) l l' (N¿ .a) ell') = N¿ _
mult-mon l .(¿ (List Nat)) vl (listR¿ .l) = N¿ _

----------------------------------
-- gradual subset types
----------------------------------

-- the lenght of two concatenated lists is the sum of their size

++-length : (A : Set ℓ) (n m : Nat) (l l' : List A) → length (l ++ l') ~ length l + length l'
++-length A n m = catch-List ExcVal A _ (λ l' → Id-refl (length l'))
                             (λ a l x l' → ap suc (x l'))
                             (λ b l' → Id-refl (exc ExcVal b Nat))

SizedList : (A : Set ℓ) (n : Nat) → Set ℓ
SizedList A n = Σ (List A) (λ l → Box (length l ~ n))

-- unprecise definition of append

append¿ : (A : Set ℓ) (n m : Nat) → SizedList A n → SizedList A m → SizedList A (n + m)
append¿ A n m (l , _) (l' , _) = (l ++ l' , ¿ _)

-- precise definition of append

append : (A : Set ℓ) (n m : Nat) → SizedList A n → SizedList A m → SizedList A (n + m)
append A n m (l , box p) (l' , box p') = (l ++ l' ,
       box (concatId Nat (++-length _ (length l + length l') (n + m) l l')
                         (ap2 {A = Nat} {B = λ _ → Nat} {C = Nat} _+_ p p')))

-- notion of valid sized list, when the proof is not an excetion 

valid-Box : {Q : Prop ℓ} → Box Q → Prop ℓ 
valid-Box {Q = Q} = catch-Box-Set ExcVal Q _ (λ _ → ⊤P) (λ { ¿val → iP ⊥ ; ✠val → iP ⊥})

validSizedList :  (A : Set ℓ) (n : Nat) → SizedList A n → Prop 
validSizedList A n (_ , p) = valid-Box p

-- proof than append return a valid SizedList when the inputs are valid

append-valid : (A : Set ℓ) (n m : Nat) (l : SizedList A n) (l' : SizedList A m)
               → validSizedList A n l → validSizedList A m l' →
               validSizedList A (n + m) (append A n m l l')
append-valid A n m (l , p) (l' , p') =
  catch-Box ExcVal (length l ~ n) (λ x → validSizedList A n (l , x) → validSizedList A m (l' , p') → validSizedList A (n + m) (append A n m (l , x) (l' , p')))
  (λ pp → catch-Box ExcVal (length l' ~ m) (λ y → validSizedList A n (l , box pp) → validSizedList A m (l' , y) → validSizedList A (n + m) (append A n m (l , box pp) (l' , y)))
    (λ q x x₁ → ttP) (λ  { ¿val → λ v () ; ✠val → λ v ()}) p') (λ  { ¿val → λ (); ✠val → λ ()}) p
