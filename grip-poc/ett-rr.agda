{-# OPTIONS --rewriting --prop --confluence-check #-}

open import Agda.Primitive
open import Agda.Builtin.Nat
open import Agda.Builtin.List
open import Agda.Builtin.Equality
open import Agda.Builtin.Equality.Rewrite
open import Agda.Builtin.Sigma
open import Agda.Builtin.Unit
open import Data.Bool
open import Data.List

variable ℓ ℓ₁ ℓ₂ ℓ₃ ℓ₄ : Level

data Box (A : Prop ℓ) : Set ℓ where
  box : A → Box A 

open Box public


module ExcTT (E : Set) where

  {- 
  Axiomatisation of ExTT with exceptions with a parameter in E
  -}

  postulate
    exc : E → (A : Set ℓ) → A

  -- we now state rewrite rules for exc

    exc-Pi : (e : E) (A : Set ℓ) (B : A → Set ℓ₁) →
             exc e ((a : A) → B a) ≡ λ a → exc e (B a)

    exc-Sigma : (e : E) (A : Set ℓ) (B : A → Set ℓ₁) →
                exc e (Σ A B) ≡ (exc e A , exc e (B (exc e A)))

-- add missing rewrite rules for + and * defined in the standard library
    plus-exc : (e : E) (n : Nat) → exc e Nat + n ≡ exc e Nat
    prod-exc : (e : E) (n : Nat) → exc e Nat * n ≡ exc e Nat


  -- add missing rewrite rules for ++, foldr and map defined in the standard library

    ++-exc : (e : E) (A : Set ℓ) (l : List A) → exc e (List A) ++ l ≡ exc e (List A)
    foldr-exc : (e : E) (A : Set ℓ) (B : Set ℓ₁) (f : A → B → B) (b : B) → foldr f b (exc e (List A)) ≡ exc e B
    map-exc : (e : E) (A : Set ℓ) (B : Set ℓ₁) (f : A → B) → map f (exc e (List A)) ≡ exc e (List B)

  {-# REWRITE exc-Pi exc-Sigma plus-exc prod-exc ++-exc foldr-exc map-exc #-}


  -- catch operation for Nat, Bool and List

  catch-Nat : (P : Nat → Prop ℓ) → P 0 → ((n : Nat) → P n → P (suc n)) → ((e : E) → P (exc e Nat)) → (n : Nat) → P n
  catch-Nat P P0 PS Pexc 0 = P0
  catch-Nat P P0 PS Pexc (suc n) = PS n (catch-Nat P P0 PS Pexc n)

  catch-Bool-Set : (P : Bool → Set ℓ) → P true → P false → ((e : E) → P (exc e Bool)) → (b : Bool) → P b 
  catch-Bool-Set P Ptrue Pfalse Pexc false = Pfalse
  catch-Bool-Set P Ptrue Pfalse Pexc true = Ptrue

  postulate catch-Bool-Setexc : (e : E) (P : Bool → Set ℓ) → (Ptrue : P true) → (Pfalse : P false) →
                                (Pexc : (e : E) → P (exc e Bool)) →
                                catch-Bool-Set P Ptrue Pfalse Pexc (exc e Bool) ≡ Pexc e

  {-# REWRITE catch-Bool-Setexc #-}

  catch-Nat-Set : (P : Nat → Set ℓ) → P 0 → ((n : Nat) → P n → P (suc n)) → ((e : E) → P (exc e Nat)) → (n : Nat) → P n
  catch-Nat-Set P P0 PS Pexc 0 = P0
  catch-Nat-Set P P0 PS Pexc (suc n) = PS n (catch-Nat-Set P P0 PS Pexc n)

  postulate catch-Nat-Setexc : (e : E) (P : Nat → Set ℓ) → (P0 : P 0) → (PS : (n : Nat) → P n → P (suc n)) →
                            (Pexc : (e : E) → P (exc e Nat)) →
                            catch-Nat-Set P P0 PS Pexc (exc e Nat) ≡ Pexc e

  {-# REWRITE catch-Nat-Setexc #-}

  catch-List : (A : Set ℓ) → (P : List A → Prop ℓ₁) → P [] →
              ((a : A) → (l : List A) → P l → P (a ∷ l)) → ((e : E) → P (exc e (List A))) → (l : List A) → P l
  catch-List A P Pnil Pcons Pexc [] = Pnil
  catch-List A P Pnil Pcons Pexc (a ∷ l) = Pcons a l (catch-List A P Pnil Pcons Pexc l)

  catch-List-Set : (A : Set ℓ) → (P : List A → Set ℓ₁) → P [] →
              ((a : A) → (l : List A) → P l → P (a ∷ l)) → ((e : E) → P (exc e (List A))) → (l : List A) → P l
  catch-List-Set A P Pnil Pcons Pexc [] = Pnil
  catch-List-Set A P Pnil Pcons Pexc (a ∷ l) = Pcons a l (catch-List-Set A P Pnil Pcons Pexc l)

  postulate catch-List-Setexc : (e : E) (A : Set ℓ) → (P : List A → Set ℓ₁) → (Pnil : P []) →
                              (Pcons : (a : A) → (l : List A) → P l → P (a ∷ l)) →
                              (Pexc  : (e : E) → P (exc e (List A))) → 
                              catch-List-Set A P Pnil Pcons Pexc (exc e (List A)) ≡ Pexc e

  {-# REWRITE catch-List-Setexc #-}

  catch-Box-Set : (Q : Prop ℓ) (P : Box Q → Set ℓ₁) → ((q : Q) → P (box q)) →
                  ((e : E) → P (exc e (Box Q))) → (x : Box Q) → P x
  catch-Box-Set Q P Pbox Pexc (box q) = Pbox q

  postulate catch-Box-Setexc : (e : E) (Q : Prop ℓ) (P : Box Q → Set ℓ₁) → (Pbox : (q : Q) → P (box q)) →
                              (Pexc : (e : E) → P (exc e (Box Q))) →
                              catch-Box-Set Q P Pbox Pexc (exc e (Box Q)) ≡ Pexc e

  {-# REWRITE catch-Box-Setexc #-}


  catch-Box : (Q : Prop ℓ) (P : Box Q → Prop ℓ₁) → ((q : Q) → P (box q)) →
                  ((e : E) → P (exc e (Box Q))) → (x : Box Q) → P x
  catch-Box Q P Pbox Pexc (box q) = Pbox q
